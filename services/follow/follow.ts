import { MutationOptions, useMutation, useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IFollow } from '../../provider/FollowProvider';

export const getFollowings = async (userId: string) => {
  const { data } = await axios.get(`/relationships/` + userId);
  return data;
};

export const follow = async (
  follower: string,
  following: string,
  type: string,
) => {
  try {
    const { data } = await axios.post(`/relationships`, {
      follower,
      following,
      type,
    });
    return data;
  } catch (err) {
    console.log(err);
  }
};

export const unfollow = async (id: string) => {
  const { data } = await axios.delete(`/relationships/` + id);
  return data;
};

export const useGetFollowings = (userId: string) => {
  return useQuery<IFollow[]>(['getFollowings'], () => getFollowings(userId), {
    enabled: Boolean(userId),
    initialData: [],
  });
};

export const useFollow = (
  followerId: string,
  followingId: string,
  type: string,
  options: MutationOptions,
) => {
  return useMutation(
    ['follow'],
    () => follow(followerId, followingId, type),
    options,
  );
};

export const useUnFollow = (id: string, options: MutationOptions) => {
  return useMutation<void>(['unfollow'], () => unfollow(id), options);
};
