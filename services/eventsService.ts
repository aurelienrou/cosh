import axios from 'axios';
import { BASE_URL } from '../constants/utils';
import { GeoPoint, IEvent, IPlace } from './types';

export interface CreateEventDto {
  title: string;
  desc: string;
  location: GeoPoint;
  coverUri: string;
  beginAt: Date;
  endAt: Date;
  price: number;
  placeId: string;
  categories: string[];
  active: boolean;
  private: boolean;
  photos?: string[];
}

export const createEvent = (place: CreateEventDto) =>
  axios.post(BASE_URL + '/event', place);

export const findEventNear = async (
  distance: number,
  coordinates: number[],
): Promise<IEvent[]> => {
  const { data } = await axios.get(BASE_URL + '/event/near', {
    params: { distance, lat: coordinates[0], long: coordinates[1] },
  });
  return data;
};

export const getMyEvents = async (name: string): Promise<IPlace[]> => {
  const { data } = await axios.get(BASE_URL + '/event', { params: { name } });
  return data;
};

export const follow = async (id: string): Promise<IPlace[]> => {
  const { data } = await axios.get(BASE_URL + '/event/follow/' + id);
  return data;
};

export const unfollow = async (id: string): Promise<IPlace[]> => {
  const { data } = await axios.get(BASE_URL + '/event/unfollow/' + id);
  return data;
};
