import { defineAbility } from '@casl/ability';
import { IUser } from './types';

export enum ROLES {
  ADMIN = 'ADMIN',
  ORGANISATOR = 'ORGANISATOR',
  USER = 'USER',
}

export enum ACTIVITY_TYPE {
  CREATE = 'CREATE',
  READ = 'READ',
  UPDATE = 'UPDATE',
  DELETE = 'DELETE',
}

export default function defineAbilityFor(user: IUser) {
  return defineAbility((can) => {
    can('read', 'Article');

    //   if (user.isLoggedIn) {
    //     can('update', 'Article', { authorId: user.id });
    //     can('leave', 'Comment');
    //     can('update', 'Comment', { authorId: user.id });
    //   }
  });
}
