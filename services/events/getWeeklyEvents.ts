import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IEventMinimize } from '../types';

export const getWeeklyEvents = async (
  lat: number,
  lng: number,
  distance: number,
  limit: number,
) => {
  const { data } = await axios.get(`/event/weekly`, {
    params: { lat, lng, distance, limit },
  });
  return data;
};

export const useWeeklyEvents = (
  lat: number,
  lng: number,
  distance: number,
  limit = 1000,
) => {
  return useQuery<IEventMinimize[]>(
    ['getWeeklyEvents'],
    () => getWeeklyEvents(lat, lng, distance, limit),
    { initialData: [] },
  );
};
