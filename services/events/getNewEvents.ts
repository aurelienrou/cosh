import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IEventMinimize } from '../types';

export const getNewEvents = async (lat: number, lng: number, limit: number) => {
  const { data } = await axios.get(`/event/news`, {
    params: { lat, lng, limit },
  });
  return data;
};

export const useNewEvents = (lat: number, lng: number, limit = 10) => {
  return useQuery<IEventMinimize[]>(
    ['newEvents'],
    () => getNewEvents(lat, lng, limit),
    { initialData: [] },
  );
};
