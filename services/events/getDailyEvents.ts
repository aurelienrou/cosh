import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IEventMinimize, ITypes } from '../types';

export const getDailyEvents = async (
  lat: number,
  lng: number,
  distance: number,
  filters: string[],
  limit: number,
) => {
  const { data } = await axios.get(`/event/today`, {
    params: { lat, lng, distance, limit, filters },
  });
  return data;
};

export const useDailyEvents = (
  lat: number,
  lng: number,
  distance: number,
  filters: string[],
  limit = 1000,
) => {
  return useQuery<IEventMinimize[]>(
    ['getDailyEvents'],
    () => getDailyEvents(lat, lng, distance, filters, limit),
    { initialData: [] },
  );
};
