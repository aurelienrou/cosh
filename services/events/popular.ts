import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IEventMinimize } from '../types';

export const getPopularEventNearUser = async (
  lat: number,
  lng: number,
  limit: number,
) => {
  const { data } = await axios.get(`/event/populars`, {
    params: { lat, lng, limit },
  });
  return data;
};

export const usePopularEventNearUser = (
  lat: number,
  lng: number,
  limit = 20,
) => {
  return useQuery<IEventMinimize[]>(
    ['popularEventNearUser'],
    () => getPopularEventNearUser(lat, lng, limit),
    { initialData: [] },
  );
};
