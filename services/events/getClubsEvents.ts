import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IEventMinimize } from '../types';
import { useLocationContext } from '../../provider/LocationProvider';
import { useFiltersContext } from '../../provider/FiltersProvider';
import { SCREENS_NAME } from '../../components/DiscoverTabView/helper';

type Params = {
  lat: number;
  lng: number;
  start?: Date | null;
  end?: Date | null;
  types?: string[];
  maxPrice: number;
};

export const getClubsEvents = async (params: Params) => {
  const { data } = await axios.get(`/event/clubs`, {
    params: {
      ...params,
      types: params.types?.join(','),
    },
  });
  return data;
};

export const useClubsEvents = () => {
  const { location } = useLocationContext();
  const { params, currentIndex } = useFiltersContext();
  const queryParams = {
    ...params[SCREENS_NAME[currentIndex]],
    lat: location.latitude,
    lng: location.longitude,
  };

  return useQuery<IEventMinimize[]>(
    ['getClubsEvents', queryParams],
    () => getClubsEvents(queryParams),
    {
      initialData: [],
      enabled: Boolean(queryParams.lat && queryParams.lng),
    },
  );
};
