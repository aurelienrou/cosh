import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IEventMinimize } from '../types';

export const getEventByName = async (name: string) => {
  const { data } = await axios.get(`/event/name`, { params: { name } });
  return data;
};

export const useEventByName = (name: string) => {
  return useQuery<IEventMinimize[]>(
    ['getEventByName', name],
    () => getEventByName(name),
    {
      enabled: Boolean(name),
      initialData: [],
    },
  );
};
