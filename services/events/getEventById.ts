import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IEvent } from '../types';

export const getEventById = async (id: string) => {
  const { data } = await axios.get(`/event/${id}`);
  return data;
};

export const useEventById = (id: string) => {
  return useQuery<IEvent>(['getEventById', id], () => getEventById(id), {
    enabled: Boolean(id),
    initialData: {} as IEvent,
  });
};
