import { useMutation } from '@tanstack/react-query';
import axios from '../axios';

type Params = {
  token: string;
  name: string;
  age: string;
  gender: string;
};

export const postGoogle = async ({ token, name, age, gender }: Params) => {
  const { data } = await axios.post(`/auth/google`, {
    token,
    name,
    age,
    gender,
  });
  return data;
};

export const useGoogleLogin = () => {
  return useMutation(['postGoogle'], postGoogle);
};
