import { useMutation } from '@tanstack/react-query';
import axios from '../axios';

export const postApple = async ({ code }: { code: string }) => {
  const { data } = await axios.post(`/auth/apple`, {
    code,
  });
  return data;
};

export const useAppleLogin = () => {
  return useMutation(['postApple'], postApple);
};
