import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IUser } from '../types';

export const getGoogleUserFromToken = async (token: string) => {
  const { data } = await axios.get(`/auth/google`, { params: { token } });
  return data;
};

export const useGetGoogleUserWithToken = (token: string) => {
  return useQuery<{ user: IUser; accessToken: string; refreshToken: string }>(
    ['getGoogleUserFromToken', token],
    () => getGoogleUserFromToken(token),
    {
      enabled: Boolean(token),
    },
  );
};
