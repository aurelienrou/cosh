import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IUser } from '../types';
import auth from '@react-native-firebase/auth';
import { useEffect, useState } from 'react';

export const getUserFromToken = async (token: string) => {
  const { data } = await axios.get(`/auth/google`, { params: { token } });
  return data;
};

export const useGetUserWithToken = () => {
  const [token, setToken] = useState<string>();

  useEffect(() => {
    const init = async () => {
      const idToken = await auth().currentUser?.getIdToken();

      if (idToken) {
        setToken(idToken);
      }
    };
    init();
  }, []);

  return useQuery<{ user: IUser; accessToken: string; refreshToken: string }>(
    ['getUserFromToken', token],
    () => getUserFromToken(token || ''),
    {
      enabled: Boolean(token),
    },
  );
};
