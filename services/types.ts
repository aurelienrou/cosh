import { Tag } from '../components/Tags/Tags';

export enum ROLE {
  ADMIN = 'admin',
  USER = 'user',
  ORGA = 'orga',
}
export enum PLAN_TYPE {
  FREE = 'free',
  PREMIUM = 'premium',
  PREMIUM_PLUS = 'premium_plus',
}
export enum PRICE_RANGE {
  FREE = 'FREE',
  $ = '$',
  $$ = '$$',
  $$$ = '$$$',
  $$$$ = '$$$$',
}
export enum PLAN_BILLING_TYPE {
  MONTHLY = 'monthly',
  ANNUAL = 'annual',
}
export enum VERIFICATION_STATUS {
  VERIFIED = 'verified',
  IN_PROGRESS = 'in_progress',
  NOT_VERIFIED = 'not_verified',
}

export interface GeoPoint {
  type: string[];
  coordinates: number[];
}

type PhoneType = {
  countryCode: string;
  number: string;
};

export type LatitudeLongitude = {
  latitude: number;
  longitude: number;
};

export interface LatLng {
  lat: number;
  lng: number;
}

export interface AddressGoogleResults {
  description: string;
  place_id: string;
}

export interface AddressGoogle {
  id?: string;
  formatted_address: string;
  place_id: string;
  street_number?: string;
  route?: string;
  locality?: string;
  country?: string;
  geocode: LatLng;
}

export type PartialPlace = Pick<IPlace, 'id' | 'name' | 'address'>;

export interface IUser {
  id: string;
  name: string;
  desc: string;
  location: IGeoPoint;
  cover: string;
  address: string;
  role: ROLE;
  places: PartialPlace[];
  followersCount: number;
  slugs: ISlug;
  contacts: IContacts;
  social: ISocial;
  events: IEventMinimize[];
}

export interface Tokens {
  accessToken: string;
  refreshToken: string;
}

export interface UserFromLogin extends Tokens {
  user: IUser;
}

export interface IGeoPoint {
  type: string;
  coordinates: [number, number];
}

export interface IArtist {
  about: string | null;
  image: string | null;
  name: string | null;
}

export interface IHours {
  mon: string | null;
  tue: string | null;
  wed: string | null;
  thu: string | null;
  fri: string | null;
  sat: string | null;
  sun: string | null;
}

export interface IEvent {
  id: string;
  name: string;
  url: string;
  desc: string;
  cover: string;
  photos: string[];
  followers: string[];
  endAt?: Date;
  beginAt: Date;
  distance: number;
  minPrice: number;
  maxPrice: number;
  userId: string[];
  categories: string[];
  subCategories: string[];
  place: {
    name: string;
    address: string;
  };
  active: boolean;
  location: {
    type: 'Point';
    coordinates: number[];
  };
  placeId: string;
  formattedAddress: string;
  hasLike?: boolean;
  createdBy: {
    id: string;
    address: string;
    cover: string;
    followersCount: number;
    name: string;
  };
  createdByModel: 'Place' | 'User';
}

export interface ISlug {
  shotgun: string;
  dice: string;
  facebook: string;
  billetreduc: string;
  schlouk: string;
  google: string;
}

export interface ISocial {
  dice: string;
  website: string;
  instagram: string;
  soundcloud: string;
  spotify: string;
  facebook: string;
  twitter: string;
  tiktok: string;
  whatsApp: string;
  messenger: string;
  discord: string;
}

export interface IContacts {
  phone: string;
  email: string;
}

export type PartialEvent = Pick<
  IEvent,
  'id' | 'name' | 'place' | 'cover' | 'beginAt' | 'minPrice'
>;

export interface IPlace {
  id: string;
  name: string;
  desc: string;
  location: number[];
  distance: number;
  cover: string;
  logo: string;
  photos: string[];
  hours: IHours;
  happyHours: IHours;
  address: string;
  slugs: ISlug;
  contacts: IContacts;
  social: ISocial;
  verificationStatus: typeof VERIFICATION_STATUS;
  price: typeof PRICE_RANGE;
  followersCount: number;
  events: IEventMinimize[];
  categories: string[];
  subCategories: string[];
  createdBy: IUser;
}
export interface IPlaceMinimize {
  id: string;
  name: string;
  cover: string;
  distance: number;
  followersCount: number;
  location: [number, number];
  isSelected: boolean;
}

export interface IEventMinimize {
  id: string;
  name: string;
  minPrice: number;
  cover: string;
  distance: number;
  location: number[];
  beginAt: Date;
  address: string;
  place: {
    id: string;
    name: string;
    address: string;
  };
  isSelected: boolean;
}

export interface ITypes {
  name: string;
  isSelected: boolean;
}

export interface IParamsFilters {
  start: Date | null;
  end: Date | null;
  types: ITypes[];
  maxPrice: number;
}

interface ITopic {
  id: string;
  name: string;
  cover: string;
  category: 'bar' | 'club';
  date: string;
}

export interface ITopicPlace extends ITopic {
  type: 'Place';
  content: IPlaceMinimize[];
}
export interface ITopicsEvent extends ITopic {
  type: 'Event';
  content: IEventMinimize[];
}

export type ITopics = ITopicsEvent | ITopicPlace;

export interface ErrorType {
  error: string;
  message: string[];
  statusCode: number;
}

export type GuestStatusCounts = {
  READY_TO_SEND: number;
  SENDING: number;
  SENT: number;
  SEND_ERROR: number;
  DELIVERY_ERROR: number;
  MAYBE: number;
  GOING: number;
  DECLINED: number;
  WAITLIST: number;
  PENDING_APPROVAL: number;
  APPROVED: number;
  WITHDRAWN: number;
  RESPONDED_TO_FIND_A_TIME: number;
};

export type PrivateEvent = {
  id: string;
  title: string;
  description: string | null;
  cover: string | null;
  startDate: Date | null;
  endDate: Date | null;
  address: string | null;

  enableGuestReminders: boolean;
  allowGuestsToShare: boolean;
  showGuestList: boolean;
  showHostList: boolean;
  allowGuestPhotoUpload: boolean;
  showActivityTimestamps: boolean;
  displayInviteButton: boolean;
  guests: Partial<IUser>[];
  owners: Partial<IUser>[];
  ownerIds: string[];
  rsvpsEnabled: boolean;
  allowGuestsToInviteMutuals: boolean;
  otherMutualsInvitedCount: number;
  invitationMessage: string | null;
  invitesSentCount: number;
  guestStatusCounts: GuestStatusCounts;
  isCapped: boolean;
  guestCount: number;
  respondedGuestCount: number;
  invitedGuestCount: number;
  attendedGuestCount: number;
  goingGuestCount: number;
  maybeGuestCount: number;
  declinedGuestCount: number;
  waitlistGuestCount: number;
  withdrawnGuestCount: number;
  pendingGuestCount: number;
  approvedGuestCount: number;
  respondedToFindATimeGuestCount: number;
  hasGuests: boolean;
  atCapacity: boolean;
  status: 'DRAFT' | 'PUBLISHED';
};
