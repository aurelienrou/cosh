import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IUser } from '../types';

export const getUserById = async (id: string) => {
  const { data } = await axios.get(`/user/${id}`);
  return data;
};

export const useUserById = (id: string) => {
  return useQuery<IUser>(['getUserById', id], () => getUserById(id), {
    enabled: Boolean(id),
  });
};
