import axios from '../axios';

export const deleteUserById = async (id: string) => {
  const { data } = await axios.delete(`/user/` + id);
  return data;
};
