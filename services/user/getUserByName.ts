import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IUser } from '../types';

export const getUserByName = async (name: string) => {
  const { data } = await axios.get(`/user/name`, { params: { name } });
  return data;
};

export const useUserByName = (name: string) => {
  return useQuery<IUser[]>(['getUserByName', name], () => getUserByName(name), {
    enabled: Boolean(name),
    initialData: [],
  });
};
