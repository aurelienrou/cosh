import axios from 'axios';

const instance = axios.create({
  // baseURL: 'https://arvala.onrender.com',
  baseURL: 'http://192.168.1.126:3000',
  // baseURL: 'http://16.170.212.49:3000',
});

export default instance;
