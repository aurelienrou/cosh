import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { ITopics } from '../types';

export const getBarsTopics = async (type: string) => {
  const { data } = await axios.get(`/topic/${type}`);
  return data;
};

export const useGetBarsTopics = (type: string) => {
  return useQuery<ITopics[]>(['getBarsTopics'], () => getBarsTopics(type), {
    initialData: [],
  });
};
