import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { ITopics } from '../types';

export const getClubsTopics = async () => {
  const { data } = await axios.get(`/topic/club`);
  return data;
};

export const useGetClubsTopics = () => {
  return useQuery<ITopics[]>(['getClubsTopics'], () => getClubsTopics(), {
    initialData: [],
  });
};
