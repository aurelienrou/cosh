import axios from 'axios';
import { GeoPoint, IPlace, PRICE_RANGE } from './types';

const baseUrl = 'http://192.168.1.33:3000/place';

export interface PlaceDto {
  name: string;
  desc: string;
  location: GeoPoint;
  coverUri: string;
  openHours: number[][];
  placeId: string;
  administrativeAreaLevel1: string;
  administrativeAreaLevel2: string;
  country: string;
  formattedAddress: string;
  locality: string;
  postalCode: string;
  route: string;
  streetNumber: string;
  email: string;
  priceRange: PRICE_RANGE;
  categories: string[];
  phone?: string;
  website?: string;
  facebook?: string;
  instagram?: string;
}
export const createPlace = (place: PlaceDto) => axios.post(baseUrl, place);

export const getPlaces = async (name: string): Promise<IPlace[]> => {
  const { data } = await axios.get(baseUrl, { params: { name } });
  return data;
};
