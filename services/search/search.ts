import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IEventMinimize, IPlaceMinimize, IUser } from '../types';
import { useMemo } from 'react';

interface variables {
  q: string;
  start: Date | null;
  end: Date | null;
}

export const searchByName = async (params: variables) => {
  const { data } = await axios.get(`/event/search`, { params });
  return data;
};

export const useSearchByName = (
  q: string,
  start: Date | null,
  end: Date | null,
) => {
  const params = { q, start, end };

  return useQuery<IEventMinimize[]>(
    ['searchByName', params],
    () => searchByName(params),
    {
      enabled: Boolean(params.q),
      initialData: [],
    },
  );
};
