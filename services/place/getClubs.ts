import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IPlaceMinimize } from '../types';

export const getClubs = async (lat: number, lng: number, distance: number) => {
  const { data } = await axios.get(`/place/club`, {
    params: {
      lat,
      lng,
      distance,
    },
  });
  return data;
};

export const useGetClubs = (lat: number, lng: number) => {
  return useQuery<IPlaceMinimize[]>(
    ['getClubs'],
    () => getClubs(lat, lng, 10000),
    {
      initialData: [],
    },
  );
};
