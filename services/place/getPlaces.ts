import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IPlaceMinimize } from '../types';

export const getPlacesNearUser = async (
  lat: number,
  lng: number,
  distance: number,
  types: string,
) => {
  const { data } = await axios.get(`/place/near`, {
    params: { lat, lng, distance, types },
  });
  return data;
};

export const useNearPlaces = (
  lat: number,
  lng: number,
  distance: number,
  types: string,
  key: string,
) => {
  return useQuery<IPlaceMinimize[]>(
    [key],
    () => getPlacesNearUser(lat, lng, distance, types),
    { initialData: [] },
  );
};
