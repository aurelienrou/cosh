import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IPlaceMinimize } from '../types';
import { useFiltersContext } from '../../provider/FiltersProvider';
import { useLocationContext } from '../../provider/LocationProvider';
import { SCREENS_NAME } from '../../components/DiscoverTabView/helper';

type Params = {
  lat: number;
  lng: number;
  start?: Date | null;
  end?: Date | null;
  types?: string[];
  minPrice?: number;
};

export const getBars = async (params: Params) => {
  const { data } = await axios.get(`/place/bars`, {
    params: {
      ...params,
      types: params.types?.join(','),
    },
  });
  return data;
};

export const useGetBars = () => {
  const { location } = useLocationContext();
  const { params, currentIndex } = useFiltersContext();
  const queryParams = {
    ...params[SCREENS_NAME[currentIndex]],
    lat: location.latitude,
    lng: location.longitude,
  };

  return useQuery<IPlaceMinimize[]>(
    ['getBars', queryParams],
    () => getBars(queryParams),
    {
      initialData: [],
      enabled: Boolean(queryParams.lat && queryParams.lng),
    },
  );
};
