import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IPlace } from '../types';

export const getPlaceById = async (id: string) => {
  const { data } = await axios.get(`/place/${id}`);
  return data;
};

export const usePlaceById = (id: string) => {
  return useQuery<IPlace>(['getPlaceById', id], () => getPlaceById(id), {
    enabled: Boolean(id),
  });
};
