import { useQuery } from '@tanstack/react-query';
import axios from '../axios';
import { IPlaceMinimize } from '../types';

export const getPlaceByName = async (name: string) => {
  const { data } = await axios.get(`/place/name`, { params: { name } });
  return data;
};

export const usePlaceByName = (name: string) => {
  return useQuery<IPlaceMinimize[]>(
    ['getPlaceByName', name],
    () => getPlaceByName(name),
    {
      enabled: Boolean(name),
      initialData: [],
    },
  );
};
