// import axios from 'axios';

// export const refreshIDToken = async () => {
//   try {
//     const refreshToken = await loadUserRefreshToken();
//     const clientId = await loadUserClientId();
//     return axios
//       .post(GOOGLE_OAUTH_TOKEN_URL, {
//         grant_type: 'refresh_token',
//         refresh_token: refreshToken,
//         clientId,
//       })
//       .then(({ data: { id_token: idToken } }) => {
//         saveUserIDToken(idToken);
//         return Promise.resolve(idToken);
//       })
//       .catch((reason) => {
//         console.error(
//           'rejected request for refresh auth with reason: ',
//           reason,
//         );
//         return Promise.resolve(null);
//       });
//   } catch (e) {
//     console.error('refreshIDToken(): ', e.message);
//     return Promise.resolve(null);
//   }
// };

// const authRefresh = async (response) => {
//   const status = response ? response.status : null;
//   if (status === 401) {
//     //Response was rejected due to Auth
//     return refreshIDToken().then((newToken) => {
//       if (newToken) {
//         response.config.headers.Authorization = `Bearer ${newToken}`;
//         response.config.baseURL = undefined;
//         return instance.request(response.config);
//       }
//       // Refresh token was also expired, prompt user to log in again
//       return response;
//     });
//   }

//   return response;
// };

// axios.interceptors.response.use(authRefresh);
