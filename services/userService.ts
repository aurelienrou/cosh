import axios, { AxiosResponse } from 'axios';
import { BASE_URL } from '../constants/utils';
import { UserFromLogin } from './types';

export const updateLocation = (
  location: number[],
): Promise<AxiosResponse<UserFromLogin>> =>
  axios.post(BASE_URL + '/user/update-location', {
    location,
  });
