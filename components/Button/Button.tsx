import { StyleSheet, TextStyle, ViewStyle, Image, Text } from 'react-native';
import React, { FC } from 'react';
import { icons, ICONS_NAME } from '../../constants/Icon';
import { TouchableOpacity } from 'react-native';
import { width } from '../../constants/Layout';
import ContentLoader, { Rect } from 'react-content-loader/native';
import { PRIMARY_COLOR } from '../../constants/Colors';
import CoshText from '../CoshText/CoshText';

interface ButtonProps {
  onPress: () => void;
  text: string;
  onlyText?: boolean;
  style?: ViewStyle;
  textStyle?: TextStyle;
  iconName?: ICONS_NAME;
  loading?: boolean;
}

const Button: FC<ButtonProps> = ({
  onPress,
  onlyText = true,
  text,
  style,
  textStyle,
  iconName,
  loading,
}) => {
  return (
    <TouchableOpacity
      style={[onlyText && styles.container, style]}
      onPress={!loading ? onPress : undefined}
    >
      {loading ? (
        <Loader />
      ) : (
        <>
          {iconName && <Image style={styles.icon} source={icons[iconName]} />}
          <Text style={[styles.text, textStyle]}>{text}</Text>
        </>
      )}
    </TouchableOpacity>
  );
};

export default Button;

const Loader = () => (
  <ContentLoader
    speed={2}
    width={width}
    height={100}
    viewBox={`0 0 ${width} 100`}
    backgroundColor={PRIMARY_COLOR}
    foregroundColor="#FFF"
  >
    <Rect x="8" y="21" rx="0" ry="0" width={width} height={100} />
  </ContentLoader>
);

const styles = StyleSheet.create({
  container: {
    height: 48,
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
    overflow: 'hidden',
    flexDirection: 'row',
  },
  text: {
    fontSize: 16,
    color: '#FFF',
  },
  icon: {
    width: 15,
    height: 15,
    marginRight: 10,
  },
});
