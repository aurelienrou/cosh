import React, { useCallback } from 'react';
import { useAuthContext } from '../../provider/AuthProvider';
import { Image } from 'expo-image';
import styled from 'styled-components/native';
import { useFollow, useUnFollow } from '../../services/follow/follow';
import { useFollowContext } from '../../provider/FollowProvider';
import { TypeToFollow } from '../BlockEvents/BlockEvent';
import { BlurView } from '@react-native-community/blur';
import { useNavigation } from '@react-navigation/native';
import { useMapsContext } from '../../provider/MapsProvider';
import { blurAmount } from '../../constants/theme';

interface FollowButtonProps {
  id: string;
  type: TypeToFollow;
  hasBackground?: boolean;
}

const FollowButton = ({
  type,
  id,
  hasBackground = false,
}: FollowButtonProps) => {
  const { user, isLoggin } = useAuthContext();
  const { followMap, setFollowMap } = useFollowContext();
  const { bottomSheetRef } = useMapsContext();
  const navigation = useNavigation();

  const { mutate: follow, isLoading: followLoading } = useFollow(
    user.id,
    id,
    type,
    {
      onSuccess: (data: any) => {
        if (!data?.following?.id) {
          return;
        }
        setFollowMap({ ...followMap, [data?.following?.id]: data });
      },
    },
  );

  const { mutate: unfollow, isLoading: unfollowLoading } = useUnFollow(
    followMap[id]?.id,
    {
      onSuccess: () => {
        delete followMap[id];

        setFollowMap({ ...followMap });
      },
    },
  );

  const isFollow = followMap[id]?.following?.id === id;

  const handleFollow = useCallback(() => {
    if (isLoggin) {
      if (isFollow) {
        unfollow();
        return;
      }

      follow();
    } else {
      bottomSheetRef.current?.expand();
      navigation.navigate('MyEventsWishlist' as never);
    }
  }, [isFollow, isLoggin]);

  const source = isFollow
    ? require('../../assets/icons/like-red.svg')
    : require('../../assets/icons/like-outline.svg');

  return (
    <S_Pressable hasBackground={hasBackground} onPress={handleFollow}>
      {hasBackground ? <S_Blur blurAmount={blurAmount} /> : null}

      {unfollowLoading || followLoading ? (
        <></>
      ) : (
        <S_Image
          source={source}
          style={{ opacity: hasBackground || isFollow ? 1 : 0.3 }}
          contentFit="cover"
        />
      )}
    </S_Pressable>
  );
};

export default FollowButton;

const S_Image = styled(Image)`
  width: 23px;
  height: 20px;
`;

const S_Pressable = styled.Pressable<{ hasBackground: boolean }>`
  height: 48px;
  width: 48px;
  border-radius: 24px;
  align-items: center;
  justify-content: center;
  position: relative;
  overflow: hidden;
`;

const S_Blur = styled(BlurView)`
  height: 48px;
  width: 48px;
  border-radius: 24px;
  position: absolute;
`;
