import React from 'react';
import styled from 'styled-components/native';

export const FiltersButton = () => {
  return (
    <S_COntainer>
      <S_Text>NEWEST</S_Text>
    </S_COntainer>
  );
};

const S_COntainer = styled.View`
  width: 125px;
  height: 48px;
  border-radius: 36px;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.background.action};
`;

const S_Text = styled.Text`
  font-family: 'neue-bold';
  font-size: 14px;
  color: ${(props) => props.theme.fontColor.primary};
`;
