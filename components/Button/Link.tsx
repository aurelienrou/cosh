import { StyleSheet, TextStyle, ViewStyle, Image } from 'react-native';
import React, { FC } from 'react';
import { icons, ICONS_NAME } from '../../constants/Icon';
import { TouchableOpacity } from 'react-native';
import CoshText from '../CoshText/CoshText';

interface LinkProps {
  onPress: () => void;
  text: string;
  onlyText?: boolean;
  style?: ViewStyle;
  textStyle?: TextStyle;
  iconName?: ICONS_NAME;
  loading?: boolean;
}

const Link: FC<LinkProps> = ({
  onPress,
  onlyText = true,
  text,
  style,
  textStyle,
  iconName,
  loading,
}) => {
  return (
    <TouchableOpacity
      style={[onlyText && !style && styles.container, style]}
      onPress={!loading ? onPress : undefined}
    >
      {iconName && <Image style={styles.icon} source={icons[iconName]} />}
      <CoshText style={[styles.text, textStyle]}>{text}</CoshText>
    </TouchableOpacity>
  );
};

export default Link;

const styles = StyleSheet.create({
  container: {
    height: 50,
    borderRadius: 10,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
    overflow: 'hidden',
  },
  text: {
    fontSize: 16,
    color: '#FFF',
  },
  icon: {
    width: 15,
    height: 15,
    marginRight: 10,
  },
});
