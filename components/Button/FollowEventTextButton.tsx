import { useQuery } from '@tanstack/react-query';
import React from 'react';
import { useAuthContext } from '../../provider/AuthProvider';
import { follow, unfollow } from '../../services/eventsService';
import { useEventsContext } from '../../provider/EventProvider';
import { IEvent } from '../../services/types';
import { Image } from 'expo-image';
import styled from 'styled-components/native';

interface FollowButtonProps {
  event: IEvent;
  isLight?: boolean;
}

export const FollowEventTextButton = ({
  event,
  isLight = false,
}: FollowButtonProps) => {
  const { events, setEvents } = useEventsContext();
  const { user } = useAuthContext();

  const handleFollow = async () => {
    if (!user) {
      // TODO : Afficher la modal de connection
      return;
    }

    try {
      await follow(event.id);

      const nextEvents = events.map((e) => {
        if (e.id === event.id) {
          return {
            ...event,
            hasLike: true,
          };
        }
        return e;
      });

      setEvents(nextEvents);
    } catch (err) {
      console.log(err);
    }
  };

  const handleUnFollow = async () => {
    if (!user) {
      return;
    }

    try {
      await unfollow(event.id);

      const nextEvents = events.map((e) => {
        if (e.id === event.id) {
          return {
            ...event,
            hasLike: false,
          };
        }
        return e;
      });

      setEvents(nextEvents);
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <S_Pressable onPress={event.hasLike ? handleUnFollow : handleFollow}>
      <S_Image
        source={
          event.hasLike
            ? require('../../assets/icons/like.svg')
            : isLight
            ? require('../../assets/icons/like-white.svg')
            : require('../../assets/icons/like.svg')
        }
        contentFit="cover"
      />
    </S_Pressable>
  );
};

const S_Pressable = styled.Pressable`
  width: 21px;
  height: 18px;
  justify-content: center;
  align-items: center;
`;
const S_Image = styled(Image)`
  width: 21px;
  height: 18px;
`;
