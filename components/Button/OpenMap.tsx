import React from 'react';
import { Image } from 'expo-image';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
} from 'react-native-reanimated';
import styled from 'styled-components/native';
import { height, width } from '../../constants/Layout';
import { useMapsContext } from '../../provider/MapsProvider';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

export const OpenMapButton = () => {
  const { animatedIndex, handleCollapse } = useMapsContext();
  const { bottom } = useSafeAreaInsets();

  const buttonStyle = useAnimatedStyle(() => {
    const translateY = interpolate(
      animatedIndex.value,
      [0, 1],
      [80, 0],
      Extrapolate.CLAMP,
    );
    const opacity = interpolate(
      animatedIndex.value,
      [0.8, 1],
      [0, 1],
      Extrapolate.CLAMP,
    );

    return {
      transform: [{ translateY }],
      opacity,
    };
  });

  return (
    <S_PressableMapContainer
      pointerEvents="box-none"
      style={[buttonStyle, { bottom: bottom + 70 }]}
    >
      <S_PressableMap onPress={handleCollapse}>
        <S_IconMap source={require('../../assets/icons/map.svg')} />
        <S_TextMap>MAP</S_TextMap>
      </S_PressableMap>
    </S_PressableMapContainer>
  );
};

const S_IconMap = styled(Image)`
  height: 19px;
  width: 19px;
`;

const S_TextMap = styled.Text`
  margin-left: 10px;
  font-family: 'neue-bold';
  font-size: 14px;
  color: #000;
`;

const S_PressableMapContainer = styled(Animated.View)`
  position: absolute;
  height: 48px;
  z-index: 10000;
  width: ${width + 'px'};
  justify-content: center;
  align-items: center;
`;

const S_PressableMap = styled.Pressable`
  height: 48px;
  border-radius: 36px;
  padding: 12px 16px 12px 16px;
  background-color: #fff;
  box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.5);
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
