import React, { useState } from 'react';
import { View, Pressable, StyleSheet, Text } from 'react-native';

interface SelectProps {
  items: string[];
  onChange: (item: string) => void;
}

export const Select = ({ items, onChange }: SelectProps) => {
  const [selected, setSelected] = useState<string>();

  const handleSelect = (item: string) => {
    setSelected(item);
    onChange(item);
  };

  return (
    <View style={styles.buttons}>
      {items.map((item) => (
        <Pressable
          style={[
            styles.button,
            { backgroundColor: `${selected === item ? '#FFF' : '#FFFFFF1F'}` },
          ]}
          key={item}
          onPress={() => handleSelect(item)}
        >
          <Text
            style={[
              styles.text,
              { color: `${selected === item ? '#000' : '#FFF'}` },
            ]}
          >
            {item}
          </Text>
        </Pressable>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  buttons: {
    flexDirection: 'row',
  },
  button: {
    height: 48,
    paddingHorizontal: 16,
    borderRadius: 36,
    marginRight: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontFamily: 'neue-bold',
    fontSize: 14,
  },
});
