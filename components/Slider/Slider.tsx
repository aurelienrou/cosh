import React, { useEffect } from 'react';
import { Gesture, GestureDetector } from 'react-native-gesture-handler';
import Animated, {
  runOnJS,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import styled from 'styled-components/native';
import { width } from '../../constants/Layout';
import { MAX_VALUE, MIN_VALUE } from '../../provider/FiltersProvider';

interface SliderProps {
  onChangeMin: (value: number) => void;
  onChangeMax: (value: number) => void;
  initialeValue: { min: number; max: number };
}

export const Slider = ({
  onChangeMin,
  onChangeMax,
  initialeValue,
}: SliderProps) => {
  const STEP = (width - 40) / MAX_VALUE;
  const MIN = MIN_VALUE * STEP - 7.5;
  const MAX = MAX_VALUE * STEP - 23;
  const offsetMin = useSharedValue(0);
  const offsetMax = useSharedValue(0);
  const translateMin = useSharedValue(MIN);
  const translateMax = useSharedValue(MAX);

  useEffect(() => {
    translateMin.value = initialeValue.min * STEP - 7.5;
    translateMax.value = initialeValue.max * STEP - 23;
  }, [initialeValue.max, initialeValue.min]);

  const panMin = Gesture.Pan()
    .onStart(() => {
      offsetMin.value = translateMin.value;
    })
    .onChange((event) => {
      if (
        event.translationX > 0 &&
        translateMin.value + STEP * 3 >= translateMax.value
      ) {
        return;
      }

      const current = Math.round(
        Math.max(Math.min(event.translationX + offsetMin.value, MAX), MIN) /
          STEP,
      );
      runOnJS(onChangeMin)(current + 1);
      translateMin.value = current * STEP;
    });

  const panMax = Gesture.Pan()
    .onStart(() => {
      offsetMax.value = translateMax.value;
    })
    .onChange((event) => {
      if (
        event.translationX < 0 &&
        translateMax.value - STEP * 3 <= translateMin.value
      ) {
        return;
      }

      const current = Math.round(
        Math.max(Math.min(event.translationX + offsetMax.value, MAX), MIN) /
          STEP,
      );

      runOnJS(onChangeMax)(current + 3);
      translateMax.value = current * STEP;
    });

  const minStyle = useAnimatedStyle(() => ({
    transform: [{ translateX: translateMin.value }],
  }));

  const maxStyle = useAnimatedStyle(() => ({
    transform: [{ translateX: translateMax.value }],
  }));

  const lineLeftStyle = useAnimatedStyle(() => ({
    width: translateMin.value + 20,
  }));

  const lineRightStyle = useAnimatedStyle(() => ({
    width: width - 55 - translateMax.value,
  }));

  return (
    <>
      <S_Container>
        <S_Line style={[lineLeftStyle]} />
        <GestureDetector gesture={panMin}>
          <S_DotContainer style={[minStyle]}>
            <S_Dot />
          </S_DotContainer>
        </GestureDetector>
        <GestureDetector gesture={panMax}>
          <S_DotContainer style={[maxStyle]}>
            <S_Dot />
          </S_DotContainer>
        </GestureDetector>
        <S_Line style={[lineRightStyle, { right: 0 }]} />
      </S_Container>
    </>
  );
};

const S_Container = styled.View`
  height: 4px;
  width: 100%;
  justify-content: center;
  background-color: ${(props) => props.theme.fontColor.primary};
`;

const S_DotContainer = styled(Animated.View)`
  position: absolute;
  z-index: 2;
  width: 30px;
  height: 30px;
  justify-content: center;
  align-items: center;
`;

const S_Dot = styled(Animated.View)`
  width: 16px;
  height: 16px;
  border-radius: 8px;
  background-color: ${(props) => props.theme.fontColor.primary};
`;

const S_Line = styled(Animated.View)`
  position: absolute;
  height: 4px;
  background-color: ${(props) => props.theme.background.action};
`;
