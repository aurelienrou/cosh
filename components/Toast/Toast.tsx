import React, { FC } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { ToastConfigParams } from 'react-native-toast-message';
import { width } from '../../constants/Layout';

interface ToastProps {
  type?: 'success' | 'info' | 'warning' | 'error';
  text: string;
}

const toastTypeConfig = {
  success: {
    icon: require('./../../assets/images/success.png'),
    primaryColor: '#3fbd60',
    secondaryColor: '#ebf7ee',
  },
  warning: {
    icon: require('../../assets/images/warning.png'),
    primaryColor: '#ef9400',
    secondaryColor: '#fef7ea',
  },
  error: {
    icon: require('../../assets/images/warning.png'),
    primaryColor: '#ec4e2c',
    secondaryColor: '#fdedea',
  },
  info: {
    icon: require('../../assets/images/info.png'),
    primaryColor: '#016de7',
    secondaryColor: '#e5effa',
  },
};

const BasicToast: FC<ToastProps> = ({ text, type = 'info' }) => {
  const testToDisplay = Array.isArray(text) ? text : [text];

  return (
    <View
      style={[
        styles.container,
        {
          backgroundColor: toastTypeConfig[type].secondaryColor,
          borderColor: toastTypeConfig[type].primaryColor,
        },
      ]}
    >
      <View
        style={[
          styles.iconContainer,
          { backgroundColor: toastTypeConfig[type].primaryColor },
        ]}
      >
        <Image source={toastTypeConfig[type].icon} style={styles.icon} />
      </View>
      <View style={styles.textContainer}>
        {testToDisplay.map((txt, index) => (
          <Text key={txt + index} style={styles.text}>
            {txt}
          </Text>
        ))}
      </View>
    </View>
  );
};

export const toastConfig = {
  success: (props: ToastConfigParams<ToastProps>) => (
    <BasicToast type="success" {...props.props} />
  ),
  error: (props: ToastConfigParams<ToastProps>) => (
    <BasicToast type="error" {...props.props} />
  ),
  warning: (props: ToastConfigParams<ToastProps>) => (
    <BasicToast type="warning" {...props.props} />
  ),
  info: (props: ToastConfigParams<ToastProps>) => (
    <BasicToast type="info" {...props.props} />
  ),
};

const styles = StyleSheet.create({
  container: {
    minHeight: 60,
    width: '96%',
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 6,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.84,
    elevation: 5,
  },
  textContainer: {
    width: width - 90,
    flexDirection: 'column',
    paddingTop: 10,
  },
  iconContainer: {
    width: 44,
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    marginRight: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  icon: { width: 30, height: 30 },
  text: { fontSize: 15, marginBottom: 10 },
});
