import { Image } from 'expo-image';
import React from 'react';
import styled from 'styled-components/native';
import { capitalizeFirstLetter } from '../../helpers/string';
import Icons from '../../assets/icons';
import { ScrollView } from 'react-native';

export type Tag = keyof typeof Icons;

interface TagsProps {
  tags: string[];
}

export const Tags = ({ tags }: TagsProps) => {
  return (
    <ScrollView showsHorizontalScrollIndicator={false} horizontal>
      {tags.map((tag, index) => (
        <S_Text key={index}>{capitalizeFirstLetter(tag)}</S_Text>
      ))}
    </ScrollView>
  );
};

const S_Text = styled.Text`
  font-family: 'neue';
  font-size: 16px;
  margin-right: 16px;
  color: ${(props) => props.theme.fontColor.secondary};
`;
