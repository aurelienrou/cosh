import {
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  Keyboard,
} from 'react-native';
import React, { FC, useState } from 'react';
import { RESULTS_INPUT_ROW_HEIGHT, width } from '../../constants/Layout';
import { getAddressFromPlaceId } from '../../services/googleGeocode';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import { AddressGoogle, IPlace } from '../../services/types';
import {
  INPUT_BACKGROUND_COLOR,
  PLACEHOLDER_COLOR,
  PRIMARY_COLOR,
} from '../../constants/Colors';
import { getPlaces } from '../../services/placesServices';
import Toast from 'react-native-toast-message';

export const MAX_ROWS_VISIBLE = 5;

interface InputPlaceFinderProps {
  onSelect: (address: AddressGoogle) => void;
  onChangeText?: (address: string) => void;
  resetAfterSelect?: boolean;
}

const InputPlaceFinder: FC<InputPlaceFinderProps> = ({
  onSelect,
  onChangeText,
  resetAfterSelect = false,
}) => {
  const [inputText, setAddress] = useState<string>('');
  const [results, setResults] = useState<IPlace[]>([]);

  const handleSearchAddress = async (text: string) => {
    onChangeText?.(text);
    setAddress(text);

    if (text.trim() === '') {
      resultsHeight.value = withSpring(0, {
        mass: 0.6,
      });

      setResults([]);
      return;
    }

    try {
      const data = await getPlaces(text);

      if (data) {
        resultsHeight.value = withSpring(
          data.length < 5
            ? data.length * RESULTS_INPUT_ROW_HEIGHT
            : MAX_ROWS_VISIBLE * RESULTS_INPUT_ROW_HEIGHT,
          {
            mass: 0.6,
          },
        );
      }
      setResults(data);
    } catch ({ message }) {
      Toast.show({
        type: 'error',
        props: { text: message },
      });
    }
  };

  const handleSelectPlace = (result: any) => {
    getAddressFromPlaceId(result.placeId).then((address) => {
      onSelect({ ...address, id: result.id });
    });

    setAddress(resetAfterSelect ? '' : result.name);
    setResults([]);
    Keyboard.dismiss();
  };

  const resultsHeight = useSharedValue(0);
  const animatedStyleResults = useAnimatedStyle(() => ({
    height: resultsHeight.value,
  }));

  return (
    <>
      <TextInput
        style={styles.input}
        onChangeText={handleSearchAddress}
        value={inputText}
        placeholder="Chercher un lieu ou une adresse"
        placeholderTextColor={PLACEHOLDER_COLOR}
        clearButtonMode="while-editing"
        returnKeyType="search"
      />
      {results.length > 0 && (
        <Animated.View style={[styles.results, animatedStyleResults]}>
          <ScrollView>
            {results.map((res, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => handleSelectPlace(res)}
              >
                <Text style={styles.result}>{res.name}</Text>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </Animated.View>
      )}
    </>
  );
};

export default InputPlaceFinder;

const styles = StyleSheet.create({
  input: {
    height: 50,
    backgroundColor: INPUT_BACKGROUND_COLOR,
    width: width - 40,
    borderRadius: 10,
    overflow: 'hidden',
    paddingHorizontal: 20,
  },
  results: {
    maxHeight: RESULTS_INPUT_ROW_HEIGHT * MAX_ROWS_VISIBLE,
    marginTop: 10,
    borderRadius: 10,
    overflow: 'hidden',
    borderColor: PRIMARY_COLOR,
    borderWidth: 1,
  },
  result: {
    height: RESULTS_INPUT_ROW_HEIGHT,
    backgroundColor: 'white',
    width: width - 40,
    justifyContent: 'center',
    padding: 10,
  },
});
