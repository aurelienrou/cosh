import { Image } from 'expo-image';
import React from 'react';
import styled from 'styled-components/native';
import { capitalizeFirstLetter } from '../../helpers/string';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import {
  Blur,
  Canvas,
  Image as SkImage,
  useImage,
} from '@shopify/react-native-skia';
import { height, width } from '../../constants/Layout';

interface PlaceOrArtistHaderProps {
  name: string;
  cover: string;
  followersCount: number;
}
export const PlaceOrArtistHeader = ({
  name,
  followersCount,
  cover,
}: PlaceOrArtistHaderProps) => {
  const { top } = useSafeAreaInsets();
  const image = useImage(cover);

  return (
    <S_Container>
      <S_InfosContainer style={{ top: top + 20 }}>
        <S_Image source={{ uri: cover }} />
        <S_Infos>
          <S_Name>{name}</S_Name>
          <S_TypeAndFollower>{followersCount} followers</S_TypeAndFollower>
        </S_Infos>
      </S_InfosContainer>
      <S_Canvas>
        {image && (
          <SkImage
            x={0}
            y={0}
            width={width}
            height={300}
            image={image}
            fit="cover"
          >
            <Blur blur={7} mode="clamp" />
          </SkImage>
        )}
      </S_Canvas>
    </S_Container>
  );
};

const S_Container = styled.View`
  height: 300px;
  background-color: ${(props) => props.theme.background.primary};
`;

const S_InfosContainer = styled.View`
  position: absolute;
  z-index: 1;
  top: 0;
  left: 0;
  width: 100%;
  align-items: center;
`;

const S_Canvas = styled(Canvas)`
  height: 300px;
  width: 100%;
  align-items: center;
`;
const S_Image = styled(Image)`
  height: 96px;
  width: 96px;
  border-radius: 48px;
`;

const S_Infos = styled.View`
  margin-top: 24px;
  align-items: center;
`;

const S_Name = styled.Text`
  font-size: 14px;
  font-family: 'gatwick-bold';
  color: ${(props) => props.theme.fontColor.primary};
  margin-bottom: 8px;
`;

const S_TypeAndFollower = styled.Text`
  font-size: 14px;
  font-family: 'neue';
  color: ${(props) => props.theme.fontColor.secondary};
`;
