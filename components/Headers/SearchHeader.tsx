import React, { useEffect, useRef, useState } from 'react';
import { View, Pressable, TextInput, StyleSheet } from 'react-native';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import { Image } from 'expo-image';
import { useMainTheme } from '../../hooks/useMainTheme';
import { width } from '../../constants/Layout';
import { Theme } from '../../constants/theme';

interface SearchHeaderProps {
  onChange: (text: string) => void;
  onClickSettings: () => void;
}

export const SearchHeader = ({
  onChange,
  onClickSettings,
}: SearchHeaderProps) => {
  const inputRef = useRef<TextInput>(null);
  const styles = useMainTheme(createStyles);
  const [textInput, setTextInput] = useState('');
  const isFocus = useSharedValue(0);
  const isTyping = useSharedValue(0);

  useEffect(() => {
    onChange(textInput);
  }, [textInput]);

  const onClear = () => {
    setTextInput('');
  };

  const onPressArrow = () => {
    inputRef.current?.blur();
  };

  const onBlur = () => {
    isFocus.value = withSpring(0);
  };

  const onFocus = () => {
    isFocus.value = withSpring(1);
  };

  const onChangeText = (text: string) => {
    isTyping.value = text.length ? withSpring(1) : withSpring(0);

    setTextInput(text);
  };

  const calendarIconStyle = useAnimatedStyle(() => {
    const opacity = interpolate(
      isFocus.value,
      [0, 1],
      [1, 0],
      Extrapolate.CLAMP,
    );
    const translateX = interpolate(
      isFocus.value,
      [0, 1],
      [0, -48],
      Extrapolate.CLAMP,
    );
    return { opacity, transform: [{ translateX }], position: 'absolute' };
  });

  const arrowIconStyle = useAnimatedStyle(() => {
    const opacity = interpolate(
      isFocus.value,
      [0, 1],
      [0, 1],
      Extrapolate.CLAMP,
    );
    const scale = interpolate(isFocus.value, [0, 1], [0, 1], Extrapolate.CLAMP);
    return { opacity, transform: [{ scale }] };
  });

  const clearIconStyle = useAnimatedStyle(() => {
    const opacity = interpolate(
      isFocus.value,
      [0, 1],
      [0, 1],
      Extrapolate.CLAMP,
    );
    return { opacity };
  });

  return (
    <View style={styles.S_Header}>
      <Animated.View style={[calendarIconStyle]}>
        <Pressable
          style={[styles.S_Button, { marginRight: 8, left: 20 }]}
          onPress={onClickSettings}
        >
          <Image
            style={[styles.S_Icon]}
            source={require('./../../assets/icons/calendar.svg')}
          />
        </Pressable>
      </Animated.View>

      <Animated.View style={[arrowIconStyle]}>
        <Pressable
          style={[styles.S_Button, { marginRight: 8 }]}
          onPress={onPressArrow}
        >
          <Image
            style={[styles.S_Icon, { height: 15, width: 18 }]}
            source={require('./../../assets/icons/arrow.svg')}
          />
        </Pressable>
      </Animated.View>

      <Animated.View style={[styles.S_SearchBarContainer]}>
        <TextInput
          ref={inputRef}
          style={[styles.S_SearchBar]}
          placeholder="Search event or places"
          placeholderTextColor="#999999"
          selectionColor="#FFF"
          onFocus={onFocus}
          onBlur={onBlur}
          onChangeText={onChangeText}
          value={textInput}
        />
        <Image
          style={[styles.S_IconSearch, { tintColor: '#494949' }]}
          source={require('./../../assets/icons/search.svg')}
        />
        <Animated.View style={[clearIconStyle, styles.S_IconClear]}>
          <Pressable style={styles.S_Button} onPress={onClear}>
            <Image
              style={[styles.S_Icon]}
              source={require('./../../assets/icons/clear.svg')}
            />
          </Pressable>
        </Animated.View>
      </Animated.View>
    </View>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_Container: {
      flex: 1,
      backgroundColor: theme.background.primary,
    },
    S_LoadingContainer: {
      paddingHorizontal: 20,
    },
    S_SearchBar: {
      height: 48,
      borderRadius: 36,
      paddingVertical: 12,
      paddingLeft: 48,
      paddingRight: 16,
      backgroundColor: theme.background.action,
      color: theme.fontColor.primary,
    },
    S_Header: {
      height: 48,
      flexDirection: 'row',
      paddingHorizontal: 20,
      marginTop: 16,
    },
    S_SearchBarContainer: {
      height: 48,
      width: width - 96,
      justifyContent: 'center',
    },
    S_IconClear: {
      position: 'absolute',
      right: 0,
    },
    S_Button: {
      width: 48,
      height: 48,
      borderRadius: 24,
      backgroundColor: theme.background.action,
      justifyContent: 'center',
      alignItems: 'center',
    },
    S_Icon: {
      width: 22,
      height: 22,
    },
    S_IconSearch: {
      width: 22,
      height: 22,
      position: 'absolute',
      left: 16,
    },
    S_Padding: {
      padding: 0,
    },
  });
