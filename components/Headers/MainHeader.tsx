import React, { Ref, RefObject, useCallback, useRef, useState } from 'react';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import { FlatList, Platform, Pressable, StyleSheet, Text } from 'react-native';
import { SCREENS_NAME, SCREEN_NAME_TYPE } from '../DiscoverTabView/helper';
import { useNavigation } from '@react-navigation/native';
import { useMapsContext } from '../../provider/MapsProvider';
import { View } from 'react-native';
import { Image } from 'expo-image';
import { BlurView } from '@react-native-community/blur';
import { setSelected } from '../../provider/MapStore';
import { useDispatch } from 'react-redux';
import { useFiltersContext } from '../../provider/FiltersProvider';
import { Filters } from '../../screens/Filters';
import { isBeforeXHour } from '../../helpers/date';
import { useAuthContext } from '../../provider/AuthProvider';
import auth from '@react-native-firebase/auth';
import { IUser } from '../../services/types';
import { ModalProfil } from '../Profil/Profil';
import BottomSheet, { BottomSheetModal } from '@gorhom/bottom-sheet';
import { useLocationContext } from '../../provider/LocationProvider';
import { deleteUserById } from '../../services/user/deleteUserById';
import analytics from '@react-native-firebase/analytics';
import { revokeSignInWithAppleToken } from '../../hooks/useSignInWithApple';
import { blurAmount } from '../../constants/theme';
import { width } from '../../constants/Layout';

export const MainHeader = () => {
  const { top } = useSafeAreaInsets();
  const dispatch = useDispatch();
  const { ref, setCurrentIndex, currentIndex } = useFiltersContext();
  const { address } = useLocationContext();
  const { bottomSheetRef, animatedIndex, isFullyOpen, setIsFullyOpen } =
    useMapsContext();
  const animatedSettingsIcon = useSharedValue(1);

  const flatlistRef: Ref<FlatList> = useRef(null);
  const profilModalRef = useRef<BottomSheetModal>(null);

  const navigation = useNavigation<any>();

  const { user, setUser, isLoggin } = useAuthContext();

  const handleOnPress = (index: number, screen: SCREEN_NAME_TYPE) => {
    animatedSettingsIcon.value = withSpring(
      screen === 'Bars' || screen === 'Clubs' || screen === 'Concerts' ? 0 : 1,
    );
    setCurrentIndex(index);
    navigation.navigate('Discover', { screen });
    flatlistRef.current?.scrollToIndex({ animated: true, index });

    analytics().logScreenView({ screen_name: screen });
    dispatch(setSelected(0));
  };

  const styleSettingsIcon = useAnimatedStyle(() => {
    const opacity = interpolate(
      animatedSettingsIcon.value,
      [0, 0.5],
      [1, 0],
      Extrapolate.CLAMP,
    );

    const translateX = interpolate(
      animatedSettingsIcon.value,
      [0, 1],
      [0, 80],
      Extrapolate.CLAMP,
    );
    return { transform: [{ translateX }], opacity };
  });

  const styleHeader = useAnimatedStyle(() => {
    const translateY = interpolate(
      animatedIndex.value,
      [0, 1],
      isFullyOpen ? [-220 - top, -220 - top] : [0, 0],
      Extrapolate.CLAMP,
    );
    return { transform: [{ translateY }] };
  });

  const handleBackContent = () => {
    if (bottomSheetRef.current) {
      bottomSheetRef.current.expand();
      navigation.navigate('Discover', { screen: SCREENS_NAME[currentIndex] });
    }
    setIsFullyOpen(false);
  };

  const handlePressSettings = () => {
    ref.current?.present();
  };

  const handleSignOut = async () => {
    await auth().signOut();
    setUser({} as IUser);
    profilModalRef.current?.close();
  };

  const handleDeleteAccount = async () => {
    try {
      await revokeSignInWithAppleToken();
      await deleteUserById(user.id);
      setUser({} as IUser);
      profilModalRef.current?.close();
    } catch (err) {
      console.log(err);
    }
  };

  const openProfil = () => {
    if (isLoggin) {
      profilModalRef.current?.present();
    } else {
      navigation.navigate('SignInOut' as never);
    }
  };

  const renderItem = useCallback(
    ({ item, index }) => {
      const c = index === currentIndex;
      return (
        <Pressable
          style={[
            styles.S_TextMenuContainer,
            {
              borderBottomColor: c ? 'rgba(255, 255, 255, 1)' : 'transparent',
            },
          ]}
          key={index}
          onPress={() => handleOnPress(index, item)}
        >
          <Text
            style={[
              styles.S_TextMenu,
              {
                color: c ? '#fff' : '#999999',
                fontFamily: c ? 'neue-bold' : 'neue',
              },
            ]}
          >
            {item}
          </Text>
        </Pressable>
      );
    },
    [currentIndex],
  );

  return (
    <>
      {isFullyOpen ? (
        <Pressable
          style={[styles.S_BackPressable, { top }]}
          onPress={handleBackContent}
        >
          <Image
            style={styles.S_BackIcon}
            source={require('../../assets/icons/arrow.svg')}
          />
          <BlurView style={styles.S_Blur} blurAmount={blurAmount} />
        </Pressable>
      ) : null}
      <Animated.View
        style={[styles.S_Header, styleHeader, { paddingTop: top }]}
      >
        <Animated.View style={[styles.S_HeaderInfos]}>
          <View style={styles.S_ProfileInfos}>
            <Pressable style={styles.S_Picture} onPress={openProfil}>
              <Image
                style={
                  user?.cover ? styles.S_ProfileIcon : styles.S_PictureIcon
                }
                source={
                  user?.cover
                    ? {
                        uri: user?.cover,
                      }
                    : require('../../assets/icons/user-rounded.svg')
                }
              />
            </Pressable>
            <Pressable
              style={styles.S_Infos}
              onPress={() =>
                navigation.navigate('GetLocationModal', { isModal: true })
              }
            >
              <Text style={styles.S_Name}>
                {address?.locality
                  ? address?.locality
                  : isBeforeXHour(new Date(), 17)
                  ? 'Bonjour !'
                  : 'Bonsoir !'}
              </Text>
              <Text style={styles.S_Position} numberOfLines={1}>
                {address?.formatted_address?.split(',')?.[0]}
              </Text>
            </Pressable>
          </View>
          <Animated.View style={[styleSettingsIcon]}>
            <Pressable
              style={styles.S_SettingsButton}
              onPress={handlePressSettings}
            >
              <Image
                style={styles.S_SettingsIcon}
                source={require('../..//assets/icons/settings.svg')}
              />
            </Pressable>
          </Animated.View>
        </Animated.View>

        <FlatList<SCREEN_NAME_TYPE>
          style={styles.S_HorizontalMenu}
          keyExtractor={(item, index) => index.toString()}
          ref={flatlistRef}
          horizontal
          data={SCREENS_NAME}
          contentContainerStyle={{
            marginLeft: 20,
            paddingRight: 20,
          }}
          showsHorizontalScrollIndicator={false}
          decelerationRate={Platform.OS === 'ios' ? 5 : 0.98}
          renderItem={renderItem}
        />
      </Animated.View>
      <Filters />
      <ModalProfil
        refBottomSheet={profilModalRef}
        onClose={() => profilModalRef.current?.close()}
        onLogOut={handleSignOut}
        onDelete={handleDeleteAccount}
      />
    </>
  );
};

const styles = StyleSheet.create({
  S_Header: {
    zIndex: 3,
    backgroundColor: '#151718',
  },
  S_HorizontalMenu: {
    paddingTop: 32,
    borderBottomColor: 'rgba(255, 255, 255, 0.14)',
    borderBottomWidth: 1,
    backgroundColor: '#151718',
  },
  S_TextMenuContainer: {
    borderBottomWidth: 1,
    minHeight: 32,
    marginRight: 24,
  },
  S_TextMenu: {
    fontFamily: 'neue',
    fontSize: 16,
  },
  S_HeaderInfos: {
    display: 'flex',
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 16,
  },
  S_SettingsButton: {
    width: 48,
    height: 48,
    borderRadius: 24,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#242627',
  },
  S_SettingsIcon: {
    width: 22,
    height: 18,
  },
  S_ProfileInfos: {
    display: 'flex',
    flexDirection: 'row',
  },
  S_Infos: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  S_Picture: {
    width: 48,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#242627',
    marginRight: 16,
    borderRadius: 24,
  },
  S_PictureIcon: {
    width: 24,
    height: 24,
  },
  S_ProfileIcon: {
    width: 48,
    height: 48,
    borderRadius: 24,
  },
  S_Name: {
    fontFamily: 'gatwick-bold',
    fontSize: 20,
    color: '#FFFFFF',
  },
  S_Position: {
    fontFamily: 'neue',
    fontSize: 16,
    color: '#999999',
    width: width - 40 - 48 * 2 - 32,
  },
  S_BackPressable: {
    height: 48,
    width: 48,
    left: 20,
    zIndex: 1,
    borderRadius: 24,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    backgroundColor: '#242627',
  },
  S_Blur: {
    height: 48,
    width: 48,
    borderRadius: 24,
    position: 'absolute',
  },
  S_BackIcon: {
    height: 14,
    width: 18,
    zIndex: 11,
  },
});
