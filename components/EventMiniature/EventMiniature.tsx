import React, { FC } from 'react';
import { StyleSheet, Pressable, Image, View } from 'react-native';
import { width } from '../../constants/Layout';
import { BlurView } from '@react-native-community/blur';
import { useNavigation } from '@react-navigation/native';
import { IEvent } from '../../services/types';
import CoshText from '../CoshText/CoshText';
import { formateDate } from '../../helpers/date';
import { formateDistance } from '../../helpers/number';

import FollowButton from '../Button/FollowButton';
import { blurAmount } from '../../constants/theme';
interface EventMiniatureProps {
  event: IEvent;
}

const EventMiniature: FC<EventMiniatureProps> = ({ event }) => {
  const navigation = useNavigation();

  return (
    <Pressable
      onPress={() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        navigation.push('Event', { id: event.id });
      }}
      style={styles.container}
    >
      <Image
        source={{ uri: event.cover }}
        style={styles.cover}
        resizeMode="cover"
      />
      <BlurView blurAmount={blurAmount} style={styles.distanceBlur}>
        <CoshText>{formateDistance(event.distance)}</CoshText>
      </BlurView>
      <BlurView blurAmount={blurAmount} style={styles.priceBlur}>
        <CoshText>
          {event.minPrice === 0 ? 'Free' : event.minPrice + ' €'}
        </CoshText>
      </BlurView>

      <BlurView blurAmount={blurAmount} style={styles.inner}>
        <View style={styles.infos}>
          <CoshText style={styles.date}>{`${formateDate(event.beginAt)} ${
            event.endAt && ' - '.concat(formateDate(event.endAt))
          }`}</CoshText>
          <CoshText numberOfLines={1} style={styles.title}>
            {event.name}
          </CoshText>
          <CoshText style={styles.place}>{event.place}</CoshText>
        </View>
        <FollowButton id={event.id} type={'Event'} />
      </BlurView>
    </Pressable>
  );
};

export default EventMiniature;

const WIDTH = width - 40;

const styles = StyleSheet.create({
  container: {
    width: WIDTH,
    height: 200,
    position: 'relative',
    marginBottom: 20,
  },
  cover: {
    width: WIDTH,
    height: 200,
    borderRadius: 20,
  },
  inner: {
    position: 'absolute',
    bottom: 10,
    backgroundColor: 'rgba(255 ,255, 255, 0.7)',
    width: WIDTH - 20,
    marginLeft: 10,
    marginTop: 200 - 70 - 10,
    height: 70,
    borderRadius: 14,
    overflow: 'hidden',
    paddingHorizontal: 10,
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  infos: {
    justifyContent: 'space-between',
    width: WIDTH - 80,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  place: {
    fontSize: 16,
  },
  date: {
    fontWeight: 'bold',
    color: '#464646',
  },
  distanceBlur: {
    position: 'absolute',
    top: 10,
    left: 10,
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: 'rgba(255 ,255, 255, 0.7)',
  },
  priceBlur: {
    position: 'absolute',
    top: 10,
    right: 10,
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: 'rgba(255 ,255, 255, 0.7)',
  },
});
