import React from 'react';
import { Image } from 'expo-image';
import styled from 'styled-components/native';
import { ITopics } from '../../services/types';
import { width } from '../../constants/Layout';

interface RubriqueProps {
  rubrique: ITopics;
  onPress: (id: string) => void;
}
export const Rubrique = ({ rubrique, onPress }: RubriqueProps) => {
  return (
    <S_Container onPress={() => onPress(rubrique.id)}>
      <S_Image source={rubrique.cover} />
      <S_Infos>
        <S_Title>{rubrique.name}</S_Title>
        <S_Time>{rubrique.date}</S_Time>
      </S_Infos>
    </S_Container>
  );
};

const S_Container = styled.Pressable`
  margin-right: 8px;
`;

const S_Image = styled(Image)`
  height: 200px;
  width: 200px;
  border-radius: 16px;
`;

const S_Infos = styled.View`
  margin-top: 16px;
`;

const S_Title = styled.Text`
  font-family: 'neue-bold';
  font-size: 14px;
  color: ${(props) => props.theme.fontColor.primary};
`;

const S_Time = styled.Text`
  font-family: 'neue';
  font-size: 14px;
  color: ${(props) => props.theme.fontColor.action};
  margin-top: 8px;
`;
