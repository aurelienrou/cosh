import React from 'react';
import styled from 'styled-components/native';
import { Rubrique } from './Rubrique';
import { useMapsContext } from '../../provider/MapsProvider';
import { useNavigation } from '@react-navigation/native';
import axios from '../../services/axios';
import { ITopics } from '../../services/types';
import ContentLoader, { Rect } from 'react-content-loader/native';
import Animated, { FadeInRight } from 'react-native-reanimated';

interface RubriquesProps {
  topics: ITopics[];
  loading: boolean;
}

const Loader = () => (
  <ContentLoader
    speed={1}
    width={200}
    height={236}
    viewBox="0 0 200 236"
    backgroundColor="#242627"
    foregroundColor="#5b5b5b"
  >
    <Rect x="0" y="216" rx="8" ry="8" width="110" height="20" />
    <Rect x="0" y="0" rx="8" ry="8" width="200" height="200" />
  </ContentLoader>
);

export const Rubriques = ({ topics, loading }: RubriquesProps) => {
  const { bottomSheetRef, setIsFullyOpen } = useMapsContext();
  const navigation = useNavigation<any>();

  const onPressRubrique = (id: string) => {
    setIsFullyOpen(true);
    bottomSheetRef.current?.collapse();

    navigation.navigate('Discover', {
      screen: 'Content',
      params: { id },
    });
  };

  return (
    <S_Scrollview
      contentContainerStyle={{ marginLeft: 20, marginBottom: 28 }}
      horizontal
      showsHorizontalScrollIndicator={false}
    >
      {loading
        ? Array(5)
            .fill(0)
            .map((number, index) => (
              <S_ContainerLoader key={number + index}>
                <Loader />
              </S_ContainerLoader>
            ))
        : topics.map((topic, index) => (
            <Animated.View
              key={index}
              entering={FadeInRight.delay(index * 50).duration(300)}
            >
              <Rubrique onPress={onPressRubrique} rubrique={topic} />
            </Animated.View>
          ))}
    </S_Scrollview>
  );
};

const S_ContainerLoader = styled.View`
  margin-right: 8px;
`;

const S_Scrollview = styled.ScrollView`
  margin-top: 16px;
`;
