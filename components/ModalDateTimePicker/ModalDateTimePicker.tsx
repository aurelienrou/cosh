import React, { FC, RefObject, useEffect, useState } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  Pressable,
} from 'react-native';
import { height, width } from '../../constants/Layout';
import CalendarPicker, { ChangedDate } from 'react-native-calendar-picker';
import { useTheme } from '../../provider/ThemeProvider';
import Animated, {
  Extrapolation,
  interpolate,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import BottomSheet from '@gorhom/bottom-sheet';
import { Theme } from '../../constants/theme';
import { useMainTheme } from '../../hooks/useMainTheme';
import { BottomSheetMethods } from '@gorhom/bottom-sheet/lib/typescript/types';

export type SelectedDate = {
  START_DATE: Date | null;
  END_DATE: Date | null;
};
interface ModalDateTimePickerProps {
  onConfirm: (date: SelectedDate) => void;
  onClose: () => void;
  refBottomSheet: RefObject<BottomSheetMethods>;
}

export const ModalDateTimePicker: FC<ModalDateTimePickerProps> = ({
  onConfirm,
  onClose,
  refBottomSheet,
}) => {
  const position = useSharedValue(0);
  const { theme } = useTheme();
  const styles = useMainTheme(createStyles);
  const [date, setDate] = useState<SelectedDate>({
    START_DATE: null,
    END_DATE: null,
  });

  const styleBackground = useAnimatedStyle(() => {
    return {
      opacity: interpolate(position.value, [0, 1], [0, 1], Extrapolation.CLAMP),
      transform: [
        {
          translateY: interpolate(
            position.value,
            [0, 0.001],
            [height, 0],
            Extrapolation.CLAMP,
          ),
        },
      ],
    };
  });
  const minDate = new Date();

  const handleDateChange = (date: Date, type: ChangedDate) => {
    setDate((prev) => ({ ...prev, [type]: date }));
  };

  return (
    <>
      <Animated.View style={[styleBackground, styles.blur]}>
        <Pressable style={{ flex: 1 }} onPress={onClose} />
      </Animated.View>
      <BottomSheet
        style={styles.S_BottomSheet}
        ref={refBottomSheet}
        animateOnMount={false}
        onClose={onClose}
        animatedIndex={position}
        snapPoints={[1, width + 130]}
        containerStyle={{ zIndex: 4 }}
        enablePanDownToClose
        backgroundStyle={{
          backgroundColor: theme.background.primary,
        }}
        handleIndicatorStyle={{ backgroundColor: theme.fontColor.primary }}
      >
        <View style={styles.container}>
          <CalendarPicker
            onDateChange={handleDateChange}
            minDate={minDate}
            allowRangeSelection
            startFromMonday
            textStyle={{ color: '#FFF' }}
            selectedDayColor={theme.fontColor.action}
          />
          <View style={styles.buttons}>
            <TouchableOpacity onPress={onClose} style={[styles.button]}>
              <Text style={styles.cancelText}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => onConfirm(date)}
              style={[styles.confirm, styles.button]}
            >
              <Text style={styles.text}>Validate</Text>
            </TouchableOpacity>
          </View>
        </View>
      </BottomSheet>
    </>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_BottomSheet: {
      paddingBottom: 0,
      borderRadius: 32,
      overflow: 'hidden',
      height,
      justifyContent: 'flex-end',
    },
    blur: {
      ...StyleSheet.absoluteFillObject,
      zIndex: 1,
      flex: 1,
      height,
      backgroundColor: 'rgba(0,0,0,0.8)',
    },
    container: {
      paddingTop: 24,
      justifyContent: 'flex-end',
      paddingHorizontal: 20,
    },
    buttons: {
      marginTop: 24,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    button: {
      height: 48,
      borderRadius: 24,
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 16,
    },
    confirm: {
      backgroundColor: theme.fontColor.action,
    },
    text: {
      fontFamily: 'neue-bold',
      fontSize: 14,
    },
    cancelText: {
      fontFamily: 'neue-bold',
      fontSize: 14,
      color: theme.fontColor.primary,
    },
  });
