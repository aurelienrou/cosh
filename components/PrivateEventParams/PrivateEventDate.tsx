import React, {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Pressable,
} from 'react-native';
import { useCreateOrEditPrivateEventContext } from '../../provider/CreateOrEditPrivateEventProvider';
import { useMainTheme } from '../../hooks/useMainTheme';
import { Theme } from '../../constants/theme';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useState } from 'react';
import { useTheme } from '../../provider/ThemeProvider';
import { width } from '../../constants/Layout';
import CalendarPicker from 'react-native-calendar-picker';
import DateTimePicker, {
  DateTimePickerEvent,
} from '@react-native-community/datetimepicker';
import dayjs from 'dayjs';

interface PrivateEventDateProps {
  onSubmit: () => void;
}

export const PrivateEventDate = ({ onSubmit }: PrivateEventDateProps) => {
  const { top } = useSafeAreaInsets();
  const { theme } = useTheme();
  const { event, setEvent } = useCreateOrEditPrivateEventContext();
  const styles = useMainTheme(createStyles);
  const [startDate, setStartDate] = useState(event.startDate ?? new Date());
  const [endDate, setEndDate] = useState(event.endDate);
  const [current, setCurrent] = useState<'START' | 'END'>('START');

  const handleDateSelect = (date: Date) => {
    if (current === 'START') {
      setStartDate(date);
      if (endDate && dayjs(endDate).isBefore(date)) {
        setEndDate(date);
        // setEndTime(date);
      }
    } else {
      setEndDate(date);
    }
  };

  const handleCurrentType = (type: 'START' | 'END') => {
    setCurrent(type);
    setEndDate(startDate);
  };

  const handleTimeSelect = (_: DateTimePickerEvent, date?: Date) => {
    if (date) {
      if (current === 'START') {
        setStartDate(date);
        // setStartTime(date);
        if (endDate && dayjs(endDate).isBefore(date)) {
          //   setEndTime(date);
          setEndDate(date);
        }
      } else {
        setEndDate(date);
      }
    }
  };
  const currentDate = current === 'START' ? startDate : endDate;
  const minDate = current === 'START' ? new Date() : startDate;

  const handleValidate = () => {
    setEvent({ ...event, startDate, endDate });
    onSubmit();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Date</Text>
      <View style={styles.buttons}>
        <View style={styles.button}>
          <TouchableOpacity
            style={{ backgroundColor: '#7d6060' }}
            onPress={() => handleCurrentType('START')}
          >
            <Text>{dayjs(startDate).format('dddd DD MMM')}</Text>
            <Text>{dayjs(startDate).format('H:mm')}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.button}>
          <TouchableOpacity
            style={{ backgroundColor: '#617d60' }}
            onPress={() => handleCurrentType('END')}
          >
            {endDate ? (
              <>
                <Text>{dayjs(endDate).format('dddd DD MMM')}</Text>
                <Text>{dayjs(endDate).format('H:mm')}</Text>
              </>
            ) : (
              <Text>Date de fin</Text>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.deleteEndDate}
            onPress={() => setEndDate(null)}
          >
            <Text>X</Text>
          </TouchableOpacity>
        </View>
      </View>

      <CalendarPicker
        onDateChange={handleDateSelect}
        minDate={minDate}
        selectedStartDate={currentDate as Date}
        startFromMonday
        selectedDayColor={theme.fontColor.action}
        previousTitle="Précédent"
        nextTitle="Suivant"
        weekdays={['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim']}
        months={[
          'Janvier',
          'Février',
          'Mars',
          'Avril',
          'Mai',
          'Juin',
          'Juillet',
          'Août',
          'Septembre',
          'Octobre',
          'Novembre',
          'Décembre',
        ]}
        todayBackgroundColor="transparent"
        textStyle={{ color: '#FFF' }}
        todayTextStyle={{ color: '#FFF' }}
      />
      <DateTimePicker
        mode="time"
        value={currentDate || new Date()}
        onChange={handleTimeSelect}
        display="spinner"
        minuteInterval={5}
      />

      <View>
        <Pressable onPress={handleValidate}>
          <Text>Validate</Text>
        </Pressable>
      </View>
    </View>
  );
};

export const createStyles = (theme: Theme) =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: theme.background.primary,
      paddingHorizontal: 20,
    },
    title: {
      textAlign: 'center',
      color: theme.fontColor.primary,
      fontSize: 16,
      fontFamily: 'neue-bold',
    },
    buttons: {
      flexDirection: 'row',
      width: '100%',
    },
    button: {
      width: (width - 40) / 2,
    },
    deleteEndDate: {
      position: 'absolute',
      right: 0,
      top: 0,
      backgroundColor: 'red',
      width: 30,
      height: 30,
    },
  });
