import React, { StyleSheet } from 'react-native';
import { useCreateOrEditPrivateEventContext } from '../../provider/CreateOrEditPrivateEventProvider';
import { useMainTheme } from '../../hooks/useMainTheme';
import { Theme } from '../../constants/theme';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { ReactNode, Ref, forwardRef } from 'react';
import { BottomSheetBackdrop, BottomSheetModal } from '@gorhom/bottom-sheet';
import { BottomSheetModalMethods } from '@gorhom/bottom-sheet/lib/typescript/types';
import { useTheme } from '../../provider/ThemeProvider';
import { height } from '../../constants/Layout';

export const PrivateEventDateModal = forwardRef(function PrivateEventDateModal(
  { children, name }: { children: ReactNode; name: string },
  ref: Ref<BottomSheetModalMethods>,
) {
  const styles = useMainTheme(createStyles);
  const { top } = useSafeAreaInsets();
  const { theme } = useTheme();

  return (
    <BottomSheetModal
      ref={ref}
      key={name}
      name={name}
      style={styles.bottomSheet}
      snapPoints={[height - top]}
      enablePanDownToClose
      backgroundStyle={{
        backgroundColor: theme.background.primary,
      }}
      handleStyle={styles.handleStyle}
      handleIndicatorStyle={{
        backgroundColor: theme.fontColor.primary,
        width: 40,
      }}
      backdropComponent={(props) => (
        <BottomSheetBackdrop
          {...props}
          opacity={0.7}
          enableTouchThrough={false}
          appearsOnIndex={0}
          disappearsOnIndex={-1}
          style={[
            { backgroundColor: 'rgba(0, 0, 0, 1)' },
            StyleSheet.absoluteFillObject,
          ]}
        />
      )}
    >
      {children}
    </BottomSheetModal>
  );
});

export const createStyles = (theme: Theme) =>
  StyleSheet.create({
    bottomSheet: {
      flex: 1,
      borderTopLeftRadius: 32,
      borderTopRightRadius: 32,
      overflow: 'hidden',
    },
    handleStyle: {
      backgroundColor: theme.background.primary,
    },
  });
