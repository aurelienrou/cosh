import React, { Pressable, Text, View } from 'react-native';
import { useCreateOrEditPrivateEventContext } from '../../provider/CreateOrEditPrivateEventProvider';

export const PrivateEventHosts = () => {
  const { event } = useCreateOrEditPrivateEventContext();

  const handleAddCohosts = () => {
    // TODO
  };
  return (
    <View>
      <Text>Hotes</Text>
      {event.owners.map((owner, index) => (
        <Text key={index}>{owner.name}</Text>
      ))}
      <Pressable>
        <Text>Ajouter un co-hote</Text>
      </Pressable>
    </View>
  );
};
