import React, { TextInput } from 'react-native';
import { PrivateEventHosts } from './PrivateEventHosts';
import { useCreateOrEditPrivateEventContext } from '../../provider/CreateOrEditPrivateEventProvider';
import styled from 'styled-components/native';
import { SelectorLine } from '../Checkbox/SelectorLine';

export const PrivateEventParams = () => {
  const { event, setEvent } = useCreateOrEditPrivateEventContext();
  const onChange = () => ({});

  return (
    <Container>
      <PrivateEventHosts />
      <BoxContainer>
        <SelectorLine
          name="Accepter des RSVP"
          isSelected={event.rsvpsEnabled}
          onSelect={() =>
            setEvent({ ...event, rsvpsEnabled: !event.rsvpsEnabled })
          }
        />
      </BoxContainer>
      <BoxContainer>
        <BoxText>Max +1</BoxText>
        <TextInput />
      </BoxContainer>
      <BoxContainer>
        <SelectorLine
          name="Permettre aux invites de partager"
          isSelected={event.allowGuestsToShare}
          onSelect={() =>
            setEvent({
              ...event,
              allowGuestsToShare: !event.allowGuestsToShare,
            })
          }
        />
      </BoxContainer>
      <BoxContainer>
        <SelectorLine
          name="Permettre aux invites d ajouter des photos"
          isSelected={event.allowGuestPhotoUpload}
          onSelect={() =>
            setEvent({
              ...event,
              allowGuestPhotoUpload: !event.allowGuestPhotoUpload,
            })
          }
        />
      </BoxContainer>
      <BoxContainer>
        <SelectorLine
          name="Afficher la liste des invites"
          isSelected={event.showGuestList}
          onSelect={() =>
            setEvent({ ...event, showGuestList: !event.showGuestList })
          }
        />
      </BoxContainer>
    </Container>
  );
};

const BoxContainer = styled.View`
  width: width + 'px';
  flex-direction: row;
  margin-bottom: 8px;
`;

const Container = styled.View`
  flex: 1;
  background-color: red;
`;

const BoxText = styled.Text`
  color: #000;
`;
