import React, { memo, useCallback, useLayoutEffect, useState } from 'react';
import { ScrollView, View } from 'react-native';
import { CheckboxList } from './CheckboxList';
import { FiltersButtons } from './FiltersButtons';
import { useAsyncStorage } from '@react-native-async-storage/async-storage';
import { SectionHeader } from '../Section/SectionHeader';
import { StyleSheet } from 'react-native';
import { Theme } from '../../constants/theme';
import { SliderWithHeaderMemo } from './SliderWithHeader';
import { Line } from '../Blocks/Line';
import { IParamsFilters } from '../../services/types';

const barTypes = [
  'bar-a-biere',
  'bar-a-billards',
  'bar-a-chicha',
  'bar-a-cidre',
  'bar-a-cocktail',
  'bar-a-jeux',
  'bar-a-musique',
  'bar-a-rhum',
  'bar-a-shooters',
  'bar-a-spectacles',
  'bar-a-spiritueux',
  'bar-a-tapas',
  'bar-a-vin',
  'bar-alternatif',
  'bar-asiatique',
  'bar-associatif',
  'bar-brasserie',
  'bar-cache',
  'bar-chic',
  'bar-dansant',
  'bar-de-quartier',
  'bar-dhotel',
  'bar-ecossais',
  'bar-espagnol',
  'bar-etudiant',
  'bar-gaming',
  'bar-hip-hop',
  'bar-indie',
  'bar-insolite',
  'bar-italien',
  'bar-jazz',
  'bar-karaoke',
  'bar-latino',
  'bar-lgbtqi',
  'bar-pmu',
  'bar-sportif',
  'beer-hall',
  'brasserie-artisanale',
  'cafe',
  'cafe-concert',
  'caveau',
  'club',
  'coffee-shop',
  'comedy-club',
  'guinguette',
  'irish-pub',
  'peniche',
  'pub',
  'rooftop',
  'salon-de-the',
  'soiree-privee',
  'speakeasy',
  'sympa-pour-travailler',
];

const has = [
  { name: 'hasFreeWifi', icon: '' },
  { name: 'hasAirConditioning', icon: '' },
  { name: 'hasTelevision', icon: '' },
  { name: 'hasDarts', icon: '' },
  { name: 'hasFoosball', icon: '' },
  { name: 'hasPinball', icon: '' },
  { name: 'hasPool', icon: '' },
  { name: 'hasTerrace', icon: '' },
  { name: 'hasHandicapAccess', icon: '' },
  { name: 'hasFood', icon: '' },
  { name: 'hasDogPolicy', icon: '' },
  { name: 'hasBoardGames', icon: '' },
  { name: 'hasLiveMusic', icon: '' },
  { name: 'hasDjSet', icon: '' },
  { name: 'hasTakeAway', icon: '' },
];

const initialeTypes = barTypes.map((name) => ({
  name,
  isSelected: false,
}));

const CheckboxListMemo = memo(CheckboxList);

interface BarsFiltersProps {
  onValidate: (params: any) => void;
}

export const BarsFilters = ({ onValidate }: BarsFiltersProps) => {
  const { getItem: getStorageToken, setItem: setItemStorage } =
    useAsyncStorage('bar');

  const initialeState = useCallback(
    () =>
      ({
        types: [...initialeTypes],
        maxPrice: 0,
      } as IParamsFilters),
    [],
  );

  const [params, setParams] = useState<Omit<IParamsFilters, 'end' | 'start'>>(
    initialeState(),
  );
  const [initialParams, setInitialParams] = useState<
    Omit<IParamsFilters, 'end' | 'start'>
  >(initialeState());

  const handleClear = () => {
    setInitialParams(initialeState());
    setParams(initialeState());
  };

  const handleValidate = async () => {
    const selectedTypes = params.types
      .filter((type) => type.isSelected)
      .map((type) => type.name);

    const filters = JSON.stringify({
      types: params.types,
      maxPrice: params.maxPrice,
    });

    await setItemStorage(filters);

    onValidate({
      types: selectedTypes,
      maxPrice: params.maxPrice,
    });
  };

  useLayoutEffect(() => {
    const init = async () => {
      const res = await getStorageToken();
      if (res) {
        const filters = JSON.parse(res);

        setParams({
          types: filters?.types || [],
          maxPrice: filters?.maxPrice || 0,
        });

        setInitialParams({
          types: filters?.types || [],
          maxPrice: filters?.maxPrice || 0,
        });
      }
    };

    init();
  }, []);

  const handleSelectTypes = useCallback((name: string) => {
    setParams((prms) => ({
      ...prms,
      types: prms.types.map((item) =>
        item.name === name ? { ...item, isSelected: !item.isSelected } : item,
      ),
    }));
  }, []);

  const handleSelectPrice = useCallback((maxPrice: number) => {
    setParams((prms) => ({
      ...prms,
      maxPrice,
    }));
  }, []);

  return (
    <>
      <ScrollView
        contentContainerStyle={{ paddingBottom: 200, paddingHorizontal: 20 }}
        showsVerticalScrollIndicator={false}
      >
        <SliderWithHeaderMemo
          onChange={handleSelectPrice}
          initialeValue={initialParams.maxPrice}
          title="Prix de la biere"
        />
        <Line />
        <View style={{ marginBottom: 16 }}>
          <SectionHeader title="Types de bar" notHorizontalPadding />
        </View>
        <CheckboxListMemo
          initialeValue={initialParams.types}
          onChange={handleSelectTypes}
        />
      </ScrollView>
      <FiltersButtons onClear={handleClear} onValidate={handleValidate} />
    </>
  );
};

const createStyle = (theme: Theme) => {
  return StyleSheet.create({
    priceContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginBottom: 16,
    },
    priceText: {
      fontFamily: 'gatwick-bold',
      color: theme.fontColor.primary,
      fontSize: 15,
    },
    price: {
      fontFamily: 'neue-bold',
      color: theme.fontColor.secondary,
      fontSize: 14,
    },
  });
};
