import React, { memo, useEffect, useState } from 'react';
import { useMainTheme } from '../../hooks/useMainTheme';
import { View, Text, StyleSheet } from 'react-native';
import { Slider } from '@miblanchard/react-native-slider';
import { Theme } from '../../constants/theme';

const SliderWithHeader = ({ onChange, initialeValue, title }: any) => {
  const styles = useMainTheme(createStyle);
  const [value, setValue] = useState(0);

  useEffect(() => {
    setValue(initialeValue);
  }, [initialeValue]);

  const handleChange = (vals: number[]) => {
    onChange(vals[0]);
    setValue(vals[0]);
  };

  return (
    <>
      <View style={styles.priceContainer}>
        <Text style={styles.priceText}>{title}</Text>
        <Text style={styles.price}>{value + ' €'}</Text>
      </View>
      <Slider
        minimumValue={0}
        step={1}
        maximumValue={60}
        value={value}
        minimumTrackTintColor="#FFFFFF"
        maximumTrackTintColor="#242627"
        thumbTintColor="#FFF"
        onValueChange={handleChange}
      />
    </>
  );
};

export const SliderWithHeaderMemo = memo(SliderWithHeader);

const createStyle = (theme: Theme) => {
  return StyleSheet.create({
    priceContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginBottom: 16,
    },
    priceText: {
      fontFamily: 'gatwick-bold',
      color: theme.fontColor.primary,
      fontSize: 15,
    },
    price: {
      fontFamily: 'neue-bold',
      color: theme.fontColor.secondary,
      fontSize: 14,
    },
  });
};
