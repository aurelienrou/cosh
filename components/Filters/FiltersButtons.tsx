import React from 'react';
import styled from 'styled-components/native';

interface FiltersButtonsProps {
  onClear: () => void;
  onValidate: () => void;
}
export const FiltersButtons = ({
  onClear,
  onValidate,
}: FiltersButtonsProps) => {
  return (
    <S_ContainerButtons>
      <S_Clear onPress={onClear}>
        <S_Text>TOUT EFFACER</S_Text>
      </S_Clear>
      <S_Validate onPress={onValidate}>
        <S_Text>VALIDER</S_Text>
      </S_Validate>
    </S_ContainerButtons>
  );
};

const S_ContainerButtons = styled.View`
  height: 112px;
  width: 100%;
  position: absolute;
  bottom: 0;
  padding: 24px 20px 0 20px;
  flex-direction: row;
  justify-content: space-between;
  box-shadow: 0px -10px 16px rgba(0, 0, 0, 0.5);
  background-color: ${(props) => props.theme.background.primary};
`;

const S_Text = styled.Text`
  font-family: 'neue-bold';
  font-size: 14px;
  color: #494949;
`;

const S_Clear = styled.Pressable`
  height: 48px;
  width: 40%;
  justify-content: center;
`;

const S_Validate = styled.Pressable`
  height: 48px;
  border-radius: 36px;
  padding: 0 16px;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.fontColor.action};
`;
