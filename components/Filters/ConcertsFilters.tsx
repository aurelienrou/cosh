import React, { memo, useCallback, useLayoutEffect, useState } from 'react';
import { ScrollView, View } from 'react-native';
import { CheckboxList } from './CheckboxList';
import { FiltersButtons } from './FiltersButtons';
import { useAsyncStorage } from '@react-native-async-storage/async-storage';
import { Line } from '../Blocks/Line';
import { SectionHeader } from '../Section/SectionHeader';
import { IParamsFilters } from '../../services/types';
import { SliderWithHeaderMemo } from './SliderWithHeader';
import { CalendarMemo } from './Calendar';

const concertTypes = [
  'afrobeat',
  'afrobeats',
  'afrohouse',
  'afropop',
  'afrotech',
  'afrotronica',
  'alternative',
  'amapiano',
  'ambient',
  'bass',
  'chansonfrançaise',
  'dance',
  'dancehall',
  'darkwave',
  'deephouse',
  'disco',
  'electro',
  'electrodisco',
  'electronic',
  'experimental',
  'folk',
  'french_pop',
  'funk',
  'hiphop',
  'house',
  'hyperpop',
  'indie',
  'indiepop',
  'indierock',
  'jazz',
  'jungle',
  'melodictechno',
  'metal',
  'neosoul',
  'pop',
  'poprock',
  'postpunk',
  'progressivehouse',
  'punk',
  'rap',
  'rnb',
  'rock',
  'singersongwriter',
  'soul',
  'tech-house',
  'techno',
  'trance',
  'trap',
];

const initialeTypes = concertTypes.map((name) => ({
  name,
  isSelected: false,
}));

interface ConcertsFiltersProps {
  onValidate: (params: IParamsFilters | { types: string[] }) => void;
}

const CheckboxListMemo = memo(CheckboxList);

export const ConcertsFilters = ({ onValidate }: ConcertsFiltersProps) => {
  const { getItem: getStorageToken, setItem: setItemStorage } =
    useAsyncStorage('concerts');

  const initialeState = useCallback(
    () =>
      ({
        start: null,
        end: null,
        types: [...initialeTypes],
        maxPrice: 0,
      } as IParamsFilters),
    [],
  );

  const [params, setParams] = useState<IParamsFilters>(initialeState());
  const [initialParams, setInitialParams] = useState<IParamsFilters>(
    initialeState(),
  );

  const handleClear = () => {
    setInitialParams(initialeState());
    setParams(initialeState());
  };

  const handleValidate = async () => {
    const selectedTypes = params.types
      .filter((type) => type.isSelected)
      .map((type) => type.name);

    const filters = JSON.stringify({
      start: params.start,
      end: params.end,
      types: params.types,
      maxPrice: params.maxPrice,
    });

    await setItemStorage(filters);

    onValidate({
      start: params.start,
      end: params.end,
      types: selectedTypes,
      maxPrice: params.maxPrice,
    });
  };

  useLayoutEffect(() => {
    const init = async () => {
      const res = await getStorageToken();
      if (res) {
        const filters = JSON.parse(res);

        setParams({
          start: filters?.start,
          end: filters?.end,
          types: filters?.types || [],
          maxPrice: filters?.maxPrice || 0,
        });

        setInitialParams({
          start: filters?.start,
          end: filters?.end,
          types: filters?.types || [],
          maxPrice: filters?.maxPrice || 0,
        });
      }
    };

    init();
  }, []);

  const handleSelectTypes = useCallback((name: string) => {
    setParams((prms) => ({
      ...prms,
      types: prms.types.map((item) =>
        item.name === name ? { ...item, isSelected: !item.isSelected } : item,
      ),
    }));
  }, []);

  const handleSelectPrice = useCallback((maxPrice: number) => {
    setParams((prms) => ({
      ...prms,
      maxPrice,
    }));
  }, []);

  const handleSelectDate = useCallback(
    (date: Date, type: 'START_DATE' | 'END_DATE') => {
      const currentType = type === 'END_DATE' ? 'end' : 'start';
      setParams((prms) => ({
        ...prms,
        [currentType]: date,
      }));
    },
    [],
  );

  return (
    <>
      <ScrollView
        contentContainerStyle={{ paddingBottom: 200, paddingHorizontal: 20 }}
        showsVerticalScrollIndicator={false}
      >
        <SliderWithHeaderMemo
          onChange={handleSelectPrice}
          initialeValue={initialParams.maxPrice}
          title="Prix max:"
        />
        <Line />
        <CalendarMemo
          onChange={handleSelectDate}
          initialeEnd={initialParams.end}
          initialeStart={initialParams.start}
        />
        <Line />
        <View style={{ marginBottom: 16, marginTop: 24 }}>
          <SectionHeader title="Types de musique" notHorizontalPadding />
        </View>

        <CheckboxListMemo
          initialeValue={initialParams.types}
          onChange={handleSelectTypes}
        />
      </ScrollView>
      <FiltersButtons onClear={handleClear} onValidate={handleValidate} />
    </>
  );
};
