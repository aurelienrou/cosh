import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import { SelectorLine } from '../Checkbox/SelectorLine';
import styled from 'styled-components/native';
import { View } from 'react-native';
import Animated, {
  useAnimatedRef,
  useSharedValue,
  useDerivedValue,
  withSpring,
  withTiming,
  useAnimatedStyle,
  interpolate,
  runOnUI,
  measure,
} from 'react-native-reanimated';
import { ITypes } from '../../services/types';

interface CheckboxListProps {
  initialeValue: ITypes[];
  onChange: (name: string) => void;
  numberToDisplay?: number;
}

const SelectorLineMemo = memo(SelectorLine);

export const CheckboxList = ({
  initialeValue,
  onChange,
  numberToDisplay = 10,
}: CheckboxListProps) => {
  const aref = useAnimatedRef<View>();
  const open = useSharedValue(false);
  const [isOpen, setIsOpen] = useState(false);
  const progress = useDerivedValue(() =>
    open.value ? withSpring(1, { damping: 15 }) : withTiming(0),
  );
  const height = useSharedValue(0);

  const animatedStyle = useAnimatedStyle(() => ({
    height: height.value * progress.value + 1,
    opacity: interpolate(progress.value, [0.3, 1], [0, 1]),
  }));

  const [values, setValues] = useState<ITypes[]>([]);

  useEffect(() => {
    setValues([...initialeValue]);
  }, [initialeValue]);

  const handleSelect = useCallback((name: string) => {
    onChange(name);

    setValues((prms) =>
      prms.map((item) =>
        item.name === name ? { ...item, isSelected: !item.isSelected } : item,
      ),
    );
  }, []);

  return (
    <>
      {values.slice(0, numberToDisplay).map(({ name, isSelected }) => {
        return (
          <SelectorLineMemo
            key={name}
            name={name}
            isSelected={isSelected}
            onSelect={handleSelect}
          />
        );
      })}
      <S_ListContainer style={[animatedStyle]}>
        <View ref={aref}>
          {values
            .slice(numberToDisplay, values.length)
            .map(({ name, isSelected }) => {
              return (
                <SelectorLineMemo
                  key={name}
                  name={name}
                  isSelected={isSelected}
                  onSelect={handleSelect}
                />
              );
            })}
        </View>
      </S_ListContainer>
      <S_Pressable
        onPress={() => {
          if (height.value === 0) {
            runOnUI(() => {
              'worklet';
              height.value = measure(aref).height;
            })();
          }
          setIsOpen(!isOpen);

          open.value = !open.value;
        }}
      >
        <S_Text>{isOpen ? 'SHOW LESS' : 'SHOW MORE'}</S_Text>
      </S_Pressable>
    </>
  );
};

const S_Pressable = styled.Pressable`
  width: 100%;
  height: 32px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const S_Text = styled.Text`
  font-family: 'neue-bold';
  font-size: 14px;
  color: #494949;
`;

const S_ListContainer = styled(Animated.View)`
  overflow: hidden;
`;
