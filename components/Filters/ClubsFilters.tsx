import React, { memo, useCallback, useLayoutEffect, useState } from 'react';
import { ScrollView, View } from 'react-native';
import { CheckboxList } from './CheckboxList';
import { FiltersButtons } from './FiltersButtons';
import { useAsyncStorage } from '@react-native-async-storage/async-storage';
import { Line } from '../Blocks/Line';
import { SectionHeader } from '../Section/SectionHeader';
import { SliderWithHeaderMemo } from './SliderWithHeader';
import { CalendarMemo } from './Calendar';
import { IParamsFilters } from '../../services/types';

const clubsTypes = [
  'acid house',
  'acid techno',
  'acidcore',
  'afro house',
  'afrobeat',
  'alternative dance',
  'ambient',
  'ambient idm',
  'axé',
  'bachata',
  'baile funk',
  'balearic',
  'bass',
  'blues',
  'brazilian',
  'breakbeat',
  'breakcore',
  'chicago house',
  'club',
  'country',
  'dance',
  'dancehall',
  'dark psytrance',
  'dark wave',
  'deep house',
  'deep tech',
  'deep techno',
  'detroit techno',
  'disco',
  'disco house',
  'downtempo',
  'drill',
  'drum & bass',
  'dub',
  'dub techno',
  'dubstep',
  'ebm',
  'edm',
  'electro',
  'electro house',
  'electronica',
  'eurodance',
  'experimental',
  'frenchcore',
  'funk',
  'gabber',
  'garage',
  'german techno',
  'ghettotech',
  'goth',
  'grime',
  'hard groove',
  'hard techno',
  'hard trance',
  'hardcore',
  'hardstyle',
  'hardtek',
  'hip hop',
  'house',
  'indie dance',
  'indie rock',
  'indus',
  'industrial',
  'industrial metal',
  'industrial techno',
  'italo disco',
  'jazz',
  'jersey club',
  'jungle',
  'k-pop',
  'melodic house & techno',
  'merengue',
  'metal',
  'micro house',
  'minimal house',
  'minimal synth',
  'minimal techno',
  'mpb',
  'neorave',
  'new rave',
  'new wave',
  'pop',
  'pop rock',
  'post-punk',
  'progressive house',
  'progressive psytrance',
  'progressive trance',
  'psychedelic rock',
  'psytrance',
  'punk',
  'r&b',
  'rap',
  'reggae',
  'reggaeton',
  'rock',
  'salsa',
  'samba',
  'soul',
  'synthpop',
  'synthwave',
  'tech house',
  'techno',
  'trance',
  'trap',
  'tribal house',
  'uk garage',
  'vaporwave',
  'world music',
];

const initialeTypes = clubsTypes.map((name) => ({
  name,
  isSelected: false,
}));

const CheckboxListMemo = memo(CheckboxList);

interface ClubsFiltersProps {
  onValidate: (params: IParamsFilters | { types: string[] }) => void;
}

export const ClubsFilters = ({ onValidate }: ClubsFiltersProps) => {
  const { getItem: getStorageToken, setItem: setItemStorage } =
    useAsyncStorage('clubs');

  const initialeState = useCallback(
    () =>
      ({
        start: null,
        end: null,
        types: [...initialeTypes],
        maxPrice: 0,
      } as IParamsFilters),
    [],
  );

  const [params, setParams] = useState<IParamsFilters>(initialeState());
  const [initialParams, setInitialParams] = useState<IParamsFilters>(
    initialeState(),
  );

  const handleClear = () => {
    setInitialParams(initialeState());
    setParams(initialeState());
  };

  const handleValidate = async () => {
    const selectedTypes = params.types
      .filter((type) => type.isSelected)
      .map((type) => type.name);

    const filters = JSON.stringify({
      start: params.start,
      end: params.end,
      types: params.types,
      maxPrice: params.maxPrice,
    });

    await setItemStorage(filters);

    onValidate({
      start: params.start,
      end: params.end,
      types: selectedTypes,
      maxPrice: params.maxPrice,
    });
  };

  useLayoutEffect(() => {
    const init = async () => {
      const res = await getStorageToken();
      if (res) {
        const filters = JSON.parse(res);

        setParams({
          start: filters?.start,
          end: filters?.end,
          types: filters?.types || [],
          maxPrice: filters?.maxPrice || 0,
        });

        setInitialParams({
          start: filters?.start,
          end: filters?.end,
          types: filters?.types || [],
          maxPrice: filters?.maxPrice || 0,
        });
      }
    };

    init();
  }, []);

  const handleSelectTypes = useCallback((name: string) => {
    setParams((prms) => ({
      ...prms,
      types: prms.types.map((item) =>
        item.name === name ? { ...item, isSelected: !item.isSelected } : item,
      ),
    }));
  }, []);

  const handleSelectPrice = useCallback((maxPrice: number) => {
    setParams((prms) => ({
      ...prms,
      maxPrice,
    }));
  }, []);

  const handleSelectDate = useCallback(
    (date: Date, type: 'START_DATE' | 'END_DATE') => {
      const currentType = type === 'END_DATE' ? 'end' : 'start';
      setParams((prms) => ({
        ...prms,
        [currentType]: date,
      }));
    },
    [],
  );

  return (
    <>
      <ScrollView
        contentContainerStyle={{ paddingBottom: 200, paddingHorizontal: 20 }}
        showsVerticalScrollIndicator={false}
      >
        <SliderWithHeaderMemo
          onChange={handleSelectPrice}
          initialeValue={initialParams.maxPrice}
          title="Prix max:"
        />
        <Line />
        <CalendarMemo
          onChange={handleSelectDate}
          initialeEnd={initialParams.end}
          initialeStart={initialParams.start}
        />
        <Line />
        <View style={{ marginBottom: 16, marginTop: 24 }}>
          <SectionHeader title="Types de musique" notHorizontalPadding />
        </View>

        <CheckboxListMemo
          initialeValue={initialParams.types}
          onChange={handleSelectTypes}
        />
      </ScrollView>
      <FiltersButtons onClear={handleClear} onValidate={handleValidate} />
    </>
  );
};
