import React, { useCallback } from 'react';
import { SelectorSquare } from '../Checkbox/SelectorSquare';
import { FlashList, ListRenderItem } from '@shopify/flash-list';
import { ITypes } from '../../services/types';

interface EventsTypeProps {
  events: ITypes[];
  onChange: (name: string) => void;
}
export const EventsType = ({ events, onChange }: EventsTypeProps) => {
  const renderItem: ListRenderItem<ITypes> = useCallback(
    ({ item: { name, isSelected } }) => {
      return (
        <SelectorSquare
          name={name}
          isSelected={isSelected}
          onSelect={() => onChange(name)}
          style={{ marginRight: 8 }}
        />
      );
    },
    [],
  );

  return (
    <FlashList<ITypes>
      showsHorizontalScrollIndicator={false}
      horizontal
      data={events}
      renderItem={renderItem}
      estimatedItemSize={100}
      keyExtractor={(item) => item.name}
    />
  );
};
