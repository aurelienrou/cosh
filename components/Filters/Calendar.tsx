import React, { memo, useEffect, useState } from 'react';
import CalendarPicker from 'react-native-calendar-picker';
import { useTheme } from '../../provider/ThemeProvider';

const Calendar = ({ onChange, initialeEnd, initialeStart }: any) => {
  const { theme } = useTheme();
  const minDate = new Date();
  const [start, setStart] = useState<Date>();
  const [end, setEnd] = useState<Date>();

  useEffect(() => {
    setEnd(initialeEnd);
    setStart(initialeStart);
  }, [initialeStart, initialeEnd]);

  const handleDateChange = (date: Date, type: 'START_DATE' | 'END_DATE') => {
    if (type === 'END_DATE') {
      setEnd(date);
    } else {
      setStart(date);
    }
    onChange(date, type);
  };

  return (
    <CalendarPicker
      onDateChange={handleDateChange}
      minDate={minDate}
      allowRangeSelection
      startFromMonday
      textStyle={{ color: '#FFF' }}
      selectedDayColor={theme.fontColor.action}
      selectedEndDate={end}
      selectedStartDate={start}
    />
  );
};

export const CalendarMemo = memo(Calendar);
