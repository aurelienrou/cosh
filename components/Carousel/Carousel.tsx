import React from 'react';
import Animated, {
  Extrapolate,
  SharedValue,
  interpolate,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import { width } from '../../constants/Layout';
import { Platform, View } from 'react-native';
import styled from 'styled-components/native';
import { Image } from 'expo-image';
import { IEvent, IEventMinimize } from '../../services/types';
import { formateDate } from '../../helpers/date';

const ITEM_SIZE = width * 0.53;

interface CarouselItemProps {
  scrollX: SharedValue<number>;
  index: number;
  event: IEvent;
}

interface CarouselProps {
  events: IEventMinimize[];
  loading: boolean
}

const CarouselItem = ({ scrollX, index, event }: CarouselItemProps) => {
  const inputRange = [
    (index - 1) * ITEM_SIZE,
    index * ITEM_SIZE,
    (index + 1) * ITEM_SIZE,
  ];

  const style = useAnimatedStyle(() => {
    return {
      transform: [
        {
          scale: interpolate(
            scrollX.value,
            inputRange,
            [1 - 0.33, 1, 1],
            Extrapolate.CLAMP,
          ),
        },
        {
          translateX: interpolate(
            scrollX.value,
            inputRange,
            [-ITEM_SIZE / 4 + 8, 0, -10],
            Extrapolate.CLAMP,
          ),
        },
        {
          translateY: interpolate(
            scrollX.value,
            inputRange,
            [ITEM_SIZE / 4, 0, 0],
            Extrapolate.CLAMP,
          ),
        },
      ],
    };
  });

  const styleText = useAnimatedStyle(() => {
    return {
      transform: [
        {
          scale: interpolate(
            scrollX.value,
            inputRange,
            [1 - 0.33, 1, 1],
            Extrapolate.CLAMP,
          ),
        },
       
      ],
    };
  });

  return (
    <View style={{ flex: 1, alignItems: 'baseline'}}>
      <Animated.View
        style={[
          {
            flex: 1,
            width: ITEM_SIZE,
            height: ITEM_SIZE,
            alignItems: 'center',
          },
          style,
        ]}
      >
        <Image source={event.cover} style={{ width: ITEM_SIZE,
            height: ITEM_SIZE, borderRadius: 8 }} />
      
      </Animated.View>
      <Animated.View style={[styleText]} >
        <S_TextContainer>
          <S_Title numberOfLines={1}>{event.name}</S_Title>
          <S_Date>{formateDate(event.beginAt)}</S_Date>
          <S_Place>{event.formattedAddress}</S_Place>
        </S_TextContainer>
      </Animated.View>
    </View>
  );
};

export const Carousel = ({events, loading}: CarouselProps) => {
    const scrollX = useSharedValue(0);

  const handleScroll = useAnimatedScrollHandler((event) => {
    scrollX.value = event.contentOffset.x;
  });

  if (loading) {
    return <></>
  }

  return (
    <S_Flatlist
      showsHorizontalScrollIndicator={false}
      data={events}
      keyExtractor={(_: any, index: number) => `${index}`}
      horizontal
      bounces={false}
      decelerationRate={Platform.OS === 'ios' ? 0 : 0.98}
      renderToHardwareTextureAndroid
      snapToInterval={ITEM_SIZE}
      contentContainerStyle={{ alignItems: 'flex-end' }}
      snapToAlignment="start"
      onScroll={handleScroll}
      scrollEventThrottle={16}
      renderItem={({ item, index }: any) => {
        return <CarouselItem scrollX={scrollX} index={index} event={item} />;
      }}
    />
  );
};

const S_Flatlist = styled(Animated.FlatList<IEventMinimize>)`
  flex: 1;
  margin-top: 24px; 
  margin-bottom: 162px;
`;

const S_Title = styled.Text`
  font-family: 'gatwick-bold';
  color: ${(props) => props.theme.fontColor.primary};
  font-size: 14px;
`;

const S_TextContainer = styled.View`
  margin-top: 16px;
  width: ${ITEM_SIZE + 'px'};
`;

const S_Date = styled.Text`
  margin-top: 14px;
  font-size: 14px;
  font-family: 'neue';
  margin-top: 8px;
  color: ${(props) => props.theme.fontColor.action};
`;

const S_Place = styled.Text`
  margin-top: 14px;
  font-family: 'neue';
  font-size: 14px;
  margin-top: 8px;
  color: ${(props) => props.theme.fontColor.secondary};
`;
