import React, { View, Text, StyleSheet } from 'react-native';
import { Accordeon } from '../Blocks/Accordeon';
import { useMainTheme } from '../../hooks/useMainTheme';
import { height, width } from '../../constants/Layout';
import { Theme } from '../../constants/theme';
import { IHours } from '../../services/types';
import dayjs from 'dayjs';
import { useTheme } from '../../provider/ThemeProvider';
import { capitalizeFirstLetter } from '../../helpers/string';
import { daysTranslate } from '../../helpers/date';
import weekday from 'dayjs/plugin/weekday';
dayjs.extend(weekday);
interface HoursProps {
  title: string;
  hours: IHours;
  color?: string;
}

export const Hours = ({ title, hours, color }: HoursProps) => {
  const styles = useMainTheme(createStyles);
  const currentDay = dayjs().weekday();
  const { theme } = useTheme();

  const hasNotHours = Object.values(hours || {}).every((h) => h === null);

  if (hasNotHours) {
    return <></>;
  }

  return (
    <Accordeon
      style={{ marginTop: 33 }}
      textStyle={{ color }}
      title={title}
      subtitle={Object.values(hours)[currentDay] ?? 'Fermé'}
    >
      {Object.entries(hours).map(([day, hour], index) => {
        return (
          <View style={styles.S_AccordeonItem} key={day}>
            <Text
              style={[
                styles.S_Date,
                {
                  color:
                    currentDay === index ? color : theme.fontColor.secondary,
                },
              ]}
            >
              {capitalizeFirstLetter(daysTranslate[day as never])}
            </Text>
            <Text
              style={[
                styles.S_Date,
                {
                  color:
                    currentDay === index ? color : theme.fontColor.secondary,
                },
              ]}
            >
              {hour ?? 'Fermé'}
            </Text>
          </View>
        );
      })}
    </Accordeon>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_Main: {
      flex: 1,
      backgroundColor: theme.background.primary,
      zIndex: 12,
    },
    S_Container: {
      position: 'absolute',
      top: 0,
      left: 0,
      flex: 1,
      height,
    },
    S_Cover: {
      width,
      height: 300,
    },
    S_AccordeonItem: {
      height: 30,
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    S_InnerView: {
      flex: 1,
      paddingHorizontal: 20,
      paddingBottom: 120,
      backgroundColor: theme.background.primary,
    },
    S_Linear: {
      width,
      height: 50,
      marginTop: 250,
    },
    S_Date: {
      fontSize: 14,
      fontFamily: 'neue',
      color: theme.fontColor.secondary,
    },
    S_SectionEvent: {
      paddingTop: 24,
    },
    S_ReadMore: {
      marginTop: 12,
      fontSize: 14,
      fontFamily: 'neue-bold',
      color: theme.fontColor.primary,
    },
    S_Description: {
      marginTop: 24,
      fontSize: 14,
      lineHeight: 20,
      fontFamily: 'neue',
      color: theme.fontColor.primary,
    },
  });
