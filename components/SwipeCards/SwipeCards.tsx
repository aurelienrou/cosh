import React from 'react';
import { Gesture, GestureDetector } from 'react-native-gesture-handler';
import Animated, {
  withSpring,
  useAnimatedStyle,
  useSharedValue,
  interpolate,
  Extrapolation,
  useDerivedValue,
  SharedValue,
  interpolateColor,
  useAnimatedProps,
  FadeIn,
  SlideInLeft,
  SlideInRight,
  FadeInUp,
  withTiming,
  withDelay,
} from 'react-native-reanimated';
import styled from 'styled-components/native';
import { width } from '../../constants/Layout';
import { snapPoint } from 'react-native-redash';
import { Image } from 'expo-image';
import FollowButton from '../Button/FollowButton';
import { IEventMinimize } from '../../services/types';
import { BlurView } from 'expo-blur';
import { useNavigation } from '@react-navigation/native';
import { Pressable } from 'react-native';
import { formateDate } from '../../helpers/date';

const SNAP_POINTS = [-width - 200, 0, width + 200];
const CARD_WIDTH = width - 40;
const CARD_HEIGHT = CARD_WIDTH;

interface CardProps {
  index: number;
  step: number;
  card: IEventMinimize;
  lenght: number;
  aIndex: SharedValue<number>;
  aTranslateX: SharedValue<number>;
}

const AnimatedBlurView = Animated.createAnimatedComponent(BlurView);

const Card = ({
  index,
  card,
  lenght,
  aIndex,
  aTranslateX,
  step,
}: CardProps) => {
  const navigation = useNavigation<any>();
  const offset = useSharedValue({ x: 0 });
  const translateX = useSharedValue(0);
  const isDragging = useSharedValue(false);
  const zIndex = useSharedValue(index);
  const position = useDerivedValue(() => {
    return (
      Math.abs(zIndex.value * step - aIndex.value) +
      (Math.abs(aTranslateX.value / 6) * step) / 100
    );
  });

  const scale = useDerivedValue(() => {
    return interpolate(
      position.value,
      [step, 1],
      [step * 2, 1],
      Extrapolation.CLAMP,
    );
  }, [position]);

  const translateY = useDerivedValue(() => {
    return interpolate(
      position.value,
      [step, 1],
      [-190, 0],
      Extrapolation.CLAMP,
    );
  }, [position]);

  const opacity = useDerivedValue(() => {
    return interpolate(
      position.value,
      [step * 4, step * 5],
      [0, 1],
      Extrapolation.CLAMP,
    );
  }, [position]);

  const panGesture = Gesture.Pan()
    .activeOffsetX([-10, 10])
    .onBegin(() => {
      isDragging.value = true;
      offset.value = { x: translateX.value };
    })
    .onUpdate(({ translationX }) => {
      translateX.value = offset.value.x + translationX;
      aTranslateX.value = offset.value.x + translationX;
    })
    .onEnd(({ velocityX }) => {
      const dest = snapPoint(translateX.value, velocityX, SNAP_POINTS);
      translateX.value = withSpring(
        dest,
        {
          velocity: velocityX,
          overshootClamping: dest === 0 ? false : true,
          restSpeedThreshold: dest === 0 ? 0.01 : 100,
          restDisplacementThreshold: dest === 0 ? 0.01 : 100,
        },
        () => {
          isDragging.value = false;
          if (dest !== 0) {
            aIndex.value = aIndex.value - step;
            translateX.value = 0;
            zIndex.value = zIndex.value - lenght;
          }
        },
      );
      aTranslateX.value = withSpring(
        dest,
        {
          velocity: velocityX,
          overshootClamping: dest === 0 ? false : true,
          restSpeedThreshold: dest === 0 ? 0.01 : 100,
          restDisplacementThreshold: dest === 0 ? 0.01 : 100,
        },
        () => {
          if (dest !== 0) {
            aTranslateX.value = 0;
          }
        },
      );
    });

  const style = useAnimatedStyle(() => ({
    transform: [
      { translateX: translateX.value },
      {
        translateY: translateY.value,
      },
      { rotateZ: `${translateX.value / 40}deg` },
      { scale: scale.value },
    ],
    zIndex: zIndex.value,
    opacity: opacity.value,
  }));

  const animatedProps = useAnimatedProps(() => {
    const intensity = interpolate(
      position.value,
      [step * 5, 1],
      [40, 0],
      Extrapolation.CLAMP,
    );

    return {
      intensity,
    };
  });

  return (
    <GestureDetector gesture={panGesture}>
      <S_Card style={[style]}>
        <S_CardInner
          style={{ alignItems: 'center' }}
          entering={(targetValues: any) => {
            'worklet';
            const animations = {
              originY: withDelay(
                (lenght - index) * 300,
                withTiming(0, { duration: 1000 }),
              ),
              opacity: withDelay(
                (lenght - index) * 300,
                withTiming(1, { duration: 1000 }),
              ),
              transform: [
                {
                  scale: withDelay(
                    (lenght - index) * 300,
                    withTiming(1, { duration: 1000 }),
                  ),
                },
              ],
            };
            const initialValues = {
              originY: -60,
              opacity: 0,
              transform: [{ scale: 0.7 }],
            };
            return {
              initialValues,
              animations,
            };
          }}
        >
          <Pressable
            onPress={() => navigation.navigate('Event', { id: card.id })}
            style={{ flex: 1, position: 'relative' }}
          >
            <S_FollowButton>
              <FollowButton hasBackground id={card.id} type="Event" />
            </S_FollowButton>
            <S_Image source={card.cover} />
            <S_BlackLayer animatedProps={animatedProps} tint="dark" />
            <S_BlurView intensity={70} tint="dark">
              <S_Title numberOfLines={1}>{card.name}</S_Title>
              <S_Date>{formateDate(card.beginAt)}</S_Date>
              <S_Place numberOfLines={1}>{card.address}</S_Place>
            </S_BlurView>
          </Pressable>
        </S_CardInner>
      </S_Card>
    </GestureDetector>
  );
};

interface SwipeCardsProps {
  events: IEventMinimize[];
  loading: boolean;
}

export const SwipeCards = ({ events }: SwipeCardsProps) => {
  const aIndex = useSharedValue(0);
  const aTranslateX = useSharedValue(1);

  const step = 1 / events.length;

  return (
    <S_Container>
      {events
        ?.slice(0, events.length)
        .map((card: IEventMinimize, index: number) => (
          <Card
            key={index}
            card={card}
            index={index + 1}
            lenght={events.length}
            step={step}
            aIndex={aIndex}
            aTranslateX={aTranslateX}
          />
        ))}
    </S_Container>
  );
};

const S_Container = styled.View`
  flex: 1;
  align-items: center;
  height: ${CARD_HEIGHT + 'px'};
  margin-top: 36px;
  background-color: ${(props) => props.theme.background.primary};
`;

const S_Card = styled(Animated.View)`
  position: absolute;
  width: ${CARD_WIDTH + 'px'};
  height: ${CARD_HEIGHT + 'px'};
  background-color: ${(props) => props.theme.background.primary};
`;

const S_CardInner = styled(Animated.View)`
  width: ${CARD_WIDTH + 'px'};
  height: ${CARD_HEIGHT + 'px'};
  border-radius: 16px;
  overflow: hidden;
  background-color: ${(props) => props.theme.background.primary};
`;

const S_BlackLayer = styled(AnimatedBlurView)`
  position: absolute;
  width: ${CARD_WIDTH + 'px'};
  height: ${CARD_HEIGHT + 'px'};
  border-radius: 16px;
  overflow: hidden;
`;

const S_FollowButton = styled(Animated.View)`
  position: absolute;
  z-index: 1;
  top: 10px;
  right: 10px;
  width: 48px;
  height: 48px;
  border-radius: 24px;
  justify-content: center;
  align-items: center;
  background: rgba(0, 0, 0, 0.2);
`;

const S_BlurView = styled(BlurView)`
  position: absolute;
  width: ${CARD_WIDTH + 'px'};
  height: 90px;
  padding: 16px;
  bottom: 0px;
  justify-content: center;
  border-bottom-left-radius: 16px;
  border-bottom-right-radius: 16px;
  overflow: hidden;
`;

const S_Image = styled(Image)`
  width: ${CARD_WIDTH + 'px'};
  height: ${CARD_WIDTH + 'px'};
`;

const S_Title = styled.Text`
  font-family: 'gatwick-bold';
  color: ${(props) => props.theme.fontColor.primary};
`;

const S_Date = styled.Text`
  margin-top: 14px;
  font-family: 'neue';
  margin-top: 8px;
  color: ${(props) => props.theme.fontColor.action};
`;

const S_Place = styled.Text`
  margin-top: 14px;
  font-family: 'neue';
  margin-top: 8px;
  color: 'rgba(255, 255, 255, 0.5)';
`;
