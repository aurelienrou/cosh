import { PlaceDto } from '../../services/placesServices';
import { PRICE_RANGE } from '../../services/types';

export const defaultValues: PlaceDto = {
  coverUri: '',
  name: '',
  placeId: '',
  administrativeAreaLevel1: '',
  administrativeAreaLevel2: '',
  country: '',
  formattedAddress: '',
  locality: '',
  postalCode: '',
  route: '',
  streetNumber: '',
  openHours: new Array(7).fill([]),
  desc: '',
  location: {
    type: ['Point'],
    coordinates: [],
  },
  phone: undefined,
  website: undefined,
  facebook: undefined,
  instagram: undefined,
  email: '',
  categories: [],
  priceRange: PRICE_RANGE.FREE,
};

export const rules = {
  name: {
    required: {
      value: true,
      message: 'Un nom est requis',
    },
    maxLength: {
      value: 255,
      message:
        'La longueur du nom doit être inférieure ou égale à 255 caractères.',
    },
    minLength: {
      value: 1,
      message: 'La longueur du nom doit être superieur ou égale à 1 caractère.',
    },
  },
  coverUri: {
    required: {
      value: true,
      message: 'Une cover est requise',
    },
  },
  desc: {
    required: {
      value: true,
      message: 'Une description est requise',
    },
    maxLength: {
      value: 5000,
      message:
        'La longueur de la description doit être inférieure ou égale à 5000 caractères.',
    },
    minLength: {
      value: 1,
      message:
        'La longueur de la description doit être superieur ou égale à 1 caractères.',
    },
  },
  placeId: {
    required: {
      value: true,
      message: 'Une adresse est requise',
    },
  },
  openHours: {
    required: {
      value: true,
      message: "Veuillez renseigner des horaires d'ouvertures",
    },
  },
  priceRange: {
    required: {
      value: true,
      message: 'Veuillez indiquer une fourchette de prix',
    },
  },
};
