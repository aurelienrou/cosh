import React, { FC } from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import {
  INPUT_BACKGROUND_COLOR,
  PLACEHOLDER_COLOR,
  PRIMARY_COLOR,
} from '../../constants/Colors';
import { height, width } from '../../constants/Layout';
import InpuAddressFinder from '../InputAddressFinder/InputAddressFinder';
import Button from '../Button/Button';
import OpenHoursSeletor from '../HoursSeletor/HoursSeletor';
import PriceRange from '../PriceRange/PriceRange';
import UploadCover from '../UploadCover/UploadCover';
import { useForm, Controller } from 'react-hook-form';
import { useMutation } from '@tanstack/react-query';
import { createPlace, PlaceDto } from '../../services/placesServices';
import Toast from 'react-native-toast-message';
import { defaultValues, rules } from './validation';
import { ErrorType } from '../../services/types';
import { AxiosError } from 'axios';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useNavigation } from '@react-navigation/native';
import CoshText from '../CoshText/CoshText';
import { toCamelCase } from '../../helpers/string';

const CreatePlaceForm: FC = () => {
  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm({ defaultValues });
  const navigation = useNavigation();

  const { mutate, isLoading } = useMutation(createPlace, {
    onSuccess: () => {
      Toast.show({
        type: 'success',
        props: { text: `Le lieu a bien été créer` },
      });
      navigation.navigate('CreateEventForm' as never);
    },
    onError: (err: AxiosError) => {
      Toast.show({
        type: 'error',
        props: { text: (err.response?.data as ErrorType)?.message },
      });
    },
  });

  const handleSubmitForm = (data: any) => {
    mutate(data);
  };

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView>
        <View style={styles.form}>
          {/** Cover */}
          <Controller
            control={control}
            rules={rules.coverUri}
            render={({ field: { onChange, value } }) => (
              <UploadCover onChange={onChange} value={value} />
            )}
            name="coverUri"
          />
          {errors.coverUri && (
            <CoshText style={styles.errors}>{errors.coverUri.message}</CoshText>
          )}

          {/** Name */}
          <View style={styles.row}>
            <Controller
              control={control}
              rules={rules.name}
              render={({ field: { onChange, value } }) => (
                <TextInput
                  style={[styles.input]}
                  onChangeText={onChange}
                  value={value}
                  placeholder="Nom"
                  placeholderTextColor={PLACEHOLDER_COLOR}
                  clearButtonMode="while-editing"
                />
              )}
              name="name"
            />
            {errors.name && (
              <CoshText style={styles.errors}>{errors.name.message}</CoshText>
            )}
          </View>

          {/** Description */}
          <View style={[styles.row, { marginTop: 10 }]}>
            <Controller
              control={control}
              rules={rules.desc}
              render={({ field: { onChange, value } }) => (
                <TextInput
                  style={styles.multiline}
                  onChangeText={onChange}
                  value={value}
                  placeholder="Description"
                  placeholderTextColor={PLACEHOLDER_COLOR}
                  clearButtonMode="while-editing"
                  multiline
                />
              )}
              name="desc"
            />
            {errors.desc && (
              <CoshText style={styles.errors}>{errors.desc.message}</CoshText>
            )}
          </View>

          {/** Adresse */}
          <View
            style={[
              styles.row,
              { flexDirection: 'column', alignItems: 'flex-start' },
            ]}
          >
            <Controller
              control={control}
              rules={rules.placeId}
              render={() => (
                <InpuAddressFinder
                  style={[styles.address]}
                  onSelect={(address) => {
                    Object.entries(address).forEach(([name, val]) =>
                      setValue(toCamelCase(name) as keyof PlaceDto, val),
                    );
                    setValue('location', {
                      type: ['Point'],
                      coordinates: [address.geocode.lat, address.geocode.lng],
                    });
                  }}
                  placeholder="Adresse"
                  resultsStyle={styles.resultsStyle}
                />
              )}
              name="placeId"
            />
            {errors.placeId && (
              <CoshText style={styles.errors}>
                {errors.placeId.message}
              </CoshText>
            )}
          </View>

          {/** Open Hours */}
          <View
            style={[
              styles.row,
              { flexDirection: 'column', alignItems: 'flex-start' },
            ]}
          >
            <CoshText style={styles.label}>Heures d&apos;ouvertures</CoshText>
            <Controller
              control={control}
              rules={rules.openHours}
              render={({ field: { onChange, value } }) => (
                <OpenHoursSeletor onChange={onChange} value={value} />
              )}
              name="openHours"
            />
            {errors.openHours && (
              <CoshText style={styles.errors}>
                {errors.openHours.message}
              </CoshText>
            )}
          </View>

          {/** Price Range */}
          <View
            style={[
              styles.row,
              { flexDirection: 'column', alignItems: 'flex-start' },
            ]}
          >
            <CoshText style={styles.label}>Fourchette de prix</CoshText>
            <Controller
              control={control}
              rules={rules.priceRange}
              render={({ field: { onChange } }) => (
                <PriceRange onChange={onChange} />
              )}
              name="priceRange"
            />
            {errors.priceRange && (
              <CoshText style={styles.errors}>
                {errors.priceRange.message}
              </CoshText>
            )}
          </View>

          {/** Email */}
          <View style={[styles.row]}>
            <Controller
              control={control}
              rules={{
                required: true,
                pattern: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
              }}
              render={({ field: { onChange, value } }) => (
                <TextInput
                  style={[styles.input]}
                  onChangeText={onChange}
                  value={value}
                  placeholder="Email"
                  placeholderTextColor={PLACEHOLDER_COLOR}
                  clearButtonMode="while-editing"
                  textContentType="emailAddress"
                  keyboardType="email-address"
                  autoCapitalize="none"
                  autoCorrect={false}
                />
              )}
              name="email"
            />
            {errors.email && (
              <CoshText style={styles.errors}>Un email est requis</CoshText>
            )}
          </View>

          {/** Phone */}
          <View style={(styles.row, { marginTop: 10 })}>
            <Controller
              control={control}
              rules={{
                pattern: /(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}/,
              }}
              render={({ field: { onChange, value } }) => (
                <TextInput
                  style={[styles.input, errors.phone && styles.errors]}
                  onChangeText={onChange}
                  value={value}
                  placeholder="Telephone"
                  placeholderTextColor={PLACEHOLDER_COLOR}
                  clearButtonMode="while-editing"
                />
              )}
              name="phone"
            />
            {errors.phone && (
              <CoshText style={styles.errors}>
                Doit etre au format +33 ou 0x
              </CoshText>
            )}
          </View>

          {/** Website */}
          <View style={[styles.row, { marginTop: 10 }]}>
            <Controller
              control={control}
              rules={{
                pattern:
                  /^[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_+.~#?&//=]*)$/,
              }}
              render={({ field: { onChange, value } }) => (
                <TextInput
                  style={[styles.input]}
                  onChangeText={onChange}
                  value={value}
                  placeholder="Website"
                  placeholderTextColor={PLACEHOLDER_COLOR}
                  clearButtonMode="while-editing"
                />
              )}
              name="website"
            />
            {errors.website && (
              <CoshText style={styles.errors}>
                Doit etre au format wwww.exemple.com
              </CoshText>
            )}
          </View>

          {/** Facebook */}
          <View style={[styles.row, { marginTop: 10 }]}>
            <Controller
              control={control}
              rules={{
                pattern:
                  /^[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_+.~#?&//=]*)$/,
              }}
              render={({ field: { onChange, value } }) => (
                <TextInput
                  style={[styles.input, styles.lastInput]}
                  onChangeText={onChange}
                  value={value}
                  placeholder="Facebook"
                  placeholderTextColor={PLACEHOLDER_COLOR}
                  clearButtonMode="while-editing"
                />
              )}
              name="facebook"
            />
            {errors.facebook && (
              <CoshText style={styles.errors}>
                Doit etre au format wwww.exemple.com
              </CoshText>
            )}
          </View>
        </View>
      </KeyboardAwareScrollView>
      <Button
        loading={isLoading}
        style={styles.addEventButton}
        onPress={handleSubmit(handleSubmitForm)}
        text="Ajouter"
      />
    </View>
  );
};

export default CreatePlaceForm;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    flex: 1,
  },
  form: {
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  input: {
    height: 50,
    width: width - 40,
    borderRadius: 10,
    overflow: 'hidden',
    paddingHorizontal: 20,
    backgroundColor: INPUT_BACKGROUND_COLOR,
  },
  multiline: {
    paddingTop: 15,
    paddingBottom: 15,
    minHeight: 100,
    maxHeight: height / 3,
    width: width - 40,
    borderRadius: 10,
    overflow: 'hidden',
    paddingHorizontal: 20,
    backgroundColor: INPUT_BACKGROUND_COLOR,
  },
  label: {
    marginBottom: 10,
  },
  address: {
    width: width - 40,
    backgroundColor: INPUT_BACKGROUND_COLOR,
    borderWidth: 0,
  },
  resultsStyle: {
    borderRadius: 10,
    borderColor: PRIMARY_COLOR,
    borderWidth: 1,
    width: width - 40,
  },
  lastInput: {
    marginBottom: 30,
  },
  errors: {
    paddingLeft: 10,
    color: 'red',
    marginTop: 5,
  },
  row: {
    marginTop: 20,
  },
  addEventButton: {
    marginHorizontal: 20,
    backgroundColor: PRIMARY_COLOR,
  },
});
