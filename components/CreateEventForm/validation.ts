import { CreateEventDto } from '../../services/eventsService';
export const defaultValues: CreateEventDto = {
  title: '',
  desc: '',
  coverUri: '',
  location: {
    type: 'Point',
    coordinates: [],
  },
  endAt: new Date(),
  beginAt: new Date(),
  price: 0,
  placeId: '',
  categories: [],
  active: true,
  private: true,
  photos: [],
};

export const rules = {
  title: {
    required: {
      value: true,
      message: 'Un titre est requis',
    },
    maxLength: {
      value: 255,
      message:
        'La longueur du titre doit être inférieure ou égale à 255 caractères.',
    },
    minLength: {
      value: 1,
      message:
        'La longueur du titre doit être superieur ou égale à 1 caractère.',
    },
  },
  coverUri: {
    required: {
      value: true,
      message: 'Une cover est requise',
    },
  },
  desc: {
    required: {
      value: true,
      message: 'Une description est requise',
    },
    maxLength: {
      value: 5000,
      message:
        'La longueur de la description doit être inférieure ou égale à 5000 caractères.',
    },
    minLength: {
      value: 1,
      message:
        'La longueur de la description doit être superieur ou égale à 1 caractères.',
    },
  },
  placeId: {
    required: {
      value: true,
      message: 'Un lieu est requis',
    },
  },
  beginAt: {
    required: {
      value: true,
      message: 'Une date de debut est requise',
    },
  },
  endAt: {
    required: {
      value: true,
      message: 'Une date de fin est requise',
    },
  },
  price: {
    required: {
      value: true,
      message: 'Un prix est requis',
    },
    min: 0,
    max: 1000,
  },
};
