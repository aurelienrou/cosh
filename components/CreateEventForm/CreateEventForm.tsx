import {
  StyleSheet,
  Switch,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, { FC, useState } from 'react';
import { height, width } from '../../constants/Layout';

import InputPlaceFinder from '../InputPlaceFinder/InputPlaceFinder';
import {
  INPUT_BACKGROUND_COLOR,
  PLACEHOLDER_COLOR,
  PRIMARY_COLOR,
} from '../../constants/Colors';
import UploadCover from '../UploadCover/UploadCover';
import { Controller, useForm } from 'react-hook-form';

import { ModalDateTimePicker } from '../ModalDateTimePicker/ModalDateTimePicker';
import dayjs from 'dayjs';
import { useMutation } from '@tanstack/react-query';
import { createEvent, CreateEventDto } from '../../services/eventsService';
import Button from '../Button/Button';
import { defaultValues, rules } from './validation';
import Toast from 'react-native-toast-message';
import { AxiosError } from 'axios';
import { ErrorType } from '../../services/types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CoshText from '../CoshText/CoshText';

require('dayjs/locale/fr');

dayjs.locale('fr');
interface DateTimeSelectorProps {
  onChange: (date: Date) => void;
  value: Date;
}

const DateTimeSelector = ({ onChange, value }: DateTimeSelectorProps) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <TouchableOpacity style={styles.dateTime} onPress={() => setIsOpen(true)}>
        <CoshText>{dayjs(value).format('DD MMM YY  HH:mm')}</CoshText>
      </TouchableOpacity>
      <ModalDateTimePicker
        onConfirm={(date) => {
          onChange(date);
          setIsOpen(false);
        }}
        mode="datetime"
        onCancel={() => setIsOpen(false)}
        isOpen={isOpen}
        minuteInterval={5}
      />
    </>
  );
};
interface CreateEventFormProps {
  onOpenPlaceForm: () => void;
  onSubmit: () => void;
}

const CreateEventForm: FC<CreateEventFormProps> = ({
  onOpenPlaceForm,
  onSubmit,
}) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm({ defaultValues });

  const { mutate, isLoading } = useMutation(createEvent, {
    onSuccess: () => {
      Toast.show({
        type: 'success',
        props: { text: `L'évenement a bien été créer` },
      });
      onSubmit();
    },
    onError: (err: AxiosError) => {
      Toast.show({
        type: 'error',
        props: { text: (err.response?.data as ErrorType)?.message },
      });
    },
  });

  const handleSubmitForm = (data: CreateEventDto) => {
    mutate(data);
  };

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView>
        <View style={styles.form}>
          {/* Cover */}
          <Controller
            control={control}
            rules={rules.coverUri}
            render={({ field: { onChange, value } }) => (
              <UploadCover onChange={onChange} value={value} />
            )}
            name="coverUri"
          />
          {errors.coverUri && (
            <CoshText style={styles.errors}>
              {errors?.coverUri?.message}
            </CoshText>
          )}

          {/* Title */}
          <View style={styles.colomn}>
            <Controller
              control={control}
              rules={rules.title}
              render={({ field: { onChange, value } }) => (
                <TextInput
                  style={[styles.input]}
                  onChangeText={onChange}
                  value={value}
                  placeholder="Nom"
                  placeholderTextColor={PLACEHOLDER_COLOR}
                  clearButtonMode="while-editing"
                />
              )}
              name="title"
            />
            {errors.title && (
              <CoshText style={styles.errors}>
                {errors?.title?.message}
              </CoshText>
            )}
          </View>
          {/* Description */}
          <View style={[styles.colomn, { marginTop: 10 }]}>
            <Controller
              control={control}
              rules={rules.desc}
              render={({ field: { onChange, value } }) => (
                <TextInput
                  style={styles.multiline}
                  onChangeText={onChange}
                  value={value}
                  placeholder="Description"
                  placeholderTextColor={PLACEHOLDER_COLOR}
                  clearButtonMode="while-editing"
                  multiline
                />
              )}
              name="desc"
            />
            {errors.desc && (
              <CoshText style={styles.errors}>{errors?.desc?.message}</CoshText>
            )}
          </View>

          {/* Lieu */}
          <View style={[styles.colomn]}>
            <Controller
              control={control}
              rules={rules.placeId}
              render={({ field: { onChange } }) => (
                <InputPlaceFinder
                  onSelect={({ id, geocode }) => {
                    onChange(id);
                    setValue('location', {
                      type: ['Point'],
                      coordinates: [geocode.lat, geocode.lng],
                    });
                  }}
                />
              )}
              name="placeId"
            />
            {errors.placeId && (
              <CoshText style={styles.errors}>
                {errors?.placeId?.message}
              </CoshText>
            )}
            <TouchableOpacity
              onPress={() => onOpenPlaceForm()}
              style={styles.addPlaceButton}
            >
              <CoshText style={styles.addPlaceButtonText}>
                Ajouter un lieu
              </CoshText>
            </TouchableOpacity>
          </View>

          {/* Debut */}
          <View style={styles.row}>
            <CoshText>Début</CoshText>
            <Controller
              control={control}
              rules={rules.beginAt}
              render={({ field: { onChange, value } }) => (
                <DateTimeSelector onChange={onChange} value={value} />
              )}
              name="beginAt"
            />
          </View>
          {errors.beginAt && (
            <CoshText style={styles.errors}>
              {errors?.beginAt?.message}
            </CoshText>
          )}

          {/* Fin */}
          <View style={[styles.row, { marginTop: 10 }]}>
            <CoshText>Fin</CoshText>
            <Controller
              control={control}
              rules={rules.endAt}
              render={({ field: { onChange, value } }) => (
                <DateTimeSelector onChange={onChange} value={value} />
              )}
              name="endAt"
            />
          </View>
          {errors.endAt && (
            <CoshText style={styles.errors}>{errors?.endAt?.message}</CoshText>
          )}

          {/* Price */}
          <View style={[styles.row, { marginTop: 10 }]}>
            <CoshText>Prix</CoshText>
            <Controller
              control={control}
              rules={rules.price}
              render={({ field: { onChange, value } }) => (
                <TextInput
                  onChangeText={(text) => onChange(text.replace(/[^0-9]/g, ''))}
                  value={`${value}`}
                  style={[styles.input, { width: 100 }]}
                  keyboardType="number-pad"
                  maxLength={4}
                />
              )}
              name="price"
            />
          </View>
          {errors.price && (
            <CoshText style={styles.errors}>{errors?.price?.message}</CoshText>
          )}

          {/* Active */}
          <View style={[styles.row, { marginTop: 10 }]}>
            <CoshText>Active</CoshText>
            <Controller
              control={control}
              render={({ field: { onChange, value } }) => (
                <Switch
                  trackColor={{ false: '#fff', true: PRIMARY_COLOR }}
                  onValueChange={onChange}
                  value={value}
                />
              )}
              name="active"
            />
          </View>

          {/* Private */}
          <View style={[styles.row, { marginTop: 10 }]}>
            <CoshText>Prive</CoshText>
            <Controller
              control={control}
              render={({ field: { onChange, value } }) => (
                <Switch
                  trackColor={{ false: '#fff', true: PRIMARY_COLOR }}
                  onValueChange={onChange}
                  value={value}
                />
              )}
              name="private"
            />
          </View>
        </View>
      </KeyboardAwareScrollView>
      <Button
        loading={isLoading}
        onPress={handleSubmit(handleSubmitForm)}
        text="Ajouter"
        style={styles.addEventButton}
      />
    </View>
  );
};

export default CreateEventForm;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    flex: 1,
  },
  form: {
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  input: {
    height: 50,
    width: width - 40,
    borderRadius: 10,
    overflow: 'hidden',
    paddingHorizontal: 20,
    backgroundColor: INPUT_BACKGROUND_COLOR,
  },
  multiline: {
    paddingTop: 15,
    paddingBottom: 15,
    minHeight: 100,
    maxHeight: height / 3,
    width: width - 40,
    borderRadius: 10,
    overflow: 'hidden',
    paddingHorizontal: 20,
    backgroundColor: INPUT_BACKGROUND_COLOR,
  },
  placeContainer: {
    flexDirection: 'row',
  },
  addPlaceButton: {
    height: 50,
    width: width - 40,
    borderRadius: 10,
    borderColor: PRIMARY_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  addPlaceButtonText: {
    color: PRIMARY_COLOR,
  },
  address: {
    width: width - 40,
  },
  resultsStyle: {
    borderRadius: 10,
    borderColor: '#000',
    borderWidth: 1,
  },
  lastInput: {
    marginBottom: 30,
  },
  errors: {
    paddingLeft: 20,
    color: 'red',
    marginTop: 5,
  },
  row: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 20,
  },
  colomn: {
    marginTop: 20,
  },
  dateTime: {
    width: 160,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: INPUT_BACKGROUND_COLOR,
  },
  addEventButton: {
    marginHorizontal: 20,
    backgroundColor: PRIMARY_COLOR,
  },
});
