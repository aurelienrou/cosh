import React, { Text, View } from 'react-native';
import { StyleSheet } from 'react-native';

interface MainMessageProps {
  title: string;
  subTitle?: string;
}

export const MainMessage = ({ title, subTitle }: MainMessageProps) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      {subTitle ? <Text style={styles.subTitle}>{subTitle}</Text> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 32,
  },
  title: {
    fontFamily: 'gatwick-bold',
    fontSize: 42,
    color: '#FFF',
  },
  subTitle: {
    marginTop: 16,
    fontFamily: 'neue',
    fontSize: 16,
    color: '#999999',
  },
});
