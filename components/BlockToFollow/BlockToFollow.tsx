import React from 'react';
import styled from 'styled-components/native';
import { Image } from 'expo-image';
import { useNavigation } from '@react-navigation/native';
import { Pressable, Text } from 'react-native';

interface BlockToFollowProps {
  name: string;
  cover: string;
  count: number;
  onPress: () => void;
  onFollow?: (id: string) => void;
}

export const BlockToFollow = ({
  cover,
  name,
  count,
  onPress,
  onFollow,
}: BlockToFollowProps) => {
  return (
    <S_Container onPress={onPress}>
      <S_Image contentFit="cover" source={{ uri: cover }} />
      <S_InfosContainer>
        <S_Name>{name}</S_Name>
        <S_Followers>{count ? count : 0} followers</S_Followers>
      </S_InfosContainer>
    </S_Container>
  );
};

const S_Container = styled.Pressable`
  height: 48px;
  width: 100%;
  align-items: center;
  flex-direction: row;
`;

const S_Image = styled(Image)`
  height: 48px;
  width: 48px;
  border-radius: 24px;
  margin-right: 16px;
`;

const S_InfosContainer = styled.View`
  height: 48px;
  justify-content: center;
`;

const S_Name = styled.Text`
  font-size: 14px;
  font-family: 'gatwick-bold';
  color: ${(props) => props.theme.fontColor.primary};
  margin-bottom: 8px;
`;

const S_Followers = styled.Text`
  font-size: 14px;
  font-family: 'neue';
  color: ${(props) => props.theme.fontColor.secondary};
`;
