import React from 'react';
import Button from '../Button/Button';
import styled from 'styled-components/native';
import { width } from '../../constants/Layout';
import { useTheme } from 'styled-components';
import { onGoogleButtonPress } from '../../hooks/useSignInWithGoogle';
import { useNavigation } from '@react-navigation/native';
import { getGoogleUserFromToken } from '../../services/auth/getGoogleUserWithToken';
import { useAuthContext } from '../../provider/AuthProvider';
import analytics from '@react-native-firebase/analytics';

export default function GoogleLoginButton() {
  const theme = useTheme();
  const navigation = useNavigation();
  const { setUser } = useAuthContext();

  const handleSign = async () => {
    try {
      const res = await onGoogleButtonPress();
      const idToken = await res?.user?.getIdToken();

      if (idToken) {
        try {
          const user = await getGoogleUserFromToken(idToken);
          setUser(user.user);
          navigation.navigate('Discover' as never);
        } catch (err) {
          const token = await res.user.getIdToken();

          navigation.navigate({
            name: 'SignUp',
            params: {
              token,
              name: res.user?.displayName || null,
              email: res.user?.email || null,
              cover: res.user?.photoURL || null,
              isSocialLogin: true,
            },
          } as never);

          analytics().logLogin({ method: 'google' });
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <S_Button
      text="CONTINU AVEC GOOGLE"
      onPress={handleSign}
      textStyle={{
        fontFamily: 'neue-bold',
        fontSize: 14,
        color: theme.background.primary,
      }}
    />
  );
}

const S_Button = styled(Button)`
  width: ${width - 40 + 'px'};
  height: 48px;
  justify-content: center;
  align-items: center;
  border-radius: 24px;
  margin-bottom: 16px;
  background-color: ${(props) => props.theme.fontColor.primary};
`;
