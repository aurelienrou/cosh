import React from 'react';
import styled from 'styled-components/native';
import GoogleLoginButton from './GoogleLoginButton';
import { Pressable } from 'react-native';
import AppleLoginButton from './AppleLoginButton';

interface RegisterProps {
  onToggle: () => void;
}

export const Register = ({ onToggle }: RegisterProps) => {
  return (
    <S_Container>
      <S_Title>{`S'INSCRIRE`}</S_Title>
      <S_SubTitle>
        Prêt à commencer ? Inscrivez-vous dès maintenant !
      </S_SubTitle>
      <GoogleLoginButton />
      <AppleLoginButton />
      <Pressable onPress={onToggle}>
        <S_HaveAccount>
          Déjà un compte ? <S_TextColor>SE CONNECTER</S_TextColor>
        </S_HaveAccount>
      </Pressable>
    </S_Container>
  );
};
const S_Container = styled.View`
  padding: 8px 20px 0 20px;
`;

const S_Title = styled.Text`
  font-family: 'gatwick-bold';
  font-size: 16px;
  margin-bottom: 24px;
  color: ${(props) => props.theme.fontColor.primary};
`;

const S_HaveAccount = styled.Text`
  font-family: 'neue';
  font-size: 14px;
  margin-top: 8px;
  text-align: center;
  color: ${(props) => props.theme.fontColor.secondary};
`;

const S_SubTitle = styled.Text`
  font-family: 'neue';
  font-size: 16px;
  line-height: 20px;
  margin-bottom: 32px;
  color: ${(props) => props.theme.fontColor.primary};
`;

const S_TextColor = styled.Text`
  font-family: 'neue-bold';
  font-size: 16px;
  margin-bottom: 32px;
  color: ${(props) => props.theme.fontColor.action};
`;
