import React from 'react';
import styled from 'styled-components/native';
import GoogleLoginButton from './GoogleLoginButton';
import AppleLoginButton from './AppleLoginButton';

interface RegisterProps {
  onToggle: () => void;
}

export const LogIn = ({ onToggle }: RegisterProps) => {
  return (
    <S_Container>
      <S_Title>CONNEXION</S_Title>
      <GoogleLoginButton />
      <AppleLoginButton />
      <S_Pressable onPress={onToggle}>
        <S_HaveNotAccount>PAS ENCORE DE COMPTE</S_HaveNotAccount>
      </S_Pressable>
    </S_Container>
  );
};
const S_Container = styled.View`
  padding: 8px 20px 0 20px;
`;

const S_Title = styled.Text`
  font-family: 'gatwick-bold';
  font-size: 16px;
  margin-bottom: 24px;
  color: ${(props) => props.theme.fontColor.primary};
`;

const S_HaveNotAccount = styled.Text`
  font-family: 'neue-blod';
  font-size: 14px;
  text-align: center;
  color: ${(props) => props.theme.fontColor.primary};
`;

const S_Pressable = styled.Pressable`
  height: 48px;
  border-radius: 36px;
  justify-content: center;
  align-items: center;
  border-color: ${(props) => props.theme.background.action};
  border-width: 1px;
`;
