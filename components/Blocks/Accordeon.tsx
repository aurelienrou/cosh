import React, { ReactNode } from 'react';
import {
  StyleProp,
  TextStyle,
  View,
  ViewStyle,
  StyleSheet,
  Pressable,
  Text,
} from 'react-native';
import Animated, {
  useAnimatedRef,
  useSharedValue,
  useDerivedValue,
  withSpring,
  withTiming,
  useAnimatedStyle,
  runOnUI,
  measure,
  interpolate,
} from 'react-native-reanimated';
import { Chevron } from './Chevron';
import { useMainTheme } from '../../hooks/useMainTheme';
import { Theme } from '../../constants/theme';

interface AccordeonProps {
  children: ReactNode;
  title: string;
  subtitle: string;
  style?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
}

export const Accordeon = ({
  title,
  subtitle,
  children,
  style,
  textStyle,
}: AccordeonProps) => {
  const aref = useAnimatedRef<View>();
  const open = useSharedValue(false);
  const progress = useDerivedValue(() =>
    open.value ? withSpring(1, { damping: 15 }) : withTiming(0),
  );
  const height = useSharedValue(0);
  const styles = useMainTheme(createStyles);

  const AnimatedStyle = useAnimatedStyle(() => ({
    height: height.value * progress.value + 1,
    opacity: interpolate(progress.value, [0.3, 1], [0, 1]),
    marginTop: progress.value * 32,
  }));

  return (
    <>
      <Pressable
        style={[styles.pressable, style]}
        onPress={() => {
          if (height.value === 0) {
            runOnUI(() => {
              'worklet';
              height.value = measure(aref).height;
            })();
          }
          open.value = !open.value;
        }}
      >
        <View style={styles.header}>
          <View>
            <Text style={[styles.title, textStyle]}>{title}</Text>
            <Text style={[styles.subTitle, textStyle]}>{subtitle}</Text>
          </View>
          <Chevron {...{ progress }} />
        </View>
      </Pressable>
      <Animated.View style={[AnimatedStyle]}>
        <View ref={aref}>{children}</View>
      </Animated.View>
    </>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    pressable: {
      minHeight: 48,
      width: '100%',
    },
    header: {
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    title: {
      fontFamily: 'neue-bold',
      fontSize: 16,
      marginBottom: 16,
      color: theme.fontColor.action,
    },
    subTitle: {
      fontFamily: 'neue',
      fontSize: 16,
      color: theme.fontColor.action,
    },
    listContainer: {
      overflow: 'hidden',
    },
  });
