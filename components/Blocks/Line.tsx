import styled from 'styled-components/native';

export const Line = styled.View`
  width: 100%;
  border-color: 'rgba(255, 255, 255, 0.1)';
  border-bottom-width: 1px;
  margin: 16px 0px;
`;
