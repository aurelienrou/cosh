import { BlurView } from '@react-native-community/blur';
import React from 'react';
import styled from 'styled-components/native';
import { Image } from 'expo-image';
import FollowButton from '../Button/FollowButton';
import { useNavigation } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { width } from '../../constants/Layout';
import { TypeToFollow } from '../BlockEvents/BlockEvent';
import { blurAmount } from '../../constants/theme';

interface BlockHeaderBackAndFollowProps {
  id: string;
  type: TypeToFollow | 'None';
}

export const BlockHeaderBackAndFollow = ({
  id,
  type,
}: BlockHeaderBackAndFollowProps) => {
  const navigation = useNavigation();
  const { top } = useSafeAreaInsets();

  return (
    <S_Header style={{ top: top + 20 }}>
      <S_BackPressable onPress={() => navigation.goBack()}>
        <S_BackIcon source={require('../../assets/icons/arrow.svg')} />
        <S_Blur blurAmount={blurAmount} />
      </S_BackPressable>
      {type !== 'None' ? (
        <FollowButton hasBackground id={id} type={type} />
      ) : (
        <></>
      )}
    </S_Header>
  );
};

const S_Header = styled.View`
  position: absolute;
  width: ${width + 'px'};
  flex-direction: row;
  padding: 0 20px;
  height: 48px;
  z-index: 10;
  align-items: center;
  justify-content: space-between;
`;

const S_BackPressable = styled.Pressable`
  height: 48px;
  width: 48px;
  border-radius: 24px;
  align-items: center;
  justify-content: center;
  position: relative;
  overflow: hidden;
`;

const S_Blur = styled(BlurView)`
  height: 48px;
  width: 48px;
  border-radius: 24px;
  position: absolute;
`;

const S_BackIcon = styled(Image)`
  height: 14px;
  width: 18px;
  z-index: 11;
`;
