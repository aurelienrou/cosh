import React from 'react';
import Animated, { useAnimatedStyle } from 'react-native-reanimated';
import { mix, mixColor } from 'react-native-redash';
import { Image } from 'expo-image';
import styled from 'styled-components/native';

interface ChevronProps {
  progress: Animated.SharedValue<number>;
}

export const Chevron = ({ progress }: ChevronProps) => {
  const style = useAnimatedStyle(() => ({
    transform: [{ rotateZ: `${mix(progress.value, Math.PI, 0)}rad` }],
  }));
  return (
    <S_Container style={[style]}>
      <S_Image source={require('../../assets/icons/chevron.svg')} />
    </S_Container>
  );
};

const S_Container = styled(Animated.View)`
  width: 15px;
  height: 7px;
`;

const S_Image = styled(Image)`
  width: 15px;
  height: 7px;
`;
