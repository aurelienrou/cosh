import { Image } from 'expo-image';
import React from 'react';
import { View } from 'react-native';
import styled from 'styled-components/native';
import { width } from '../../constants/Layout';

interface BlockAdressProps {
  onPress?: () => void;
  name?: string;
  adress?: string;
}
export const BlockAdress = ({ name, adress, onPress }: BlockAdressProps) => {
  return (
    <Container onPress={onPress}>
      <Infos>
        {name ? <Name numberOfLines={1}>{name}</Name> : null}
        {adress ? <Adress numberOfLines={1}>{adress}</Adress> : null}
      </Infos>

      <Arrow source={require('../../assets/icons/miniArrow.svg')} />
    </Container>
  );
};

const Container = styled.Pressable`
  flex-direction: row;
  align-items: center;
`;

const Infos = styled.View`
  width: ${width - 40 - 7.5 - 8 + 'px'};
  /* flex-direction: row; */
`;

const Name = styled.Text`
  font-size: 16px;
  margin-bottom: 16px;
  font-family: 'neue-bold';
  color: ${(props) => props.theme.fontColor.primary};
`;

const Adress = styled.Text`
  font-size: 16px;
  font-family: 'neue';
  color: ${(props) => props.theme.fontColor.primary};
`;

const Arrow = styled(Image)`
  width: 7.5px;
  height: 15.5px;
  margin-left: 8px;
`;
