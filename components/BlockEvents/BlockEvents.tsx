import React from 'react';
import styled from 'styled-components/native';
import { BlockEvent } from './BlockEvent';
import { IEvent, IEventMinimize } from '../../services/types';
import { StyleProp, ViewStyle } from 'react-native';

interface BlockEventsProps {
  events: IEventMinimize[];
  loading?: boolean;
  max?: number;
  style?: StyleProp<ViewStyle>;
}

export const BlockEvents = ({
  events,
  loading,
  style,
  max = 4,
}: BlockEventsProps) => {
  const roundLayout = events.length > max;

  if (!events) {
    return <></>;
  }

  return (
    <S_Container style={style} isRounded={roundLayout}>
      {events.slice(0, max).map((event, index) => (
        <BlockEvent key={index} event={event} isBig={!roundLayout} />
      ))}
    </S_Container>
  );
};

const S_Container = styled.View<{ isRounded: boolean }>`
  background-color: ${(props) =>
    props.isRounded
      ? props.theme.background.action
      : props.theme.background.primary};
  gap: 16px;
  padding: ${(props) => (props.isRounded ? '16px' : '0px')};
  padding-right: 0px;
  border-radius: 16px;
`;
