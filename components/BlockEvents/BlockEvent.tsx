import React from 'react';
import styled from 'styled-components/native';
import FollowButton from '../Button/FollowButton';
import { IEventMinimize } from '../../services/types';
import { StyleProp, ViewStyle } from 'react-native';
import { Image } from 'expo-image';
import { useNavigation } from '@react-navigation/native';
import { width } from '../../constants/Layout';
import { formateDate } from '../../helpers/date';
import { convertPrice, capitalizeFirstLetter } from '../../helpers/string';

interface BlockEventProps {
  event: IEventMinimize;
  isBig: boolean;
  style?: StyleProp<ViewStyle>;
}

export type TypeToFollow = 'Event' | 'Place' | 'User';

export const BlockEvent = ({ event, isBig, style }: BlockEventProps) => {
  const navigation = useNavigation<any>();
  return (
    <S_Container
      onPress={() => navigation.navigate('Event', { id: event.id })}
      style={[style]}
      isBig={isBig}
    >
      <S_Image
        isBig={isBig}
        source={{
          uri: event?.cover?.replace('http://', 'https://'),
        }}
      />
      <S_TextContainer isBig={isBig}>
        <S_Title isBig={isBig} ellipsizeMode="tail" numberOfLines={1}>
          {capitalizeFirstLetter(event.name.toLowerCase())}
        </S_Title>
        <S_Date numberOfLines={1}>
          {formateDate((event as IEventMinimize).beginAt)}
        </S_Date>
        <S_Place numberOfLines={1}>{event?.place?.name}</S_Place>
        {isBig && (
          <S_Price numberOfLines={1}>{convertPrice(event.minPrice)}</S_Price>
        )}
      </S_TextContainer>
      <S_FollowContainer isBig={isBig}>
        <FollowButton id={event.id} type={'Event'} />
      </S_FollowContainer>
    </S_Container>
  );
};

const S_Container = styled.Pressable<{ isBig: boolean }>`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 16px;
  padding-right: ${(props) => (props.isBig ? '0' : '16px')};
  height: ${(props) => (props.isBig ? '84px' : '64px')};
`;

const S_Image = styled(Image)<{ isBig: boolean }>`
  height: ${(props) => (props.isBig ? '84px' : '64px')};
  width: ${(props) => (props.isBig ? '84px' : '64px')};
  border-radius: 8px;
`;

const S_TextContainer = styled.View<{ isBig: boolean }>`
  height: ${(props) => (props.isBig ? '84px' : '64px')};
  justify-content: space-between;
  padding: 6px 0;
`;
const S_Title = styled.Text<{ isBig: boolean }>`
  font-size: 12px;
  width: ${(props) =>
    props.isBig ? width - 68 - 130 + 'px' : width - 48 - 155 + 'px'};
  font-family: 'gatwick-bold';
  color: ${(props) => props.theme.fontColor.primary};
`;

const S_Date = styled.Text`
  margin-top: 14px;
  flex: 1;
  font-family: 'neue';
  margin-top: 8px;
  color: ${(props) => props.theme.fontColor.action};
`;

const S_Place = styled.Text`
  margin-top: 14px;
  flex: 1;
  width: ${width - 40 - 150 + 'px'};
  font-family: 'neue';
  margin-top: 8px;
  color: ${(props) => props.theme.fontColor.secondary};
`;

const S_Price = styled.Text`
  margin-top: 14px;
  flex: 1;
  width: ${width - 40 - 150 + 'px'};
  font-family: 'neue';
  margin-top: 8px;
  color: ${(props) => props.theme.fontColor.primary};
`;

const S_FollowContainer = styled.View<{ isBig: boolean }>`
  position: absolute;
  right: ${(props) => (props.isBig ? '-6px' : '6px')};
`;
