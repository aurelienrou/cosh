import React, { FC } from 'react';
import { StyleSheet, Text as RNText, TextProps } from 'react-native';

interface TextPrp extends TextProps {
  children: React.ReactNode;
}

const CoshText: FC<TextPrp> = ({ children, ...props }) => {
  return (
    <RNText {...props} style={[props.style, styles.text]}>
      {children}
    </RNText>
  );
};

export default CoshText;

const styles = StyleSheet.create({
  text: {
    fontFamily: 'clash-regular',
  },
});
