import { useFocusEffect } from '@react-navigation/native';
import React, { useCallback, useMemo } from 'react';
import styled from 'styled-components/native';
import { SwipeCards } from '../../SwipeCards/SwipeCards';
import { BlockEvents } from '../../BlockEvents/BlockEvents';
import { BottomSheetScrollView } from '@gorhom/bottom-sheet';
import { SectionHeader } from '../../Section/SectionHeader';
import { usePopularEventNearUser } from '../../../services/events/popular';
import { useLocationContext } from '../../../provider/LocationProvider';
import { useNewEvents } from '../../../services/events/getNewEvents';
import { useDispatch } from 'react-redux';
import { setDataForMap } from '../../../provider/MapStore';
import { MainMessage } from '../../Errors/MainMessage';
import { StyleSheet, View } from 'react-native';
import { height, width } from '../../../constants/Layout';
import Animated, { FadeIn } from 'react-native-reanimated';
import { IEventMinimize } from '../../../services/types';
import { FlashList } from '@shopify/flash-list';
import { useMainTheme } from '../../../hooks/useMainTheme';
import { Theme } from '../../../constants/theme';
import { useTheme } from '../../../provider/ThemeProvider';

type Data = { type: string; data: IEventMinimize[] };
export const All = () => {
  const { location } = useLocationContext();
  const dispatch = useDispatch();
  const styles = useMainTheme(createStyles);
  const { theme } = useTheme();

  const {
    data: popularEvents,
    isLoading: isPopularEventsLoading,
    isError: isPopularEventsErrors,
  } = usePopularEventNearUser(location.latitude, location.longitude);

  const {
    data: newEvents,
    isLoading: isNewEventsLoading,
    isError: isNewEventsErrors,
  } = useNewEvents(location.latitude, location.longitude);

  useFocusEffect(
    useCallback(() => {
      dispatch(setDataForMap(newEvents.concat(popularEvents)));
    }, [newEvents]),
  );

  const data = useMemo(() => {
    const returnedData = [];

    if (popularEvents.length) {
      returnedData.push('Populaires', { type: 'popular', data: popularEvents });
    }

    if (newEvents.length) {
      returnedData.push('Nouveautés', { type: 'new', data: newEvents });
    }

    return returnedData;
  }, [popularEvents, newEvents]);

  const stickyHeaderIndices = useMemo(
    () =>
      data
        .map((item, index) => {
          if (typeof item === 'string') {
            return index;
          } else {
            return null;
          }
        })
        .filter((item) => item !== null) as number[],
    [newEvents.concat(popularEvents)],
  );

  const renderItems = useCallback(
    ({ item }) => {
      if (typeof item === 'string') {
        return (
          <Animated.View entering={FadeIn.delay(500).duration(300)}>
            <SectionHeader title={item} />
          </Animated.View>
        );
      } else if (item.type === 'popular') {
        return (
          <SwipeCards events={popularEvents} loading={isPopularEventsLoading} />
        );
      } else if (item.type === 'new') {
        return (
          <View style={styles.StyledPadding}>
            <BlockEvents
              events={newEvents}
              max={9}
              loading={isNewEventsLoading}
            />
          </View>
        );
      } else {
        return <></>;
      }
    },
    [popularEvents, newEvents],
  );

  return (
    <View style={styles.StyledContainer}>
      {isNewEventsErrors && isPopularEventsErrors ? (
        <MainMessage
          title="Oops!"
          subTitle="An error has occurred in the app"
        />
      ) : (
        <FlashList<Data | string>
          data={data}
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          //@ts-ignore
          renderScrollComponent={BottomSheetScrollView}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: 200,
            // backgroundColor: theme.background.primary,
          }}
          keyExtractor={(item) => (typeof item === 'string' ? item : item.type)}
          renderItem={renderItems}
          estimatedItemSize={width + 30}
          stickyHeaderIndices={stickyHeaderIndices}
        />
      )}
    </View>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    StyledPadding: {
      marginTop: 16,
      paddingHorizontal: 20,
    },
    StyledContainer: {
      flex: 1,
      height,
      backgroundColor: theme.background.primary,
    },
  });
