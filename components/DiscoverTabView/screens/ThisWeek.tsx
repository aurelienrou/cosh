import React, { useCallback } from 'react';
import { View } from 'react-native';
import styled from 'styled-components/native';
import { BottomSheetScrollView } from '@gorhom/bottom-sheet';
import { useFocusEffect } from '@react-navigation/native';
import { useLocationContext } from '../../../provider/LocationProvider';
import { useWeeklyEvents } from '../../../services/events/getWeeklyEvents';
import { useDispatch } from 'react-redux';
import { setDataForMap } from '../../../provider/MapStore';
import { EventsLoader } from '../../Loader/EventLoader';
import { EventsByDateList } from '../../Lists/EventsByDateList';
import { FlashListProps } from '@shopify/flash-list/dist/FlashListProps';
import { IEventMinimize } from '../../../services/types';
import { MainMessage } from '../../Errors/MainMessage';

export const ThisWeek = () => {
  const { location } = useLocationContext();
  const dispatch = useDispatch();

  const {
    data,
    isFetching,
    isLoading: isEventsLoading,
    isError,
  } = useWeeklyEvents(location.latitude, location.longitude, 10000);

  const isLoading = isEventsLoading || isFetching;

  useFocusEffect(
    useCallback(() => {
      dispatch(setDataForMap(data));
    }, [data]),
  );

  return (
    <S_Container>
      {isLoading ? (
        <EventsLoader />
      ) : isError ? (
        <MainMessage
          title="Oops!"
          subTitle="An error has occurred in the app"
        />
      ) : (
        <View style={{ flex: 1 }}>
          <EventsByDateList
            events={data}
            renderScrollComponent={
              BottomSheetScrollView as FlashListProps<
                IEventMinimize | string
              >['renderScrollComponent']
            }
          />
        </View>
      )}
    </S_Container>
  );
};

const S_Container = styled.View`
  flex: 1;
  background-color: ${(props) => props.theme.background.primary};
  padding: 0 20px;
`;
