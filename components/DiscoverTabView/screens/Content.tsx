import { BottomSheetFlatList } from '@gorhom/bottom-sheet';
import { RouteProp, useFocusEffect, useRoute } from '@react-navigation/native';
import { useQuery } from '@tanstack/react-query';
import React, { useCallback } from 'react';
import { FiltersButton } from '../../Button/FiltersButton';
import styled from 'styled-components/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { SectionHeader } from '../../Section/SectionHeader';
import axios from '../../../services/axios';
import {
  IEventMinimize,
  IPlaceMinimize,
  ITopics,
} from '../../../services/types';
import { setDataForMap } from '../../../provider/MapStore';
import { useDispatch } from 'react-redux';
import { BlockEvent } from '../../BlockEvents/BlockEvent';
import { BlockPlace } from '../../BlockPlaces/BlockPlace';

export interface ContentProps {
  id: string;
}

export type ParamList = {
  Content: ContentProps;
};

export const getTopicById = async (id: string) => {
  const { data } = await axios.get(`/topic/${id}`);
  return data;
};

export const useTopicById = (id: string) => {
  return useQuery<ITopics>(['getTopicById', id], () => getTopicById(id), {
    enabled: Boolean(id),
    initialData: {} as ITopics,
  });
};

export const Content = () => {
  const { top } = useSafeAreaInsets();
  const route = useRoute<RouteProp<ParamList, 'Content'>>();
  const { id } = route.params;
  const dispatch = useDispatch();

  const { data, isFetching, isLoading } = useTopicById(id);

  useFocusEffect(
    useCallback(() => {
      if (data.content) {
        dispatch(setDataForMap(data.content));
      }
    }, [data]),
  );

  const renderItems = useCallback(
    ({ item }: { item: IEventMinimize | IPlaceMinimize }) => {
      if ('minPrice' in item) {
        return (
          <S_Padding>
            <BlockEvent style={{ marginTop: 16 }} event={item} isBig />
          </S_Padding>
        );
      }

      return (
        <S_Padding>
          <BlockPlace style={{ marginTop: 16 }} place={item} isBig />
        </S_Padding>
      );
    },
    [data],
  );

  return (
    <S_BottomSheetFlatList
      data={data.content}
      showsVerticalScrollIndicator={false}
      contentContainerStyle={{ paddingBottom: 100, paddingTop: top - 8 }}
      keyExtractor={(item) => item.id}
      renderItem={renderItems}
    >
      <S_Header>
        <FiltersButton />
      </S_Header>
      <SectionHeader title={data.name} />
    </S_BottomSheetFlatList>
  );
};

const S_Header = styled.View`
  padding: 0 20px;
  justify-content: center;
  align-items: flex-end;
  z-index: 2;
`;

const S_BottomSheetFlatList = styled(
  BottomSheetFlatList<IPlaceMinimize | IEventMinimize>,
)`
  background-color: ${(props) => props.theme.background.primary};
`;

const S_Padding = styled.View`
  padding: 0 20px;
`;
