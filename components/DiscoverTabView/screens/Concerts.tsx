import { useFocusEffect } from '@react-navigation/native';
import React, { useCallback } from 'react';
import { BottomSheetScrollView } from '@gorhom/bottom-sheet';
import { useDispatch } from 'react-redux';
import { setDataForMap } from '../../../provider/MapStore';
import { useConcertsEvents } from '../../../services/events/getConcerts';
import { IEventMinimize } from '../../../services/types';
import { FlashListProps } from '@shopify/flash-list';
import { View } from 'react-native';
import styled from 'styled-components/native';
import { EventsLoader } from '../../Loader/EventLoader';
import { EventsByDateList } from '../../Lists/EventsByDateList';
import { TitleLoader } from '../../Loader/TitleLoader';
import { MainMessage } from '../../Errors/MainMessage';

export const Concerts = () => {
  const dispatch = useDispatch();

  const {
    data,
    isFetching: isBarFetching,
    isLoading: isConcertsLoading,
    isError: isConcertsErrors,
  } = useConcertsEvents();

  //   const { data: topics, isLoading: isTopicsLoading } = useGetConcertsTopics();

  const isLoading = isConcertsLoading || isBarFetching;

  useFocusEffect(
    useCallback(() => {
      dispatch(setDataForMap(data));
    }, [data]),
  );

  return (
    <S_Container>
      {isLoading ? (
        <View style={{ marginTop: 32 }}>
          <TitleLoader />
          <EventsLoader />
        </View>
      ) : isConcertsErrors ? (
        <MainMessage
          title="Oops!"
          subTitle="An error has occurred in the app"
        />
      ) : (
        <View style={{ flex: 1 }}>
          <EventsByDateList
            events={data}
            renderScrollComponent={
              BottomSheetScrollView as FlashListProps<
                IEventMinimize | string
              >['renderScrollComponent']
            }
          />
        </View>
      )}
    </S_Container>
  );
};

const S_Container = styled.View`
  flex: 1;
  background-color: ${(props) => props.theme.background.primary};
  padding: 0 20px;
`;
