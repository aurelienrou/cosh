import { useFocusEffect } from '@react-navigation/native';
import React, { useCallback, useMemo } from 'react';
import { BottomSheetScrollView } from '@gorhom/bottom-sheet';
import { SectionHeader } from '../../Section/SectionHeader';
import { Rubriques } from '../../Rubriques/Rubriques';
import { useGetBarsTopics } from '../../../services/topics/getBarsTopics';
import { useDispatch } from 'react-redux';
import { setDataForMap } from '../../../provider/MapStore';
import { useGetBars } from '../../../services/place/getBars';
import { IPlaceMinimize } from '../../../services/types';
import { FlashList, FlashListProps } from '@shopify/flash-list';
import { BlockPlace } from '../../BlockPlaces/BlockPlace';
import { StyleSheet, View } from 'react-native';
import { useTheme } from 'styled-components';
import Animated, { FadeInRight } from 'react-native-reanimated';
import { capitalizeFirstLetter } from '../../../helpers/string';
import { Theme } from '../../../constants/theme';
import { useMainTheme } from '../../../hooks/useMainTheme';
import { TitleLoader } from '../../Loader/TitleLoader';
import { EventsLoader } from '../../Loader/EventLoader';
import { MainMessage } from '../../Errors/MainMessage';

type Type = IPlaceMinimize | string;

export const Bars = () => {
  const dispatch = useDispatch();
  const theme = useTheme();
  const styles = useMainTheme(createStyles);

  const {
    data: bars,
    isFetching: isBarFetching,
    isLoading: isBarsLoading,
    isError: isBarsErrors,
  } = useGetBars();

  const { data: topics, isLoading: isTopicsLoading } = useGetBarsTopics('bar');

  const isLoading = isBarsLoading || isBarFetching;

  useFocusEffect(
    useCallback(() => {
      dispatch(setDataForMap(bars));
    }, [bars]),
  );

  const renderItems = useCallback(
    ({ item, index }: { item: Type; index: number }) => {
      if (Array.isArray(item)) {
        return <Rubriques topics={item} loading={isTopicsLoading} />;
      } else if (typeof item === 'string') {
        return (
          <View style={styles.padding}>
            <SectionHeader
              notHorizontalPadding
              title={capitalizeFirstLetter(item)}
            />
          </View>
        );
      } else {
        return (
          <Animated.View
            style={styles.padding}
            entering={FadeInRight.delay(index * 50).duration(300)}
          >
            <BlockPlace place={item} />
          </Animated.View>
        );
      }
    },
    [],
  );

  const data = topics ? ([topics] as unknown as Type[]).concat(bars) : bars;

  const stickyHeaderIndices = useMemo(
    () =>
      data
        .map((item, index) => {
          if (typeof item === 'string') {
            return index;
          } else {
            return null;
          }
        })
        .filter((item) => item !== null) as number[],
    [data],
  );

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View style={{ marginTop: 32, marginLeft: 20 }}>
          <TitleLoader />
          <EventsLoader />
        </View>
      ) : isBarsErrors ? (
        <MainMessage
          title="Oops!"
          subTitle="An error has occurred in the app"
        />
      ) : (
        <FlashList<Type>
          data={data}
          showsVerticalScrollIndicator={false}
          renderItem={renderItems}
          stickyHeaderIndices={stickyHeaderIndices}
          contentContainerStyle={{
            paddingBottom: 112,
            backgroundColor: theme.background.primary,
          }}
          keyExtractor={(item) =>
            typeof item === 'string' ? item : item.id + item.name
          }
          estimatedItemSize={48}
          renderScrollComponent={
            BottomSheetScrollView as FlashListProps<Type>['renderScrollComponent']
          }
        />
      )}
    </View>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    padding: {
      paddingHorizontal: 20,
    },
    container: {
      flex: 1,
      backgroundColor: theme.background.primary,
    },
  });
