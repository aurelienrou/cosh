import React, { useCallback, useMemo } from 'react';
import { SectionHeader } from '../../Section/SectionHeader';
import { useFocusEffect } from '@react-navigation/native';
import { IEventMinimize } from '../../../services/types';
import styled from 'styled-components/native';
import { BottomSheetScrollView } from '@gorhom/bottom-sheet';
import { isBeforeXHour } from '../../../helpers/date';
import { useLocationContext } from '../../../provider/LocationProvider';
import { useDailyEvents } from '../../../services/events/getDailyEvents';
import { BlockEvent } from '../../BlockEvents/BlockEvent';
import { useDispatch } from 'react-redux';
import { setDataForMap } from '../../../provider/MapStore';
import { EventsLoader } from '../../Loader/EventLoader';
import { View } from 'react-native';
import { FlashList } from '@shopify/flash-list';
import Animated, { FadeInRight } from 'react-native-reanimated';
import { MainMessage } from '../../Errors/MainMessage';

export const Today = () => {
  const { location } = useLocationContext();
  const dispatch = useDispatch();

  const {
    data: events,
    isLoading: isEventsLoading,
    isFetching,
    isError,
  } = useDailyEvents(location.latitude, location.longitude, 10000, ['']);

  const isLoading = isEventsLoading || isFetching;

  const data = useMemo(() => {
    const daytimeEvents: (string | IEventMinimize)[] = [];
    const tonightEvents: (string | IEventMinimize)[] = [];

    for (const event of events) {
      if (isBeforeXHour(event.beginAt, 18)) {
        daytimeEvents.push(event);
      } else {
        tonightEvents.push(event);
      }
    }

    if (daytimeEvents.length) {
      daytimeEvents.unshift('En journée');
    }

    if (tonightEvents.length) {
      tonightEvents.unshift('En soirêe');
    }

    return [...daytimeEvents, ...tonightEvents];
  }, [events]);

  useFocusEffect(
    useCallback(() => {
      dispatch(setDataForMap(events));
    }, [events]),
  );

  const stickyHeaderIndices = useMemo(
    () =>
      data
        .map((item, index) => {
          if (typeof item === 'string') {
            return index;
          } else {
            return null;
          }
        })
        .filter((item) => item !== null) as number[],
    [events],
  );

  const renderItems = useCallback(({ item, index }) => {
    if (typeof item === 'string') {
      return (
        <Animated.View entering={FadeInRight.delay(index * 50).duration(300)}>
          <SectionHeader notHorizontalPadding title={item} />
        </Animated.View>
      );
    } else {
      return (
        <Animated.View entering={FadeInRight.delay(index * 50).duration(300)}>
          <BlockEvent event={item} style={{ marginTop: 16 }} isBig />
        </Animated.View>
      );
    }
  }, []);

  return (
    <S_Container>
      {isLoading ? (
        <EventsLoader />
      ) : isError ? (
        <MainMessage
          title="Oops!"
          subTitle="An error has occurred in the app"
        />
      ) : (
        <View style={{ flex: 1 }}>
          <FlashList<IEventMinimize | string>
            data={data}
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            //@ts-ignore
            renderScrollComponent={BottomSheetScrollView}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: 200 }}
            keyExtractor={(item) => (typeof item === 'string' ? item : item.id)}
            renderItem={renderItems}
            estimatedItemSize={56}
            stickyHeaderIndices={stickyHeaderIndices}
          />
        </View>
      )}
    </S_Container>
  );
};

const S_Container = styled.View`
  flex: 1;
  background-color: ${(props) => props.theme.background.primary};
  padding: 0 20px;
`;
