import React, { Animated, StyleSheet, View } from 'react-native';
import { height } from '../../constants/Layout';
import BottomSheet from '@gorhom/bottom-sheet';
import { useTheme } from 'styled-components/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useMapsContext } from '../../provider/MapsProvider';
import { DiscoverNavigator } from '../../navigation/DiscoverNav';
import { memo } from 'react';
import { useRoute, RouteProp } from '@react-navigation/native';
import { interpolate, useAnimatedProps } from 'react-native-reanimated';

const DiscoverNavigatorMemo = memo(DiscoverNavigator);

export const DiscoverTabView = () => {
  const theme = useTheme();
  const { top, bottom } = useSafeAreaInsets();
  const { bottomSheetRef, animatedIndex, setIsFullyOpen } = useMapsContext();
  const route = useRoute<RouteProp<any>>();

  const handleSheetChanges = () => {
    if (route?.params?.screen === 'Content') {
      setIsFullyOpen(true);
    }
  };

  const animatedProps = useAnimatedProps(() => {
    return {
      borderRadius: interpolate(animatedIndex.value, [0, 1], [32, 0]),
    };
  });

  return (
    <BottomSheet
      style={[styles.bottomSheet, animatedProps]}
      ref={bottomSheetRef}
      index={1}
      onChange={handleSheetChanges}
      snapPoints={[80 + bottom, height - 105 - top]}
      animatedIndex={animatedIndex}
      handleStyle={{ backgroundColor: theme.background.primary }}
      handleIndicatorStyle={{
        backgroundColor: theme.fontColor.primary,
        width: 40,
      }}
    >
      <View style={{ height, backgroundColor: theme.background.primary }}>
        <DiscoverNavigatorMemo />
      </View>
    </BottomSheet>
  );
};

const styles = StyleSheet.create({
  bottomSheet: {
    zIndex: 0,
    paddingBottom: 0,
    borderRadius: 32,
    overflow: 'hidden',
  },
});
