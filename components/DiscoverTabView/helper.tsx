import { ReactElement } from 'react';
import { IEvent, IPlace } from '../../services/types';
import { width, height } from '../../constants/Layout';
import GeoViewport, { BoundingBox } from '@mapbox/geo-viewport';
import { ContentProps } from './screens/Content';

export type Screen = {
  [key in keyof typeof SCREENS_NAME]: {
    title: string;
    component: ReactElement;
  };
};

export type SCREEN_NAME_TYPE =
  | 'Tendances'
  | "Aujourd'hui"
  | 'Cette semaine'
  | 'Bars'
  | 'Clubs'
  | 'Comedy'
  | 'Culture'
  | 'Concerts';

export const SCREENS_NAME: SCREEN_NAME_TYPE[] = [
  'Tendances',
  "Aujourd'hui",
  'Cette semaine',
  'Bars',
  'Clubs',
  'Concerts',
];

interface ComponentProps {
  onScroll: any;
}

type OtherScreens = {
  Content: ReactElement<ContentProps>;
  Event: ReactElement<ContentProps>;
};

export type DiscoverStackParamList = {
  [K in SCREEN_NAME_TYPE]: ReactElement<ComponentProps>;
} & OtherScreens;

export const filterLocationAndSortByDistance = (
  arr: Array<IPlace | IEvent>,
) => {
  const array = arr.sort((value) => value.distance);

  return [...new Map(array.map((item) => [item.id, item])).values()];
};

export const calculateBBox = (region: {
  longitudeDelta: number;
  longitude: number;
  latitude: number;
  latitudeDelta: number;
}): BoundingBox => {
  let lngD;
  if (region.longitudeDelta < 0) lngD = region.longitudeDelta + 360;
  else lngD = region.longitudeDelta;

  return [
    region.longitude - lngD,
    region.latitude - region.latitudeDelta,
    region.longitude + lngD,
    region.latitude + region.latitudeDelta,
  ];
};

export const returnMapZoom = (
  region: { longitudeDelta: number },
  bBox: BoundingBox,
  minZoom = 5,
) => {
  const viewport =
    region.longitudeDelta >= 40
      ? { zoom: minZoom }
      : GeoViewport.viewport(bBox, [width, height]);

  return viewport.zoom + 1;
};
