import React from 'react';
import { IUser } from '../../services/types';
import styled from 'styled-components/native';
import { Image } from 'expo-image';
import { useNavigation } from '@react-navigation/native';
import { StyleProp, ViewStyle } from 'react-native';
import { width } from '../../constants/Layout';

interface BlockPlaceProps {
  user: IUser;
  isBig?: boolean;
  style?: StyleProp<ViewStyle>;
}
export const BlockUser = ({ user, style, isBig = false }: BlockPlaceProps) => {
  const navigation = useNavigation<any>();
  return (
    <S_Container
      style={[style]}
      onPress={() => navigation.navigate('User', { id: user.id })}
    >
      {user.cover ? (
        <S_Image
          isBig={isBig}
          source={{ uri: user.cover }}
          contentFit="cover"
        />
      ) : (
        <S_ReuserCover isBig={isBig} />
      )}
      <S_Infos>
        <S_Name isBig={isBig} numberOfLines={1} ellipsizeMode="tail">
          {user.name}
        </S_Name>
        <S_DistanceAndFollowersCount>
          {`${
            user.followersCount
              ? user.followersCount + ' followers'
              : 'No followers'
          }`}
        </S_DistanceAndFollowersCount>
      </S_Infos>
    </S_Container>
  );
};

const S_Container = styled.Pressable`
  height: 48px;
  width: 100%;
  display: flex;
  margin-bottom: 16px;
  flex-direction: row;
`;

const S_Image = styled(Image)<{ isBig: boolean }>`
  height: ${(props) => (props.isBig ? '64px' : '48px')};
  width: ${(props) => (props.isBig ? '64px' : '48px')};
  border-radius: ${(props) => (props.isBig ? '4px' : '24px')};
  display: flex;
  flex-direction: row;
`;

const S_ReuserCover = styled.View<{ isBig: boolean }>`
  height: ${(props) => (props.isBig ? '64px' : '48px')};
  width: ${(props) => (props.isBig ? '64px' : '48px')};
  border-radius: ${(props) => (props.isBig ? '4px' : '24px')};
  background-color: ${(props) => props.theme.background.action};
`;

const S_Infos = styled.View`
  height: 48px;
  width: 100%;
  margin-left: 16px;
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

const S_Name = styled.Text<{ isBig: boolean }>`
  width: ${(props) => width - (props.isBig ? 84 : 64) - 64 + 'px'};
  font-size: 14px;
  font-family: 'gatwick-bold';
  color: ${(props) => props.theme.fontColor.primary};
`;

const S_DistanceAndFollowersCount = styled.Text`
  margin-top: 14px;
  font-family: 'neue';
  margin-top: 8px;
  color: ${(props) => props.theme.fontColor.primary};
`;
