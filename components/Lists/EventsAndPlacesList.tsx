import React, { useCallback, useMemo } from 'react';
import {
  IEventMinimize,
  IPlaceMinimize,
  IUser,
  ROLE,
} from '../../services/types';
import { FlashList } from '@shopify/flash-list';
import { BlockEvent } from '../BlockEvents/BlockEvent';
import { SectionHeader } from '../Section/SectionHeader';
import { BlockPlace } from '../BlockPlaces/BlockPlace';
import { BlockUser } from '../BlockUser/BlockUser';

export type dataType = IEventMinimize | IPlaceMinimize | IUser | string;
interface EventsAndPlacesListList {
  data: dataType[];
}
export const EventsAndPlacesList = ({ data }: EventsAndPlacesListList) => {
  const renderItems = useCallback(({ item }: { item: dataType }) => {
    if (typeof item === 'string') {
      return <SectionHeader notHorizontalPadding title={item} />;
    } else if ('beginAt' in item) {
      return <BlockEvent style={{ marginTop: 16 }} event={item} isBig />;
    } else if ('role' in item) {
      return <BlockUser style={{ marginTop: 16 }} user={item} isBig />;
    } else {
      return (
        <BlockPlace style={{ marginTop: 16 }} place={item as IPlaceMinimize} />
      );
    }
  }, []);

  const stickyHeaderIndices = useMemo(
    () =>
      data
        .map((item, index) => {
          if (typeof item === 'string') {
            return index;
          } else {
            return null;
          }
        })
        .filter((item) => item !== null) as number[],
    [data],
  );

  return (
    <FlashList<dataType>
      data={data}
      showsVerticalScrollIndicator={false}
      renderItem={renderItems}
      stickyHeaderIndices={stickyHeaderIndices}
      contentContainerStyle={{
        paddingBottom: 112,
      }}
      keyExtractor={(item) =>
        typeof item === 'string' ? item : item.id + item.name
      }
      estimatedItemSize={56}
    />
  );
};
