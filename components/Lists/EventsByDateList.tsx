import React, { useCallback, useMemo } from 'react';
import { IEventMinimize } from '../../services/types';
import { FlashList, FlashListProps } from '@shopify/flash-list';
import { BlockEvent } from '../BlockEvents/BlockEvent';
import { SectionHeader } from '../Section/SectionHeader';
import dayjs from 'dayjs';
import Animated, { FadeInRight } from 'react-native-reanimated';
import { capitalizeFirstLetter } from '../../helpers/string';
import { View } from 'react-native';

type Type = IEventMinimize | string;

interface EventsByDateList extends Partial<FlashListProps<Type>> {
  events: IEventMinimize[];
}
export const EventsByDateList = ({ events, ...props }: EventsByDateList) => {
  const data = useMemo(() => {
    const today = dayjs();
    const tomorrow = today.add(1, 'day');

    if (!events.length) {
      return [];
    }

    const eventsToSort: IEventMinimize[] = events.map((e) => ({ ...e }));
    eventsToSort.sort((a, b) => {
      return new Date(a.beginAt).valueOf() - new Date(b.beginAt).valueOf();
    });

    const days: (IEventMinimize | string)[] = [];
    let currentDate: string | null = null;

    for (const event of eventsToSort) {
      let eventDate;

      if (today.isSame(event.beginAt, 'day')) {
        eventDate = `Aujourd'hui`;
      } else if (tomorrow.isSame(event.beginAt, 'day')) {
        eventDate = `Demain`;
      } else {
        eventDate = dayjs(event.beginAt).format('dddd D MMM');
      }

      if (eventDate !== currentDate) {
        currentDate = eventDate;

        days.push(eventDate, event);
        continue;
      }

      days.push(event);
    }

    return days;
  }, [events]);

  const renderItems = useCallback(
    ({ item, index }: { item: IEventMinimize | string; index: number }) => {
      return typeof item === 'string' ? (
        <Animated.View entering={FadeInRight.delay(index * 50).duration(300)}>
          <SectionHeader
            notHorizontalPadding
            title={capitalizeFirstLetter(item)}
          />
        </Animated.View>
      ) : (
        <Animated.View entering={FadeInRight.delay(index * 50).duration(300)}>
          <BlockEvent style={{ marginTop: 16 }} event={item} isBig />
        </Animated.View>
      );
    },
    [],
  );

  const stickyHeaderIndices = useMemo(
    () =>
      data
        .map((item, index) => {
          if (typeof item === 'string') {
            return index;
          } else {
            return null;
          }
        })
        .filter((item) => item !== null) as number[],
    [data],
  );

  return (
    <FlashList<Type>
      data={data}
      showsVerticalScrollIndicator={false}
      renderItem={renderItems}
      stickyHeaderIndices={stickyHeaderIndices}
      contentContainerStyle={{
        paddingBottom: 112,
      }}
      ItemSeparatorComponent={(item) =>
        typeof item === 'string' ? <View style={{ height: 16 }} /> : <></>
      }
      keyExtractor={(item) =>
        typeof item === 'string' ? item : item.id + item.name
      }
      estimatedItemSize={56}
      {...props}
    />
  );
};
