import React, { FC, RefObject } from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  Pressable,
  View,
} from 'react-native';
import { height, width } from '../../constants/Layout';
import { useTheme } from '../../provider/ThemeProvider';
import Animated, {
  Extrapolation,
  interpolate,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import BottomSheet, {
  BottomSheetBackdrop,
  BottomSheetModal,
} from '@gorhom/bottom-sheet';
import { Theme } from '../../constants/theme';
import { useMainTheme } from '../../hooks/useMainTheme';
import { BottomSheetMethods } from '@gorhom/bottom-sheet/lib/typescript/types';

export type SelectedDate = {
  START_DATE: Date | null;
  END_DATE: Date | null;
};
interface ModalProfilProps {
  onClose: () => void;
  onLogOut: () => void;
  onDelete: () => void;
  refBottomSheet: RefObject<BottomSheetModal>;
}

export const ModalProfil: FC<ModalProfilProps> = ({
  onClose,
  onLogOut,
  onDelete,
  refBottomSheet,
}) => {
  const position = useSharedValue(0);
  const { theme } = useTheme();
  const styles = useMainTheme(createStyles);

  const styleBackground = useAnimatedStyle(() => {
    return {
      opacity: interpolate(position.value, [0, 1], [0, 1], Extrapolation.CLAMP),
      transform: [
        {
          translateY: interpolate(
            position.value,
            [0, 0.001],
            [height, 0],
            Extrapolation.CLAMP,
          ),
        },
      ],
    };
  });

  return (
    <BottomSheetModal
      ref={refBottomSheet}
      key="profil"
      name="profil"
      style={styles.S_BottomSheet}
      index={0}
      snapPoints={['90%']}
      enablePanDownToClose
      backgroundStyle={{
        backgroundColor: theme.background.primary,
      }}
      handleStyle={{
        backgroundColor: theme.background.primary,
      }}
      handleIndicatorStyle={{
        backgroundColor: theme.fontColor.primary,
        width: 40,
      }}
      backdropComponent={(props) => (
        <BottomSheetBackdrop
          {...props}
          opacity={0.7}
          enableTouchThrough={false}
          appearsOnIndex={0}
          disappearsOnIndex={-1}
          style={[
            { backgroundColor: 'rgba(0, 0, 0, 1)' },
            StyleSheet.absoluteFillObject,
          ]}
        />
      )}
    >
      <View style={styles.buttons}>
        <TouchableOpacity style={styles.button} onPress={onLogOut}>
          <Text style={styles.text}>DÉCONNEXION</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={onDelete}>
          <Text style={styles.text}>SUPPRIMER LE COMPTE</Text>
        </TouchableOpacity>
      </View>
    </BottomSheetModal>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_BottomSheet: {
      flex: 1,
      paddingBottom: 0,
      borderRadius: 32,
      overflow: 'hidden',
      height,
      justifyContent: 'flex-end',
      zIndex: 10,
    },
    blur: {
      ...StyleSheet.absoluteFillObject,
      zIndex: 10,
      flex: 1,
      height,
      backgroundColor: 'rgba(0,0,0,0.8)',
    },
    container: {
      paddingTop: 24,
      justifyContent: 'flex-end',
      paddingHorizontal: 20,
    },
    buttons: {
      marginTop: 24,
      justifyContent: 'space-between',
      paddingHorizontal: 20,
    },
    button: {
      height: 48,
      borderRadius: 24,
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 16,
      backgroundColor: '#FFF',
      marginBottom: 16,
    },
    confirm: {
      backgroundColor: theme.fontColor.action,
    },
    text: {
      fontFamily: 'neue-bold',
      fontSize: 14,
    },
    cancelText: {
      fontFamily: 'neue-bold',
      fontSize: 14,
      color: theme.fontColor.primary,
    },
  });
