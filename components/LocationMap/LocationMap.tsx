import React, { useCallback } from 'react';
import { StyleSheet } from 'react-native';
import { useOpenMap } from '../../hooks/useOpenMap';
import Mapbox, { CircleLayer, ShapeSource } from '@rnmapbox/maps';
import { useMainTheme } from '../../hooks/useMainTheme';
import { layerStyles } from '../SearchMap/Map';
import { useLocationContext } from '../../provider/LocationProvider';

interface LocationMapProps {
  latitude: number;
  longitude: number;
}

export const LocationMap = ({ latitude, longitude }: LocationMapProps) => {
  const styles = useMainTheme(createStyles);
  const onClickOpenMap = useOpenMap();

  if (!latitude && !longitude) {
    return <></>;
  }

  const centerCoordinate = [longitude, latitude];

  return (
    <Mapbox.MapView
      styleURL="mapbox://styles/aurelienrou/clqft7liu00j001pjgv949agd"
      style={styles.map}
      rotateEnabled={false}
      pitchEnabled={false}
      zoomEnabled={false}
      scrollEnabled={false}
      logoEnabled={false}
      scaleBarEnabled={false}
      compassEnabled={false}
      attributionEnabled={false}
      onPress={() => onClickOpenMap(latitude, longitude)}
    >
      <Mapbox.Camera
        defaultSettings={{
          zoomLevel: 13.5,
          centerCoordinate,
        }}
        minZoomLevel={4}
      />
      <ShapeSource
        id="shape-source"
        shape={{
          type: 'Point',
          coordinates: centerCoordinate,
        }}
      >
        <CircleLayer id="circle-layer" style={layerStyles.currentSinglePoint} />
      </ShapeSource>
    </Mapbox.MapView>
  );
};

const createStyles = () =>
  StyleSheet.create({
    map: {
      marginTop: 16,
      height: 200,
      width: '100%',
      borderRadius: 10,
      overflow: 'hidden',
    },
  });
