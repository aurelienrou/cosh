import React from 'react';
import { useLocationContext } from '../../provider/LocationProvider';
import { CircleLayer, CircleLayerStyle, ShapeSource } from '@rnmapbox/maps';

const layerStyles: {
  currentLocation: CircleLayerStyle;
} = {
  currentLocation: {
    circleColor: '#4BFF47',
    circleStrokeColor: '#4BFF471F',
    circleRadius: 3,
    circleStrokeWidth: 7,
    circlePitchAlignment: 'map',
  },
};

export const MyLocation = () => {
  const { location } = useLocationContext();

  return (
    <ShapeSource
      id="myLocation"
      cluster
      clusterRadius={50}
      clusterMaxZoomLevel={14}
      shape={{
        type: 'Feature',
        properties: {
          myLocation: true,
        },
        geometry: {
          type: 'Point',
          coordinates: [location.longitude, location.latitude],
        },
      }}
    >
      <CircleLayer
        id="myLocation"
        filter={['has', 'myLocation']}
        style={layerStyles.currentLocation}
      />
    </ShapeSource>
  );
};
