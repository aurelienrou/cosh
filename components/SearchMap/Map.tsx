import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Pressable, StyleSheet, View } from 'react-native';
import { Feature, GeoJsonProperties, Geometry } from 'geojson';
import Mapbox, {
  CircleLayer,
  CircleLayerStyle,
  ShapeSource,
  SymbolLayer,
  SymbolLayerStyle,
} from '@rnmapbox/maps';
import { useAppSelector } from '../../store';
import { FlashList } from '@shopify/flash-list';
import { width } from '../../constants/Layout';
import Animated, { FadeIn, FadeInUp } from 'react-native-reanimated';
import { BlockEvent } from '../BlockEvents/BlockEvent';
import { BlockPlace } from '../BlockPlaces/BlockPlace';
import { useMainTheme } from '../../hooks/useMainTheme';
import { Theme, blurAmount } from '../../constants/theme';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Image } from 'expo-image';
import { BlurView } from '@react-native-community/blur';
import { useLocationContext } from '../../provider/LocationProvider';
import { MyLocation } from './MyLocation';

export const DEFAULT_REGION = [2.338085286319256, 48.86266513510206];

export const layerStyles: {
  singlePoint: CircleLayerStyle;
  currentSinglePoint: CircleLayerStyle;
  selectedPoint: CircleLayerStyle;
  currentLocation: CircleLayerStyle;
  clusteredPoints: CircleLayerStyle;
  clusterCount: SymbolLayerStyle;
} = {
  singlePoint: {
    circleColor: '#f1bbce',
    circlePitchAlignment: 'map',
  },
  currentSinglePoint: {
    circleColor: '#f0699b',
    circlePitchAlignment: 'map',
  },

  currentLocation: {
    circleColor: '#4BFF47',
    circleStrokeColor: '#4BFF471F',
    circleRadius: 3,
    circleStrokeWidth: 7,
    circlePitchAlignment: 'map',
  },

  selectedPoint: {
    circleColor: '#fff',
    circlePitchAlignment: 'map',
  },

  clusteredPoints: {
    circlePitchAlignment: 'map',
    circleColor: [
      'step',
      ['get', 'point_count'],
      '#f1bbce',
      5,
      '#f3a4c1',
      15,
      '#ee86ac',
      30,
      '#f0699b',
    ],
    circleRadius: ['step', ['get', 'point_count'], 10, 10, 12, 15, 15, 20, 20],
  },

  clusterCount: {
    textField: ['get', 'point_count'],
    textSize: 12,
    textOffset: [0, 0.2],
    textFont: ['Gatwick Bold'],
    textPitchAlignment: 'map',
  },
};

export const Map = () => {
  const { location } = useLocationContext();
  const data = useAppSelector((state) => state.map.data);
  const styles = useMainTheme(createStyles);
  const shapeSource = useRef<ShapeSource>(null);
  const cameraRef = useRef<Mapbox.Camera>(null);
  const { bottom } = useSafeAreaInsets();

  const miniatureRef =
    useRef<FlashList<Feature<Geometry, GeoJsonProperties>>>(null);
  const [selectedFeatureId, setSelectedFeatureId] = useState();

  const handlePressCluster = useCallback(
    async (pressedShape: {
      features: [any] | Feature<Geometry, GeoJsonProperties>[];
    }) => {
      if (shapeSource.current && cameraRef.current) {
        try {
          const [cluster] = pressedShape.features;

          const zoomLevel = await shapeSource.current.getClusterExpansionZoom(
            cluster,
          );
          cameraRef.current.setCamera({
            centerCoordinate: cluster.geometry.coordinates,
            zoomLevel,
            animationDuration: 200,
          });
        } catch {
          cameraRef.current.setCamera({
            centerCoordinate: pressedShape.features[0].geometry.coordinates,
            zoomLevel: 16,
            animationDuration: 400,
          });

          if (miniatureRef.current) {
            miniatureRef.current.scrollToOffset({
              offset: pressedShape.features[0].properties?.index * (width - 24),
              animated: false,
            });
          }

          setSelectedFeatureId(pressedShape.features[0].properties?.id);
        }
      }
    },
    [],
  );
  const renderItem = useCallback(({ item: { properties } }) => {
    if ('minPrice' in properties) {
      return (
        <Animated.View
          style={styles.containerBlock}
          entering={FadeInUp.duration(300)}
        >
          <BlurView style={styles.containerBlock} blurAmount={blurAmount}>
            <BlockEvent
              style={styles.blockEvent}
              event={properties}
              isBig={false}
            />
          </BlurView>
        </Animated.View>
      );
    }

    return (
      <Animated.View
        style={styles.containerBlock}
        entering={FadeInUp.duration(300)}
      >
        <BlurView style={styles.containerBlock} blurAmount={blurAmount}>
          <BlockPlace
            style={styles.blockPlace}
            isBig={true}
            place={properties}
          />
        </BlurView>
      </Animated.View>
    );
  }, []);

  const onScroll = useCallback(
    (event) => {
      if (cameraRef.current) {
        const index = Math.round(
          event.nativeEvent.contentOffset.x / (width - 24),
        );
        setSelectedFeatureId(data.features[index].properties?.id);

        cameraRef.current.setCamera({
          centerCoordinate: [
            data.features[index].properties?.location[1],
            data.features[index].properties?.location[0],
          ],
          zoomLevel: 16,
          animationDuration: 400,
        });
      }
    },
    [data],
  );

  useEffect(() => {
    if (data.features.length && cameraRef.current && miniatureRef.current) {
      miniatureRef.current.scrollToOffset({
        offset: 0,
        animated: true,
      });
      setSelectedFeatureId(data.features[0].properties?.id);
      cameraRef.current.setCamera({
        centerCoordinate: DEFAULT_REGION,
        zoomLevel: 10.5,
        animationDuration: 200,
      });
    }
  }, [data]);

  const handleNearMe = () => {
    cameraRef.current?.setCamera({
      centerCoordinate: [location.longitude, location.latitude],
      zoomLevel: 14.5,
      animationDuration: 300,
    });
  };

  return (
    <Animated.View style={styles.container} entering={FadeIn.duration(300)}>
      {data.features.length ? (
        <View style={[styles.flatlistContainer, { bottom: 100 + bottom }]}>
          <BlurView style={styles.nearButton} blurAmount={blurAmount}>
            <Pressable onPress={handleNearMe}>
              <Image
                style={styles.near}
                source={require('./../../assets/icons/nearme.svg')}
              />
            </Pressable>
          </BlurView>
          <FlashList
            ref={miniatureRef}
            estimatedItemSize={width - 24}
            data={data.features}
            renderItem={renderItem}
            horizontal
            scrollEventThrottle={100}
            showsHorizontalScrollIndicator={false}
            disableIntervalMomentum
            snapToInterval={width - 24}
            decelerationRate="fast"
            onMomentumScrollEnd={onScroll}
            contentContainerStyle={{ paddingLeft: 16 }}
          />
        </View>
      ) : null}

      <Mapbox.MapView
        styleURL="mapbox://styles/aurelienrou/clqft7liu00j001pjgv949agd"
        style={styles.container}
        rotateEnabled={false}
        logoEnabled={false}
        scaleBarEnabled={false}
        compassEnabled={false}
      >
        <Mapbox.Camera
          ref={cameraRef}
          defaultSettings={{
            zoomLevel: 10.5,
            centerCoordinate: DEFAULT_REGION,
          }}
          minZoomLevel={4}
        />
        <ShapeSource
          ref={shapeSource}
          id="earthquakes"
          onPress={handlePressCluster}
          cluster
          clusterRadius={50}
          clusterMaxZoomLevel={14}
          shape={data}
        >
          <SymbolLayer id="pointCount" style={layerStyles.clusterCount} />

          <CircleLayer
            id="clusteredPoints"
            belowLayerID="pointCount"
            filter={['has', 'point_count']}
            style={layerStyles.clusteredPoints}
          />

          <CircleLayer
            id="singlePoint"
            filter={['!', ['has', 'point_count']]}
            style={layerStyles.singlePoint}
          />

          {selectedFeatureId ? (
            <CircleLayer
              id="selectedPoint"
              sourceID="circle-source"
              filter={['==', 'id', selectedFeatureId]}
              style={layerStyles.selectedPoint}
            />
          ) : (
            <></>
          )}
        </ShapeSource>
        <MyLocation />
      </Mapbox.MapView>
    </Animated.View>
  );
};

const createStyles = (theme: Theme) => {
  return StyleSheet.create({
    container: {
      flex: 1,
      position: 'relative',
    },
    logo: {
      ...StyleSheet.absoluteFillObject,
      width: 60,
      height: 60,
      top: 300,
      left: width / 2 - 30,
      zIndex: 1000,
    },
    contend: {
      marginLeft: 20,
    },
    containerBlock: {
      height: 96,
      width: width - 32,
      marginRight: 8,
      alignItems: 'center',
      borderRadius: 16,
    },
    flatlistContainer: {
      height: 96,
      width: width,
      zIndex: 1,
      position: 'absolute',
    },
    blockEvent: {
      height: 96,
      width: width - 32,
      padding: 16,
      borderRadius: 16,
    },
    blockPlace: {
      height: 96,
      width: width - 32,
      padding: 16,
      borderRadius: 16,
      alignItems: 'center',
      // backgroundColor: theme.background.primary,
    },
    nearButton: {
      width: 48,
      height: 48,
      borderRadius: 24,
      position: 'absolute',
      right: 20,
      top: -64,
      zIndex: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
    near: {
      width: 28,
      height: 28,
      transform: [{ rotate: '-45deg' }],
    },
  });
};
