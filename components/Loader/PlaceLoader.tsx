import { memo } from 'react';
import React, { View, StyleSheet } from 'react-native';
import { width } from '../../constants/Layout';

import ContentLoader, { Circle, Rect } from 'react-content-loader/native';

const styles = StyleSheet.create({
  S_ContainerLoader: {
    marginTop: 16,
  },
});
const Loader = ({ style }: { style?: View['props']['style'] }) => {
  return (
    <View>
      {Array(5)
        .fill(0)
        .map((number, index) => (
          <View style={styles.S_ContainerLoader} key={number + index}>
            <ContentLoader
              speed={1}
              width={width}
              height={48}
              viewBox={`0 0 ${width} 48`}
              backgroundColor="#242627"
              foregroundColor="#5b5b5b"
            >
              <Circle x="24" y="24" r="24" />
              <Rect x="56" y="12" rx="2" ry="2" width={width / 3} height="8" />
              <Rect x="56" y="32" rx="2" ry="2" width={width / 2} height="8" />
            </ContentLoader>
          </View>
        ))}
    </View>
  );
};
export const PlacesLoader = memo(Loader);
