import React, { StyleSheet } from 'react-native';
import Animated, {
  Easing,
  FadeIn,
  FadeOut,
  cancelAnimation,
  useAnimatedStyle,
  useSharedValue,
  withDelay,
  withRepeat,
  withSequence,
  withSpring,
  withTiming,
} from 'react-native-reanimated';
import { useMainTheme } from '../../hooks/useMainTheme';
import { useEffect } from 'react';
import { width } from '../../constants/Layout';
import { BlurView } from '@react-native-community/blur';
import { Theme } from '../../constants/theme';

export const AnimatedDot = ({ delay }: { delay: number }) => {
  const styles = useMainTheme(createStyles);

  const progress = useSharedValue(0.4);
  const style = useAnimatedStyle(
    () => ({
      transform: [
        {
          scale: progress.value,
        },
      ],
    }),
    [progress.value],
  );

  useEffect(() => {
    progress.value = withDelay(
      delay,
      withRepeat(withTiming(1, { duration: 1000 }), -1, true),
    );
    return () => cancelAnimation(progress);
  }, []);
  return <Animated.View style={[style, styles.dot]} />;
};

export const MapLoader = () => {
  const styles = useMainTheme(createStyles);

  return (
    <Animated.View
      style={[styles.container]}
      entering={FadeIn.duration(300)}
      exiting={FadeOut.duration(300)}
    >
      <BlurView style={styles.blur}>
        <AnimatedDot delay={0} />
        <AnimatedDot delay={500} />
        <AnimatedDot delay={1000} />
      </BlurView>
    </Animated.View>
  );
};

const createStyles = (theme: Theme) => {
  return StyleSheet.create({
    container: {
      position: 'absolute',
      zIndex: 1,
      top: 250,
      left: width / 2 - 48,
      height: 48,
      width: 48,
      borderRadius: 25,
    },
    blur: {
      alignItems: 'center',
      justifyContent: 'center',
      height: 32,
      width: 96,
      paddingHorizontal: 16,
      borderRadius: 25,
      flexDirection: 'row',
    },
    dot: {
      width: 16,
      height: 16,
      backgroundColor: theme.fontColor.action,
      borderRadius: 12,
      marginHorizontal: 4,
    },
  });
};
