import React from 'react';
import ContentLoader, { Rect } from 'react-content-loader/native';
import { width } from '../../constants/Layout';
import { View } from 'react-native';

export const TitleLoader = () => {
  return (
    <View>
      <ContentLoader
        speed={1}
        width={width}
        height={24}
        viewBox={`0 0 ${width} 24`}
        backgroundColor="#242627"
        foregroundColor="#5b5b5b"
      >
        <Rect x="0" y="0" rx="4" ry="4" width={width / 2} height="24" />
      </ContentLoader>
    </View>
  );
};
