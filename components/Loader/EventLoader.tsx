import { memo } from 'react';
import React, { View, StyleSheet } from 'react-native';
import { width } from '../../constants/Layout';

import ContentLoader, { Rect } from 'react-content-loader/native';

const styles = StyleSheet.create({
  S_ContainerLoader: {
    marginTop: 16,
  },
});
const Loader = () => {
  return (
    <>
      {Array(10)
        .fill(0)
        .map((number, index) => (
          <View style={styles.S_ContainerLoader} key={number + index}>
            <ContentLoader
              speed={1}
              width={width}
              height={84}
              viewBox={`0 0 ${width} 84`}
              backgroundColor="#242627"
              foregroundColor="#5b5b5b"
            >
              <Rect x="100" y="6" rx="2" ry="2" width={width / 2} height="8" />
              <Rect x="100" y="28" rx="2" ry="2" width={width / 3} height="8" />
              <Rect x="100" y="50" rx="2" ry="2" width="40" height="8" />
              <Rect x="100" y="72" rx="2" ry="2" width="80" height="8" />
              <Rect x="0" y="0" rx="8" ry="8" width="84" height="84" />
            </ContentLoader>
          </View>
        ))}
    </>
  );
};

export const EventsLoader = memo(Loader);
