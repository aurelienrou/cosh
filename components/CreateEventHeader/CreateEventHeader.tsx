import React, { FC } from 'react';
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import Animated, {
  interpolate,
  SharedValue,
  useAnimatedStyle,
} from 'react-native-reanimated';
import CoshText from '../CoshText/CoshText';

interface CreateEventFormProps {
  onClose: () => void;
  onBackPlaceForm: () => void;
  progress: SharedValue<number>;
}

const CreateEventHeader: FC<CreateEventFormProps> = ({
  onClose,
  progress,
  onBackPlaceForm,
}) => {
  const translateXArrowAnimated = useAnimatedStyle(() => ({
    transform: [{ translateX: interpolate(progress.value, [0, 1], [-100, 0]) }],
  }));

  const translateYTextAnimated = useAnimatedStyle(() => ({
    transform: [{ translateY: interpolate(progress.value, [0, 1], [0, -50]) }],
    opacity: interpolate(progress.value, [0, 0.5, 1], [1, 0, 1]),
  }));

  return (
    <View style={styles.header}>
      <Animated.View style={[styles.iconContainer, translateXArrowAnimated]}>
        <TouchableOpacity
          style={styles.iconContainer}
          onPress={onBackPlaceForm}
        >
          <Image
            style={styles.icon}
            source={require('../../assets/images/left-arrow.png')}
          />
        </TouchableOpacity>
      </Animated.View>
      <Animated.View
        style={[styles.headerTextContainer, translateYTextAnimated]}
      >
        <View style={styles.titleContainer}>
          <CoshText style={styles.title}>Creer un event</CoshText>
        </View>
        <View style={styles.titleContainer}>
          <CoshText style={styles.title}>Ajouter un lieu</CoshText>
        </View>
      </Animated.View>

      <TouchableOpacity style={styles.iconContainer} onPress={onClose}>
        <Image
          style={styles.icon}
          source={require('../../assets/images/close.png')}
        />
      </TouchableOpacity>
    </View>
  );
};

export default CreateEventHeader;

const styles = StyleSheet.create({
  header: {
    height: 50,
    borderBottomColor: '#e2e2e2',
    borderBottomWidth: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingHorizontal: 20,
    justifyContent: 'space-between',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  iconContainer: {
    width: 30,
    height: 50,
    marginRight: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: { width: 15, height: 15 },
  headerTextContainer: { height: 100 },
  titleContainer: { height: 50, justifyContent: 'center' },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
