import React from 'react';
import { IPlaceMinimize } from '../../services/types';
import { BlockPlace } from './BlockPlace';
import styled from 'styled-components/native';
import { StyleProp, ViewStyle, View } from 'react-native';
import ContentLoader, { Circle, Rect } from 'react-content-loader/native';
import { width } from '../../constants/Layout';

interface BlockPlacesProps {
  places: IPlaceMinimize[];
  loading: boolean;
  style?: StyleProp<ViewStyle>;
}

const Loader = () => (
  <ContentLoader
    speed={1}
    width={476}
    height={48}
    viewBox="0 0 476 48"
    backgroundColor="#242627"
    foregroundColor="#5b5b5b"
  >
    <Rect x="62" y="6" rx="2" ry="2" width={width / 2} height="8" />
    <Rect x="62" y="34" rx="2" ry="2" width={width / 3} height="8" />
    <Circle cx="24" cy="24" r="24" />
  </ContentLoader>
);

export const BlockPlaces = ({ places, style, loading }: BlockPlacesProps) => {
  return (
    <S_Container style={style}>
      {loading
        ? Array(5)
            .fill(0)
            .map((number, index) => (
              <S_ContainerLoader key={number + index}>
                <Loader key={number + index} />
              </S_ContainerLoader>
            ))
        : places.map((place, index) => (
            <BlockPlace key={`${index}-${place.id}`} place={place} />
          ))}
    </S_Container>
  );
};

const S_Container = styled(View)`
  padding: 20 20px 0 20px;
`;

const S_ContainerLoader = styled(View)`
  margin-bottom: 16px;
`;
