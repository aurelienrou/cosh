import React, {
  StyleSheet,
  NativeSyntheticEvent,
  Text,
  TextLayoutEventData,
} from 'react-native';
import { useState, useCallback } from 'react';
import { useMainTheme } from '../../hooks/useMainTheme';
import { Theme } from '../../constants/theme';

const NUMBER_OF_VISIBLE_LINES = 10;

interface DescriptionProps {
  text: string;
}

export const Description = ({ text }: DescriptionProps) => {
  const [textShown, setTextShown] = useState(false);
  const [lengthMore, setLengthMore] = useState(false);
  const styles = useMainTheme(createStyles);
  const toggleNumberOfLines = () => {
    setTextShown(!textShown);
  };

  const onTextLayout = useCallback(
    (e: NativeSyntheticEvent<TextLayoutEventData>) => {
      setLengthMore(e.nativeEvent.lines.length >= NUMBER_OF_VISIBLE_LINES);
    },
    [],
  );

  return (
    <>
      <Text
        style={styles.S_Description}
        onTextLayout={onTextLayout}
        numberOfLines={textShown ? undefined : NUMBER_OF_VISIBLE_LINES}
      >
        {text}
      </Text>

      {lengthMore ? (
        <Text style={[styles.S_ReadMore]} onPress={toggleNumberOfLines}>
          {textShown ? 'VOIR MOINS' : 'VOIR PLUS'}
        </Text>
      ) : null}
    </>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_ReadMore: {
      marginTop: 16,
      fontSize: 14,
      fontFamily: 'neue-bold',
      color: '#494949',
    },
    S_Description: {
      fontSize: 14,
      lineHeight: 20,
      marginTop: 16,
      fontFamily: 'neue',
      color: theme.fontColor.primary,
    },
  });
