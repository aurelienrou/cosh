import React, { useCallback } from 'react';
import styled from 'styled-components/native';
import Animated, {
  interpolateColor,
  useAnimatedStyle,
  useDerivedValue,
  withSpring,
} from 'react-native-reanimated';
import { StyleProp, ViewStyle } from 'react-native';
import { capitalizeFirstLetter } from '../../helpers/string';
import { useTheme } from 'styled-components';

interface SelectorLineProps {
  name: string;
  isSelected: boolean;
  onSelect: (name: string) => void;
  style?: StyleProp<ViewStyle>;
}
export const SelectorLine = ({
  name,
  isSelected,
  onSelect,
  style,
}: SelectorLineProps) => {
  const theme = useTheme();
  const progress = useDerivedValue(() => {
    return withSpring(isSelected ? 1 : 0);
  });
  const bgStyle = useAnimatedStyle(() => {
    const backgroundColor = interpolateColor(
      progress.value,
      [0, 1],
      [theme.background.primary, theme.fontColor.primary],
    );

    const borderColor = interpolateColor(
      progress.value,
      [0, 1],
      ['#494949', theme.fontColor.primary],
    );

    return {
      backgroundColor,
      borderColor,
    };
  });

  const handleSelect = useCallback(() => {
    onSelect(name);
  }, []);

  return (
    <S_Pessable style={[style]} onPress={handleSelect}>
      <S_Name numberOfLines={1}>
        {capitalizeFirstLetter(name.replace('bar-a-', '').replace('bar-', ''))}
      </S_Name>
      <S_Checkbox style={[bgStyle, style]} />
    </S_Pessable>
  );
};

const S_Pessable = styled.Pressable`
  width: 100%;
  height: 40px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const S_Checkbox = styled(Animated.View)`
  width: 24px;
  height: 24px;
  border-radius: 4px;
  border-width: 1px;
`;

const S_Name = styled.Text`
  font-family: 'neue';
  font-size: 16px;
  color: ${(props) => props.theme.fontColor.primary};
`;
