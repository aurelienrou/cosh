import React, { useState } from 'react';
import styled from 'styled-components/native';
import Animated, {
  interpolateColor,
  useAnimatedStyle,
  useDerivedValue,
  withSpring,
} from 'react-native-reanimated';
import { StyleProp, ViewStyle } from 'react-native';
import { Image } from 'expo-image';
import { capitalizeFirstLetter } from '../../helpers/string';
import Colors from '../../constants/Colors';
import { useTheme } from 'styled-components';

interface SelectorSquare {
  name: string;
  isSelected: boolean;
  onSelect: () => void;
  style?: StyleProp<Animated.AnimateStyle<StyleProp<ViewStyle>>>;
}
export const SelectorSquare = ({
  name,
  isSelected,
  onSelect,
  style,
}: SelectorSquare) => {
  const theme = useTheme();

  const progress = useDerivedValue(() => {
    return withSpring(isSelected ? 1 : 0);
  });

  const bgStyle = useAnimatedStyle(() => {
    const backgroundColor = interpolateColor(
      progress.value,
      [0, 1],
      [theme.background.primary, theme.fontColor.primary],
    );

    const borderColor = interpolateColor(
      progress.value,
      [0, 1],
      [theme.fontColor.secondary, theme.fontColor.primary],
    );

    return {
      backgroundColor,
      borderColor,
    };
  });

  const nameStyle = useAnimatedStyle(() => {
    const color = interpolateColor(
      progress.value,
      [0, 1],
      [Colors.light.background, Colors.dark.background],
    );

    return { color };
  });

  return (
    <S_Container style={[bgStyle, style]}>
      <S_Pessable onPress={onSelect}>
        <S_Icon />
        <S_Name style={[nameStyle]} numberOfLines={1}>
          {capitalizeFirstLetter(name)}
        </S_Name>
      </S_Pessable>
    </S_Container>
  );
};

const S_Pessable = styled.Pressable`
  width: 100px;
  height: 100px;
`;

const S_Container = styled(Animated.View)`
  width: 100px;
  height: 100px;
  border-radius: 8px;
  border-color: ${(props) => props.theme.fontColor.secondary};
  border-width: 1px;
`;

const S_Icon = styled(Image)`
  position: absolute;
  width: 100px;
`;

const S_Name = styled(Animated.Text)`
  position: absolute;
  width: 84px;
  bottom: 10px;
  left: 8px;
  font-family: 'neue-bold';
  font-size: 16px;
`;
