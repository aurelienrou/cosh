import {
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  Keyboard,
  TextStyle,
  View,
  Pressable,
} from 'react-native';
import React, {
  Dispatch,
  FC,
  SetStateAction,
  useEffect,
  useState,
} from 'react';
import { height, width } from '../../constants/Layout';
import {
  getAddressFromText,
  getAddressFromPlaceId,
} from '../../services/googleGeocode';
import { AddressGoogle, AddressGoogleResults } from '../../services/types';
import { Image } from 'expo-image';
import Animated, {
  Easing,
  FadeIn,
  FadeOut,
  SharedValue,
  interpolate,
  runOnJS,
  runOnUI,
  useAnimatedKeyboard,
  useAnimatedStyle,
  useDerivedValue,
  useSharedValue,
  withDecay,
  withDelay,
  withSpring,
  withTiming,
} from 'react-native-reanimated';

interface AddressFinderProps {
  onSelect: (address: AddressGoogle) => void;
  selectedAdress: string;
  setSelectedAdress: Dispatch<SetStateAction<string>>;
  onFocus: () => void;
  onBlur: () => void;
  resetAfterSelect?: boolean;
  resultsStyle?: TextStyle;
  placeholder?: string;
}

interface ResultProps {
  onSelect: () => void;
  index: number;
  keyboardHeight: SharedValue<number>;
  isOpen: boolean;
  res: AddressGoogleResults;
}

const HEIGHT_INPUT_MARGIN = 16;
const HEIGHT_RESULT_MARGIN = 8;
const HEIGHT_INPUT_OR_RESULT = 48;

const BUTTON_BOTTOM_MARGIN = 20;

const Result = ({ onSelect, res, index, isOpen }: ResultProps) => {
  const distance = useSharedValue(
    height -
      (336 + HEIGHT_INPUT_OR_RESULT * 3 + BUTTON_BOTTOM_MARGIN) -
      (HEIGHT_INPUT_OR_RESULT +
        HEIGHT_INPUT_MARGIN +
        (HEIGHT_INPUT_OR_RESULT + HEIGHT_RESULT_MARGIN) * (index + 1)),
  );

  const opacity = useAnimatedStyle(() => {
    return {
      opacity: interpolate(distance.value, [0, 112], [isOpen ? 0 : 1, 1]),
    };
  });

  return (
    <Animated.View
      entering={FadeIn.delay(index * 50).duration(200)}
      exiting={FadeOut.delay((5 - index) * 20)}
    >
      <Animated.View style={[opacity]}>
        <TouchableOpacity onPress={onSelect}>
          <View style={styles.result}>
            <Text numberOfLines={1} style={styles.text}>
              {res.description}
            </Text>
          </View>
        </TouchableOpacity>
      </Animated.View>
    </Animated.View>
  );
};

const AddressFinder: FC<AddressFinderProps> = ({
  onSelect,
  selectedAdress,
  setSelectedAdress,
  onFocus,
  onBlur,
  resetAfterSelect = false,
  placeholder,
}) => {
  const [results, setResults] = useState<AddressGoogleResults[]>([]);

  const handleSearchAddress = async (text: string) => {
    setSelectedAdress(text);

    if (text.trim() === '') {
      setResults([]);
      return;
    }

    try {
      const data = await getAddressFromText(text);

      setResults(data);
    } catch (err) {
      console.log(err);
    }
  };

  const handleSelectAddress = (result: AddressGoogleResults) => {
    getAddressFromPlaceId(result.place_id).then((address) => {
      onSelect(address);
    });

    setSelectedAdress(resetAfterSelect ? '' : result.description);
    setResults([]);
    Keyboard.dismiss();
  };

  const isFocus = useSharedValue(0);
  const onClear = () => {
    setResults([]);
    setSelectedAdress('');
  };

  const [isOpen, setIsOpen] = useState(false);

  const handleBlur = () => {
    onBlur();
    setIsOpen(false);
    isFocus.value = withSpring(0);
  };

  const handleFocus = () => {
    onFocus();
    setIsOpen(true);
    isFocus.value = withSpring(1);
  };

  const style = useAnimatedStyle(() => {
    return {
      opacity: interpolate(isFocus.value, [0, 1], [0, 1]),
    };
  });
  const keyboardHeight = useAnimatedKeyboard().height;

  return (
    <>
      <View>
        <TextInput
          style={styles.inputText}
          onChangeText={handleSearchAddress}
          value={selectedAdress}
          placeholder={placeholder}
          placeholderTextColor="#FFFFFF80"
          clearButtonMode="never"
          onBlur={handleBlur}
          onFocus={handleFocus}
        />
        <Animated.View style={[style, styles.iconContainer]}>
          <Pressable style={styles.button} onPress={onClear}>
            <Image
              style={[styles.icon]}
              source={require('./../../assets/icons/clear.svg')}
            />
          </Pressable>
        </Animated.View>
      </View>
      {results.length > 0 && (
        <View style={styles.results}>
          {results.map((res, index) => (
            <Result
              key={index}
              res={res}
              keyboardHeight={keyboardHeight}
              index={index}
              isOpen={isOpen}
              onSelect={() => handleSelectAddress(res)}
            />
          ))}
        </View>
      )}
    </>
  );
};

export default AddressFinder;

const styles = StyleSheet.create({
  inputText: {
    fontFamily: 'neue',
    fontSize: 16,
    height: 48,
    color: '#7f8383',
    marginBottom: 16,
  },
  results: {
    flex: 1,
  },
  result: {
    height: 48,
    borderRadius: 24,
    paddingHorizontal: 16,
    backgroundColor: '#FFFFFF1F',
    justifyContent: 'center',
    marginBottom: 8,
  },
  text: {
    color: '#FFF',
    fontFamily: 'neue-bold',
    fontSize: 14,
    width: width - 72,
  },
  iconContainer: {
    position: 'absolute',
    right: 0,
  },
  button: {
    width: 48,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 22,
    height: 22,
  },
});
