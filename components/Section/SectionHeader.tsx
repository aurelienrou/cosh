import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { Pressable, StyleSheet, Text, View } from 'react-native';
import { useMainTheme } from '../../hooks/useMainTheme';
import { useTheme } from '../../provider/ThemeProvider';
import { Theme } from '../../constants/theme';
import { width } from '../../constants/Layout';

interface SectionHeaderProps {
  title: string;
  notHorizontalPadding?: boolean;
  onPress?: () => void;
}
export const SectionHeader = ({
  title,
  onPress,
  notHorizontalPadding,
}: SectionHeaderProps) => {
  const { theme } = useTheme();
  const styles = useMainTheme(createStyle);

  return (
    <View style={[styles.S_Header]}>
      <View style={styles.infos}>
        <Text
          style={[
            styles.S_Title,
            { paddingHorizontal: notHorizontalPadding ? 0 : 20 },
          ]}
        >
          {title}
        </Text>
        {onPress && (
          <Pressable onPress={onPress}>
            <Text style={styles.S_Link}>SEE ALL</Text>
          </Pressable>
        )}
      </View>

      <LinearGradient
        style={styles.S_LinearGradient}
        colors={[theme.background.primary, '#15171800']}
      />
    </View>
  );
};

const createStyle = (theme: Theme) => {
  return StyleSheet.create({
    S_LinearGradient: {
      height: 14,
      width,
    },
    S_Header: {
      position: 'relative',
      height: 50,
      marginBottom: 8,
    },
    S_Title: {
      fontSize: 16,
      paddingHorizontal: 20,
      fontFamily: 'gatwick-bold',
      color: theme.fontColor.primary,
    },
    infos: {
      height: 60,
      paddingTop: 32,
      flexDirection: 'row',
      justifyContent: 'space-between',
      overflow: 'visible',
      backgroundColor: theme.background.primary,
    },
    S_Link: {
      fontSize: 14,
      fontFamily: 'neue-bold',
      color: theme.fontColor.secondary,
    },
  });
};
