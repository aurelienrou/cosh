import React from 'react';
import styled from 'styled-components/native';
import { SectionHeader } from './SectionHeader';

interface SectionProps {
  children: React.ReactNode;
  title: string;
  notHorizontalPadding?: boolean;
  noMarginTop?: boolean;
  onPress?: () => void;
}

export const Section = ({
  children,
  title,
  onPress,
  notHorizontalPadding,
  noMarginTop,
}: SectionProps) => {
  return (
    <S_Container
      noMarginTop={noMarginTop}
      notHorizontalPadding={notHorizontalPadding}
    >
      <SectionHeader
        notHorizontalPadding={notHorizontalPadding}
        title={title}
        onPress={onPress}
      />
      {children}
    </S_Container>
  );
};

const S_Container = styled.View<{
  notHorizontalPadding?: boolean;
  noMarginTop?: boolean;
}>`
  flex: 1;
  padding: 0px ${(props) => (props.notHorizontalPadding ? '0px' : '20px')};
  margin-top: 0px ${(props) => (props.noMarginTop ? '0px' : '32px')};
  overflow: visible;
`;
