import { useEffect, useState } from 'react';
// import { app } from '../services/firebase';
import storage from '@react-native-firebase/storage';

// const storage = getStorage(app);

export const useUploadImage = (uri: string) => {
  const [isLoading, setIsloading] = useState(false);
  const [error, setError] = useState();
  const [url, setUrl] = useState<string>();

  // /idproduit/fsdgrds.jpg
  // useEffect(() => {
  //   const uploadImage = async () => {
  //     setIsloading(true);
  //     const blob: Blob = await new Promise((resolve, reject) => {
  //       const xhr = new XMLHttpRequest();
  //       xhr.onload = function () {
  //         resolve(xhr.response);
  //       };
  //       xhr.onerror = function (e) {
  //         reject(new TypeError('Network request failed'));
  //       };
  //       xhr.responseType = 'blob';
  //       xhr.open('GET', uri, true);
  //       xhr.send(null);
  //     });

  //     const filename = uri.substring(uri.lastIndexOf('/') + 1);
  //     const storageRef = ref(storage, filename);

  //     await uploadBytes(storageRef, blob as Blob);

  //     setUrl(await getDownloadURL(storageRef));

  //     setIsloading(false);
  //   };

  //   if (uri) {
  //     uploadImage();
  //   }
  // }, [uri]);

  return {
    isLoading,
    error,
    url,
  };
};
