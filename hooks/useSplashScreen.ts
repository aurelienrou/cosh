import useCachedResources from './useCachedResources';
import { useLocationContext } from '../provider/LocationProvider';
import { useEffect } from 'react';
import * as SplashScreen from 'expo-splash-screen';

export const useSplashScreen = () => {
  const { isLoadingComplete } = useCachedResources();
  const { locationIsReady } = useLocationContext();

  useEffect(() => {
    if (isLoadingComplete && locationIsReady) {
      SplashScreen.hideAsync();
    }
  }, [isLoadingComplete, locationIsReady]);

  return { isReady: isLoadingComplete && locationIsReady };
};
