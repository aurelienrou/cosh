import axios from 'axios';

export const useAxiosToken = () => {
  const configureAxiosHeader = (token: string): boolean => {
    try {
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
      return true;
    } catch (error) {
      return false;
    }
  };

  return {
    configureAxiosHeader,
  };
};
