import { useCallback } from 'react';
import { Linking, Platform } from 'react-native';

export const useOpenMap = () => {
  return useCallback(
    async (latitude: number, longitude: number, label = '') => {
      const scheme = Platform.select({
        ios: 'maps:0,0?q=',
        android: 'geo:0,0?q=',
      });
      const latLng = `${latitude},${longitude}`;

      const url = Platform.select({
        ios: `${scheme}${label}@${latLng}`,
        android: `${scheme}${latLng}(${label})`,
      });

      try {
        if (url) {
          const supported = await Linking.canOpenURL(url);
          if (supported) Linking.openURL(url);
        }
      } catch (error) {
        console.log(error);
      }
    },
    [],
  );
};
