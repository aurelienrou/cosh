import { useState } from 'react';

enum MODE {
  DARK = 'dark',
  LIGHT = 'light',
}

export const useDarkMode = () => {
  const [theme, setTheme] = useState<MODE>(MODE.DARK);

  const setMode = (mode: MODE) => {
    setTheme(mode);
  };

  const themeToggler = () => {
    theme === 'light' ? setMode(MODE.DARK) : setMode(MODE.LIGHT);
  };

  return [theme, themeToggler];
};
