import { FontAwesome } from '@expo/vector-icons';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import { useEffect, useState } from 'react';
import { Image } from 'react-native';

function cacheImages(images: string[] | number[]) {
  return images.map((image) => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

export default function useCachedResources() {
  const [isLoadingComplete, setLoadingComplete] = useState(false);

  // Load any resources or data that we need prior to rendering the app
  useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        // Load fonts
        await Font.loadAsync({
          ...FontAwesome.font,
          'clash-regular': require('../assets/fonts/ClashDisplay-Regular.otf'),
          'clash-bold': require('../assets/fonts/ClashDisplay-Bold.otf'),
          // Gatwick
          'gatwick-bold': require('../assets/fonts/Gatwick-Bold.otf'),
          gatwick: require('../assets/fonts/Gatwick-Regular.otf'),
          'gatwick-ultralight': require('../assets/fonts/Gatwick-Ultralight.otf'),
          // Neue
          neue: require('../assets/fonts/NeueMachina-Regular.otf'),
          'neue-light': require('../assets/fonts/NeueMachina-Light.otf'),
          'neue-ultralight': require('../assets/fonts/NeueMachina-Ultralight.otf'),
          'neue-ultrabold': require('../assets/fonts/NeueMachina-Ultrabold.otf'),
          'neue-bold': require('../assets/fonts/NeueMachina-Bold.ttf'),
        });

        // Load Icons
        const imageAssets = cacheImages([
          require('../assets/icons/arrow.svg'),
          require('../assets/icons/home.svg'),
          require('../assets/icons/home-outline.svg'),
          require('../assets/icons/search.svg'),
          require('../assets/icons/map.svg'),
          require('../assets/icons/search-outline.svg'),
          require('../assets/icons/like.svg'),
          require('../assets/icons/like-red.svg'),
          require('../assets/icons/location.svg'),
          require('../assets/icons/notes.svg'),
          require('../assets/icons/notes-outline.svg'),
          require('../assets/icons/settings.svg'),
          require('../assets/icons/user.svg'),
          require('../assets/icons/user-outline.svg'),
          require('../assets/icons/art.svg'),
          require('../assets/icons/bar.svg'),
          require('../assets/icons/club.svg'),
          require('../assets/icons/game.svg'),
          require('../assets/icons/chevron.svg'),
          require('../assets/icons/user-rounded.svg'),
          require('../assets/icons/here.svg'),
          require('../assets/icons/calendar.svg'),
          require('../assets/icons/clear.svg'),
          require('../assets/icons/nearme.svg'),
          require('../assets/icons/logo.png'),
          require('../assets/icons/miniArrow.svg'),
        ]);

        await Promise.all(imageAssets);
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  return { isLoadingComplete };
}
