import React from 'react';
import { darkTheme, lightTheme } from '../constants/theme';
import { useTheme } from '../provider/ThemeProvider';

type Generator<T extends object> = (
  theme: typeof darkTheme | typeof lightTheme,
) => T;

export const useMainTheme = <T extends object>(fn: Generator<T>) => {
  const { theme } = useTheme();

  return React.useMemo(() => fn(theme), [fn, theme]);
};
