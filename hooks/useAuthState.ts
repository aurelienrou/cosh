// import auth from '@react-native-firebase/auth';
// import { User } from '@react-native-google-signin/google-signin';
// import { useEffect, useState } from 'react';
// import { useAuthContext } from '../provider/AuthProvider';
// import { useGoogleLogin } from '../services/auth/google';

// export const useAuthState = () => {
//   const [initializing, setInitializing] = useState(true);
//   const { setUser } = useAuthContext();
//   const { mutate, data, isLoading } = useGoogleLogin();

//   const onAuthStateChanged = (user: any) => {
//     console.log({ user });
//     mutate({
//       token: user.refreshToken,
//     });
//     setUser(user);
//     if (initializing) setInitializing(false);
//   };

//   useEffect(() => {
//     const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
//     return subscriber;
//   }, []);

//   //   if (initializing) return null;

//   return { initializing };
// };
