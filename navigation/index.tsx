import {
  BottomTabBarProps,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';
import { SafeAreaView, View, StyleSheet, Pressable } from 'react-native';
import { Image } from 'expo-image';
import Search from '../screens/Search';
import Event from '../screens/Event';
import GetLocation from '../screens/GetLocation';
import { useAsyncStorage } from '@react-native-async-storage/async-storage';
import { useEffect } from 'react';
import { useLocationContext } from '../provider/LocationProvider';
import CreateEvent from '../screens/CreateEvent';
import { useAuthContext } from '../provider/AuthProvider';
import { useAxiosToken } from '../hooks/useAxiosToken';
import { useTheme } from 'styled-components';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
} from 'react-native-reanimated';
import { height, width } from '../constants/Layout';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { LinearGradient } from 'expo-linear-gradient';
import { Place } from '../screens/Place';
import { createStackNavigator } from '@react-navigation/stack';
import { useSplashScreen } from '../hooks/useSplashScreen';
import { SignInOut } from '../screens/SignInOut';
import { MyEventsWishlist } from '../screens/MyEventsWishlist';
import { Discover } from '../screens/Discover';
import { useMapsContext } from '../provider/MapsProvider';
import { Theme, blurAmount } from '../constants/theme';
import { useMainTheme } from '../hooks/useMainTheme';
import { BlurView } from '@react-native-community/blur';
import { OpenMapButton } from '../components/Button/OpenMap';
import { SignUp } from '../screens/SignUp';
import { useGetUserWithToken } from '../services/auth/getUserWithToken';
import { User } from '../screens/User';
import { PrivateEvents } from '../screens/PrivateEvents';
import { CreatePrivateEvent } from '../screens/CreatePrivateEvent';

const resetFilters = async () => {
  await useAsyncStorage('concerts').removeItem();
  await useAsyncStorage('clubs').removeItem();
};

export default function Navigation() {
  return (
    <NavigationContainer>
      <RootNavigator />
    </NavigationContainer>
  );
}

export type RootStackParamList = {
  GetLocation: undefined;
  GetLocationModal: undefined;
  Home: undefined;
  CreatePrivateEvent: undefined;
  SignInOut: undefined;
  SignUp: undefined;
  Event: { id: string };
  Place: { id: string };
  User: { id: string };
};
const RootStack = createStackNavigator<RootStackParamList>();

function RootNavigator() {
  const { isReady } = useSplashScreen();
  const { location } = useLocationContext();
  const { setUser } = useAuthContext();
  const { configureAxiosHeader } = useAxiosToken();

  const { data, isLoading } = useGetUserWithToken();

  useEffect(() => {
    resetFilters();
  }, []);

  useEffect(() => {
    if (data?.user && data?.accessToken) {
      configureAxiosHeader(data.accessToken);
      setUser(data.user);
    }
  }, [data]);

  if (!isReady && isLoading) {
    return <></>;
  }

  return (
    <RootStack.Navigator
      initialRouteName="Home"
      screenOptions={{ headerShown: false }}
    >
      {Object.keys(location).length ? (
        <RootStack.Group>
          <RootStack.Screen name="Home" component={BottomTabNavigator} />
          <RootStack.Screen
            name="CreatePrivateEvent"
            component={CreatePrivateEvent}
          />
          <RootStack.Screen
            name="Event"
            component={Event}
            options={{ presentation: 'transparentModal' }}
          />
          <RootStack.Screen name="Place" component={Place} />
          <RootStack.Screen name="User" component={User} />
          <RootStack.Group
            screenOptions={{
              presentation: 'modal',
              gestureResponseDistance: height,
            }}
          >
            <RootStack.Screen
              name="GetLocationModal"
              component={GetLocation}
              options={{ headerShown: false }}
            />
            <RootStack.Screen name="SignInOut" component={SignInOut} />
            <RootStack.Screen name="SignUp" component={SignUp} />
          </RootStack.Group>
        </RootStack.Group>
      ) : (
        <RootStack.Group>
          <RootStack.Screen
            name="GetLocation"
            component={GetLocation}
            options={{ headerShown: false }}
          />
        </RootStack.Group>
      )}
    </RootStack.Navigator>
  );
}

export type EventStackParamList = {
  Events: undefined;
  CreateEvent: undefined;
};
const CreateEventStack = createNativeStackNavigator<EventStackParamList>();

function CreateEventNav() {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <CreateEventStack.Navigator initialRouteName="CreateEvent">
        <CreateEventStack.Screen
          name="CreateEvent"
          component={CreateEvent}
          options={{ headerShown: false }}
        />
      </CreateEventStack.Navigator>
    </SafeAreaView>
  );
}

export type BottomTabStackParamList = {
  Discover: undefined;
  Search: undefined;
  Favorites: undefined;
  MyEventsWishlist: undefined;
  PrivateEvents: undefined;
};
const BottomTab = createBottomTabNavigator<BottomTabStackParamList>();

const BottomTabBar = ({
  state: { index: selectedTab },
  navigation,
}: BottomTabBarProps) => {
  const theme = useTheme();
  const styles = useMainTheme(createStyles);
  const { animatedIndex } = useMapsContext();

  const handleNavigation = (screen: string) => {
    navigation.navigate(screen);
  };

  const { bottom } = useSafeAreaInsets();
  const style = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateY: interpolate(
            animatedIndex.value,
            [0, 1],
            [90, 0],
            Extrapolate.CLAMP,
          ),
        },
      ],
    };
  });
  return (
    <Animated.View style={[style, styles.S_TabBarContainer]}>
      {navigation.getState().index === 0 ? <OpenMapButton /> : null}

      <LinearGradient
        style={styles.S_LinearGradient}
        colors={['#15171800', theme.background.primary]}
        pointerEvents="none"
      />
      <BlurView
        blurAmount={blurAmount}
        style={[styles.S_TabBar, { height: 80, paddingBottom: bottom / 2 }]}
      >
        <View
          style={[styles.S_Blur, { height: 80, paddingBottom: bottom / 2 }]}
        />
        <Pressable
          style={styles.S_Pressable}
          onPress={() => handleNavigation('Discover')}
        >
          <Image
            style={styles.S_Icon}
            source={
              selectedTab === 0
                ? require('../assets/icons/home-outline.svg')
                : require('../assets/icons/home.svg')
            }
            contentFit="cover"
          />
        </Pressable>
        <Pressable
          style={styles.S_Pressable}
          onPress={() => handleNavigation('Search')}
        >
          <Image
            style={styles.S_Icon}
            source={
              selectedTab === 1
                ? require('../assets/icons/search-outline.svg')
                : require('../assets/icons/search.svg')
            }
            contentFit="cover"
          />
        </Pressable>
        <Pressable
          style={styles.S_Pressable}
          onPress={() => handleNavigation('MyEventsWishlist')}
        >
          <Image
            style={[styles.S_Icon, { width: 23, height: 20 }]}
            source={
              selectedTab === 2
                ? require('../assets/icons/like-white.svg')
                : require('../assets/icons/like-outline.svg')
            }
            contentFit="cover"
          />
        </Pressable>
        <Pressable
          style={styles.S_Pressable}
          onPress={() => handleNavigation('PrivateEvents')}
        >
          <Image
            style={[styles.S_Icon]}
            source={
              selectedTab === 3
                ? require('../assets/icons/notes-outline.svg')
                : require('../assets/icons/notes.svg')
            }
            contentFit="cover"
          />
        </Pressable>
      </BlurView>
    </Animated.View>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_Pressable: {
      height: '100%',
      width: width / 4,
      justifyContent: 'center',
      alignItems: 'center',
    },
    S_LinearGradient: {
      position: 'absolute',
      top: -45,
      height: 50,
      width: '100%',
    },
    S_TabBarContainer: {
      position: 'absolute',
      bottom: 0,
      width: '100%',
      zIndex: 0,
    },
    S_TabBar: {
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
    S_Blur: {
      position: 'absolute',
      bottom: 0,
      width: '100%',
      opacity: 0.7,
      backgroundColor: theme.background.primary,
    },
    S_Icon: {
      height: 22,
      width: 22,
    },
  });

function BottomTabNavigator() {
  return (
    <View style={[{ flex: 1 }]}>
      <BottomTab.Navigator
        initialRouteName="Discover"
        screenOptions={{
          headerShown: false,
          tabBarShowLabel: false,
        }}
        sceneContainerStyle={{ zIndex: 0 }}
        tabBar={(props) => <BottomTabBar {...props} />}
      >
        <BottomTab.Group>
          <BottomTab.Screen name="Discover" component={Discover} />
          <BottomTab.Screen name="Search" component={Search} />
          <BottomTab.Screen
            name="MyEventsWishlist"
            component={MyEventsWishlist}
          />
          <BottomTab.Screen name="PrivateEvents" component={PrivateEvents} />
        </BottomTab.Group>
      </BottomTab.Navigator>
    </View>
  );
}
