import {
  StackCardStyleInterpolator,
  createStackNavigator,
} from '@react-navigation/stack';
import React from 'react';
import { DiscoverStackParamList } from '../components/DiscoverTabView/helper';
import { All } from '../components/DiscoverTabView/screens/All';
import { ThisWeek } from '../components/DiscoverTabView/screens/ThisWeek';
import { Today } from '../components/DiscoverTabView/screens/Today';
import { Content } from '../components/DiscoverTabView/screens/Content';
import { Bars } from '../components/DiscoverTabView/screens/Bars';
import { Clubs } from '../components/DiscoverTabView/screens/Clubs';
import { Tonight } from '../components/DiscoverTabView/screens/Tonight';
import { Concerts } from '../components/DiscoverTabView/screens/Concerts';

const forFade: StackCardStyleInterpolator = ({ current }) => ({
  cardStyle: {
    opacity: current.progress,
  },
});

const DiscoverNav = createStackNavigator<DiscoverStackParamList>();

const AllMemo = React.memo(All);
const TodayMemo = React.memo(Today);
const ThisWeekMemo = React.memo(ThisWeek);
const ContentMemo = React.memo(Content);
const BarsMemo = React.memo(Bars);
const ClubsMemo = React.memo(Clubs);
const ConcertsMemo = React.memo(Concerts);
const TonightMemo = React.memo(Tonight);

export const DiscoverNavigator = () => {
  return (
    <DiscoverNav.Navigator
      initialRouteName="Tendances"
      screenOptions={{ headerShown: false, cardStyleInterpolator: forFade }}
    >
      <DiscoverNav.Screen name="Tendances" component={AllMemo} />
      <DiscoverNav.Screen name="Aujourd'hui" component={TodayMemo} />
      <DiscoverNav.Screen name="Cette semaine" component={ThisWeekMemo} />
      <DiscoverNav.Screen name="Bars" component={BarsMemo} />
      <DiscoverNav.Screen name="Clubs" component={ClubsMemo} />
      <DiscoverNav.Screen name="Concerts" component={ConcertsMemo} />
      <DiscoverNav.Screen name="Comedy" component={TonightMemo} />
      <DiscoverNav.Screen name="Culture" component={TonightMemo} />
      <DiscoverNav.Screen name="Content" component={ContentMemo} />
    </DiscoverNav.Navigator>
  );
};
