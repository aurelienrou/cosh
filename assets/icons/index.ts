import art from './art.svg';
import bar from './bar.svg';
import club from './club.svg';
import game from './game.svg';
import here from './here.svg';

export default {
  art,
  bar,
  club,
  game,
  here,
};
