import { Tag } from '../components/Tags/Tags';

interface ContactInfo {
  phone: string;
  email?: string;
  website?: string;
}

interface SocialMedia {
  facebook?: string;
  instagram?: string;
}

interface Location {
  address: string;
  city: string;
  geo: {
    lat: number;
    lng: number;
  };
  country: string;
}
