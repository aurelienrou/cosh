import 'styled-components/native';

declare module 'styled-components/native' {
  export interface DefaultTheme {
    background: {
      primary: string;
      secondary: string;
      action: string;
    };
    fontColor: {
      primary: string;
      secondary: string;
      action: string;
    };
  }
}
