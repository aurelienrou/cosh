module.exports = {
    project: {
      android: {
        unstable_reactLegacyComponentNames: ["YGUnitPercent"]
      },
      ios: {
        unstable_reactLegacyComponentNames: ["YGUnitPercent"]
      }
    },
  };