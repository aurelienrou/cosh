export enum ICONS_NAME {
  NAVIGATION = 'navigation',
  GOOGLE = 'google',
  FACEBOOK = 'facebook',
}

export const icons = {
  navigation: require('../assets/images/navigation.png'),
  facebook: require('../assets/images/facebook.png'),
  google: require('../assets/images/google.png'),
};
