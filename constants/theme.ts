export const lightTheme = {
  background: {
    primary: '#151718',
    secondary: '#000000',
    action: '#242627',
  },
  fontColor: {
    primary: '#FFFFFF',
    secondary: '#999999',
    action: '#EFB0C5',
  },
} as const;

export const darkTheme = {
  background: {
    primary: '#151718',
    secondary: '#000000',
    action: '#242627',
  },
  fontColor: {
    primary: '#FFFFFF',
    secondary: '#999999',
    action: '#EFB0C5',
  },
} as const;

export const blurAmount = 40;

export type Theme = typeof lightTheme | typeof darkTheme;
