const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';
export const PLACEHOLDER_COLOR = '#808080';
export const INPUT_BACKGROUND_COLOR = '#f4f6ff';
export const PRIMARY_COLOR = '#3461fd';

export default {
  light: {
    text: '#000',
    background: '#fff',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
  },
  dark: {
    text: '#fff',
    background: '#000',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
  },
  input: {
    background: '#f4f6ff',
  },
};
