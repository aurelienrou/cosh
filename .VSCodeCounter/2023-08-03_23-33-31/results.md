# Summary

Date : 2023-08-03 23:33:31

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 184 files,  42318 codes, 567 comments, 1098 blanks, all 43983 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 11 | 33,497 | 0 | 5 | 33,502 |
| TypeScript JSX | 76 | 6,719 | 83 | 702 | 7,504 |
| TypeScript | 36 | 867 | 100 | 140 | 1,107 |
| Java | 6 | 307 | 75 | 55 | 437 |
| XML | 30 | 272 | 24 | 19 | 315 |
| Groovy | 3 | 250 | 150 | 47 | 447 |
| C++ | 8 | 165 | 21 | 56 | 242 |
| Batch | 1 | 68 | 0 | 22 | 90 |
| JavaScript | 5 | 43 | 3 | 10 | 56 |
| JSON with Comments | 3 | 40 | 60 | 1 | 101 |
| Objective-C++ | 1 | 40 | 7 | 14 | 61 |
| Makefile | 1 | 28 | 12 | 10 | 50 |
| Java Properties | 2 | 15 | 32 | 13 | 60 |
| Objective-C | 1 | 7 | 0 | 4 | 11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 184 | 42,318 | 567 | 1,098 | 43,983 |
| . (Files) | 14 | 33,395 | 68 | 30 | 33,493 |
| .expo-shared | 1 | 6 | 0 | 1 | 7 |
| android | 31 | 973 | 313 | 206 | 1,492 |
| android (Files) | 4 | 137 | 39 | 45 | 221 |
| android/app | 26 | 831 | 274 | 160 | 1,265 |
| android/app (Files) | 2 | 238 | 143 | 36 | 417 |
| android/app/src | 24 | 593 | 131 | 124 | 848 |
| android/app/src/debug | 2 | 64 | 8 | 5 | 77 |
| android/app/src/debug (Files) | 1 | 5 | 0 | 3 | 8 |
| android/app/src/debug/java | 1 | 59 | 8 | 2 | 69 |
| android/app/src/debug/java/com | 1 | 59 | 8 | 2 | 69 |
| android/app/src/debug/java/com/cosh | 1 | 59 | 8 | 2 | 69 |
| android/app/src/main | 22 | 529 | 123 | 119 | 771 |
| android/app/src/main (Files) | 1 | 40 | 0 | 0 | 40 |
| android/app/src/main/java | 5 | 248 | 67 | 53 | 368 |
| android/app/src/main/java/com | 5 | 248 | 67 | 53 | 368 |
| android/app/src/main/java/com/cosh | 5 | 248 | 67 | 53 | 368 |
| android/app/src/main/java/com/cosh (Files) | 2 | 113 | 29 | 23 | 165 |
| android/app/src/main/java/com/cosh/newarchitecture | 3 | 135 | 38 | 30 | 203 |
| android/app/src/main/java/com/cosh/newarchitecture (Files) | 1 | 83 | 20 | 14 | 117 |
| android/app/src/main/java/com/cosh/newarchitecture/components | 1 | 22 | 8 | 7 | 37 |
| android/app/src/main/java/com/cosh/newarchitecture/modules | 1 | 30 | 10 | 9 | 49 |
| android/app/src/main/jni | 8 | 188 | 33 | 63 | 284 |
| android/app/src/main/res | 8 | 53 | 23 | 3 | 79 |
| android/app/src/main/res/drawable | 2 | 14 | 23 | 3 | 40 |
| android/app/src/main/res/mipmap-anydpi-v26 | 2 | 10 | 0 | 0 | 10 |
| android/app/src/main/res/values | 3 | 28 | 0 | 0 | 28 |
| android/app/src/main/res/values-night | 1 | 1 | 0 | 0 | 1 |
| android/gradle | 1 | 5 | 0 | 1 | 6 |
| android/gradle/wrapper | 1 | 5 | 0 | 1 | 6 |
| assets | 20 | 94 | 0 | 14 | 108 |
| assets/icons | 20 | 94 | 0 | 14 | 108 |
| components | 57 | 5,116 | 47 | 493 | 5,656 |
| components/AddressEditorModal | 1 | 52 | 1 | 5 | 58 |
| components/BlockEvents | 2 | 135 | 0 | 16 | 151 |
| components/BlockPlaces | 2 | 82 | 0 | 13 | 95 |
| components/BlockPlaces (Files) | 1 | 20 | 0 | 4 | 24 |
| components/BlockPlaces/BlockPlace | 1 | 62 | 0 | 9 | 71 |
| components/BlockToFollow | 1 | 58 | 1 | 8 | 67 |
| components/Blocks | 6 | 269 | 0 | 35 | 304 |
| components/Button | 4 | 298 | 2 | 36 | 336 |
| components/Carousel | 1 | 157 | 0 | 19 | 176 |
| components/ChangeAddress | 1 | 57 | 0 | 9 | 66 |
| components/CirclePeoples | 1 | 36 | 0 | 5 | 41 |
| components/ConnectModal | 1 | 31 | 0 | 4 | 35 |
| components/CoshText | 1 | 18 | 0 | 5 | 23 |
| components/CreateEventForm | 2 | 433 | 9 | 22 | 464 |
| components/CreateEventHeader | 1 | 85 | 0 | 8 | 93 |
| components/CreatePlaceForm | 2 | 437 | 10 | 19 | 466 |
| components/DiscoverTabView | 7 | 641 | 0 | 71 | 712 |
| components/DiscoverTabView (Files) | 2 | 279 | 0 | 33 | 312 |
| components/DiscoverTabView/screens | 5 | 362 | 0 | 38 | 400 |
| components/EventMiniature | 1 | 113 | 2 | 8 | 123 |
| components/EventsList | 1 | 26 | 0 | 5 | 31 |
| components/Headers | 1 | 91 | 0 | 9 | 100 |
| components/HoursSeletor | 1 | 227 | 0 | 21 | 248 |
| components/IconAndText | 1 | 81 | 0 | 5 | 86 |
| components/InputAddressFinder | 1 | 141 | 0 | 16 | 157 |
| components/InputPlaceFinder | 1 | 133 | 0 | 15 | 148 |
| components/InputPlaceFinder.tsx | 1 | 112 | 0 | 11 | 123 |
| components/LoginButton | 1 | 90 | 0 | 11 | 101 |
| components/MiniaturesPlan | 1 | 86 | 0 | 6 | 92 |
| components/ModalDateTimePicker | 1 | 87 | 0 | 5 | 92 |
| components/PeoplesAreJoined | 1 | 46 | 0 | 6 | 52 |
| components/PriceRange | 1 | 65 | 0 | 7 | 72 |
| components/RadiusSelector | 1 | 32 | 0 | 5 | 37 |
| components/SearchMap | 2 | 272 | 2 | 12 | 286 |
| components/Section | 2 | 69 | 0 | 9 | 78 |
| components/SwipeCards | 1 | 270 | 0 | 25 | 295 |
| components/SwitchButton | 1 | 94 | 0 | 10 | 104 |
| components/Tags | 1 | 40 | 0 | 8 | 48 |
| components/Toast | 1 | 115 | 0 | 8 | 123 |
| components/UpdateEventButton | 1 | 17 | 18 | 4 | 39 |
| components/UploadCover | 1 | 120 | 2 | 12 | 134 |
| constants | 5 | 69 | 0 | 13 | 82 |
| helpers | 3 | 32 | 0 | 8 | 40 |
| hooks | 7 | 149 | 36 | 38 | 223 |
| ios | 9 | 207 | 8 | 24 | 239 |
| ios (Files) | 1 | 4 | 0 | 1 | 5 |
| ios/Cosh | 8 | 203 | 8 | 23 | 234 |
| ios/Cosh (Files) | 4 | 142 | 8 | 22 | 172 |
| ios/Cosh/Images.xcassets | 4 | 61 | 0 | 1 | 62 |
| ios/Cosh/Images.xcassets (Files) | 1 | 6 | 0 | 1 | 7 |
| ios/Cosh/Images.xcassets/AppIcon.appiconset | 1 | 13 | 0 | 0 | 13 |
| ios/Cosh/Images.xcassets/SplashScreen.imageset | 1 | 21 | 0 | 0 | 21 |
| ios/Cosh/Images.xcassets/SplashScreenBackground.imageset | 1 | 21 | 0 | 0 | 21 |
| navigation | 1 | 254 | 0 | 23 | 277 |
| provider | 6 | 428 | 0 | 40 | 468 |
| screens | 13 | 1,172 | 31 | 136 | 1,339 |
| services | 16 | 404 | 64 | 68 | 536 |
| services (Files) | 11 | 336 | 64 | 54 | 454 |
| services/events | 3 | 44 | 0 | 9 | 53 |
| services/place | 2 | 24 | 0 | 5 | 29 |
| types | 1 | 19 | 0 | 4 | 23 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)