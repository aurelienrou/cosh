# Diff Summary

Date : 2023-08-03 23:33:31

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 45 files,  -3586 codes, 39 comments, 63 blanks, all -3484 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript JSX | 28 | 493 | 1 | 67 | 561 |
| TypeScript | 11 | 19 | 42 | 17 | 78 |
| JSON with Comments | 1 | 1 | 1 | 0 | 2 |
| C++ | 1 | -1 | 0 | -1 | -2 |
| Objective-C++ | 1 | -81 | -5 | -20 | -106 |
| JSON | 3 | -4,017 | 0 | 0 | -4,017 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 45 | -3,586 | 39 | 63 | -3,484 |
| . (Files) | 3 | -3,987 | 1 | 0 | -3,986 |
| components | 17 | 189 | -4 | 24 | 209 |
| components/BlockEvents | 2 | 24 | 0 | 2 | 26 |
| components/BlockPlaces | 2 | 82 | 0 | 13 | 95 |
| components/BlockPlaces (Files) | 1 | 20 | 0 | 4 | 24 |
| components/BlockPlaces/BlockPlace | 1 | 62 | 0 | 9 | 71 |
| components/Button | 1 | -1 | 0 | 0 | -1 |
| components/Carousel | 1 | 22 | -2 | 6 | 26 |
| components/DiscoverTabView | 5 | 98 | 0 | 11 | 109 |
| components/DiscoverTabView (Files) | 2 | 3 | 0 | 0 | 3 |
| components/DiscoverTabView/screens | 3 | 95 | 0 | 11 | 106 |
| components/Headers | 1 | -1 | 0 | 0 | -1 |
| components/MiniaturesPlan | 1 | -2 | 0 | 0 | -2 |
| components/SearchMap | 1 | 5 | 0 | 1 | 6 |
| components/SkeletonContent | 1 | -30 | -2 | -9 | -41 |
| components/SwipeCards | 1 | -7 | 0 | 0 | -7 |
| components/Tags | 1 | -1 | 0 | 0 | -1 |
| helpers | 1 | 2 | 0 | 1 | 3 |
| hooks | 1 | -35 | 27 | 0 | -8 |
| ios | 4 | -106 | -5 | -21 | -132 |
| ios (Files) | 1 | 1 | 0 | 0 | 1 |
| ios/Cosh | 3 | -107 | -5 | -21 | -133 |
| ios/Cosh (Files) | 2 | -82 | -5 | -21 | -108 |
| ios/Cosh/Images.xcassets | 1 | -25 | 0 | 0 | -25 |
| ios/Cosh/Images.xcassets/AppIcon.appiconset | 1 | -25 | 0 | 0 | -25 |
| provider | 4 | 9 | 0 | 13 | 22 |
| screens | 6 | 290 | 5 | 30 | 325 |
| services | 8 | 64 | 15 | 17 | 96 |
| services (Files) | 3 | -4 | 15 | 3 | 14 |
| services/events | 3 | 44 | 0 | 9 | 53 |
| services/place | 2 | 24 | 0 | 5 | 29 |
| types | 1 | -12 | 0 | -1 | -13 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)