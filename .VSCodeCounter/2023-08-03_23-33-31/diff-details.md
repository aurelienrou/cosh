# Diff Details

Date : 2023-08-03 23:33:31

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 45 files, -3586 codes, 39 comments, 63 blanks, all -3484 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files

| filename                                                                                                                | language           |   code | comment | blank |  total |
| :---------------------------------------------------------------------------------------------------------------------- | :----------------- | -----: | ------: | ----: | -----: |
| [App.tsx](/App.tsx)                                                                                                     | TypeScript JSX     |      5 |       0 |     0 |      5 |
| [app.json](/app.json)                                                                                                   | JSON with Comments |      1 |       1 |     0 |      2 |
| [components/BlockEvents/BlockEvent.tsx](/components/BlockEvents/BlockEvent.tsx)                                         | TypeScript JSX     |     15 |       0 |     1 |     16 |
| [components/BlockEvents/BlockEvents.tsx](/components/BlockEvents/BlockEvents.tsx)                                       | TypeScript JSX     |      9 |       0 |     1 |     10 |
| [components/BlockPlaces/BlockPlace/BlockPlace.tsx](/components/BlockPlaces/BlockPlace/BlockPlace.tsx)                   | TypeScript JSX     |     62 |       0 |     9 |     71 |
| [components/BlockPlaces/BlockPlaces.tsx](/components/BlockPlaces/BlockPlaces.tsx)                                       | TypeScript JSX     |     20 |       0 |     4 |     24 |
| [components/Button/FollowButton.tsx](/components/Button/FollowButton.tsx)                                               | TypeScript JSX     |     -1 |       0 |     0 |     -1 |
| [components/Carousel/Carousel.tsx](/components/Carousel/Carousel.tsx)                                                   | TypeScript JSX     |     22 |      -2 |     6 |     26 |
| [components/DiscoverTabView/DiscoverTabView.tsx](/components/DiscoverTabView/DiscoverTabView.tsx)                       | TypeScript JSX     |      2 |       0 |     0 |      2 |
| [components/DiscoverTabView/helper.tsx](/components/DiscoverTabView/helper.tsx)                                         | TypeScript JSX     |      1 |       0 |     0 |      1 |
| [components/DiscoverTabView/screens/All.tsx](/components/DiscoverTabView/screens/All.tsx)                               | TypeScript JSX     |     27 |       0 |     4 |     31 |
| [components/DiscoverTabView/screens/Clubs.tsx](/components/DiscoverTabView/screens/Clubs.tsx)                           | TypeScript JSX     |     67 |       0 |     7 |     74 |
| [components/DiscoverTabView/screens/ThisWeek.tsx](/components/DiscoverTabView/screens/ThisWeek.tsx)                     | TypeScript JSX     |      1 |       0 |     0 |      1 |
| [components/Headers/PlaceOrArtistHeader.tsx](/components/Headers/PlaceOrArtistHeader.tsx)                               | TypeScript JSX     |     -1 |       0 |     0 |     -1 |
| [components/MiniaturesPlan/MiniaturesPlan.tsx](/components/MiniaturesPlan/MiniaturesPlan.tsx)                           | TypeScript JSX     |     -2 |       0 |     0 |     -2 |
| [components/SearchMap/SearchMap.tsx](/components/SearchMap/SearchMap.tsx)                                               | TypeScript JSX     |      5 |       0 |     1 |      6 |
| [components/SkeletonContent/SkeletonContent.tsx](/components/SkeletonContent/SkeletonContent.tsx)                       | TypeScript JSX     |    -30 |      -2 |    -9 |    -41 |
| [components/SwipeCards/SwipeCards.tsx](/components/SwipeCards/SwipeCards.tsx)                                           | TypeScript JSX     |     -7 |       0 |     0 |     -7 |
| [components/Tags/Tags.tsx](/components/Tags/Tags.tsx)                                                                   | TypeScript JSX     |     -1 |       0 |     0 |     -1 |
| [helpers/string.ts](/helpers/string.ts)                                                                                 | TypeScript         |      2 |       0 |     1 |      3 |
| [hooks/useUploadImage.ts](/hooks/useUploadImage.ts)                                                                     | TypeScript         |    -35 |      27 |     0 |     -8 |
| [ios/Cosh/AppDelegate.h](/ios/Cosh/AppDelegate.h)                                                                       | C++                |     -1 |       0 |    -1 |     -2 |
| [ios/Cosh/AppDelegate.mm](/ios/Cosh/AppDelegate.mm)                                                                     | Objective-C++      |    -81 |      -5 |   -20 |   -106 |
| [ios/Cosh/Images.xcassets/AppIcon.appiconset/Contents.json](/ios/Cosh/Images.xcassets/AppIcon.appiconset/Contents.json) | JSON               |    -25 |       0 |     0 |    -25 |
| [ios/Podfile.properties.json](/ios/Podfile.properties.json)                                                             | JSON               |      1 |       0 |     0 |      1 |
| [package-lock.json](/package-lock.json)                                                                                 | JSON               | -3,993 |       0 |     0 | -3,993 |
| [provider/EventProvider.tsx](/provider/EventProvider.tsx)                                                               | TypeScript JSX     |    -54 |       0 |     0 |    -54 |
| [provider/LocationProvider.tsx](/provider/LocationProvider.tsx)                                                         | TypeScript JSX     |     -3 |       0 |     0 |     -3 |
| [provider/MapsProvider.tsx](/provider/MapsProvider.tsx)                                                                 | TypeScript JSX     |     33 |       0 |     7 |     40 |
| [provider/PlacesProvider.tsx](/provider/PlacesProvider.tsx)                                                             | TypeScript JSX     |     33 |       0 |     6 |     39 |
| [screens/Event.tsx](/screens/Event.tsx)                                                                                 | TypeScript JSX     |     -1 |       0 |     1 |      0 |
| [screens/MyEvents.tsx](/screens/MyEvents.tsx)                                                                           | TypeScript JSX     |     85 |       5 |    10 |    100 |
| [screens/MyEventsTickets.tsx](/screens/MyEventsTickets.tsx)                                                             | TypeScript JSX     |     84 |       0 |     9 |     93 |
| [screens/MyEventsWishlist.tsx](/screens/MyEventsWishlist.tsx)                                                           | TypeScript JSX     |     73 |       0 |     6 |     79 |
| [screens/Place.tsx](/screens/Place.tsx)                                                                                 | TypeScript JSX     |     35 |       0 |     2 |     37 |
| [screens/Search.tsx](/screens/Search.tsx)                                                                               | TypeScript JSX     |     14 |       0 |     2 |     16 |
| [services/axios.ts](/services/axios.ts)                                                                                 | TypeScript         |      5 |       0 |     3 |      8 |
| [services/events/getEventById.ts](/services/events/getEventById.ts)                                                     | TypeScript         |     12 |       0 |     3 |     15 |
| [services/events/getNewEvents.ts](/services/events/getNewEvents.ts)                                                     | TypeScript         |     16 |       0 |     3 |     19 |
| [services/events/popular.ts](/services/events/popular.ts)                                                               | TypeScript         |     16 |       0 |     3 |     19 |
| [services/firebase.ts](/services/firebase.ts)                                                                           | TypeScript         |    -15 |      15 |     0 |      0 |
| [services/place/getPlaceById.ts](/services/place/getPlaceById.ts)                                                       | TypeScript         |     13 |       0 |     3 |     16 |
| [services/place/getPlaces.ts](/services/place/getPlaces.ts)                                                             | TypeScript         |     11 |       0 |     2 |     13 |
| [services/types.ts](/services/types.ts)                                                                                 | TypeScript         |      6 |       0 |     0 |      6 |
| [types/place.ts](/types/place.ts)                                                                                       | TypeScript         |    -12 |       0 |    -1 |    -13 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details
