# Summary

Date : 2023-12-22 19:18:26

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 192 files,  42041 codes, 273 comments, 1223 blanks, all 43537 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 10 | 30,870 | 0 | 5 | 30,875 |
| TypeScript JSX | 98 | 9,677 | 65 | 972 | 10,714 |
| TypeScript | 46 | 1,204 | 125 | 194 | 1,523 |
| XML | 25 | 150 | 0 | 18 | 168 |
| JavaScript | 5 | 45 | 3 | 10 | 58 |
| JSON with Comments | 3 | 43 | 66 | 1 | 110 |
| Objective-C++ | 1 | 40 | 7 | 14 | 61 |
| Objective-C | 1 | 7 | 0 | 4 | 11 |
| C++ | 2 | 5 | 3 | 4 | 12 |
| Swift | 1 | 0 | 4 | 1 | 5 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 192 | 42,041 | 273 | 1,223 | 43,537 |
| . (Files) | 15 | 30,817 | 78 | 34 | 30,929 |
| .expo-shared | 1 | 6 | 0 | 1 | 7 |
| assets | 25 | 111 | 0 | 20 | 131 |
| assets/icons | 25 | 111 | 0 | 20 | 131 |
| components | 73 | 6,509 | 74 | 684 | 7,267 |
| components/AddressEditorModal | 1 | 52 | 1 | 5 | 58 |
| components/BlockEvents | 2 | 134 | 0 | 17 | 151 |
| components/BlockPlaces | 2 | 117 | 0 | 14 | 131 |
| components/BlockToFollow | 1 | 58 | 1 | 8 | 67 |
| components/Blocks | 6 | 270 | 0 | 35 | 305 |
| components/BottomSheetIndicator | 1 | 131 | 2 | 8 | 141 |
| components/Button | 6 | 393 | 1 | 52 | 446 |
| components/Carousel | 1 | 157 | 0 | 19 | 176 |
| components/Checkbox | 2 | 156 | 0 | 22 | 178 |
| components/CirclePeoples | 1 | 36 | 0 | 5 | 41 |
| components/CoshText | 1 | 18 | 0 | 5 | 23 |
| components/CreateEventForm | 2 | 433 | 9 | 22 | 464 |
| components/CreateEventHeader | 1 | 85 | 0 | 8 | 93 |
| components/CreatePlaceForm | 2 | 437 | 10 | 19 | 466 |
| components/DiscoverTabView | 9 | 576 | 0 | 91 | 667 |
| components/DiscoverTabView (Files) | 2 | 112 | 0 | 19 | 131 |
| components/DiscoverTabView/screens | 7 | 464 | 0 | 72 | 536 |
| components/EventMiniature | 1 | 113 | 2 | 8 | 123 |
| components/EventsList | 1 | 26 | 0 | 5 | 31 |
| components/Filters | 3 | 172 | 0 | 18 | 190 |
| components/Headers | 2 | 319 | 0 | 23 | 342 |
| components/HoursSeletor | 1 | 227 | 0 | 21 | 248 |
| components/IconAndText | 1 | 81 | 0 | 5 | 86 |
| components/InputAddressFinder | 1 | 141 | 0 | 16 | 157 |
| components/InputPlaceFinder | 1 | 133 | 0 | 15 | 148 |
| components/InputPlaceFinder.tsx | 1 | 112 | 0 | 11 | 123 |
| components/MiniaturesPlan | 1 | 120 | 0 | 13 | 133 |
| components/ModalDateTimePicker | 1 | 87 | 0 | 5 | 92 |
| components/PeoplesAreJoined | 1 | 46 | 0 | 6 | 52 |
| components/PriceRange | 1 | 65 | 0 | 7 | 72 |
| components/RadiusSelector | 1 | 32 | 0 | 5 | 37 |
| components/Rubriques | 2 | 106 | 0 | 16 | 122 |
| components/SearchMap | 6 | 754 | 28 | 77 | 859 |
| components/Section | 2 | 96 | 0 | 11 | 107 |
| components/SignInOut | 2 | 143 | 0 | 19 | 162 |
| components/Slider | 1 | 123 | 0 | 18 | 141 |
| components/SwipeCards | 1 | 268 | 0 | 23 | 291 |
| components/Tags | 1 | 40 | 0 | 8 | 48 |
| components/Toast | 1 | 115 | 0 | 8 | 123 |
| components/UpdateEventButton | 1 | 17 | 18 | 4 | 39 |
| components/UploadCover | 1 | 120 | 2 | 12 | 134 |
| constants | 5 | 71 | 0 | 14 | 85 |
| helpers | 3 | 34 | 0 | 9 | 43 |
| hooks | 8 | 162 | 36 | 41 | 239 |
| ios | 11 | 169 | 14 | 25 | 208 |
| ios (Files) | 1 | 4 | 0 | 1 | 5 |
| ios/Cosh | 10 | 165 | 14 | 24 | 203 |
| ios/Cosh (Files) | 6 | 103 | 14 | 23 | 140 |
| ios/Cosh/Images.xcassets | 4 | 62 | 0 | 1 | 63 |
| ios/Cosh/Images.xcassets (Files) | 1 | 6 | 0 | 1 | 7 |
| ios/Cosh/Images.xcassets/AppIcon.appiconset | 1 | 14 | 0 | 0 | 14 |
| ios/Cosh/Images.xcassets/SplashScreen.imageset | 1 | 21 | 0 | 0 | 21 |
| ios/Cosh/Images.xcassets/SplashScreenBackground.imageset | 1 | 21 | 0 | 0 | 21 |
| navigation | 2 | 337 | 0 | 27 | 364 |
| provider | 10 | 1,263 | 2 | 72 | 1,337 |
| screens | 15 | 1,906 | 5 | 193 | 2,104 |
| services | 23 | 637 | 64 | 99 | 800 |
| services (Files) | 11 | 383 | 64 | 59 | 506 |
| services/events | 6 | 115 | 0 | 18 | 133 |
| services/follow | 1 | 48 | 0 | 7 | 55 |
| services/place | 3 | 67 | 0 | 9 | 76 |
| services/topics | 2 | 24 | 0 | 6 | 30 |
| types | 1 | 19 | 0 | 4 | 23 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)