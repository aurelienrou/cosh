# Diff Details

Date : 2023-12-22 19:18:26

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 128 files,  -277 codes, -294 comments, 125 blanks, all -446 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [App.tsx](/App.tsx) | TypeScript JSX | -18 | 1 | 0 | -17 |
| [android/app/build.gradle](/android/app/build.gradle) | Groovy | -191 | -143 | -36 | -370 |
| [android/app/google-services.json](/android/app/google-services.json) | JSON | -47 | 0 | 0 | -47 |
| [android/app/src/debug/AndroidManifest.xml](/android/app/src/debug/AndroidManifest.xml) | XML | -5 | 0 | -3 | -8 |
| [android/app/src/debug/java/com/cosh/ReactNativeFlipper.java](/android/app/src/debug/java/com/cosh/ReactNativeFlipper.java) | Java | -59 | -8 | -2 | -69 |
| [android/app/src/main/AndroidManifest.xml](/android/app/src/main/AndroidManifest.xml) | XML | -40 | 0 | 0 | -40 |
| [android/app/src/main/java/com/cosh/MainActivity.java](/android/app/src/main/java/com/cosh/MainActivity.java) | Java | -34 | -15 | -9 | -58 |
| [android/app/src/main/java/com/cosh/MainApplication.java](/android/app/src/main/java/com/cosh/MainApplication.java) | Java | -79 | -14 | -14 | -107 |
| [android/app/src/main/java/com/cosh/newarchitecture/MainApplicationReactNativeHost.java](/android/app/src/main/java/com/cosh/newarchitecture/MainApplicationReactNativeHost.java) | Java | -83 | -20 | -14 | -117 |
| [android/app/src/main/java/com/cosh/newarchitecture/components/MainComponentsRegistry.java](/android/app/src/main/java/com/cosh/newarchitecture/components/MainComponentsRegistry.java) | Java | -22 | -8 | -7 | -37 |
| [android/app/src/main/java/com/cosh/newarchitecture/modules/MainApplicationTurboModuleManagerDelegate.java](/android/app/src/main/java/com/cosh/newarchitecture/modules/MainApplicationTurboModuleManagerDelegate.java) | Java | -30 | -10 | -9 | -49 |
| [android/app/src/main/jni/Android.mk](/android/app/src/main/jni/Android.mk) | Makefile | -28 | -12 | -10 | -50 |
| [android/app/src/main/jni/MainApplicationModuleProvider.cpp](/android/app/src/main/jni/MainApplicationModuleProvider.cpp) | C++ | -11 | -9 | -5 | -25 |
| [android/app/src/main/jni/MainApplicationModuleProvider.h](/android/app/src/main/jni/MainApplicationModuleProvider.h) | C++ | -11 | 0 | -6 | -17 |
| [android/app/src/main/jni/MainApplicationTurboModuleManagerDelegate.cpp](/android/app/src/main/jni/MainApplicationTurboModuleManagerDelegate.cpp) | C++ | -37 | -1 | -8 | -46 |
| [android/app/src/main/jni/MainApplicationTurboModuleManagerDelegate.h](/android/app/src/main/jni/MainApplicationTurboModuleManagerDelegate.h) | C++ | -25 | -5 | -9 | -39 |
| [android/app/src/main/jni/MainComponentsRegistry.cpp](/android/app/src/main/jni/MainComponentsRegistry.cpp) | C++ | -43 | -5 | -14 | -62 |
| [android/app/src/main/jni/MainComponentsRegistry.h](/android/app/src/main/jni/MainComponentsRegistry.h) | C++ | -23 | -1 | -9 | -33 |
| [android/app/src/main/jni/OnLoad.cpp](/android/app/src/main/jni/OnLoad.cpp) | C++ | -10 | 0 | -2 | -12 |
| [android/app/src/main/res/drawable/rn_edit_text_material.xml](/android/app/src/main/res/drawable/rn_edit_text_material.xml) | XML | -11 | -23 | -3 | -37 |
| [android/app/src/main/res/drawable/splashscreen.xml](/android/app/src/main/res/drawable/splashscreen.xml) | XML | -3 | 0 | 0 | -3 |
| [android/app/src/main/res/mipmap-anydpi-v26/ic_launcher.xml](/android/app/src/main/res/mipmap-anydpi-v26/ic_launcher.xml) | XML | -5 | 0 | 0 | -5 |
| [android/app/src/main/res/mipmap-anydpi-v26/ic_launcher_round.xml](/android/app/src/main/res/mipmap-anydpi-v26/ic_launcher_round.xml) | XML | -5 | 0 | 0 | -5 |
| [android/app/src/main/res/values-night/colors.xml](/android/app/src/main/res/values-night/colors.xml) | XML | -1 | 0 | 0 | -1 |
| [android/app/src/main/res/values/colors.xml](/android/app/src/main/res/values/colors.xml) | XML | -6 | 0 | 0 | -6 |
| [android/app/src/main/res/values/strings.xml](/android/app/src/main/res/values/strings.xml) | XML | -5 | 0 | 0 | -5 |
| [android/app/src/main/res/values/styles.xml](/android/app/src/main/res/values/styles.xml) | XML | -17 | 0 | 0 | -17 |
| [android/build.gradle](/android/build.gradle) | Groovy | -48 | -7 | -6 | -61 |
| [android/gradle.properties](/android/gradle.properties) | Java Properties | -10 | -32 | -12 | -54 |
| [android/gradle/wrapper/gradle-wrapper.properties](/android/gradle/wrapper/gradle-wrapper.properties) | Java Properties | -5 | 0 | -1 | -6 |
| [android/gradlew.bat](/android/gradlew.bat) | Batch | -68 | 0 | -22 | -90 |
| [android/settings.gradle](/android/settings.gradle) | Groovy | -11 | 0 | -5 | -16 |
| [app.json](/app.json) | JSON with Comments | 0 | 6 | 0 | 6 |
| [assets/icons/calendar.svg](/assets/icons/calendar.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/clear.svg](/assets/icons/clear.svg) | XML | 4 | 0 | 1 | 5 |
| [assets/icons/here.svg](/assets/icons/here.svg) | XML | 4 | 0 | 1 | 5 |
| [assets/icons/index.ts](/assets/icons/index.ts) | TypeScript | 2 | 0 | 0 | 2 |
| [assets/icons/like-outline.svg](/assets/icons/like-outline.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/like-red.svg](/assets/icons/like-red.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/like-white.svg](/assets/icons/like-white.svg) | XML | -2 | 0 | 1 | -1 |
| [components/BlockEvents/BlockEvent.tsx](/components/BlockEvents/BlockEvent.tsx) | TypeScript JSX | -1 | 0 | 1 | 0 |
| [components/BlockPlaces/BlockPlace.tsx](/components/BlockPlaces/BlockPlace.tsx) | TypeScript JSX | 68 | 0 | 8 | 76 |
| [components/BlockPlaces/BlockPlace/BlockPlace.tsx](/components/BlockPlaces/BlockPlace/BlockPlace.tsx) | TypeScript JSX | -62 | 0 | -9 | -71 |
| [components/BlockPlaces/BlockPlaces.tsx](/components/BlockPlaces/BlockPlaces.tsx) | TypeScript JSX | 29 | 0 | 2 | 31 |
| [components/Blocks/HeaderBackAndFollow.tsx](/components/Blocks/HeaderBackAndFollow.tsx) | TypeScript JSX | 1 | 0 | 0 | 1 |
| [components/BottomSheetIndicator/BottomSheetIndicator.tsx](/components/BottomSheetIndicator/BottomSheetIndicator.tsx) | TypeScript JSX | 131 | 2 | 8 | 141 |
| [components/Button/Button.tsx](/components/Button/Button.tsx) | TypeScript JSX | -1 | 0 | 0 | -1 |
| [components/Button/FiltersButton.tsx](/components/Button/FiltersButton.tsx) | TypeScript JSX | 22 | 0 | 4 | 26 |
| [components/Button/FollowButton.tsx](/components/Button/FollowButton.tsx) | TypeScript JSX | 1 | -1 | 2 | 2 |
| [components/Button/OpenMap.tsx](/components/Button/OpenMap.tsx) | TypeScript JSX | 73 | 0 | 10 | 83 |
| [components/ChangeAddress/ChangeAddress.tsx](/components/ChangeAddress/ChangeAddress.tsx) | TypeScript JSX | -57 | 0 | -9 | -66 |
| [components/Checkbox/SelectorLine.tsx](/components/Checkbox/SelectorLine.tsx) | TypeScript JSX | 69 | 0 | 9 | 78 |
| [components/Checkbox/SelectorSquare.tsx](/components/Checkbox/SelectorSquare.tsx) | TypeScript JSX | 87 | 0 | 13 | 100 |
| [components/ConnectModal/ConnectModal.tsx](/components/ConnectModal/ConnectModal.tsx) | TypeScript JSX | -31 | 0 | -4 | -35 |
| [components/DiscoverTabView/DiscoverTabView.tsx](/components/DiscoverTabView/DiscoverTabView.tsx) | TypeScript JSX | -185 | 0 | -22 | -207 |
| [components/DiscoverTabView/helper.tsx](/components/DiscoverTabView/helper.tsx) | TypeScript JSX | 18 | 0 | 8 | 26 |
| [components/DiscoverTabView/screens/All.tsx](/components/DiscoverTabView/screens/All.tsx) | TypeScript JSX | -38 | 0 | -2 | -40 |
| [components/DiscoverTabView/screens/Bars.tsx](/components/DiscoverTabView/screens/Bars.tsx) | TypeScript JSX | 58 | 0 | 9 | 67 |
| [components/DiscoverTabView/screens/Clubs.tsx](/components/DiscoverTabView/screens/Clubs.tsx) | TypeScript JSX | -11 | 0 | 1 | -10 |
| [components/DiscoverTabView/screens/Content.tsx](/components/DiscoverTabView/screens/Content.tsx) | TypeScript JSX | 93 | 0 | 14 | 107 |
| [components/DiscoverTabView/screens/ThisWeek.tsx](/components/DiscoverTabView/screens/ThisWeek.tsx) | TypeScript JSX | -5 | 0 | 5 | 0 |
| [components/DiscoverTabView/screens/Today.tsx](/components/DiscoverTabView/screens/Today.tsx) | TypeScript JSX | 5 | 0 | 7 | 12 |
| [components/Filters/CheckboxList.tsx](/components/Filters/CheckboxList.tsx) | TypeScript JSX | 91 | 0 | 9 | 100 |
| [components/Filters/EventsType.tsx](/components/Filters/EventsType.tsx) | TypeScript JSX | 33 | 0 | 3 | 36 |
| [components/Filters/FiltersButtons.tsx](/components/Filters/FiltersButtons.tsx) | TypeScript JSX | 48 | 0 | 6 | 54 |
| [components/Headers/MainHeader.tsx](/components/Headers/MainHeader.tsx) | TypeScript JSX | 228 | 0 | 14 | 242 |
| [components/LoginButton/GoogleLoginButton.tsx](/components/LoginButton/GoogleLoginButton.tsx) | TypeScript JSX | -90 | 0 | -11 | -101 |
| [components/MiniaturesPlan/MiniaturesPlan.tsx](/components/MiniaturesPlan/MiniaturesPlan.tsx) | TypeScript JSX | 34 | 0 | 7 | 41 |
| [components/Rubriques/Rubrique.tsx](/components/Rubriques/Rubrique.tsx) | TypeScript JSX | 41 | 0 | 7 | 48 |
| [components/Rubriques/Rubriques.tsx](/components/Rubriques/Rubriques.tsx) | TypeScript JSX | 65 | 0 | 9 | 74 |
| [components/SearchMap/Map.tsx](/components/SearchMap/Map.tsx) | TypeScript JSX | 201 | 6 | 19 | 226 |
| [components/SearchMap/Marker.tsx](/components/SearchMap/Marker.tsx) | TypeScript JSX | 58 | 0 | 5 | 63 |
| [components/SearchMap/Markers.tsx](/components/SearchMap/Markers.tsx) | TypeScript JSX | 133 | 0 | 20 | 153 |
| [components/SearchMap/SearchMap.tsx](/components/SearchMap/SearchMap.tsx) | TypeScript JSX | -86 | -2 | -12 | -100 |
| [components/SearchMap/useMarkers.ts](/components/SearchMap/useMarkers.ts) | TypeScript | 69 | 22 | 14 | 105 |
| [components/SearchMap/useMarkers.tsx](/components/SearchMap/useMarkers.tsx) | TypeScript JSX | 107 | 0 | 19 | 126 |
| [components/Section/Section.tsx](/components/Section/Section.tsx) | TypeScript JSX | 4 | 0 | 0 | 4 |
| [components/Section/SectionHeader.tsx](/components/Section/SectionHeader.tsx) | TypeScript JSX | 23 | 0 | 2 | 25 |
| [components/SignInOut/GoogleLoginButton.tsx](/components/SignInOut/GoogleLoginButton.tsx) | TypeScript JSX | 93 | 0 | 11 | 104 |
| [components/SignInOut/Register.tsx](/components/SignInOut/Register.tsx) | TypeScript JSX | 50 | 0 | 8 | 58 |
| [components/Slider/Slider.tsx](/components/Slider/Slider.tsx) | TypeScript JSX | 123 | 0 | 18 | 141 |
| [components/SwipeCards/SwipeCards.tsx](/components/SwipeCards/SwipeCards.tsx) | TypeScript JSX | -2 | 0 | -2 | -4 |
| [components/SwitchButton/SwitchButton.tsx](/components/SwitchButton/SwitchButton.tsx) | TypeScript JSX | -94 | 0 | -10 | -104 |
| [constants/theme.ts](/constants/theme.ts) | TypeScript | 2 | 0 | 1 | 3 |
| [eas.json](/eas.json) | JSON with Comments | 3 | 0 | 0 | 3 |
| [helpers/date.ts](/helpers/date.ts) | TypeScript | 2 | 0 | 1 | 3 |
| [hooks/useCachedResources.ts](/hooks/useCachedResources.ts) | TypeScript | 2 | 0 | -1 | 1 |
| [hooks/useFollow.ts](/hooks/useFollow.ts) | TypeScript | -13 | 0 | -4 | -17 |
| [hooks/useMainTheme.ts](/hooks/useMainTheme.ts) | TypeScript | 10 | 0 | 4 | 14 |
| [hooks/useSplashScreen.ts](/hooks/useSplashScreen.ts) | TypeScript | 14 | 0 | 4 | 18 |
| [index.js](/index.js) | JavaScript | 2 | 0 | 0 | 2 |
| [ios/Cosh/Cosh-Bridging-Header.h](/ios/Cosh/Cosh-Bridging-Header.h) | C++ | 0 | 3 | 1 | 4 |
| [ios/Cosh/Images.xcassets/AppIcon.appiconset/Contents.json](/ios/Cosh/Images.xcassets/AppIcon.appiconset/Contents.json) | JSON | 1 | 0 | 0 | 1 |
| [ios/Cosh/SplashScreen.storyboard](/ios/Cosh/SplashScreen.storyboard) | XML | -39 | -1 | -1 | -41 |
| [ios/Cosh/noop-file.swift](/ios/Cosh/noop-file.swift) | Swift | 0 | 4 | 1 | 5 |
| [navigation/DiscoverNav.tsx](/navigation/DiscoverNav.tsx) | TypeScript JSX | 43 | 0 | 5 | 48 |
| [navigation/index.tsx](/navigation/index.tsx) | TypeScript JSX | 40 | 0 | -1 | 39 |
| [package-lock.json](/package-lock.json) | JSON | -2,588 | 0 | 0 | -2,588 |
| [package.json](/package.json) | JSON | 7 | 0 | 0 | 7 |
| [provider/AnimatedHeaderProvider.tsx](/provider/AnimatedHeaderProvider.tsx) | TypeScript JSX | 13 | 0 | 0 | 13 |
| [provider/EventProvider.tsx](/provider/EventProvider.tsx) | TypeScript JSX | -58 | 0 | 0 | -58 |
| [provider/FiltersProvider.tsx](/provider/FiltersProvider.tsx) | TypeScript JSX | 673 | 0 | 7 | 680 |
| [provider/FollowProvider.tsx](/provider/FollowProvider.tsx) | TypeScript JSX | 52 | 0 | 8 | 60 |
| [provider/LocationProvider.tsx](/provider/LocationProvider.tsx) | TypeScript JSX | 1 | 0 | 1 | 2 |
| [provider/MapStore.tsx](/provider/MapStore.tsx) | TypeScript JSX | 101 | 1 | 7 | 109 |
| [provider/MapsProvider.tsx](/provider/MapsProvider.tsx) | TypeScript JSX | 3 | 0 | -1 | 2 |
| [provider/PlacesProvider.tsx](/provider/PlacesProvider.tsx) | TypeScript JSX | 6 | 0 | 0 | 6 |
| [provider/ThemeProvider.tsx](/provider/ThemeProvider.tsx) | TypeScript JSX | 44 | 1 | 10 | 55 |
| [screens/Discover.tsx](/screens/Discover.tsx) | TypeScript JSX | 46 | 0 | 3 | 49 |
| [screens/Event.tsx](/screens/Event.tsx) | TypeScript JSX | 25 | 0 | -12 | 13 |
| [screens/Filters.tsx](/screens/Filters.tsx) | TypeScript JSX | 179 | 0 | 24 | 203 |
| [screens/MyEvents.tsx](/screens/MyEvents.tsx) | TypeScript JSX | -11 | -5 | 0 | -16 |
| [screens/MyEventsTickets.tsx](/screens/MyEventsTickets.tsx) | TypeScript JSX | 6 | 0 | -1 | 5 |
| [screens/MyEventsWishlist.tsx](/screens/MyEventsWishlist.tsx) | TypeScript JSX | -9 | 0 | -1 | -10 |
| [screens/Place.tsx](/screens/Place.tsx) | TypeScript JSX | -36 | 3 | -6 | -39 |
| [screens/Search.tsx](/screens/Search.tsx) | TypeScript JSX | 289 | -25 | 26 | 290 |
| [screens/SignInOut.tsx](/screens/SignInOut.tsx) | TypeScript JSX | 245 | 1 | 24 | 270 |
| [services/events/getDailyEvents.ts](/services/events/getDailyEvents.ts) | TypeScript | 28 | 0 | 3 | 31 |
| [services/events/getEventsByName.ts](/services/events/getEventsByName.ts) | TypeScript | 17 | 0 | 3 | 20 |
| [services/events/getWeeklyEvents.ts](/services/events/getWeeklyEvents.ts) | TypeScript | 26 | 0 | 3 | 29 |
| [services/follow/follow.ts](/services/follow/follow.ts) | TypeScript | 48 | 0 | 7 | 55 |
| [services/place/getClubs.ts](/services/place/getClubs.ts) | TypeScript | 28 | 0 | 3 | 31 |
| [services/place/getPlaceById.ts](/services/place/getPlaceById.ts) | TypeScript | -1 | 0 | 0 | -1 |
| [services/place/getPlaces.ts](/services/place/getPlaces.ts) | TypeScript | 16 | 0 | 1 | 17 |
| [services/topics/getBarsTopics.ts](/services/topics/getBarsTopics.ts) | TypeScript | 12 | 0 | 3 | 15 |
| [services/topics/getClubsTopics.ts](/services/topics/getClubsTopics.ts) | TypeScript | 12 | 0 | 3 | 15 |
| [services/types.ts](/services/types.ts) | TypeScript | 47 | 0 | 5 | 52 |
| [store.ts](/store.ts) | TypeScript | 16 | 3 | 4 | 23 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details