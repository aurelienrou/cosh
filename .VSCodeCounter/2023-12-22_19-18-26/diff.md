# Diff Summary

Date : 2023-12-22 19:18:26

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 128 files,  -277 codes, -294 comments, 125 blanks, all -446 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript JSX | 63 | 2,958 | -18 | 270 | 3,210 |
| TypeScript | 19 | 337 | 25 | 54 | 416 |
| JSON with Comments | 2 | 3 | 6 | 0 | 9 |
| JavaScript | 1 | 2 | 0 | 0 | 2 |
| Swift | 1 | 0 | 4 | 1 | 5 |
| Java Properties | 2 | -15 | -32 | -13 | -60 |
| Makefile | 1 | -28 | -12 | -10 | -50 |
| Batch | 1 | -68 | 0 | -22 | -90 |
| XML | 17 | -122 | -24 | -1 | -147 |
| C++ | 8 | -160 | -18 | -52 | -230 |
| Groovy | 3 | -250 | -150 | -47 | -447 |
| Java | 6 | -307 | -75 | -55 | -437 |
| JSON | 4 | -2,627 | 0 | 0 | -2,627 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 128 | -277 | -294 | 125 | -446 |
| . (Files) | 7 | -2,578 | 10 | 4 | -2,564 |
| android | 31 | -973 | -313 | -206 | -1,492 |
| android (Files) | 4 | -137 | -39 | -45 | -221 |
| android/app | 26 | -831 | -274 | -160 | -1,265 |
| android/app (Files) | 2 | -238 | -143 | -36 | -417 |
| android/app/src | 24 | -593 | -131 | -124 | -848 |
| android/app/src/debug | 2 | -64 | -8 | -5 | -77 |
| android/app/src/debug (Files) | 1 | -5 | 0 | -3 | -8 |
| android/app/src/debug/java | 1 | -59 | -8 | -2 | -69 |
| android/app/src/debug/java/com | 1 | -59 | -8 | -2 | -69 |
| android/app/src/debug/java/com/cosh | 1 | -59 | -8 | -2 | -69 |
| android/app/src/main | 22 | -529 | -123 | -119 | -771 |
| android/app/src/main (Files) | 1 | -40 | 0 | 0 | -40 |
| android/app/src/main/java | 5 | -248 | -67 | -53 | -368 |
| android/app/src/main/java/com | 5 | -248 | -67 | -53 | -368 |
| android/app/src/main/java/com/cosh | 5 | -248 | -67 | -53 | -368 |
| android/app/src/main/java/com/cosh (Files) | 2 | -113 | -29 | -23 | -165 |
| android/app/src/main/java/com/cosh/newarchitecture | 3 | -135 | -38 | -30 | -203 |
| android/app/src/main/java/com/cosh/newarchitecture (Files) | 1 | -83 | -20 | -14 | -117 |
| android/app/src/main/java/com/cosh/newarchitecture/components | 1 | -22 | -8 | -7 | -37 |
| android/app/src/main/java/com/cosh/newarchitecture/modules | 1 | -30 | -10 | -9 | -49 |
| android/app/src/main/jni | 8 | -188 | -33 | -63 | -284 |
| android/app/src/main/res | 8 | -53 | -23 | -3 | -79 |
| android/app/src/main/res/drawable | 2 | -14 | -23 | -3 | -40 |
| android/app/src/main/res/mipmap-anydpi-v26 | 2 | -10 | 0 | 0 | -10 |
| android/app/src/main/res/values | 3 | -28 | 0 | 0 | -28 |
| android/app/src/main/res/values-night | 1 | -1 | 0 | 0 | -1 |
| android/gradle | 1 | -5 | 0 | -1 | -6 |
| android/gradle/wrapper | 1 | -5 | 0 | -1 | -6 |
| assets | 7 | 17 | 0 | 6 | 23 |
| assets/icons | 7 | 17 | 0 | 6 | 23 |
| components | 43 | 1,393 | 27 | 191 | 1,611 |
| components/BlockEvents | 1 | -1 | 0 | 1 | 0 |
| components/BlockPlaces | 3 | 35 | 0 | 1 | 36 |
| components/BlockPlaces (Files) | 2 | 97 | 0 | 10 | 107 |
| components/BlockPlaces/BlockPlace | 1 | -62 | 0 | -9 | -71 |
| components/Blocks | 1 | 1 | 0 | 0 | 1 |
| components/BottomSheetIndicator | 1 | 131 | 2 | 8 | 141 |
| components/Button | 4 | 95 | -1 | 16 | 110 |
| components/ChangeAddress | 1 | -57 | 0 | -9 | -66 |
| components/Checkbox | 2 | 156 | 0 | 22 | 178 |
| components/ConnectModal | 1 | -31 | 0 | -4 | -35 |
| components/DiscoverTabView | 8 | -65 | 0 | 20 | -45 |
| components/DiscoverTabView (Files) | 2 | -167 | 0 | -14 | -181 |
| components/DiscoverTabView/screens | 6 | 102 | 0 | 34 | 136 |
| components/Filters | 3 | 172 | 0 | 18 | 190 |
| components/Headers | 1 | 228 | 0 | 14 | 242 |
| components/LoginButton | 1 | -90 | 0 | -11 | -101 |
| components/MiniaturesPlan | 1 | 34 | 0 | 7 | 41 |
| components/Rubriques | 2 | 106 | 0 | 16 | 122 |
| components/SearchMap | 6 | 482 | 26 | 65 | 573 |
| components/Section | 2 | 27 | 0 | 2 | 29 |
| components/SignInOut | 2 | 143 | 0 | 19 | 162 |
| components/Slider | 1 | 123 | 0 | 18 | 141 |
| components/SwipeCards | 1 | -2 | 0 | -2 | -4 |
| components/SwitchButton | 1 | -94 | 0 | -10 | -104 |
| constants | 1 | 2 | 0 | 1 | 3 |
| helpers | 1 | 2 | 0 | 1 | 3 |
| hooks | 4 | 13 | 0 | 3 | 16 |
| ios | 4 | -38 | 6 | 1 | -31 |
| ios/Cosh | 4 | -38 | 6 | 1 | -31 |
| ios/Cosh (Files) | 3 | -39 | 6 | 1 | -32 |
| ios/Cosh/Images.xcassets | 1 | 1 | 0 | 0 | 1 |
| ios/Cosh/Images.xcassets/AppIcon.appiconset | 1 | 1 | 0 | 0 | 1 |
| navigation | 2 | 83 | 0 | 4 | 87 |
| provider | 9 | 835 | 2 | 32 | 869 |
| screens | 9 | 734 | -26 | 57 | 765 |
| services | 10 | 233 | 0 | 31 | 264 |
| services (Files) | 1 | 47 | 0 | 5 | 52 |
| services/events | 3 | 71 | 0 | 9 | 80 |
| services/follow | 1 | 48 | 0 | 7 | 55 |
| services/place | 3 | 43 | 0 | 4 | 47 |
| services/topics | 2 | 24 | 0 | 6 | 30 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)