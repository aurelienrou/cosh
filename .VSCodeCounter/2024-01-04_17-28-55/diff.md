# Diff Summary

Date : 2024-01-04 17:28:55

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 60 files,  666 codes, 196 comments, 59 blanks, all 921 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript JSX | 29 | 174 | 15 | -35 | 154 |
| Java | 4 | 165 | 47 | 32 | 244 |
| Groovy | 3 | 147 | 76 | 35 | 258 |
| XML | 10 | 98 | 23 | 6 | 127 |
| JSON | 2 | 56 | 0 | 0 | 56 |
| Batch | 1 | 41 | 29 | 22 | 92 |
| Properties | 2 | 18 | 33 | 14 | 65 |
| JavaScript | 1 | 10 | 0 | 0 | 10 |
| JSON with Comments | 1 | 0 | 10 | 0 | 10 |
| Objective-C++ | 1 | -2 | 0 | -1 | -3 |
| TypeScript | 6 | -41 | -37 | -14 | -92 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 60 | 666 | 196 | 59 | 921 |
| . (Files) | 3 | 12 | 10 | 0 | 22 |
| android | 21 | 523 | 208 | 109 | 840 |
| android (Files) | 4 | 105 | 68 | 45 | 218 |
| android/app | 16 | 412 | 140 | 63 | 615 |
| android/app (Files) | 2 | 149 | 70 | 25 | 244 |
| android/app/src | 14 | 263 | 70 | 38 | 371 |
| android/app/src/debug | 2 | 63 | 12 | 9 | 84 |
| android/app/src/debug (Files) | 1 | 5 | 0 | 3 | 8 |
| android/app/src/debug/java | 1 | 58 | 12 | 6 | 76 |
| android/app/src/debug/java/com | 1 | 58 | 12 | 6 | 76 |
| android/app/src/debug/java/com/cosh | 1 | 58 | 12 | 6 | 76 |
| android/app/src/main | 11 | 193 | 47 | 26 | 266 |
| android/app/src/main (Files) | 1 | 40 | 0 | 0 | 40 |
| android/app/src/main/java | 2 | 100 | 24 | 23 | 147 |
| android/app/src/main/java/com | 2 | 100 | 24 | 23 | 147 |
| android/app/src/main/java/com/cosh | 2 | 100 | 24 | 23 | 147 |
| android/app/src/main/res | 8 | 53 | 23 | 3 | 79 |
| android/app/src/main/res/drawable | 2 | 14 | 23 | 3 | 40 |
| android/app/src/main/res/mipmap-anydpi-v26 | 2 | 10 | 0 | 0 | 10 |
| android/app/src/main/res/values | 3 | 28 | 0 | 0 | 28 |
| android/app/src/main/res/values-night | 1 | 1 | 0 | 0 | 1 |
| android/app/src/release | 1 | 7 | 11 | 3 | 21 |
| android/app/src/release/java | 1 | 7 | 11 | 3 | 21 |
| android/app/src/release/java/com | 1 | 7 | 11 | 3 | 21 |
| android/app/src/release/java/com/cosh | 1 | 7 | 11 | 3 | 21 |
| android/gradle | 1 | 6 | 0 | 1 | 7 |
| android/gradle/wrapper | 1 | 6 | 0 | 1 | 7 |
| components | 21 | -140 | -7 | -55 | -202 |
| components/BlockEvents | 1 | 5 | 0 | 0 | 5 |
| components/BlockPlaces | 1 | 22 | 0 | 0 | 22 |
| components/Button | 1 | 5 | 0 | 0 | 5 |
| components/DiscoverTabView | 5 | 69 | 4 | -1 | 72 |
| components/DiscoverTabView (Files) | 1 | -2 | 0 | 0 | -2 |
| components/DiscoverTabView/screens | 4 | 71 | 4 | -1 | 74 |
| components/EventMiniature | 1 | 2 | 0 | 0 | 2 |
| components/Loader | 2 | 71 | 0 | 6 | 77 |
| components/Rubriques | 2 | -2 | 0 | 0 | -2 |
| components/SearchMap | 5 | -326 | -28 | -55 | -409 |
| components/Section | 1 | 13 | 0 | -3 | 10 |
| components/SignInOut | 2 | 1 | 17 | -2 | 16 |
| ios | 1 | -2 | 0 | -1 | -3 |
| ios/Cosh | 1 | -2 | 0 | -1 | -3 |
| navigation | 1 | 24 | 0 | 1 | 25 |
| provider | 2 | -39 | 0 | -5 | -44 |
| screens | 6 | 260 | 0 | 10 | 270 |
| services | 5 | 28 | -15 | 0 | 13 |
| services (Files) | 2 | -9 | -15 | -6 | -30 |
| services/auth | 1 | 21 | 0 | 3 | 24 |
| services/place | 2 | 16 | 0 | 3 | 19 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)