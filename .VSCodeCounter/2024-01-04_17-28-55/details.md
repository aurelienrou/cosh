# Details

Date : 2024-01-04 17:28:55

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 212 files,  42707 codes, 469 comments, 1282 blanks, all 44458 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [ react-native.config.js](/%20react-native.config.js) | JavaScript | 10 | 0 | 0 | 10 |
| [.eslintrc.js](/.eslintrc.js) | JavaScript | 21 | 0 | 1 | 22 |
| [.expo-shared/assets.json](/.expo-shared/assets.json) | JSON | 6 | 0 | 1 | 7 |
| [.prettierrc.js](/.prettierrc.js) | JavaScript | 7 | 0 | 1 | 8 |
| [App.tsx](/App.tsx) | TypeScript JSX | 53 | 1 | 8 | 62 |
| [android/app/build.gradle](/android/app/build.gradle) | Groovy | 95 | 70 | 25 | 190 |
| [android/app/google-services.json](/android/app/google-services.json) | JSON | 54 | 0 | 0 | 54 |
| [android/app/src/debug/AndroidManifest.xml](/android/app/src/debug/AndroidManifest.xml) | XML | 5 | 0 | 3 | 8 |
| [android/app/src/debug/java/com/cosh/ReactNativeFlipper.java](/android/app/src/debug/java/com/cosh/ReactNativeFlipper.java) | Java | 58 | 12 | 6 | 76 |
| [android/app/src/main/AndroidManifest.xml](/android/app/src/main/AndroidManifest.xml) | XML | 40 | 0 | 0 | 40 |
| [android/app/src/main/java/com/cosh/MainActivity.java](/android/app/src/main/java/com/cosh/MainActivity.java) | Java | 36 | 21 | 9 | 66 |
| [android/app/src/main/java/com/cosh/MainApplication.java](/android/app/src/main/java/com/cosh/MainApplication.java) | Java | 64 | 3 | 14 | 81 |
| [android/app/src/main/res/drawable/rn_edit_text_material.xml](/android/app/src/main/res/drawable/rn_edit_text_material.xml) | XML | 11 | 23 | 3 | 37 |
| [android/app/src/main/res/drawable/splashscreen.xml](/android/app/src/main/res/drawable/splashscreen.xml) | XML | 3 | 0 | 0 | 3 |
| [android/app/src/main/res/mipmap-anydpi-v26/ic_launcher.xml](/android/app/src/main/res/mipmap-anydpi-v26/ic_launcher.xml) | XML | 5 | 0 | 0 | 5 |
| [android/app/src/main/res/mipmap-anydpi-v26/ic_launcher_round.xml](/android/app/src/main/res/mipmap-anydpi-v26/ic_launcher_round.xml) | XML | 5 | 0 | 0 | 5 |
| [android/app/src/main/res/values-night/colors.xml](/android/app/src/main/res/values-night/colors.xml) | XML | 1 | 0 | 0 | 1 |
| [android/app/src/main/res/values/colors.xml](/android/app/src/main/res/values/colors.xml) | XML | 6 | 0 | 0 | 6 |
| [android/app/src/main/res/values/strings.xml](/android/app/src/main/res/values/strings.xml) | XML | 5 | 0 | 0 | 5 |
| [android/app/src/main/res/values/styles.xml](/android/app/src/main/res/values/styles.xml) | XML | 17 | 0 | 0 | 17 |
| [android/app/src/release/java/com/cosh/ReactNativeFlipper.java](/android/app/src/release/java/com/cosh/ReactNativeFlipper.java) | Java | 7 | 11 | 3 | 21 |
| [android/build.gradle](/android/build.gradle) | Groovy | 45 | 6 | 6 | 57 |
| [android/gradle.properties](/android/gradle.properties) | Properties | 12 | 33 | 13 | 58 |
| [android/gradle/wrapper/gradle-wrapper.properties](/android/gradle/wrapper/gradle-wrapper.properties) | Properties | 6 | 0 | 1 | 7 |
| [android/gradlew.bat](/android/gradlew.bat) | Batch | 41 | 29 | 22 | 92 |
| [android/settings.gradle](/android/settings.gradle) | Groovy | 7 | 0 | 4 | 11 |
| [app.json](/app.json) | JSON with Comments | 20 | 76 | 0 | 96 |
| [assets/icons/arrow.svg](/assets/icons/arrow.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/art.svg](/assets/icons/art.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/bar.svg](/assets/icons/bar.svg) | XML | 4 | 0 | 1 | 5 |
| [assets/icons/calendar.svg](/assets/icons/calendar.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/chevron.svg](/assets/icons/chevron.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/clear.svg](/assets/icons/clear.svg) | XML | 4 | 0 | 1 | 5 |
| [assets/icons/club.svg](/assets/icons/club.svg) | XML | 4 | 0 | 1 | 5 |
| [assets/icons/game.svg](/assets/icons/game.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/here.svg](/assets/icons/here.svg) | XML | 4 | 0 | 1 | 5 |
| [assets/icons/home-outline.svg](/assets/icons/home-outline.svg) | XML | 5 | 0 | 0 | 5 |
| [assets/icons/home.svg](/assets/icons/home.svg) | XML | 4 | 0 | 1 | 5 |
| [assets/icons/index.ts](/assets/icons/index.ts) | TypeScript | 12 | 0 | 2 | 14 |
| [assets/icons/like-outline.svg](/assets/icons/like-outline.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/like-red.svg](/assets/icons/like-red.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/like-white.svg](/assets/icons/like-white.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/like.svg](/assets/icons/like.svg) | XML | 5 | 0 | 0 | 5 |
| [assets/icons/location.svg](/assets/icons/location.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/map.svg](/assets/icons/map.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/notes-outline.svg](/assets/icons/notes-outline.svg) | XML | 8 | 0 | 0 | 8 |
| [assets/icons/notes.svg](/assets/icons/notes.svg) | XML | 3 | 0 | 1 | 4 |
| [assets/icons/search-outline.svg](/assets/icons/search-outline.svg) | XML | 5 | 0 | 0 | 5 |
| [assets/icons/search.svg](/assets/icons/search.svg) | XML | 5 | 0 | 0 | 5 |
| [assets/icons/settings.svg](/assets/icons/settings.svg) | XML | 8 | 0 | 1 | 9 |
| [assets/icons/user-outline.svg](/assets/icons/user-outline.svg) | XML | 6 | 0 | 0 | 6 |
| [assets/icons/user.svg](/assets/icons/user.svg) | XML | 4 | 0 | 1 | 5 |
| [babel.config.js](/babel.config.js) | JavaScript | 7 | 0 | 1 | 8 |
| [components/AddressEditorModal/AddressEditorModal.tsx](/components/AddressEditorModal/AddressEditorModal.tsx) | TypeScript JSX | 52 | 1 | 5 | 58 |
| [components/BlockEvents/BlockEvent.tsx](/components/BlockEvents/BlockEvent.tsx) | TypeScript JSX | 99 | 0 | 11 | 110 |
| [components/BlockEvents/BlockEvents.tsx](/components/BlockEvents/BlockEvents.tsx) | TypeScript JSX | 40 | 0 | 6 | 46 |
| [components/BlockPlaces/BlockPlace.tsx](/components/BlockPlaces/BlockPlace.tsx) | TypeScript JSX | 90 | 0 | 8 | 98 |
| [components/BlockPlaces/BlockPlaces.tsx](/components/BlockPlaces/BlockPlaces.tsx) | TypeScript JSX | 49 | 0 | 6 | 55 |
| [components/BlockToFollow/BlockToFollow.tsx](/components/BlockToFollow/BlockToFollow.tsx) | TypeScript JSX | 58 | 1 | 8 | 67 |
| [components/Blocks/Accordeon.tsx](/components/Blocks/Accordeon.tsx) | TypeScript JSX | 90 | 0 | 10 | 100 |
| [components/Blocks/Adress.tsx](/components/Blocks/Adress.tsx) | TypeScript JSX | 26 | 0 | 4 | 30 |
| [components/Blocks/Chevron.tsx](/components/Blocks/Chevron.tsx) | TypeScript JSX | 26 | 0 | 5 | 31 |
| [components/Blocks/HeaderBackAndFollow.tsx](/components/Blocks/HeaderBackAndFollow.tsx) | TypeScript JSX | 59 | 0 | 8 | 67 |
| [components/Blocks/Line.tsx](/components/Blocks/Line.tsx) | TypeScript JSX | 7 | 0 | 2 | 9 |
| [components/Blocks/Map.tsx](/components/Blocks/Map.tsx) | TypeScript JSX | 62 | 0 | 6 | 68 |
| [components/BottomSheetIndicator/BottomSheetIndicator.tsx](/components/BottomSheetIndicator/BottomSheetIndicator.tsx) | TypeScript JSX | 131 | 2 | 8 | 141 |
| [components/Button/Button.tsx](/components/Button/Button.tsx) | TypeScript JSX | 75 | 0 | 6 | 81 |
| [components/Button/FiltersButton.tsx](/components/Button/FiltersButton.tsx) | TypeScript JSX | 22 | 0 | 4 | 26 |
| [components/Button/FollowButton.tsx](/components/Button/FollowButton.tsx) | TypeScript JSX | 86 | 0 | 15 | 101 |
| [components/Button/FollowEventTextButton.tsx](/components/Button/FollowEventTextButton.tsx) | TypeScript JSX | 83 | 1 | 12 | 96 |
| [components/Button/Link.tsx](/components/Button/Link.tsx) | TypeScript JSX | 54 | 0 | 5 | 59 |
| [components/Button/OpenMap.tsx](/components/Button/OpenMap.tsx) | TypeScript JSX | 78 | 0 | 10 | 88 |
| [components/Carousel/Carousel.tsx](/components/Carousel/Carousel.tsx) | TypeScript JSX | 157 | 0 | 19 | 176 |
| [components/Checkbox/SelectorLine.tsx](/components/Checkbox/SelectorLine.tsx) | TypeScript JSX | 69 | 0 | 9 | 78 |
| [components/Checkbox/SelectorSquare.tsx](/components/Checkbox/SelectorSquare.tsx) | TypeScript JSX | 87 | 0 | 13 | 100 |
| [components/CirclePeoples/CirclePeoples.tsx](/components/CirclePeoples/CirclePeoples.tsx) | TypeScript JSX | 36 | 0 | 5 | 41 |
| [components/CoshText/CoshText.tsx](/components/CoshText/CoshText.tsx) | TypeScript JSX | 18 | 0 | 5 | 23 |
| [components/CreateEventForm/CreateEventForm.tsx](/components/CreateEventForm/CreateEventForm.tsx) | TypeScript JSX | 349 | 9 | 20 | 378 |
| [components/CreateEventForm/validation.ts](/components/CreateEventForm/validation.ts) | TypeScript | 84 | 0 | 2 | 86 |
| [components/CreateEventHeader/CreateEventHeader.tsx](/components/CreateEventHeader/CreateEventHeader.tsx) | TypeScript JSX | 85 | 0 | 8 | 93 |
| [components/CreatePlaceForm/CreatePlaceForm.tsx](/components/CreatePlaceForm/CreatePlaceForm.tsx) | TypeScript JSX | 352 | 10 | 16 | 378 |
| [components/CreatePlaceForm/validation.ts](/components/CreatePlaceForm/validation.ts) | TypeScript | 85 | 0 | 3 | 88 |
| [components/DiscoverTabView/DiscoverTabView.tsx](/components/DiscoverTabView/DiscoverTabView.tsx) | TypeScript JSX | 44 | 0 | 6 | 50 |
| [components/DiscoverTabView/helper.tsx](/components/DiscoverTabView/helper.tsx) | TypeScript JSX | 66 | 0 | 13 | 79 |
| [components/DiscoverTabView/screens/All.tsx](/components/DiscoverTabView/screens/All.tsx) | TypeScript JSX | 84 | 0 | 10 | 94 |
| [components/DiscoverTabView/screens/Bars.tsx](/components/DiscoverTabView/screens/Bars.tsx) | TypeScript JSX | 76 | 0 | 9 | 85 |
| [components/DiscoverTabView/screens/Clubs.tsx](/components/DiscoverTabView/screens/Clubs.tsx) | TypeScript JSX | 77 | 0 | 9 | 86 |
| [components/DiscoverTabView/screens/Content.tsx](/components/DiscoverTabView/screens/Content.tsx) | TypeScript JSX | 93 | 0 | 14 | 107 |
| [components/DiscoverTabView/screens/ThisWeek.tsx](/components/DiscoverTabView/screens/ThisWeek.tsx) | TypeScript JSX | 101 | 2 | 12 | 115 |
| [components/DiscoverTabView/screens/Today.tsx](/components/DiscoverTabView/screens/Today.tsx) | TypeScript JSX | 99 | 2 | 15 | 116 |
| [components/DiscoverTabView/screens/Tonight.tsx](/components/DiscoverTabView/screens/Tonight.tsx) | TypeScript JSX | 5 | 0 | 2 | 7 |
| [components/EventMiniature/EventMiniature.tsx](/components/EventMiniature/EventMiniature.tsx) | TypeScript JSX | 115 | 2 | 8 | 125 |
| [components/EventsList/EventsList.tsx](/components/EventsList/EventsList.tsx) | TypeScript JSX | 26 | 0 | 5 | 31 |
| [components/Filters/CheckboxList.tsx](/components/Filters/CheckboxList.tsx) | TypeScript JSX | 91 | 0 | 9 | 100 |
| [components/Filters/EventsType.tsx](/components/Filters/EventsType.tsx) | TypeScript JSX | 33 | 0 | 3 | 36 |
| [components/Filters/FiltersButtons.tsx](/components/Filters/FiltersButtons.tsx) | TypeScript JSX | 48 | 0 | 6 | 54 |
| [components/Headers/MainHeader.tsx](/components/Headers/MainHeader.tsx) | TypeScript JSX | 228 | 0 | 14 | 242 |
| [components/Headers/PlaceOrArtistHeader.tsx](/components/Headers/PlaceOrArtistHeader.tsx) | TypeScript JSX | 91 | 0 | 9 | 100 |
| [components/HoursSeletor/HoursSeletor.tsx](/components/HoursSeletor/HoursSeletor.tsx) | TypeScript JSX | 227 | 0 | 21 | 248 |
| [components/IconAndText/IconAndText.tsx](/components/IconAndText/IconAndText.tsx) | TypeScript JSX | 81 | 0 | 5 | 86 |
| [components/InputAddressFinder/InputAddressFinder.tsx](/components/InputAddressFinder/InputAddressFinder.tsx) | TypeScript JSX | 141 | 0 | 16 | 157 |
| [components/InputPlaceFinder.tsx/InputPlaceFinder.tsx](/components/InputPlaceFinder.tsx/InputPlaceFinder.tsx) | TypeScript JSX | 112 | 0 | 11 | 123 |
| [components/InputPlaceFinder/InputPlaceFinder.tsx](/components/InputPlaceFinder/InputPlaceFinder.tsx) | TypeScript JSX | 133 | 0 | 15 | 148 |
| [components/Loader/EventLoader.tsx](/components/Loader/EventLoader.tsx) | TypeScript JSX | 37 | 0 | 3 | 40 |
| [components/Loader/PlaceLoader.tsx](/components/Loader/PlaceLoader.tsx) | TypeScript JSX | 34 | 0 | 3 | 37 |
| [components/MiniaturesPlan/MiniaturesPlan.tsx](/components/MiniaturesPlan/MiniaturesPlan.tsx) | TypeScript JSX | 120 | 0 | 13 | 133 |
| [components/ModalDateTimePicker/ModalDateTimePicker.tsx](/components/ModalDateTimePicker/ModalDateTimePicker.tsx) | TypeScript JSX | 87 | 0 | 5 | 92 |
| [components/PeoplesAreJoined/PeoplesAreJoined.tsx](/components/PeoplesAreJoined/PeoplesAreJoined.tsx) | TypeScript JSX | 46 | 0 | 6 | 52 |
| [components/PriceRange/PriceRange.tsx](/components/PriceRange/PriceRange.tsx) | TypeScript JSX | 65 | 0 | 7 | 72 |
| [components/RadiusSelector/RadiusSelector.tsx](/components/RadiusSelector/RadiusSelector.tsx) | TypeScript JSX | 32 | 0 | 5 | 37 |
| [components/Rubriques/Rubrique.tsx](/components/Rubriques/Rubrique.tsx) | TypeScript JSX | 42 | 0 | 7 | 49 |
| [components/Rubriques/Rubriques.tsx](/components/Rubriques/Rubriques.tsx) | TypeScript JSX | 62 | 0 | 9 | 71 |
| [components/SearchMap/Map.tsx](/components/SearchMap/Map.tsx) | TypeScript JSX | 242 | 0 | 22 | 264 |
| [components/SearchMap/style.json](/components/SearchMap/style.json) | JSON | 186 | 0 | 0 | 186 |
| [components/Section/Section.tsx](/components/Section/Section.tsx) | TypeScript JSX | 33 | 0 | 4 | 37 |
| [components/Section/SectionHeader.tsx](/components/Section/SectionHeader.tsx) | TypeScript JSX | 76 | 0 | 4 | 80 |
| [components/SignInOut/GoogleLoginButton.tsx](/components/SignInOut/GoogleLoginButton.tsx) | TypeScript JSX | 85 | 17 | 9 | 111 |
| [components/SignInOut/Register.tsx](/components/SignInOut/Register.tsx) | TypeScript JSX | 59 | 0 | 8 | 67 |
| [components/Slider/Slider.tsx](/components/Slider/Slider.tsx) | TypeScript JSX | 123 | 0 | 18 | 141 |
| [components/SwipeCards/SwipeCards.tsx](/components/SwipeCards/SwipeCards.tsx) | TypeScript JSX | 268 | 0 | 23 | 291 |
| [components/Tags/Tags.tsx](/components/Tags/Tags.tsx) | TypeScript JSX | 40 | 0 | 8 | 48 |
| [components/Toast/Toast.tsx](/components/Toast/Toast.tsx) | TypeScript JSX | 115 | 0 | 8 | 123 |
| [components/UpdateEventButton/UpdateEventButton.tsx](/components/UpdateEventButton/UpdateEventButton.tsx) | TypeScript JSX | 17 | 18 | 4 | 39 |
| [components/UploadCover/UploadCover.tsx](/components/UploadCover/UploadCover.tsx) | TypeScript JSX | 120 | 2 | 12 | 134 |
| [constants/Colors.ts](/constants/Colors.ts) | TypeScript | 24 | 0 | 2 | 26 |
| [constants/Icon.ts](/constants/Icon.ts) | TypeScript | 10 | 0 | 2 | 12 |
| [constants/Layout.ts](/constants/Layout.ts) | TypeScript | 7 | 0 | 4 | 11 |
| [constants/theme.ts](/constants/theme.ts) | TypeScript | 25 | 0 | 3 | 28 |
| [constants/utils.ts](/constants/utils.ts) | TypeScript | 5 | 0 | 3 | 8 |
| [eas.json](/eas.json) | JSON with Comments | 15 | 0 | 0 | 15 |
| [google-services.json](/google-services.json) | JSON | 54 | 0 | 0 | 54 |
| [helpers/date.ts](/helpers/date.ts) | TypeScript | 6 | 0 | 4 | 10 |
| [helpers/number.ts](/helpers/number.ts) | TypeScript | 6 | 0 | 2 | 8 |
| [helpers/string.ts](/helpers/string.ts) | TypeScript | 22 | 0 | 3 | 25 |
| [hooks/useAxiosToken.ts](/hooks/useAxiosToken.ts) | TypeScript | 14 | 0 | 3 | 17 |
| [hooks/useCachedResources.ts](/hooks/useCachedResources.ts) | TypeScript | 68 | 6 | 8 | 82 |
| [hooks/useColorScheme.ts](/hooks/useColorScheme.ts) | TypeScript | 4 | 3 | 2 | 9 |
| [hooks/useDarkMode.ts](/hooks/useDarkMode.ts) | TypeScript | 15 | 0 | 6 | 21 |
| [hooks/useMainTheme.ts](/hooks/useMainTheme.ts) | TypeScript | 10 | 0 | 4 | 14 |
| [hooks/useOpenMap.ts](/hooks/useOpenMap.ts) | TypeScript | 26 | 0 | 4 | 30 |
| [hooks/useSplashScreen.ts](/hooks/useSplashScreen.ts) | TypeScript | 14 | 0 | 4 | 18 |
| [hooks/useUploadImage.ts](/hooks/useUploadImage.ts) | TypeScript | 11 | 27 | 10 | 48 |
| [index.js](/index.js) | JavaScript | 6 | 3 | 3 | 12 |
| [ios/Cosh/AppDelegate.h](/ios/Cosh/AppDelegate.h) | C++ | 5 | 0 | 3 | 8 |
| [ios/Cosh/AppDelegate.mm](/ios/Cosh/AppDelegate.mm) | Objective-C++ | 38 | 7 | 13 | 58 |
| [ios/Cosh/Cosh-Bridging-Header.h](/ios/Cosh/Cosh-Bridging-Header.h) | C++ | 0 | 3 | 1 | 4 |
| [ios/Cosh/Images.xcassets/AppIcon.appiconset/Contents.json](/ios/Cosh/Images.xcassets/AppIcon.appiconset/Contents.json) | JSON | 14 | 0 | 0 | 14 |
| [ios/Cosh/Images.xcassets/Contents.json](/ios/Cosh/Images.xcassets/Contents.json) | JSON | 6 | 0 | 1 | 7 |
| [ios/Cosh/Images.xcassets/SplashScreen.imageset/Contents.json](/ios/Cosh/Images.xcassets/SplashScreen.imageset/Contents.json) | JSON | 21 | 0 | 0 | 21 |
| [ios/Cosh/Images.xcassets/SplashScreenBackground.imageset/Contents.json](/ios/Cosh/Images.xcassets/SplashScreenBackground.imageset/Contents.json) | JSON | 21 | 0 | 0 | 21 |
| [ios/Cosh/SplashScreen.storyboard](/ios/Cosh/SplashScreen.storyboard) | XML | 51 | 0 | 0 | 51 |
| [ios/Cosh/main.m](/ios/Cosh/main.m) | Objective-C | 7 | 0 | 4 | 11 |
| [ios/Cosh/noop-file.swift](/ios/Cosh/noop-file.swift) | Swift | 0 | 4 | 1 | 5 |
| [ios/Podfile.properties.json](/ios/Podfile.properties.json) | JSON | 4 | 0 | 1 | 5 |
| [metro.config.js](/metro.config.js) | JavaScript | 4 | 0 | 4 | 8 |
| [navigation/DiscoverNav.tsx](/navigation/DiscoverNav.tsx) | TypeScript JSX | 43 | 0 | 5 | 48 |
| [navigation/index.tsx](/navigation/index.tsx) | TypeScript JSX | 318 | 0 | 23 | 341 |
| [package-lock.json](/package-lock.json) | JSON | 30,448 | 0 | 1 | 30,449 |
| [package.json](/package.json) | JSON | 112 | 0 | 1 | 113 |
| [provider/AuthProvider.tsx](/provider/AuthProvider.tsx) | TypeScript JSX | 21 | 0 | 5 | 26 |
| [provider/EventProvider.tsx](/provider/EventProvider.tsx) | TypeScript JSX | 180 | 0 | 6 | 186 |
| [provider/FiltersProvider.tsx](/provider/FiltersProvider.tsx) | TypeScript JSX | 673 | 0 | 7 | 680 |
| [provider/FollowProvider.tsx](/provider/FollowProvider.tsx) | TypeScript JSX | 52 | 0 | 8 | 60 |
| [provider/LocationProvider.tsx](/provider/LocationProvider.tsx) | TypeScript JSX | 77 | 0 | 12 | 89 |
| [provider/MapStore.tsx](/provider/MapStore.tsx) | TypeScript JSX | 102 | 1 | 7 | 110 |
| [provider/MapsProvider.tsx](/provider/MapsProvider.tsx) | TypeScript JSX | 36 | 0 | 6 | 42 |
| [provider/PlacesProvider.tsx](/provider/PlacesProvider.tsx) | TypeScript JSX | 39 | 0 | 6 | 45 |
| [provider/ThemeProvider.tsx](/provider/ThemeProvider.tsx) | TypeScript JSX | 44 | 1 | 10 | 55 |
| [screens/CreateEvent.tsx](/screens/CreateEvent.tsx) | TypeScript JSX | 101 | 0 | 9 | 110 |
| [screens/CreatePlace.tsx](/screens/CreatePlace.tsx) | TypeScript JSX | 26 | 0 | 6 | 32 |
| [screens/Discover.tsx](/screens/Discover.tsx) | TypeScript JSX | 61 | 0 | 7 | 68 |
| [screens/Event.tsx](/screens/Event.tsx) | TypeScript JSX | 340 | 0 | 24 | 364 |
| [screens/Favorites.tsx](/screens/Favorites.tsx) | TypeScript JSX | 9 | 0 | 3 | 12 |
| [screens/Filters.tsx](/screens/Filters.tsx) | TypeScript JSX | 187 | 0 | 25 | 212 |
| [screens/GetLocation.tsx](/screens/GetLocation.tsx) | TypeScript JSX | 183 | 1 | 22 | 206 |
| [screens/MyEvents.tsx](/screens/MyEvents.tsx) | TypeScript JSX | 117 | 0 | 14 | 131 |
| [screens/MyEventsTickets.tsx](/screens/MyEventsTickets.tsx) | TypeScript JSX | 90 | 0 | 8 | 98 |
| [screens/MyEventsWishlist.tsx](/screens/MyEventsWishlist.tsx) | TypeScript JSX | 64 | 0 | 5 | 69 |
| [screens/NotFoundScreen.tsx](/screens/NotFoundScreen.tsx) | TypeScript JSX | 38 | 0 | 3 | 41 |
| [screens/Place.tsx](/screens/Place.tsx) | TypeScript JSX | 128 | 3 | 7 | 138 |
| [screens/Search.tsx](/screens/Search.tsx) | TypeScript JSX | 320 | 0 | 33 | 353 |
| [screens/Settings.tsx](/screens/Settings.tsx) | TypeScript JSX | 12 | 0 | 3 | 15 |
| [screens/SignInOut.tsx](/screens/SignInOut.tsx) | TypeScript JSX | 247 | 1 | 24 | 272 |
| [screens/SignUp.tsx](/screens/SignUp.tsx) | TypeScript JSX | 243 | 0 | 10 | 253 |
| [services/ability.ts](/services/ability.ts) | TypeScript | 18 | 5 | 5 | 28 |
| [services/api.ts](/services/api.ts) | TypeScript | 0 | 44 | 5 | 49 |
| [services/auth/google.ts](/services/auth/google.ts) | TypeScript | 21 | 0 | 3 | 24 |
| [services/axios.ts](/services/axios.ts) | TypeScript | 5 | 0 | 3 | 8 |
| [services/events/getDailyEvents.ts](/services/events/getDailyEvents.ts) | TypeScript | 28 | 0 | 3 | 31 |
| [services/events/getEventById.ts](/services/events/getEventById.ts) | TypeScript | 12 | 0 | 3 | 15 |
| [services/events/getEventsByName.ts](/services/events/getEventsByName.ts) | TypeScript | 17 | 0 | 3 | 20 |
| [services/events/getNewEvents.ts](/services/events/getNewEvents.ts) | TypeScript | 16 | 0 | 3 | 19 |
| [services/events/getWeeklyEvents.ts](/services/events/getWeeklyEvents.ts) | TypeScript | 26 | 0 | 3 | 29 |
| [services/events/popular.ts](/services/events/popular.ts) | TypeScript | 16 | 0 | 3 | 19 |
| [services/eventsService.ts](/services/eventsService.ts) | TypeScript | 40 | 0 | 7 | 47 |
| [services/follow/follow.ts](/services/follow/follow.ts) | TypeScript | 48 | 0 | 7 | 55 |
| [services/googleGeocode.ts](/services/googleGeocode.ts) | TypeScript | 68 | 0 | 5 | 73 |
| [services/place/getBars.ts](/services/place/getBars.ts) | TypeScript | 22 | 0 | 3 | 25 |
| [services/place/getClubs.ts](/services/place/getClubs.ts) | TypeScript | 22 | 0 | 3 | 25 |
| [services/place/getPlaceById.ts](/services/place/getPlaceById.ts) | TypeScript | 12 | 0 | 3 | 15 |
| [services/place/getPlaces.ts](/services/place/getPlaces.ts) | TypeScript | 27 | 0 | 3 | 30 |
| [services/placesServices.ts](/services/placesServices.ts) | TypeScript | 31 | 0 | 4 | 35 |
| [services/topics/getBarsTopics.ts](/services/topics/getBarsTopics.ts) | TypeScript | 12 | 0 | 3 | 15 |
| [services/topics/getClubsTopics.ts](/services/topics/getClubsTopics.ts) | TypeScript | 12 | 0 | 3 | 15 |
| [services/types.ts](/services/types.ts) | TypeScript | 178 | 0 | 19 | 197 |
| [services/userService.ts](/services/userService.ts) | TypeScript | 9 | 0 | 2 | 11 |
| [services/utils.ts](/services/utils.ts) | TypeScript | 25 | 0 | 3 | 28 |
| [store.ts](/store.ts) | TypeScript | 16 | 3 | 4 | 23 |
| [styled.d.ts](/styled.d.ts) | TypeScript | 15 | 0 | 2 | 17 |
| [tsconfig.json](/tsconfig.json) | JSON with Comments | 8 | 0 | 1 | 9 |
| [types.tsx](/types.tsx) | TypeScript JSX | 33 | 5 | 7 | 45 |
| [types/place.ts](/types/place.ts) | TypeScript | 19 | 0 | 4 | 23 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)