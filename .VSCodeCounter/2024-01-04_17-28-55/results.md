# Summary

Date : 2024-01-04 17:28:55

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 212 files,  42707 codes, 469 comments, 1282 blanks, all 44458 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 11 | 30,926 | 0 | 5 | 30,931 |
| TypeScript JSX | 97 | 9,851 | 80 | 937 | 10,868 |
| TypeScript | 45 | 1,163 | 88 | 180 | 1,431 |
| XML | 35 | 248 | 23 | 24 | 295 |
| Java | 4 | 165 | 47 | 32 | 244 |
| Groovy | 3 | 147 | 76 | 35 | 258 |
| JavaScript | 6 | 55 | 3 | 10 | 68 |
| JSON with Comments | 3 | 43 | 76 | 1 | 120 |
| Batch | 1 | 41 | 29 | 22 | 92 |
| Objective-C++ | 1 | 38 | 7 | 13 | 58 |
| Properties | 2 | 18 | 33 | 14 | 65 |
| Objective-C | 1 | 7 | 0 | 4 | 11 |
| C++ | 2 | 5 | 3 | 4 | 12 |
| Swift | 1 | 0 | 4 | 1 | 5 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 212 | 42,707 | 469 | 1,282 | 44,458 |
| . (Files) | 16 | 30,829 | 88 | 34 | 30,951 |
| .expo-shared | 1 | 6 | 0 | 1 | 7 |
| android | 21 | 523 | 208 | 109 | 840 |
| android (Files) | 4 | 105 | 68 | 45 | 218 |
| android/app | 16 | 412 | 140 | 63 | 615 |
| android/app (Files) | 2 | 149 | 70 | 25 | 244 |
| android/app/src | 14 | 263 | 70 | 38 | 371 |
| android/app/src/debug | 2 | 63 | 12 | 9 | 84 |
| android/app/src/debug (Files) | 1 | 5 | 0 | 3 | 8 |
| android/app/src/debug/java | 1 | 58 | 12 | 6 | 76 |
| android/app/src/debug/java/com | 1 | 58 | 12 | 6 | 76 |
| android/app/src/debug/java/com/cosh | 1 | 58 | 12 | 6 | 76 |
| android/app/src/main | 11 | 193 | 47 | 26 | 266 |
| android/app/src/main (Files) | 1 | 40 | 0 | 0 | 40 |
| android/app/src/main/java | 2 | 100 | 24 | 23 | 147 |
| android/app/src/main/java/com | 2 | 100 | 24 | 23 | 147 |
| android/app/src/main/java/com/cosh | 2 | 100 | 24 | 23 | 147 |
| android/app/src/main/res | 8 | 53 | 23 | 3 | 79 |
| android/app/src/main/res/drawable | 2 | 14 | 23 | 3 | 40 |
| android/app/src/main/res/mipmap-anydpi-v26 | 2 | 10 | 0 | 0 | 10 |
| android/app/src/main/res/values | 3 | 28 | 0 | 0 | 28 |
| android/app/src/main/res/values-night | 1 | 1 | 0 | 0 | 1 |
| android/app/src/release | 1 | 7 | 11 | 3 | 21 |
| android/app/src/release/java | 1 | 7 | 11 | 3 | 21 |
| android/app/src/release/java/com | 1 | 7 | 11 | 3 | 21 |
| android/app/src/release/java/com/cosh | 1 | 7 | 11 | 3 | 21 |
| android/gradle | 1 | 6 | 0 | 1 | 7 |
| android/gradle/wrapper | 1 | 6 | 0 | 1 | 7 |
| assets | 25 | 111 | 0 | 20 | 131 |
| assets/icons | 25 | 111 | 0 | 20 | 131 |
| components | 71 | 6,369 | 67 | 629 | 7,065 |
| components/AddressEditorModal | 1 | 52 | 1 | 5 | 58 |
| components/BlockEvents | 2 | 139 | 0 | 17 | 156 |
| components/BlockPlaces | 2 | 139 | 0 | 14 | 153 |
| components/BlockToFollow | 1 | 58 | 1 | 8 | 67 |
| components/Blocks | 6 | 270 | 0 | 35 | 305 |
| components/BottomSheetIndicator | 1 | 131 | 2 | 8 | 141 |
| components/Button | 6 | 398 | 1 | 52 | 451 |
| components/Carousel | 1 | 157 | 0 | 19 | 176 |
| components/Checkbox | 2 | 156 | 0 | 22 | 178 |
| components/CirclePeoples | 1 | 36 | 0 | 5 | 41 |
| components/CoshText | 1 | 18 | 0 | 5 | 23 |
| components/CreateEventForm | 2 | 433 | 9 | 22 | 464 |
| components/CreateEventHeader | 1 | 85 | 0 | 8 | 93 |
| components/CreatePlaceForm | 2 | 437 | 10 | 19 | 466 |
| components/DiscoverTabView | 9 | 645 | 4 | 90 | 739 |
| components/DiscoverTabView (Files) | 2 | 110 | 0 | 19 | 129 |
| components/DiscoverTabView/screens | 7 | 535 | 4 | 71 | 610 |
| components/EventMiniature | 1 | 115 | 2 | 8 | 125 |
| components/EventsList | 1 | 26 | 0 | 5 | 31 |
| components/Filters | 3 | 172 | 0 | 18 | 190 |
| components/Headers | 2 | 319 | 0 | 23 | 342 |
| components/HoursSeletor | 1 | 227 | 0 | 21 | 248 |
| components/IconAndText | 1 | 81 | 0 | 5 | 86 |
| components/InputAddressFinder | 1 | 141 | 0 | 16 | 157 |
| components/InputPlaceFinder | 1 | 133 | 0 | 15 | 148 |
| components/InputPlaceFinder.tsx | 1 | 112 | 0 | 11 | 123 |
| components/Loader | 2 | 71 | 0 | 6 | 77 |
| components/MiniaturesPlan | 1 | 120 | 0 | 13 | 133 |
| components/ModalDateTimePicker | 1 | 87 | 0 | 5 | 92 |
| components/PeoplesAreJoined | 1 | 46 | 0 | 6 | 52 |
| components/PriceRange | 1 | 65 | 0 | 7 | 72 |
| components/RadiusSelector | 1 | 32 | 0 | 5 | 37 |
| components/Rubriques | 2 | 104 | 0 | 16 | 120 |
| components/SearchMap | 2 | 428 | 0 | 22 | 450 |
| components/Section | 2 | 109 | 0 | 8 | 117 |
| components/SignInOut | 2 | 144 | 17 | 17 | 178 |
| components/Slider | 1 | 123 | 0 | 18 | 141 |
| components/SwipeCards | 1 | 268 | 0 | 23 | 291 |
| components/Tags | 1 | 40 | 0 | 8 | 48 |
| components/Toast | 1 | 115 | 0 | 8 | 123 |
| components/UpdateEventButton | 1 | 17 | 18 | 4 | 39 |
| components/UploadCover | 1 | 120 | 2 | 12 | 134 |
| constants | 5 | 71 | 0 | 14 | 85 |
| helpers | 3 | 34 | 0 | 9 | 43 |
| hooks | 8 | 162 | 36 | 41 | 239 |
| ios | 11 | 167 | 14 | 24 | 205 |
| ios (Files) | 1 | 4 | 0 | 1 | 5 |
| ios/Cosh | 10 | 163 | 14 | 23 | 200 |
| ios/Cosh (Files) | 6 | 101 | 14 | 22 | 137 |
| ios/Cosh/Images.xcassets | 4 | 62 | 0 | 1 | 63 |
| ios/Cosh/Images.xcassets (Files) | 1 | 6 | 0 | 1 | 7 |
| ios/Cosh/Images.xcassets/AppIcon.appiconset | 1 | 14 | 0 | 0 | 14 |
| ios/Cosh/Images.xcassets/SplashScreen.imageset | 1 | 21 | 0 | 0 | 21 |
| ios/Cosh/Images.xcassets/SplashScreenBackground.imageset | 1 | 21 | 0 | 0 | 21 |
| navigation | 2 | 361 | 0 | 28 | 389 |
| provider | 9 | 1,224 | 2 | 67 | 1,293 |
| screens | 16 | 2,166 | 5 | 203 | 2,374 |
| services | 23 | 665 | 49 | 99 | 813 |
| services (Files) | 9 | 374 | 49 | 53 | 476 |
| services/auth | 1 | 21 | 0 | 3 | 24 |
| services/events | 6 | 115 | 0 | 18 | 133 |
| services/follow | 1 | 48 | 0 | 7 | 55 |
| services/place | 4 | 83 | 0 | 12 | 95 |
| services/topics | 2 | 24 | 0 | 6 | 30 |
| types | 1 | 19 | 0 | 4 | 23 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)