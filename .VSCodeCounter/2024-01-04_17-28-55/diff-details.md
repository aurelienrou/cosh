# Diff Details

Date : 2024-01-04 17:28:55

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 60 files,  666 codes, 196 comments, 59 blanks, all 921 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [ react-native.config.js](/%20react-native.config.js) | JavaScript | 10 | 0 | 0 | 10 |
| [android/app/build.gradle](/android/app/build.gradle) | Groovy | 95 | 70 | 25 | 190 |
| [android/app/google-services.json](/android/app/google-services.json) | JSON | 54 | 0 | 0 | 54 |
| [android/app/src/debug/AndroidManifest.xml](/android/app/src/debug/AndroidManifest.xml) | XML | 5 | 0 | 3 | 8 |
| [android/app/src/debug/java/com/cosh/ReactNativeFlipper.java](/android/app/src/debug/java/com/cosh/ReactNativeFlipper.java) | Java | 58 | 12 | 6 | 76 |
| [android/app/src/main/AndroidManifest.xml](/android/app/src/main/AndroidManifest.xml) | XML | 40 | 0 | 0 | 40 |
| [android/app/src/main/java/com/cosh/MainActivity.java](/android/app/src/main/java/com/cosh/MainActivity.java) | Java | 36 | 21 | 9 | 66 |
| [android/app/src/main/java/com/cosh/MainApplication.java](/android/app/src/main/java/com/cosh/MainApplication.java) | Java | 64 | 3 | 14 | 81 |
| [android/app/src/main/res/drawable/rn_edit_text_material.xml](/android/app/src/main/res/drawable/rn_edit_text_material.xml) | XML | 11 | 23 | 3 | 37 |
| [android/app/src/main/res/drawable/splashscreen.xml](/android/app/src/main/res/drawable/splashscreen.xml) | XML | 3 | 0 | 0 | 3 |
| [android/app/src/main/res/mipmap-anydpi-v26/ic_launcher.xml](/android/app/src/main/res/mipmap-anydpi-v26/ic_launcher.xml) | XML | 5 | 0 | 0 | 5 |
| [android/app/src/main/res/mipmap-anydpi-v26/ic_launcher_round.xml](/android/app/src/main/res/mipmap-anydpi-v26/ic_launcher_round.xml) | XML | 5 | 0 | 0 | 5 |
| [android/app/src/main/res/values-night/colors.xml](/android/app/src/main/res/values-night/colors.xml) | XML | 1 | 0 | 0 | 1 |
| [android/app/src/main/res/values/colors.xml](/android/app/src/main/res/values/colors.xml) | XML | 6 | 0 | 0 | 6 |
| [android/app/src/main/res/values/strings.xml](/android/app/src/main/res/values/strings.xml) | XML | 5 | 0 | 0 | 5 |
| [android/app/src/main/res/values/styles.xml](/android/app/src/main/res/values/styles.xml) | XML | 17 | 0 | 0 | 17 |
| [android/app/src/release/java/com/cosh/ReactNativeFlipper.java](/android/app/src/release/java/com/cosh/ReactNativeFlipper.java) | Java | 7 | 11 | 3 | 21 |
| [android/build.gradle](/android/build.gradle) | Groovy | 45 | 6 | 6 | 57 |
| [android/gradle.properties](/android/gradle.properties) | Properties | 12 | 33 | 13 | 58 |
| [android/gradle/wrapper/gradle-wrapper.properties](/android/gradle/wrapper/gradle-wrapper.properties) | Properties | 6 | 0 | 1 | 7 |
| [android/gradlew.bat](/android/gradlew.bat) | Batch | 41 | 29 | 22 | 92 |
| [android/settings.gradle](/android/settings.gradle) | Groovy | 7 | 0 | 4 | 11 |
| [app.json](/app.json) | JSON with Comments | 0 | 10 | 0 | 10 |
| [components/BlockEvents/BlockEvent.tsx](/components/BlockEvents/BlockEvent.tsx) | TypeScript JSX | 5 | 0 | 0 | 5 |
| [components/BlockPlaces/BlockPlace.tsx](/components/BlockPlaces/BlockPlace.tsx) | TypeScript JSX | 22 | 0 | 0 | 22 |
| [components/Button/OpenMap.tsx](/components/Button/OpenMap.tsx) | TypeScript JSX | 5 | 0 | 0 | 5 |
| [components/DiscoverTabView/DiscoverTabView.tsx](/components/DiscoverTabView/DiscoverTabView.tsx) | TypeScript JSX | -2 | 0 | 0 | -2 |
| [components/DiscoverTabView/screens/Bars.tsx](/components/DiscoverTabView/screens/Bars.tsx) | TypeScript JSX | 18 | 0 | 0 | 18 |
| [components/DiscoverTabView/screens/Clubs.tsx](/components/DiscoverTabView/screens/Clubs.tsx) | TypeScript JSX | 21 | 0 | 1 | 22 |
| [components/DiscoverTabView/screens/ThisWeek.tsx](/components/DiscoverTabView/screens/ThisWeek.tsx) | TypeScript JSX | 22 | 2 | -2 | 22 |
| [components/DiscoverTabView/screens/Today.tsx](/components/DiscoverTabView/screens/Today.tsx) | TypeScript JSX | 10 | 2 | 0 | 12 |
| [components/EventMiniature/EventMiniature.tsx](/components/EventMiniature/EventMiniature.tsx) | TypeScript JSX | 2 | 0 | 0 | 2 |
| [components/Loader/EventLoader.tsx](/components/Loader/EventLoader.tsx) | TypeScript JSX | 37 | 0 | 3 | 40 |
| [components/Loader/PlaceLoader.tsx](/components/Loader/PlaceLoader.tsx) | TypeScript JSX | 34 | 0 | 3 | 37 |
| [components/Rubriques/Rubrique.tsx](/components/Rubriques/Rubrique.tsx) | TypeScript JSX | 1 | 0 | 0 | 1 |
| [components/Rubriques/Rubriques.tsx](/components/Rubriques/Rubriques.tsx) | TypeScript JSX | -3 | 0 | 0 | -3 |
| [components/SearchMap/Map.tsx](/components/SearchMap/Map.tsx) | TypeScript JSX | 41 | -6 | 3 | 38 |
| [components/SearchMap/Marker.tsx](/components/SearchMap/Marker.tsx) | TypeScript JSX | -58 | 0 | -5 | -63 |
| [components/SearchMap/Markers.tsx](/components/SearchMap/Markers.tsx) | TypeScript JSX | -133 | 0 | -20 | -153 |
| [components/SearchMap/useMarkers.ts](/components/SearchMap/useMarkers.ts) | TypeScript | -69 | -22 | -14 | -105 |
| [components/SearchMap/useMarkers.tsx](/components/SearchMap/useMarkers.tsx) | TypeScript JSX | -107 | 0 | -19 | -126 |
| [components/Section/SectionHeader.tsx](/components/Section/SectionHeader.tsx) | TypeScript JSX | 13 | 0 | -3 | 10 |
| [components/SignInOut/GoogleLoginButton.tsx](/components/SignInOut/GoogleLoginButton.tsx) | TypeScript JSX | -8 | 17 | -2 | 7 |
| [components/SignInOut/Register.tsx](/components/SignInOut/Register.tsx) | TypeScript JSX | 9 | 0 | 0 | 9 |
| [ios/Cosh/AppDelegate.mm](/ios/Cosh/AppDelegate.mm) | Objective-C++ | -2 | 0 | -1 | -3 |
| [navigation/index.tsx](/navigation/index.tsx) | TypeScript JSX | 24 | 0 | 1 | 25 |
| [package.json](/package.json) | JSON | 2 | 0 | 0 | 2 |
| [provider/AnimatedHeaderProvider.tsx](/provider/AnimatedHeaderProvider.tsx) | TypeScript JSX | -40 | 0 | -5 | -45 |
| [provider/MapStore.tsx](/provider/MapStore.tsx) | TypeScript JSX | 1 | 0 | 0 | 1 |
| [screens/CreateEvent.tsx](/screens/CreateEvent.tsx) | TypeScript JSX | 1 | 0 | 0 | 1 |
| [screens/Filters.tsx](/screens/Filters.tsx) | TypeScript JSX | 8 | 0 | 1 | 9 |
| [screens/MyEvents.tsx](/screens/MyEvents.tsx) | TypeScript JSX | 3 | 0 | 0 | 3 |
| [screens/Search.tsx](/screens/Search.tsx) | TypeScript JSX | 3 | 0 | -1 | 2 |
| [screens/SignInOut.tsx](/screens/SignInOut.tsx) | TypeScript JSX | 2 | 0 | 0 | 2 |
| [screens/SignUp.tsx](/screens/SignUp.tsx) | TypeScript JSX | 243 | 0 | 10 | 253 |
| [services/auth/google.ts](/services/auth/google.ts) | TypeScript | 21 | 0 | 3 | 24 |
| [services/authService.ts](/services/authService.ts) | TypeScript | -9 | 0 | -2 | -11 |
| [services/firebase.ts](/services/firebase.ts) | TypeScript | 0 | -15 | -4 | -19 |
| [services/place/getBars.ts](/services/place/getBars.ts) | TypeScript | 22 | 0 | 3 | 25 |
| [services/place/getClubs.ts](/services/place/getClubs.ts) | TypeScript | -6 | 0 | 0 | -6 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details