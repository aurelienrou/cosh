# Diff Details

Date : 2024-01-23 02:47:10

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 94 files,  1296 codes, 8 comments, 91 blanks, all 1395 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [App.tsx](/App.tsx) | TypeScript JSX | 16 | 0 | 2 | 18 |
| [android/app/google-services.json](/android/app/google-services.json) | JSON | 1 | 0 | 0 | 1 |
| [android/app/src/debug/java/com/cosh/ReactNativeFlipper.java](/android/app/src/debug/java/com/cosh/ReactNativeFlipper.java) | Java | -58 | -12 | -6 | -76 |
| [android/app/src/debug/java/com/evenly/ReactNativeFlipper.java](/android/app/src/debug/java/com/evenly/ReactNativeFlipper.java) | Java | 58 | 12 | 6 | 76 |
| [android/app/src/main/java/com/cosh/MainActivity.java](/android/app/src/main/java/com/cosh/MainActivity.java) | Java | -36 | -21 | -9 | -66 |
| [android/app/src/main/java/com/cosh/MainApplication.java](/android/app/src/main/java/com/cosh/MainApplication.java) | Java | -64 | -3 | -14 | -81 |
| [android/app/src/main/java/com/evenly/MainActivity.java](/android/app/src/main/java/com/evenly/MainActivity.java) | Java | 36 | 21 | 9 | 66 |
| [android/app/src/main/java/com/evenly/MainApplication.java](/android/app/src/main/java/com/evenly/MainApplication.java) | Java | 66 | 3 | 14 | 83 |
| [android/app/src/release/java/com/cosh/ReactNativeFlipper.java](/android/app/src/release/java/com/cosh/ReactNativeFlipper.java) | Java | -7 | -11 | -3 | -21 |
| [android/app/src/release/java/com/evenly/ReactNativeFlipper.java](/android/app/src/release/java/com/evenly/ReactNativeFlipper.java) | Java | 7 | 11 | 3 | 21 |
| [android/gradle.properties](/android/gradle.properties) | Properties | 3 | 0 | 0 | 3 |
| [app.json](/app.json) | JSON with Comments | 95 | -76 | 1 | 20 |
| [components/Blocks/Adress.tsx](/components/Blocks/Adress.tsx) | TypeScript JSX | 2 | 0 | 0 | 2 |
| [components/Blocks/Map.tsx](/components/Blocks/Map.tsx) | TypeScript JSX | -62 | 0 | -6 | -68 |
| [components/Button/OpenMap.tsx](/components/Button/OpenMap.tsx) | TypeScript JSX | -3 | 0 | 0 | -3 |
| [components/DiscoverTabView/DiscoverTabView.tsx](/components/DiscoverTabView/DiscoverTabView.tsx) | TypeScript JSX | 2 | 0 | 0 | 2 |
| [components/DiscoverTabView/helper.tsx](/components/DiscoverTabView/helper.tsx) | TypeScript JSX | 5 | 0 | 0 | 5 |
| [components/DiscoverTabView/screens/All.tsx](/components/DiscoverTabView/screens/All.tsx) | TypeScript JSX | -25 | 13 | -1 | -13 |
| [components/DiscoverTabView/screens/Bars.tsx](/components/DiscoverTabView/screens/Bars.tsx) | TypeScript JSX | 5 | 0 | 0 | 5 |
| [components/DiscoverTabView/screens/Clubs.tsx](/components/DiscoverTabView/screens/Clubs.tsx) | TypeScript JSX | -23 | 1 | -1 | -23 |
| [components/DiscoverTabView/screens/Concerts.tsx](/components/DiscoverTabView/screens/Concerts.tsx) | TypeScript JSX | 53 | 1 | 8 | 62 |
| [components/DiscoverTabView/screens/ThisWeek.tsx](/components/DiscoverTabView/screens/ThisWeek.tsx) | TypeScript JSX | -50 | -2 | -5 | -57 |
| [components/DiscoverTabView/screens/Today.tsx](/components/DiscoverTabView/screens/Today.tsx) | TypeScript JSX | -5 | 0 | -1 | -6 |
| [components/Filters/BarsFilters.tsx](/components/Filters/BarsFilters.tsx) | TypeScript JSX | 0 | 0 | 1 | 1 |
| [components/Filters/CheckboxList.tsx](/components/Filters/CheckboxList.tsx) | TypeScript JSX | 6 | 0 | 0 | 6 |
| [components/Filters/ClubsFilters.tsx](/components/Filters/ClubsFilters.tsx) | TypeScript JSX | 233 | 0 | 21 | 254 |
| [components/Filters/ConcertsFilters.tsx](/components/Filters/ConcertsFilters.tsx) | TypeScript JSX | 173 | 0 | 21 | 194 |
| [components/Headers/MainHeader.tsx](/components/Headers/MainHeader.tsx) | TypeScript JSX | 49 | 0 | 3 | 52 |
| [components/Headers/SearchHeader.tsx](/components/Headers/SearchHeader.tsx) | TypeScript JSX | 186 | 0 | 17 | 203 |
| [components/InputAddressFinder/InputAddressFinder.tsx](/components/InputAddressFinder/InputAddressFinder.tsx) | TypeScript JSX | -3 | 0 | -2 | -5 |
| [components/Lists/EventsAndPlacesList.tsx](/components/Lists/EventsAndPlacesList.tsx) | TypeScript JSX | 51 | 0 | 4 | 55 |
| [components/Lists/EventsByDateList.tsx](/components/Lists/EventsByDateList.tsx) | TypeScript JSX | 81 | 0 | 13 | 94 |
| [components/Loader/EventLoader.tsx](/components/Loader/EventLoader.tsx) | TypeScript JSX | -1 | 0 | 1 | 0 |
| [components/Loader/TitleLoader.tsx](/components/Loader/TitleLoader.tsx) | TypeScript JSX | 20 | 0 | 2 | 22 |
| [components/MiniaturesPlan/MiniaturesPlan.tsx](/components/MiniaturesPlan/MiniaturesPlan.tsx) | TypeScript JSX | -120 | 0 | -13 | -133 |
| [components/ModalDateTimePicker/ModalDateTimePicker.tsx](/components/ModalDateTimePicker/ModalDateTimePicker.tsx) | TypeScript JSX | 64 | 0 | 2 | 66 |
| [components/Rubriques/Rubriques.tsx](/components/Rubriques/Rubriques.tsx) | TypeScript JSX | 6 | 0 | 0 | 6 |
| [components/SearchMap/Map.tsx](/components/SearchMap/Map.tsx) | TypeScript JSX | 4 | 0 | -1 | 3 |
| [components/Section/Section.tsx](/components/Section/Section.tsx) | TypeScript JSX | 7 | 0 | 0 | 7 |
| [components/Select/Select.tsx](/components/Select/Select.tsx) | TypeScript JSX | 53 | 0 | 6 | 59 |
| [components/SignInOut/GoogleLoginButton.tsx](/components/SignInOut/GoogleLoginButton.tsx) | TypeScript JSX | -43 | 14 | -1 | -30 |
| [components/SignInOut/Register.tsx](/components/SignInOut/Register.tsx) | TypeScript JSX | -8 | 0 | 0 | -8 |
| [components/SwipeCards/SwipeCards.tsx](/components/SwipeCards/SwipeCards.tsx) | TypeScript JSX | -4 | 0 | 3 | -1 |
| [components/Tags/Tags.tsx](/components/Tags/Tags.tsx) | TypeScript JSX | -15 | 0 | -3 | -18 |
| [google-services.json](/google-services.json) | JSON | 1 | 0 | 0 | 1 |
| [helpers/date.ts](/helpers/date.ts) | TypeScript | 9 | 0 | 1 | 10 |
| [hooks/useAuthState.ts](/hooks/useAuthState.ts) | TypeScript | 23 | 1 | 6 | 30 |
| [hooks/useSignInWithGoogle.ts](/hooks/useSignInWithGoogle.ts) | TypeScript | 8 | 4 | 4 | 16 |
| [index.js](/index.js) | JavaScript | 1 | 0 | 1 | 2 |
| [ios/Cosh/AppDelegate.h](/ios/Cosh/AppDelegate.h) | C++ | -5 | 0 | -3 | -8 |
| [ios/Cosh/AppDelegate.mm](/ios/Cosh/AppDelegate.mm) | Objective-C++ | -38 | -7 | -13 | -58 |
| [ios/Cosh/Cosh-Bridging-Header.h](/ios/Cosh/Cosh-Bridging-Header.h) | C++ | 0 | -3 | -1 | -4 |
| [ios/Cosh/Images.xcassets/AppIcon.appiconset/Contents.json](/ios/Cosh/Images.xcassets/AppIcon.appiconset/Contents.json) | JSON | -14 | 0 | 0 | -14 |
| [ios/Cosh/Images.xcassets/Contents.json](/ios/Cosh/Images.xcassets/Contents.json) | JSON | -6 | 0 | -1 | -7 |
| [ios/Cosh/Images.xcassets/SplashScreen.imageset/Contents.json](/ios/Cosh/Images.xcassets/SplashScreen.imageset/Contents.json) | JSON | -21 | 0 | 0 | -21 |
| [ios/Cosh/Images.xcassets/SplashScreenBackground.imageset/Contents.json](/ios/Cosh/Images.xcassets/SplashScreenBackground.imageset/Contents.json) | JSON | -21 | 0 | 0 | -21 |
| [ios/Cosh/SplashScreen.storyboard](/ios/Cosh/SplashScreen.storyboard) | XML | -51 | 0 | 0 | -51 |
| [ios/Cosh/main.m](/ios/Cosh/main.m) | Objective-C | -7 | 0 | -4 | -11 |
| [ios/Cosh/noop-file.swift](/ios/Cosh/noop-file.swift) | Swift | 0 | -4 | -1 | -5 |
| [ios/Evenly/AppDelegate.h](/ios/Evenly/AppDelegate.h) | C++ | 5 | 0 | 3 | 8 |
| [ios/Evenly/AppDelegate.mm](/ios/Evenly/AppDelegate.mm) | Objective-C++ | 40 | 9 | 13 | 62 |
| [ios/Evenly/Evenly-Bridging-Header.h](/ios/Evenly/Evenly-Bridging-Header.h) | C++ | 0 | 3 | 1 | 4 |
| [ios/Evenly/Images.xcassets/AppIcon.appiconset/Contents.json](/ios/Evenly/Images.xcassets/AppIcon.appiconset/Contents.json) | JSON | 14 | 0 | 0 | 14 |
| [ios/Evenly/Images.xcassets/Contents.json](/ios/Evenly/Images.xcassets/Contents.json) | JSON | 6 | 0 | 1 | 7 |
| [ios/Evenly/Images.xcassets/SplashScreen.imageset/Contents.json](/ios/Evenly/Images.xcassets/SplashScreen.imageset/Contents.json) | JSON | 21 | 0 | 0 | 21 |
| [ios/Evenly/Images.xcassets/SplashScreenBackground.imageset/Contents.json](/ios/Evenly/Images.xcassets/SplashScreenBackground.imageset/Contents.json) | JSON | 21 | 0 | 0 | 21 |
| [ios/Evenly/SplashScreen.storyboard](/ios/Evenly/SplashScreen.storyboard) | XML | 51 | 0 | 0 | 51 |
| [ios/Evenly/main.m](/ios/Evenly/main.m) | Objective-C | 7 | 0 | 4 | 11 |
| [ios/Evenly/noop-file.swift](/ios/Evenly/noop-file.swift) | Swift | 0 | 4 | 1 | 5 |
| [ios/Podfile.properties.json](/ios/Podfile.properties.json) | JSON | 2 | 0 | 0 | 2 |
| [navigation/DiscoverNav.tsx](/navigation/DiscoverNav.tsx) | TypeScript JSX | 3 | 0 | 0 | 3 |
| [navigation/index.tsx](/navigation/index.tsx) | TypeScript JSX | 25 | 0 | 6 | 31 |
| [package-lock.json](/package-lock.json) | JSON | 390 | 0 | 0 | 390 |
| [package.json](/package.json) | JSON | 4 | 0 | 0 | 4 |
| [provider/FiltersProvider.tsx](/provider/FiltersProvider.tsx) | TypeScript JSX | 18 | 0 | 0 | 18 |
| [provider/MapStore.tsx](/provider/MapStore.tsx) | TypeScript JSX | -57 | 0 | -1 | -58 |
| [screens/Event.tsx](/screens/Event.tsx) | TypeScript JSX | 1 | 0 | -1 | 0 |
| [screens/Filters.tsx](/screens/Filters.tsx) | TypeScript JSX | -66 | 33 | -17 | -50 |
| [screens/GetLocation.tsx](/screens/GetLocation.tsx) | TypeScript JSX | 54 | 0 | 5 | 59 |
| [screens/MyEventsWishlist.tsx](/screens/MyEventsWishlist.tsx) | TypeScript JSX | 4 | 0 | 0 | 4 |
| [screens/Place.tsx](/screens/Place.tsx) | TypeScript JSX | 46 | -3 | 2 | 45 |
| [screens/Search.tsx](/screens/Search.tsx) | TypeScript JSX | -189 | 0 | -22 | -211 |
| [screens/SignUp.tsx](/screens/SignUp.tsx) | TypeScript JSX | 85 | 19 | 6 | 110 |
| [services/auth/apple.ts](/services/auth/apple.ts) | TypeScript | 11 | 0 | 3 | 14 |
| [services/auth/google.ts](/services/auth/google.ts) | TypeScript | -10 | 0 | 0 | -10 |
| [services/axios.ts](/services/axios.ts) | TypeScript | 0 | 1 | 0 | 1 |
| [services/events/getClubsEvents.ts](/services/events/getClubsEvents.ts) | TypeScript | 40 | 0 | 6 | 46 |
| [services/events/getConcerts.ts](/services/events/getConcerts.ts) | TypeScript | 40 | 0 | 6 | 46 |
| [services/events/getEventById.ts](/services/events/getEventById.ts) | TypeScript | 1 | 0 | 0 | 1 |
| [services/events/popular.ts](/services/events/popular.ts) | TypeScript | 8 | 0 | 0 | 8 |
| [services/place/getPlacesByName.ts](/services/place/getPlacesByName.ts) | TypeScript | 17 | 0 | 3 | 20 |
| [services/search/search.ts](/services/search/search.ts) | TypeScript | 40 | 0 | 8 | 48 |
| [services/types.ts](/services/types.ts) | TypeScript | 55 | 0 | 7 | 62 |
| [services/utils.ts](/services/utils.ts) | TypeScript | -25 | 0 | -3 | -28 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details