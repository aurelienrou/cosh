Date : 2024-01-23 02:47:10
Directory : /Users/aurelien/Documents/Developpement/cosh
Total : 94 files,  1296 codes, 8 comments, 91 blanks, all 1395 lines

Languages
+--------------------+------------+------------+------------+------------+------------+
| language           | files      | code       | comment    | blank      | total      |
+--------------------+------------+------------+------------+------------+------------+
| TypeScript JSX     |         44 |        578 |         76 |         48 |        702 |
| JSON               |         13 |        398 |          0 |          0 |        398 |
| TypeScript         |         14 |        217 |          6 |         41 |        264 |
| JSON with Comments |          1 |         95 |        -76 |          1 |         20 |
| Properties         |          1 |          3 |          0 |          0 |          3 |
| Java               |          8 |          2 |          0 |          0 |          2 |
| Objective-C++      |          2 |          2 |          2 |          0 |          4 |
| JavaScript         |          1 |          1 |          0 |          1 |          2 |
| C++                |          4 |          0 |          0 |          0 |          0 |
| Swift              |          2 |          0 |          0 |          0 |          0 |
| XML                |          2 |          0 |          0 |          0 |          0 |
| Objective-C        |          2 |          0 |          0 |          0 |          0 |
+--------------------+------------+------------+------------+------------+------------+

Directories
+-----------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                                                                  | files      | code       | comment    | blank      | total      |
+-----------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                                                                     |         94 |      1,296 |          8 |         91 |      1,395 |
| . (Files)                                                                                                             |          6 |        507 |        -76 |          4 |        435 |
| android                                                                                                               |         10 |          6 |          0 |          0 |          6 |
| android (Files)                                                                                                       |          1 |          3 |          0 |          0 |          3 |
| android/app                                                                                                           |          9 |          3 |          0 |          0 |          3 |
| android/app (Files)                                                                                                   |          1 |          1 |          0 |          0 |          1 |
| android/app/src                                                                                                       |          8 |          2 |          0 |          0 |          2 |
| android/app/src/debug                                                                                                 |          2 |          0 |          0 |          0 |          0 |
| android/app/src/debug/java                                                                                            |          2 |          0 |          0 |          0 |          0 |
| android/app/src/debug/java/com                                                                                        |          2 |          0 |          0 |          0 |          0 |
| android/app/src/debug/java/com/cosh                                                                                   |          1 |        -58 |        -12 |         -6 |        -76 |
| android/app/src/debug/java/com/evenly                                                                                 |          1 |         58 |         12 |          6 |         76 |
| android/app/src/main                                                                                                  |          4 |          2 |          0 |          0 |          2 |
| android/app/src/main/java                                                                                             |          4 |          2 |          0 |          0 |          2 |
| android/app/src/main/java/com                                                                                         |          4 |          2 |          0 |          0 |          2 |
| android/app/src/main/java/com/cosh                                                                                    |          2 |       -100 |        -24 |        -23 |       -147 |
| android/app/src/main/java/com/evenly                                                                                  |          2 |        102 |         24 |         23 |        149 |
| android/app/src/release                                                                                               |          2 |          0 |          0 |          0 |          0 |
| android/app/src/release/java                                                                                          |          2 |          0 |          0 |          0 |          0 |
| android/app/src/release/java/com                                                                                      |          2 |          0 |          0 |          0 |          0 |
| android/app/src/release/java/com/cosh                                                                                 |          1 |         -7 |        -11 |         -3 |        -21 |
| android/app/src/release/java/com/evenly                                                                               |          1 |          7 |         11 |          3 |         21 |
| components                                                                                                            |         32 |        638 |         27 |         68 |        733 |
| components/Blocks                                                                                                     |          2 |        -60 |          0 |         -6 |        -66 |
| components/Button                                                                                                     |          1 |         -3 |          0 |          0 |         -3 |
| components/DiscoverTabView                                                                                            |          8 |        -38 |         13 |          0 |        -25 |
| components/DiscoverTabView (Files)                                                                                    |          2 |          7 |          0 |          0 |          7 |
| components/DiscoverTabView/screens                                                                                    |          6 |        -45 |         13 |          0 |        -32 |
| components/Filters                                                                                                    |          4 |        412 |          0 |         43 |        455 |
| components/Headers                                                                                                    |          2 |        235 |          0 |         20 |        255 |
| components/InputAddressFinder                                                                                         |          1 |         -3 |          0 |         -2 |         -5 |
| components/Lists                                                                                                      |          2 |        132 |          0 |         17 |        149 |
| components/Loader                                                                                                     |          2 |         19 |          0 |          3 |         22 |
| components/MiniaturesPlan                                                                                             |          1 |       -120 |          0 |        -13 |       -133 |
| components/ModalDateTimePicker                                                                                        |          1 |         64 |          0 |          2 |         66 |
| components/Rubriques                                                                                                  |          1 |          6 |          0 |          0 |          6 |
| components/SearchMap                                                                                                  |          1 |          4 |          0 |         -1 |          3 |
| components/Section                                                                                                    |          1 |          7 |          0 |          0 |          7 |
| components/Select                                                                                                     |          1 |         53 |          0 |          6 |         59 |
| components/SignInOut                                                                                                  |          2 |        -51 |         14 |         -1 |        -38 |
| components/SwipeCards                                                                                                 |          1 |         -4 |          0 |          3 |         -1 |
| components/Tags                                                                                                       |          1 |        -15 |          0 |         -3 |        -18 |
| helpers                                                                                                               |          1 |          9 |          0 |          1 |         10 |
| hooks                                                                                                                 |          2 |         31 |          5 |         10 |         46 |
| ios                                                                                                                   |         21 |          4 |          2 |          0 |          6 |
| ios (Files)                                                                                                           |          1 |          2 |          0 |          0 |          2 |
| ios/Cosh                                                                                                              |         10 |       -163 |        -14 |        -23 |       -200 |
| ios/Cosh (Files)                                                                                                      |          6 |       -101 |        -14 |        -22 |       -137 |
| ios/Cosh/Images.xcassets                                                                                              |          4 |        -62 |          0 |         -1 |        -63 |
| ios/Cosh/Images.xcassets (Files)                                                                                      |          1 |         -6 |          0 |         -1 |         -7 |
| ios/Cosh/Images.xcassets/AppIcon.appiconset                                                                           |          1 |        -14 |          0 |          0 |        -14 |
| ios/Cosh/Images.xcassets/SplashScreen.imageset                                                                        |          1 |        -21 |          0 |          0 |        -21 |
| ios/Cosh/Images.xcassets/SplashScreenBackground.imageset                                                              |          1 |        -21 |          0 |          0 |        -21 |
| ios/Evenly                                                                                                            |         10 |        165 |         16 |         23 |        204 |
| ios/Evenly (Files)                                                                                                    |          6 |        103 |         16 |         22 |        141 |
| ios/Evenly/Images.xcassets                                                                                            |          4 |         62 |          0 |          1 |         63 |
| ios/Evenly/Images.xcassets (Files)                                                                                    |          1 |          6 |          0 |          1 |          7 |
| ios/Evenly/Images.xcassets/AppIcon.appiconset                                                                         |          1 |         14 |          0 |          0 |         14 |
| ios/Evenly/Images.xcassets/SplashScreen.imageset                                                                      |          1 |         21 |          0 |          0 |         21 |
| ios/Evenly/Images.xcassets/SplashScreenBackground.imageset                                                            |          1 |         21 |          0 |          0 |         21 |
| navigation                                                                                                            |          2 |         28 |          0 |          6 |         34 |
| provider                                                                                                              |          2 |        -39 |          0 |         -1 |        -40 |
| screens                                                                                                               |          7 |        -65 |         49 |        -27 |        -43 |
| services                                                                                                              |         11 |        177 |          1 |         30 |        208 |
| services (Files)                                                                                                      |          3 |         30 |          1 |          4 |         35 |
| services/auth                                                                                                         |          2 |          1 |          0 |          3 |          4 |
| services/events                                                                                                       |          4 |         89 |          0 |         12 |        101 |
| services/place                                                                                                        |          1 |         17 |          0 |          3 |         20 |
| services/search                                                                                                       |          1 |         40 |          0 |          8 |         48 |
+-----------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+-----------------------------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+
| filename                                                                                                              | language           | code       | comment    | blank      | total      |
+-----------------------------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+
| /Users/aurelien/Documents/Developpement/cosh/App.tsx                                                                  | TypeScript JSX     |         16 |          0 |          2 |         18 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/google-services.json                                         | JSON               |          1 |          0 |          0 |          1 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/debug/java/com/cosh/ReactNativeFlipper.java              | Java               |        -58 |        -12 |         -6 |        -76 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/debug/java/com/evenly/ReactNativeFlipper.java            | Java               |         58 |         12 |          6 |         76 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/java/com/cosh/MainActivity.java                     | Java               |        -36 |        -21 |         -9 |        -66 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/java/com/cosh/MainApplication.java                  | Java               |        -64 |         -3 |        -14 |        -81 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/java/com/evenly/MainActivity.java                   | Java               |         36 |         21 |          9 |         66 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/java/com/evenly/MainApplication.java                | Java               |         66 |          3 |         14 |         83 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/release/java/com/cosh/ReactNativeFlipper.java            | Java               |         -7 |        -11 |         -3 |        -21 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/release/java/com/evenly/ReactNativeFlipper.java          | Java               |          7 |         11 |          3 |         21 |
| /Users/aurelien/Documents/Developpement/cosh/android/gradle.properties                                                | Properties         |          3 |          0 |          0 |          3 |
| /Users/aurelien/Documents/Developpement/cosh/app.json                                                                 | JSON with Comments |         95 |        -76 |          1 |         20 |
| /Users/aurelien/Documents/Developpement/cosh/components/Blocks/Adress.tsx                                             | TypeScript JSX     |          2 |          0 |          0 |          2 |
| /Users/aurelien/Documents/Developpement/cosh/components/Blocks/Map.tsx                                                | TypeScript JSX     |        -62 |          0 |         -6 |        -68 |
| /Users/aurelien/Documents/Developpement/cosh/components/Button/OpenMap.tsx                                            | TypeScript JSX     |         -3 |          0 |          0 |         -3 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/DiscoverTabView.tsx                           | TypeScript JSX     |          2 |          0 |          0 |          2 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/helper.tsx                                    | TypeScript JSX     |          5 |          0 |          0 |          5 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/screens/All.tsx                               | TypeScript JSX     |        -25 |         13 |         -1 |        -13 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/screens/Bars.tsx                              | TypeScript JSX     |          5 |          0 |          0 |          5 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/screens/Clubs.tsx                             | TypeScript JSX     |        -23 |          1 |         -1 |        -23 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/screens/Concerts.tsx                          | TypeScript JSX     |         53 |          1 |          8 |         62 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/screens/ThisWeek.tsx                          | TypeScript JSX     |        -50 |         -2 |         -5 |        -57 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/screens/Today.tsx                             | TypeScript JSX     |         -5 |          0 |         -1 |         -6 |
| /Users/aurelien/Documents/Developpement/cosh/components/Filters/BarsFilters.tsx                                       | TypeScript JSX     |          0 |          0 |          1 |          1 |
| /Users/aurelien/Documents/Developpement/cosh/components/Filters/CheckboxList.tsx                                      | TypeScript JSX     |          6 |          0 |          0 |          6 |
| /Users/aurelien/Documents/Developpement/cosh/components/Filters/ClubsFilters.tsx                                      | TypeScript JSX     |        233 |          0 |         21 |        254 |
| /Users/aurelien/Documents/Developpement/cosh/components/Filters/ConcertsFilters.tsx                                   | TypeScript JSX     |        173 |          0 |         21 |        194 |
| /Users/aurelien/Documents/Developpement/cosh/components/Headers/MainHeader.tsx                                        | TypeScript JSX     |         49 |          0 |          3 |         52 |
| /Users/aurelien/Documents/Developpement/cosh/components/Headers/SearchHeader.tsx                                      | TypeScript JSX     |        186 |          0 |         17 |        203 |
| /Users/aurelien/Documents/Developpement/cosh/components/InputAddressFinder/InputAddressFinder.tsx                     | TypeScript JSX     |         -3 |          0 |         -2 |         -5 |
| /Users/aurelien/Documents/Developpement/cosh/components/Lists/EventsAndPlacesList.tsx                                 | TypeScript JSX     |         51 |          0 |          4 |         55 |
| /Users/aurelien/Documents/Developpement/cosh/components/Lists/EventsByDateList.tsx                                    | TypeScript JSX     |         81 |          0 |         13 |         94 |
| /Users/aurelien/Documents/Developpement/cosh/components/Loader/EventLoader.tsx                                        | TypeScript JSX     |         -1 |          0 |          1 |          0 |
| /Users/aurelien/Documents/Developpement/cosh/components/Loader/TitleLoader.tsx                                        | TypeScript JSX     |         20 |          0 |          2 |         22 |
| /Users/aurelien/Documents/Developpement/cosh/components/MiniaturesPlan/MiniaturesPlan.tsx                             | TypeScript JSX     |       -120 |          0 |        -13 |       -133 |
| /Users/aurelien/Documents/Developpement/cosh/components/ModalDateTimePicker/ModalDateTimePicker.tsx                   | TypeScript JSX     |         64 |          0 |          2 |         66 |
| /Users/aurelien/Documents/Developpement/cosh/components/Rubriques/Rubriques.tsx                                       | TypeScript JSX     |          6 |          0 |          0 |          6 |
| /Users/aurelien/Documents/Developpement/cosh/components/SearchMap/Map.tsx                                             | TypeScript JSX     |          4 |          0 |         -1 |          3 |
| /Users/aurelien/Documents/Developpement/cosh/components/Section/Section.tsx                                           | TypeScript JSX     |          7 |          0 |          0 |          7 |
| /Users/aurelien/Documents/Developpement/cosh/components/Select/Select.tsx                                             | TypeScript JSX     |         53 |          0 |          6 |         59 |
| /Users/aurelien/Documents/Developpement/cosh/components/SignInOut/GoogleLoginButton.tsx                               | TypeScript JSX     |        -43 |         14 |         -1 |        -30 |
| /Users/aurelien/Documents/Developpement/cosh/components/SignInOut/Register.tsx                                        | TypeScript JSX     |         -8 |          0 |          0 |         -8 |
| /Users/aurelien/Documents/Developpement/cosh/components/SwipeCards/SwipeCards.tsx                                     | TypeScript JSX     |         -4 |          0 |          3 |         -1 |
| /Users/aurelien/Documents/Developpement/cosh/components/Tags/Tags.tsx                                                 | TypeScript JSX     |        -15 |          0 |         -3 |        -18 |
| /Users/aurelien/Documents/Developpement/cosh/google-services.json                                                     | JSON               |          1 |          0 |          0 |          1 |
| /Users/aurelien/Documents/Developpement/cosh/helpers/date.ts                                                          | TypeScript         |          9 |          0 |          1 |         10 |
| /Users/aurelien/Documents/Developpement/cosh/hooks/useAuthState.ts                                                    | TypeScript         |         23 |          1 |          6 |         30 |
| /Users/aurelien/Documents/Developpement/cosh/hooks/useSignInWithGoogle.ts                                             | TypeScript         |          8 |          4 |          4 |         16 |
| /Users/aurelien/Documents/Developpement/cosh/index.js                                                                 | JavaScript         |          1 |          0 |          1 |          2 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Cosh/AppDelegate.h                                                   | C++                |         -5 |          0 |         -3 |         -8 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Cosh/AppDelegate.mm                                                  | Objective-C++      |        -38 |         -7 |        -13 |        -58 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Cosh/Cosh-Bridging-Header.h                                          | C++                |          0 |         -3 |         -1 |         -4 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Cosh/Images.xcassets/AppIcon.appiconset/Contents.json                | JSON               |        -14 |          0 |          0 |        -14 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Cosh/Images.xcassets/Contents.json                                   | JSON               |         -6 |          0 |         -1 |         -7 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Cosh/Images.xcassets/SplashScreen.imageset/Contents.json             | JSON               |        -21 |          0 |          0 |        -21 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Cosh/Images.xcassets/SplashScreenBackground.imageset/Contents.json   | JSON               |        -21 |          0 |          0 |        -21 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Cosh/SplashScreen.storyboard                                         | XML                |        -51 |          0 |          0 |        -51 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Cosh/main.m                                                          | Objective-C        |         -7 |          0 |         -4 |        -11 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Cosh/noop-file.swift                                                 | Swift              |          0 |         -4 |         -1 |         -5 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/AppDelegate.h                                                 | C++                |          5 |          0 |          3 |          8 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/AppDelegate.mm                                                | Objective-C++      |         40 |          9 |         13 |         62 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/Evenly-Bridging-Header.h                                      | C++                |          0 |          3 |          1 |          4 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/Images.xcassets/AppIcon.appiconset/Contents.json              | JSON               |         14 |          0 |          0 |         14 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/Images.xcassets/Contents.json                                 | JSON               |          6 |          0 |          1 |          7 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/Images.xcassets/SplashScreen.imageset/Contents.json           | JSON               |         21 |          0 |          0 |         21 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/Images.xcassets/SplashScreenBackground.imageset/Contents.json | JSON               |         21 |          0 |          0 |         21 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/SplashScreen.storyboard                                       | XML                |         51 |          0 |          0 |         51 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/main.m                                                        | Objective-C        |          7 |          0 |          4 |         11 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/noop-file.swift                                               | Swift              |          0 |          4 |          1 |          5 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Podfile.properties.json                                              | JSON               |          2 |          0 |          0 |          2 |
| /Users/aurelien/Documents/Developpement/cosh/navigation/DiscoverNav.tsx                                               | TypeScript JSX     |          3 |          0 |          0 |          3 |
| /Users/aurelien/Documents/Developpement/cosh/navigation/index.tsx                                                     | TypeScript JSX     |         25 |          0 |          6 |         31 |
| /Users/aurelien/Documents/Developpement/cosh/package-lock.json                                                        | JSON               |        390 |          0 |          0 |        390 |
| /Users/aurelien/Documents/Developpement/cosh/package.json                                                             | JSON               |          4 |          0 |          0 |          4 |
| /Users/aurelien/Documents/Developpement/cosh/provider/FiltersProvider.tsx                                             | TypeScript JSX     |         18 |          0 |          0 |         18 |
| /Users/aurelien/Documents/Developpement/cosh/provider/MapStore.tsx                                                    | TypeScript JSX     |        -57 |          0 |         -1 |        -58 |
| /Users/aurelien/Documents/Developpement/cosh/screens/Event.tsx                                                        | TypeScript JSX     |          1 |          0 |         -1 |          0 |
| /Users/aurelien/Documents/Developpement/cosh/screens/Filters.tsx                                                      | TypeScript JSX     |        -66 |         33 |        -17 |        -50 |
| /Users/aurelien/Documents/Developpement/cosh/screens/GetLocation.tsx                                                  | TypeScript JSX     |         54 |          0 |          5 |         59 |
| /Users/aurelien/Documents/Developpement/cosh/screens/MyEventsWishlist.tsx                                             | TypeScript JSX     |          4 |          0 |          0 |          4 |
| /Users/aurelien/Documents/Developpement/cosh/screens/Place.tsx                                                        | TypeScript JSX     |         46 |         -3 |          2 |         45 |
| /Users/aurelien/Documents/Developpement/cosh/screens/Search.tsx                                                       | TypeScript JSX     |       -189 |          0 |        -22 |       -211 |
| /Users/aurelien/Documents/Developpement/cosh/screens/SignUp.tsx                                                       | TypeScript JSX     |         85 |         19 |          6 |        110 |
| /Users/aurelien/Documents/Developpement/cosh/services/auth/apple.ts                                                   | TypeScript         |         11 |          0 |          3 |         14 |
| /Users/aurelien/Documents/Developpement/cosh/services/auth/google.ts                                                  | TypeScript         |        -10 |          0 |          0 |        -10 |
| /Users/aurelien/Documents/Developpement/cosh/services/axios.ts                                                        | TypeScript         |          0 |          1 |          0 |          1 |
| /Users/aurelien/Documents/Developpement/cosh/services/events/getClubsEvents.ts                                        | TypeScript         |         40 |          0 |          6 |         46 |
| /Users/aurelien/Documents/Developpement/cosh/services/events/getConcerts.ts                                           | TypeScript         |         40 |          0 |          6 |         46 |
| /Users/aurelien/Documents/Developpement/cosh/services/events/getEventById.ts                                          | TypeScript         |          1 |          0 |          0 |          1 |
| /Users/aurelien/Documents/Developpement/cosh/services/events/popular.ts                                               | TypeScript         |          8 |          0 |          0 |          8 |
| /Users/aurelien/Documents/Developpement/cosh/services/place/getPlacesByName.ts                                        | TypeScript         |         17 |          0 |          3 |         20 |
| /Users/aurelien/Documents/Developpement/cosh/services/search/search.ts                                                | TypeScript         |         40 |          0 |          8 |         48 |
| /Users/aurelien/Documents/Developpement/cosh/services/types.ts                                                        | TypeScript         |         55 |          0 |          7 |         62 |
| /Users/aurelien/Documents/Developpement/cosh/services/utils.ts                                                        | TypeScript         |        -25 |          0 |         -3 |        -28 |
| Total                                                                                                                 |                    |      1,296 |          8 |         91 |      1,395 |
+-----------------------------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+