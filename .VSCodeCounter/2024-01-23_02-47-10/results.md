# Summary

Date : 2024-01-23 02:47:10

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 225 files,  44003 codes, 477 comments, 1373 blanks, all 45853 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 11 | 31,324 | 0 | 5 | 31,329 |
| TypeScript JSX | 104 | 10,429 | 156 | 985 | 11,570 |
| TypeScript | 51 | 1,380 | 94 | 221 | 1,695 |
| XML | 35 | 248 | 23 | 24 | 295 |
| Java | 4 | 167 | 47 | 32 | 246 |
| Groovy | 3 | 147 | 76 | 35 | 258 |
| JSON with Comments | 3 | 138 | 0 | 2 | 140 |
| JavaScript | 6 | 56 | 3 | 11 | 70 |
| Batch | 1 | 41 | 29 | 22 | 92 |
| Objective-C++ | 1 | 40 | 9 | 13 | 62 |
| Properties | 2 | 21 | 33 | 14 | 68 |
| Objective-C | 1 | 7 | 0 | 4 | 11 |
| C++ | 2 | 5 | 3 | 4 | 12 |
| Swift | 1 | 0 | 4 | 1 | 5 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 225 | 44,003 | 477 | 1,373 | 45,853 |
| . (Files) | 16 | 31,336 | 12 | 38 | 31,386 |
| .expo-shared | 1 | 6 | 0 | 1 | 7 |
| android | 21 | 529 | 208 | 109 | 846 |
| android (Files) | 4 | 108 | 68 | 45 | 221 |
| android/app | 16 | 415 | 140 | 63 | 618 |
| android/app (Files) | 2 | 150 | 70 | 25 | 245 |
| android/app/src | 14 | 265 | 70 | 38 | 373 |
| android/app/src/debug | 2 | 63 | 12 | 9 | 84 |
| android/app/src/debug (Files) | 1 | 5 | 0 | 3 | 8 |
| android/app/src/debug/java | 1 | 58 | 12 | 6 | 76 |
| android/app/src/debug/java/com | 1 | 58 | 12 | 6 | 76 |
| android/app/src/debug/java/com/evenly | 1 | 58 | 12 | 6 | 76 |
| android/app/src/main | 11 | 195 | 47 | 26 | 268 |
| android/app/src/main (Files) | 1 | 40 | 0 | 0 | 40 |
| android/app/src/main/java | 2 | 102 | 24 | 23 | 149 |
| android/app/src/main/java/com | 2 | 102 | 24 | 23 | 149 |
| android/app/src/main/java/com/evenly | 2 | 102 | 24 | 23 | 149 |
| android/app/src/main/res | 8 | 53 | 23 | 3 | 79 |
| android/app/src/main/res/drawable | 2 | 14 | 23 | 3 | 40 |
| android/app/src/main/res/mipmap-anydpi-v26 | 2 | 10 | 0 | 0 | 10 |
| android/app/src/main/res/values | 3 | 28 | 0 | 0 | 28 |
| android/app/src/main/res/values-night | 1 | 1 | 0 | 0 | 1 |
| android/app/src/release | 1 | 7 | 11 | 3 | 21 |
| android/app/src/release/java | 1 | 7 | 11 | 3 | 21 |
| android/app/src/release/java/com | 1 | 7 | 11 | 3 | 21 |
| android/app/src/release/java/com/evenly | 1 | 7 | 11 | 3 | 21 |
| android/gradle | 1 | 6 | 0 | 1 | 7 |
| android/gradle/wrapper | 1 | 6 | 0 | 1 | 7 |
| assets | 25 | 111 | 0 | 20 | 131 |
| assets/icons | 25 | 111 | 0 | 20 | 131 |
| components | 78 | 7,007 | 94 | 697 | 7,798 |
| components/AddressEditorModal | 1 | 52 | 1 | 5 | 58 |
| components/BlockEvents | 2 | 139 | 0 | 17 | 156 |
| components/BlockPlaces | 2 | 139 | 0 | 14 | 153 |
| components/BlockToFollow | 1 | 58 | 1 | 8 | 67 |
| components/Blocks | 5 | 210 | 0 | 29 | 239 |
| components/BottomSheetIndicator | 1 | 131 | 2 | 8 | 141 |
| components/Button | 6 | 395 | 1 | 52 | 448 |
| components/Carousel | 1 | 157 | 0 | 19 | 176 |
| components/Checkbox | 2 | 156 | 0 | 22 | 178 |
| components/CirclePeoples | 1 | 36 | 0 | 5 | 41 |
| components/CoshText | 1 | 18 | 0 | 5 | 23 |
| components/CreateEventForm | 2 | 433 | 9 | 22 | 464 |
| components/CreateEventHeader | 1 | 85 | 0 | 8 | 93 |
| components/CreatePlaceForm | 2 | 437 | 10 | 19 | 466 |
| components/DiscoverTabView | 10 | 607 | 17 | 90 | 714 |
| components/DiscoverTabView (Files) | 2 | 117 | 0 | 19 | 136 |
| components/DiscoverTabView/screens | 8 | 490 | 17 | 71 | 578 |
| components/EventMiniature | 1 | 115 | 2 | 8 | 125 |
| components/EventsList | 1 | 26 | 0 | 5 | 31 |
| components/Filters | 6 | 584 | 0 | 61 | 645 |
| components/Headers | 3 | 554 | 0 | 43 | 597 |
| components/HoursSeletor | 1 | 227 | 0 | 21 | 248 |
| components/IconAndText | 1 | 81 | 0 | 5 | 86 |
| components/InputAddressFinder | 1 | 138 | 0 | 14 | 152 |
| components/InputPlaceFinder | 1 | 133 | 0 | 15 | 148 |
| components/InputPlaceFinder.tsx | 1 | 112 | 0 | 11 | 123 |
| components/Lists | 2 | 132 | 0 | 17 | 149 |
| components/Loader | 3 | 90 | 0 | 9 | 99 |
| components/ModalDateTimePicker | 1 | 151 | 0 | 7 | 158 |
| components/PeoplesAreJoined | 1 | 46 | 0 | 6 | 52 |
| components/PriceRange | 1 | 65 | 0 | 7 | 72 |
| components/RadiusSelector | 1 | 32 | 0 | 5 | 37 |
| components/Rubriques | 2 | 110 | 0 | 16 | 126 |
| components/SearchMap | 2 | 432 | 0 | 21 | 453 |
| components/Section | 2 | 116 | 0 | 8 | 124 |
| components/Select | 1 | 53 | 0 | 6 | 59 |
| components/SignInOut | 2 | 93 | 31 | 16 | 140 |
| components/Slider | 1 | 123 | 0 | 18 | 141 |
| components/SwipeCards | 1 | 264 | 0 | 26 | 290 |
| components/Tags | 1 | 25 | 0 | 5 | 30 |
| components/Toast | 1 | 115 | 0 | 8 | 123 |
| components/UpdateEventButton | 1 | 17 | 18 | 4 | 39 |
| components/UploadCover | 1 | 120 | 2 | 12 | 134 |
| constants | 5 | 71 | 0 | 14 | 85 |
| helpers | 3 | 43 | 0 | 10 | 53 |
| hooks | 10 | 193 | 41 | 51 | 285 |
| ios | 11 | 171 | 16 | 24 | 211 |
| ios (Files) | 1 | 6 | 0 | 1 | 7 |
| ios/Evenly | 10 | 165 | 16 | 23 | 204 |
| ios/Evenly (Files) | 6 | 103 | 16 | 22 | 141 |
| ios/Evenly/Images.xcassets | 4 | 62 | 0 | 1 | 63 |
| ios/Evenly/Images.xcassets (Files) | 1 | 6 | 0 | 1 | 7 |
| ios/Evenly/Images.xcassets/AppIcon.appiconset | 1 | 14 | 0 | 0 | 14 |
| ios/Evenly/Images.xcassets/SplashScreen.imageset | 1 | 21 | 0 | 0 | 21 |
| ios/Evenly/Images.xcassets/SplashScreenBackground.imageset | 1 | 21 | 0 | 0 | 21 |
| navigation | 2 | 389 | 0 | 34 | 423 |
| provider | 9 | 1,185 | 2 | 66 | 1,253 |
| screens | 16 | 2,101 | 54 | 176 | 2,331 |
| services | 27 | 842 | 50 | 129 | 1,021 |
| services (Files) | 8 | 404 | 50 | 57 | 511 |
| services/auth | 2 | 22 | 0 | 6 | 28 |
| services/events | 8 | 204 | 0 | 30 | 234 |
| services/follow | 1 | 48 | 0 | 7 | 55 |
| services/place | 5 | 100 | 0 | 15 | 115 |
| services/search | 1 | 40 | 0 | 8 | 48 |
| services/topics | 2 | 24 | 0 | 6 | 30 |
| types | 1 | 19 | 0 | 4 | 23 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)