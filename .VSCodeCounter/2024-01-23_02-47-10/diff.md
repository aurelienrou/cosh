# Diff Summary

Date : 2024-01-23 02:47:10

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 94 files,  1296 codes, 8 comments, 91 blanks, all 1395 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript JSX | 44 | 578 | 76 | 48 | 702 |
| JSON | 13 | 398 | 0 | 0 | 398 |
| TypeScript | 14 | 217 | 6 | 41 | 264 |
| JSON with Comments | 1 | 95 | -76 | 1 | 20 |
| Properties | 1 | 3 | 0 | 0 | 3 |
| Java | 8 | 2 | 0 | 0 | 2 |
| Objective-C++ | 2 | 2 | 2 | 0 | 4 |
| JavaScript | 1 | 1 | 0 | 1 | 2 |
| C++ | 4 | 0 | 0 | 0 | 0 |
| Swift | 2 | 0 | 0 | 0 | 0 |
| XML | 2 | 0 | 0 | 0 | 0 |
| Objective-C | 2 | 0 | 0 | 0 | 0 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 94 | 1,296 | 8 | 91 | 1,395 |
| . (Files) | 6 | 507 | -76 | 4 | 435 |
| android | 10 | 6 | 0 | 0 | 6 |
| android (Files) | 1 | 3 | 0 | 0 | 3 |
| android/app | 9 | 3 | 0 | 0 | 3 |
| android/app (Files) | 1 | 1 | 0 | 0 | 1 |
| android/app/src | 8 | 2 | 0 | 0 | 2 |
| android/app/src/debug | 2 | 0 | 0 | 0 | 0 |
| android/app/src/debug/java | 2 | 0 | 0 | 0 | 0 |
| android/app/src/debug/java/com | 2 | 0 | 0 | 0 | 0 |
| android/app/src/debug/java/com/cosh | 1 | -58 | -12 | -6 | -76 |
| android/app/src/debug/java/com/evenly | 1 | 58 | 12 | 6 | 76 |
| android/app/src/main | 4 | 2 | 0 | 0 | 2 |
| android/app/src/main/java | 4 | 2 | 0 | 0 | 2 |
| android/app/src/main/java/com | 4 | 2 | 0 | 0 | 2 |
| android/app/src/main/java/com/cosh | 2 | -100 | -24 | -23 | -147 |
| android/app/src/main/java/com/evenly | 2 | 102 | 24 | 23 | 149 |
| android/app/src/release | 2 | 0 | 0 | 0 | 0 |
| android/app/src/release/java | 2 | 0 | 0 | 0 | 0 |
| android/app/src/release/java/com | 2 | 0 | 0 | 0 | 0 |
| android/app/src/release/java/com/cosh | 1 | -7 | -11 | -3 | -21 |
| android/app/src/release/java/com/evenly | 1 | 7 | 11 | 3 | 21 |
| components | 32 | 638 | 27 | 68 | 733 |
| components/Blocks | 2 | -60 | 0 | -6 | -66 |
| components/Button | 1 | -3 | 0 | 0 | -3 |
| components/DiscoverTabView | 8 | -38 | 13 | 0 | -25 |
| components/DiscoverTabView (Files) | 2 | 7 | 0 | 0 | 7 |
| components/DiscoverTabView/screens | 6 | -45 | 13 | 0 | -32 |
| components/Filters | 4 | 412 | 0 | 43 | 455 |
| components/Headers | 2 | 235 | 0 | 20 | 255 |
| components/InputAddressFinder | 1 | -3 | 0 | -2 | -5 |
| components/Lists | 2 | 132 | 0 | 17 | 149 |
| components/Loader | 2 | 19 | 0 | 3 | 22 |
| components/MiniaturesPlan | 1 | -120 | 0 | -13 | -133 |
| components/ModalDateTimePicker | 1 | 64 | 0 | 2 | 66 |
| components/Rubriques | 1 | 6 | 0 | 0 | 6 |
| components/SearchMap | 1 | 4 | 0 | -1 | 3 |
| components/Section | 1 | 7 | 0 | 0 | 7 |
| components/Select | 1 | 53 | 0 | 6 | 59 |
| components/SignInOut | 2 | -51 | 14 | -1 | -38 |
| components/SwipeCards | 1 | -4 | 0 | 3 | -1 |
| components/Tags | 1 | -15 | 0 | -3 | -18 |
| helpers | 1 | 9 | 0 | 1 | 10 |
| hooks | 2 | 31 | 5 | 10 | 46 |
| ios | 21 | 4 | 2 | 0 | 6 |
| ios (Files) | 1 | 2 | 0 | 0 | 2 |
| ios/Cosh | 10 | -163 | -14 | -23 | -200 |
| ios/Cosh (Files) | 6 | -101 | -14 | -22 | -137 |
| ios/Cosh/Images.xcassets | 4 | -62 | 0 | -1 | -63 |
| ios/Cosh/Images.xcassets (Files) | 1 | -6 | 0 | -1 | -7 |
| ios/Cosh/Images.xcassets/AppIcon.appiconset | 1 | -14 | 0 | 0 | -14 |
| ios/Cosh/Images.xcassets/SplashScreen.imageset | 1 | -21 | 0 | 0 | -21 |
| ios/Cosh/Images.xcassets/SplashScreenBackground.imageset | 1 | -21 | 0 | 0 | -21 |
| ios/Evenly | 10 | 165 | 16 | 23 | 204 |
| ios/Evenly (Files) | 6 | 103 | 16 | 22 | 141 |
| ios/Evenly/Images.xcassets | 4 | 62 | 0 | 1 | 63 |
| ios/Evenly/Images.xcassets (Files) | 1 | 6 | 0 | 1 | 7 |
| ios/Evenly/Images.xcassets/AppIcon.appiconset | 1 | 14 | 0 | 0 | 14 |
| ios/Evenly/Images.xcassets/SplashScreen.imageset | 1 | 21 | 0 | 0 | 21 |
| ios/Evenly/Images.xcassets/SplashScreenBackground.imageset | 1 | 21 | 0 | 0 | 21 |
| navigation | 2 | 28 | 0 | 6 | 34 |
| provider | 2 | -39 | 0 | -1 | -40 |
| screens | 7 | -65 | 49 | -27 | -43 |
| services | 11 | 177 | 1 | 30 | 208 |
| services (Files) | 3 | 30 | 1 | 4 | 35 |
| services/auth | 2 | 1 | 0 | 3 | 4 |
| services/events | 4 | 89 | 0 | 12 | 101 |
| services/place | 1 | 17 | 0 | 3 | 20 |
| services/search | 1 | 40 | 0 | 8 | 48 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)