# Summary

Date : 2024-02-20 01:46:26

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 217 files,  13854 codes, 235 comments, 1356 blanks, all 15445 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript JSX | 113 | 11,575 | 98 | 1,063 | 12,736 |
| TypeScript | 54 | 1,442 | 118 | 235 | 1,795 |
| JSON | 9 | 432 | 0 | 4 | 436 |
| XML | 27 | 155 | 0 | 19 | 174 |
| JSON with Comments | 3 | 142 | 0 | 2 | 144 |
| JavaScript | 6 | 56 | 3 | 11 | 70 |
| Objective-C++ | 1 | 40 | 9 | 13 | 62 |
| Objective-C | 1 | 7 | 0 | 4 | 11 |
| C++ | 2 | 5 | 3 | 4 | 12 |
| Swift | 1 | 0 | 4 | 1 | 5 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 217 | 13,854 | 235 | 1,356 | 15,445 |
| . (Files) | 15 | 503 | 12 | 37 | 552 |
| .expo-shared | 1 | 6 | 0 | 1 | 7 |
| assets | 27 | 116 | 0 | 21 | 137 |
| assets/icons | 27 | 116 | 0 | 21 | 137 |
| components | 85 | 7,892 | 52 | 767 | 8,711 |
| components/AddressEditorModal | 1 | 52 | 1 | 5 | 58 |
| components/BlockEvents | 2 | 139 | 0 | 17 | 156 |
| components/BlockPlaces | 2 | 139 | 0 | 14 | 153 |
| components/BlockToFollow | 1 | 56 | 0 | 8 | 64 |
| components/Blocks | 5 | 214 | 0 | 29 | 243 |
| components/BottomSheetIndicator | 1 | 131 | 2 | 8 | 141 |
| components/Button | 6 | 403 | 1 | 53 | 457 |
| components/Carousel | 1 | 157 | 0 | 19 | 176 |
| components/Checkbox | 2 | 157 | 0 | 22 | 179 |
| components/CirclePeoples | 1 | 36 | 0 | 5 | 41 |
| components/CoshText | 1 | 18 | 0 | 5 | 23 |
| components/CreateEventForm | 2 | 433 | 9 | 22 | 464 |
| components/CreateEventHeader | 1 | 85 | 0 | 8 | 93 |
| components/CreatePlaceForm | 2 | 437 | 10 | 19 | 466 |
| components/Description | 1 | 59 | 0 | 8 | 67 |
| components/DiscoverTabView | 10 | 716 | 4 | 92 | 812 |
| components/DiscoverTabView (Files) | 2 | 120 | 0 | 19 | 139 |
| components/DiscoverTabView/screens | 8 | 596 | 4 | 73 | 673 |
| components/Errors | 1 | 32 | 0 | 4 | 36 |
| components/EventMiniature | 1 | 115 | 2 | 8 | 125 |
| components/EventsList | 1 | 26 | 0 | 5 | 31 |
| components/Filters | 6 | 767 | 1 | 79 | 847 |
| components/Headers | 3 | 581 | 0 | 47 | 628 |
| components/HoursSeletor | 1 | 227 | 0 | 21 | 248 |
| components/IconAndText | 1 | 81 | 0 | 5 | 86 |
| components/InputAddressFinder | 1 | 138 | 0 | 14 | 152 |
| components/InputPlaceFinder | 1 | 133 | 0 | 15 | 148 |
| components/InputPlaceFinder.tsx | 1 | 112 | 0 | 11 | 123 |
| components/Lists | 2 | 136 | 0 | 17 | 153 |
| components/Loader | 4 | 176 | 0 | 16 | 192 |
| components/LocationMap | 1 | 61 | 0 | 7 | 68 |
| components/ModalDateTimePicker | 1 | 151 | 0 | 7 | 158 |
| components/PeoplesAreJoined | 1 | 46 | 0 | 6 | 52 |
| components/PriceRange | 1 | 65 | 0 | 7 | 72 |
| components/Profil | 1 | 127 | 0 | 6 | 133 |
| components/RadiusSelector | 1 | 32 | 0 | 5 | 37 |
| components/Rubriques | 2 | 110 | 0 | 16 | 126 |
| components/SearchMap | 3 | 559 | 2 | 28 | 589 |
| components/Section | 2 | 116 | 0 | 9 | 125 |
| components/Select | 1 | 53 | 0 | 6 | 59 |
| components/SignInOut | 3 | 153 | 0 | 20 | 173 |
| components/Slider | 1 | 123 | 0 | 18 | 141 |
| components/SwipeCards | 1 | 263 | 0 | 27 | 290 |
| components/Tags | 1 | 25 | 0 | 5 | 30 |
| components/Toast | 1 | 115 | 0 | 8 | 123 |
| components/UpdateEventButton | 1 | 17 | 18 | 4 | 39 |
| components/UploadCover | 1 | 120 | 2 | 12 | 134 |
| constants | 5 | 71 | 0 | 14 | 85 |
| helpers | 3 | 43 | 0 | 10 | 53 |
| hooks | 10 | 172 | 64 | 51 | 287 |
| ios | 11 | 171 | 16 | 24 | 211 |
| ios (Files) | 1 | 6 | 0 | 1 | 7 |
| ios/Closy | 10 | 165 | 16 | 23 | 204 |
| ios/Closy (Files) | 6 | 103 | 16 | 22 | 141 |
| ios/Closy/Images.xcassets | 4 | 62 | 0 | 1 | 63 |
| ios/Closy/Images.xcassets (Files) | 1 | 6 | 0 | 1 | 7 |
| ios/Closy/Images.xcassets/AppIcon.appiconset | 1 | 14 | 0 | 0 | 14 |
| ios/Closy/Images.xcassets/SplashScreen.imageset | 1 | 21 | 0 | 0 | 21 |
| ios/Closy/Images.xcassets/SplashScreenBackground.imageset | 1 | 21 | 0 | 0 | 21 |
| navigation | 2 | 356 | 0 | 24 | 380 |
| provider | 9 | 1,204 | 2 | 68 | 1,274 |
| screens | 18 | 2,376 | 38 | 192 | 2,606 |
| services | 30 | 925 | 51 | 143 | 1,119 |
| services (Files) | 8 | 407 | 51 | 58 | 516 |
| services/auth | 4 | 75 | 0 | 16 | 91 |
| services/events | 8 | 202 | 0 | 28 | 230 |
| services/follow | 1 | 48 | 0 | 7 | 55 |
| services/place | 5 | 117 | 0 | 17 | 134 |
| services/search | 1 | 40 | 0 | 8 | 48 |
| services/topics | 2 | 24 | 0 | 6 | 30 |
| services/user | 1 | 12 | 0 | 3 | 15 |
| types | 1 | 19 | 0 | 4 | 23 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)