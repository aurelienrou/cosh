# Diff Summary

Date : 2024-02-20 01:46:26

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 103 files,  -30149 codes, -242 comments, -17 blanks, all -30408 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript JSX | 45 | 1,146 | -58 | 78 | 1,166 |
| TypeScript | 11 | 62 | 24 | 14 | 100 |
| JSON with Comments | 2 | 4 | 0 | 0 | 4 |
| Swift | 2 | 0 | 0 | 0 | 0 |
| Objective-C++ | 2 | 0 | 0 | 0 | 0 |
| Objective-C | 2 | 0 | 0 | 0 | 0 |
| C++ | 4 | 0 | 0 | 0 | 0 |
| Properties | 2 | -21 | -33 | -14 | -68 |
| Batch | 1 | -41 | -29 | -22 | -92 |
| XML | 14 | -93 | -23 | -5 | -121 |
| Groovy | 3 | -147 | -76 | -35 | -258 |
| Java | 4 | -167 | -47 | -32 | -246 |
| JSON | 11 | -30,892 | 0 | -1 | -30,893 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 103 | -30,149 | -242 | -17 | -30,408 |
| . (Files) | 4 | -30,833 | 0 | -1 | -30,834 |
| android | 21 | -529 | -208 | -109 | -846 |
| android (Files) | 4 | -108 | -68 | -45 | -221 |
| android/app | 16 | -415 | -140 | -63 | -618 |
| android/app (Files) | 2 | -150 | -70 | -25 | -245 |
| android/app/src | 14 | -265 | -70 | -38 | -373 |
| android/app/src/debug | 2 | -63 | -12 | -9 | -84 |
| android/app/src/debug (Files) | 1 | -5 | 0 | -3 | -8 |
| android/app/src/debug/java | 1 | -58 | -12 | -6 | -76 |
| android/app/src/debug/java/com | 1 | -58 | -12 | -6 | -76 |
| android/app/src/debug/java/com/evenly | 1 | -58 | -12 | -6 | -76 |
| android/app/src/main | 11 | -195 | -47 | -26 | -268 |
| android/app/src/main (Files) | 1 | -40 | 0 | 0 | -40 |
| android/app/src/main/java | 2 | -102 | -24 | -23 | -149 |
| android/app/src/main/java/com | 2 | -102 | -24 | -23 | -149 |
| android/app/src/main/java/com/evenly | 2 | -102 | -24 | -23 | -149 |
| android/app/src/main/res | 8 | -53 | -23 | -3 | -79 |
| android/app/src/main/res/drawable | 2 | -14 | -23 | -3 | -40 |
| android/app/src/main/res/mipmap-anydpi-v26 | 2 | -10 | 0 | 0 | -10 |
| android/app/src/main/res/values | 3 | -28 | 0 | 0 | -28 |
| android/app/src/main/res/values-night | 1 | -1 | 0 | 0 | -1 |
| android/app/src/release | 1 | -7 | -11 | -3 | -21 |
| android/app/src/release/java | 1 | -7 | -11 | -3 | -21 |
| android/app/src/release/java/com | 1 | -7 | -11 | -3 | -21 |
| android/app/src/release/java/com/evenly | 1 | -7 | -11 | -3 | -21 |
| android/gradle | 1 | -6 | 0 | -1 | -7 |
| android/gradle/wrapper | 1 | -6 | 0 | -1 | -7 |
| assets | 2 | 5 | 0 | 1 | 6 |
| assets/icons | 2 | 5 | 0 | 1 | 6 |
| components | 32 | 885 | -42 | 70 | 913 |
| components/BlockEvents | 2 | 0 | 0 | 0 | 0 |
| components/BlockToFollow | 1 | -2 | -1 | 0 | -3 |
| components/Blocks | 1 | 4 | 0 | 0 | 4 |
| components/Button | 1 | 8 | 0 | 1 | 9 |
| components/Checkbox | 1 | 1 | 0 | 0 | 1 |
| components/Description | 1 | 59 | 0 | 8 | 67 |
| components/DiscoverTabView | 7 | 109 | -13 | 2 | 98 |
| components/DiscoverTabView (Files) | 1 | 3 | 0 | 0 | 3 |
| components/DiscoverTabView/screens | 6 | 106 | -13 | 2 | 95 |
| components/Errors | 1 | 32 | 0 | 4 | 36 |
| components/Filters | 4 | 183 | 1 | 18 | 202 |
| components/Headers | 2 | 27 | 0 | 4 | 31 |
| components/Lists | 1 | 4 | 0 | 0 | 4 |
| components/Loader | 1 | 86 | 0 | 7 | 93 |
| components/LocationMap | 1 | 61 | 0 | 7 | 68 |
| components/Profil | 1 | 127 | 0 | 6 | 133 |
| components/SearchMap | 2 | 127 | 2 | 7 | 136 |
| components/Section | 1 | 0 | 0 | 1 | 1 |
| components/SignInOut | 3 | 60 | -31 | 4 | 33 |
| components/SwipeCards | 1 | -1 | 0 | 1 | 0 |
| hooks | 2 | -21 | 23 | 0 | 2 |
| ios | 20 | 0 | 0 | 0 | 0 |
| ios/Closy | 10 | 165 | 16 | 23 | 204 |
| ios/Closy (Files) | 6 | 103 | 16 | 22 | 141 |
| ios/Closy/Images.xcassets | 4 | 62 | 0 | 1 | 63 |
| ios/Closy/Images.xcassets (Files) | 1 | 6 | 0 | 1 | 7 |
| ios/Closy/Images.xcassets/AppIcon.appiconset | 1 | 14 | 0 | 0 | 14 |
| ios/Closy/Images.xcassets/SplashScreen.imageset | 1 | 21 | 0 | 0 | 21 |
| ios/Closy/Images.xcassets/SplashScreenBackground.imageset | 1 | 21 | 0 | 0 | 21 |
| ios/Evenly | 10 | -165 | -16 | -23 | -204 |
| ios/Evenly (Files) | 6 | -103 | -16 | -22 | -141 |
| ios/Evenly/Images.xcassets | 4 | -62 | 0 | -1 | -63 |
| ios/Evenly/Images.xcassets (Files) | 1 | -6 | 0 | -1 | -7 |
| ios/Evenly/Images.xcassets/AppIcon.appiconset | 1 | -14 | 0 | 0 | -14 |
| ios/Evenly/Images.xcassets/SplashScreen.imageset | 1 | -21 | 0 | 0 | -21 |
| ios/Evenly/Images.xcassets/SplashScreenBackground.imageset | 1 | -21 | 0 | 0 | -21 |
| navigation | 1 | -33 | 0 | -10 | -43 |
| provider | 3 | 19 | 0 | 2 | 21 |
| screens | 9 | 275 | -16 | 16 | 275 |
| services | 9 | 83 | 1 | 14 | 98 |
| services (Files) | 2 | 3 | 1 | 1 | 5 |
| services/auth | 3 | 53 | 0 | 10 | 63 |
| services/events | 2 | -2 | 0 | -2 | -4 |
| services/place | 1 | 17 | 0 | 2 | 19 |
| services/user | 1 | 12 | 0 | 3 | 15 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)