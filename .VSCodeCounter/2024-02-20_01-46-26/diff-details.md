# Diff Details

Date : 2024-02-20 01:46:26

Directory /Users/aurelien/Documents/Developpement/cosh

Total : 103 files,  -30149 codes, -242 comments, -17 blanks, all -30408 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [android/app/build.gradle](/android/app/build.gradle) | Groovy | -95 | -70 | -25 | -190 |
| [android/app/google-services.json](/android/app/google-services.json) | JSON | -55 | 0 | 0 | -55 |
| [android/app/src/debug/AndroidManifest.xml](/android/app/src/debug/AndroidManifest.xml) | XML | -5 | 0 | -3 | -8 |
| [android/app/src/debug/java/com/evenly/ReactNativeFlipper.java](/android/app/src/debug/java/com/evenly/ReactNativeFlipper.java) | Java | -58 | -12 | -6 | -76 |
| [android/app/src/main/AndroidManifest.xml](/android/app/src/main/AndroidManifest.xml) | XML | -40 | 0 | 0 | -40 |
| [android/app/src/main/java/com/evenly/MainActivity.java](/android/app/src/main/java/com/evenly/MainActivity.java) | Java | -36 | -21 | -9 | -66 |
| [android/app/src/main/java/com/evenly/MainApplication.java](/android/app/src/main/java/com/evenly/MainApplication.java) | Java | -66 | -3 | -14 | -83 |
| [android/app/src/main/res/drawable/rn_edit_text_material.xml](/android/app/src/main/res/drawable/rn_edit_text_material.xml) | XML | -11 | -23 | -3 | -37 |
| [android/app/src/main/res/drawable/splashscreen.xml](/android/app/src/main/res/drawable/splashscreen.xml) | XML | -3 | 0 | 0 | -3 |
| [android/app/src/main/res/mipmap-anydpi-v26/ic_launcher.xml](/android/app/src/main/res/mipmap-anydpi-v26/ic_launcher.xml) | XML | -5 | 0 | 0 | -5 |
| [android/app/src/main/res/mipmap-anydpi-v26/ic_launcher_round.xml](/android/app/src/main/res/mipmap-anydpi-v26/ic_launcher_round.xml) | XML | -5 | 0 | 0 | -5 |
| [android/app/src/main/res/values-night/colors.xml](/android/app/src/main/res/values-night/colors.xml) | XML | -1 | 0 | 0 | -1 |
| [android/app/src/main/res/values/colors.xml](/android/app/src/main/res/values/colors.xml) | XML | -6 | 0 | 0 | -6 |
| [android/app/src/main/res/values/strings.xml](/android/app/src/main/res/values/strings.xml) | XML | -5 | 0 | 0 | -5 |
| [android/app/src/main/res/values/styles.xml](/android/app/src/main/res/values/styles.xml) | XML | -17 | 0 | 0 | -17 |
| [android/app/src/release/java/com/evenly/ReactNativeFlipper.java](/android/app/src/release/java/com/evenly/ReactNativeFlipper.java) | Java | -7 | -11 | -3 | -21 |
| [android/build.gradle](/android/build.gradle) | Groovy | -45 | -6 | -6 | -57 |
| [android/gradle.properties](/android/gradle.properties) | Properties | -15 | -33 | -13 | -61 |
| [android/gradle/wrapper/gradle-wrapper.properties](/android/gradle/wrapper/gradle-wrapper.properties) | Properties | -6 | 0 | -1 | -7 |
| [android/gradlew.bat](/android/gradlew.bat) | Batch | -41 | -29 | -22 | -92 |
| [android/settings.gradle](/android/settings.gradle) | Groovy | -7 | 0 | -4 | -11 |
| [app.json](/app.json) | JSON with Comments | -5 | 0 | 0 | -5 |
| [assets/icons/nearme.svg](/assets/icons/nearme.svg) | XML | 1 | 0 | 0 | 1 |
| [assets/icons/user-rounded.svg](/assets/icons/user-rounded.svg) | XML | 4 | 0 | 1 | 5 |
| [components/BlockEvents/BlockEvent.tsx](/components/BlockEvents/BlockEvent.tsx) | TypeScript JSX | 1 | 0 | 0 | 1 |
| [components/BlockEvents/BlockEvents.tsx](/components/BlockEvents/BlockEvents.tsx) | TypeScript JSX | -1 | 0 | 0 | -1 |
| [components/BlockToFollow/BlockToFollow.tsx](/components/BlockToFollow/BlockToFollow.tsx) | TypeScript JSX | -2 | -1 | 0 | -3 |
| [components/Blocks/HeaderBackAndFollow.tsx](/components/Blocks/HeaderBackAndFollow.tsx) | TypeScript JSX | 4 | 0 | 0 | 4 |
| [components/Button/FollowButton.tsx](/components/Button/FollowButton.tsx) | TypeScript JSX | 8 | 0 | 1 | 9 |
| [components/Checkbox/SelectorLine.tsx](/components/Checkbox/SelectorLine.tsx) | TypeScript JSX | 1 | 0 | 0 | 1 |
| [components/Description/Description.tsx](/components/Description/Description.tsx) | TypeScript JSX | 59 | 0 | 8 | 67 |
| [components/DiscoverTabView/DiscoverTabView.tsx](/components/DiscoverTabView/DiscoverTabView.tsx) | TypeScript JSX | 3 | 0 | 0 | 3 |
| [components/DiscoverTabView/screens/All.tsx](/components/DiscoverTabView/screens/All.tsx) | TypeScript JSX | 23 | -13 | -1 | 9 |
| [components/DiscoverTabView/screens/Bars.tsx](/components/DiscoverTabView/screens/Bars.tsx) | TypeScript JSX | 46 | 0 | 3 | 49 |
| [components/DiscoverTabView/screens/Clubs.tsx](/components/DiscoverTabView/screens/Clubs.tsx) | TypeScript JSX | 7 | 0 | 0 | 7 |
| [components/DiscoverTabView/screens/Concerts.tsx](/components/DiscoverTabView/screens/Concerts.tsx) | TypeScript JSX | 7 | 0 | 0 | 7 |
| [components/DiscoverTabView/screens/ThisWeek.tsx](/components/DiscoverTabView/screens/ThisWeek.tsx) | TypeScript JSX | 7 | 0 | 0 | 7 |
| [components/DiscoverTabView/screens/Today.tsx](/components/DiscoverTabView/screens/Today.tsx) | TypeScript JSX | 16 | 0 | 0 | 16 |
| [components/Errors/MainMessage.tsx](/components/Errors/MainMessage.tsx) | TypeScript JSX | 32 | 0 | 4 | 36 |
| [components/Filters/BarsFilters.tsx](/components/Filters/BarsFilters.tsx) | TypeScript JSX | 164 | 0 | 20 | 184 |
| [components/Filters/ClubsFilters.tsx](/components/Filters/ClubsFilters.tsx) | TypeScript JSX | 18 | 1 | -1 | 18 |
| [components/Filters/ConcertsFilters.tsx](/components/Filters/ConcertsFilters.tsx) | TypeScript JSX | 2 | 0 | -1 | 1 |
| [components/Filters/FiltersButtons.tsx](/components/Filters/FiltersButtons.tsx) | TypeScript JSX | -1 | 0 | 0 | -1 |
| [components/Headers/MainHeader.tsx](/components/Headers/MainHeader.tsx) | TypeScript JSX | 31 | 0 | 4 | 35 |
| [components/Headers/PlaceOrArtistHeader.tsx](/components/Headers/PlaceOrArtistHeader.tsx) | TypeScript JSX | -4 | 0 | 0 | -4 |
| [components/Lists/EventsByDateList.tsx](/components/Lists/EventsByDateList.tsx) | TypeScript JSX | 4 | 0 | 0 | 4 |
| [components/Loader/MapLoader.tsx](/components/Loader/MapLoader.tsx) | TypeScript JSX | 86 | 0 | 7 | 93 |
| [components/LocationMap/LocationMap.tsx](/components/LocationMap/LocationMap.tsx) | TypeScript JSX | 61 | 0 | 7 | 68 |
| [components/Profil/Profil.tsx](/components/Profil/Profil.tsx) | TypeScript JSX | 127 | 0 | 6 | 133 |
| [components/SearchMap/Map.tsx](/components/SearchMap/Map.tsx) | TypeScript JSX | 86 | 2 | 3 | 91 |
| [components/SearchMap/MyLocation.tsx](/components/SearchMap/MyLocation.tsx) | TypeScript JSX | 41 | 0 | 4 | 45 |
| [components/Section/SectionHeader.tsx](/components/Section/SectionHeader.tsx) | TypeScript JSX | 0 | 0 | 1 | 1 |
| [components/SignInOut/GoogleLoginButton.tsx](/components/SignInOut/GoogleLoginButton.tsx) | TypeScript JSX | 19 | -31 | -1 | -13 |
| [components/SignInOut/LogIn.tsx](/components/SignInOut/LogIn.tsx) | TypeScript JSX | 40 | 0 | 6 | 46 |
| [components/SignInOut/Register.tsx](/components/SignInOut/Register.tsx) | TypeScript JSX | 1 | 0 | -1 | 0 |
| [components/SwipeCards/SwipeCards.tsx](/components/SwipeCards/SwipeCards.tsx) | TypeScript JSX | -1 | 0 | 1 | 0 |
| [eas.json](/eas.json) | JSON with Comments | 9 | 0 | 0 | 9 |
| [hooks/useAuthState.ts](/hooks/useAuthState.ts) | TypeScript | -23 | 23 | 0 | 0 |
| [hooks/useCachedResources.ts](/hooks/useCachedResources.ts) | TypeScript | 2 | 0 | 0 | 2 |
| [ios/Closy/AppDelegate.h](/ios/Closy/AppDelegate.h) | C++ | 5 | 0 | 3 | 8 |
| [ios/Closy/AppDelegate.mm](/ios/Closy/AppDelegate.mm) | Objective-C++ | 40 | 9 | 13 | 62 |
| [ios/Closy/Closy-Bridging-Header.h](/ios/Closy/Closy-Bridging-Header.h) | C++ | 0 | 3 | 1 | 4 |
| [ios/Closy/Images.xcassets/AppIcon.appiconset/Contents.json](/ios/Closy/Images.xcassets/AppIcon.appiconset/Contents.json) | JSON | 14 | 0 | 0 | 14 |
| [ios/Closy/Images.xcassets/Contents.json](/ios/Closy/Images.xcassets/Contents.json) | JSON | 6 | 0 | 1 | 7 |
| [ios/Closy/Images.xcassets/SplashScreen.imageset/Contents.json](/ios/Closy/Images.xcassets/SplashScreen.imageset/Contents.json) | JSON | 21 | 0 | 0 | 21 |
| [ios/Closy/Images.xcassets/SplashScreenBackground.imageset/Contents.json](/ios/Closy/Images.xcassets/SplashScreenBackground.imageset/Contents.json) | JSON | 21 | 0 | 0 | 21 |
| [ios/Closy/SplashScreen.storyboard](/ios/Closy/SplashScreen.storyboard) | XML | 51 | 0 | 0 | 51 |
| [ios/Closy/main.m](/ios/Closy/main.m) | Objective-C | 7 | 0 | 4 | 11 |
| [ios/Closy/noop-file.swift](/ios/Closy/noop-file.swift) | Swift | 0 | 4 | 1 | 5 |
| [ios/Evenly/AppDelegate.h](/ios/Evenly/AppDelegate.h) | C++ | -5 | 0 | -3 | -8 |
| [ios/Evenly/AppDelegate.mm](/ios/Evenly/AppDelegate.mm) | Objective-C++ | -40 | -9 | -13 | -62 |
| [ios/Evenly/Evenly-Bridging-Header.h](/ios/Evenly/Evenly-Bridging-Header.h) | C++ | 0 | -3 | -1 | -4 |
| [ios/Evenly/Images.xcassets/AppIcon.appiconset/Contents.json](/ios/Evenly/Images.xcassets/AppIcon.appiconset/Contents.json) | JSON | -14 | 0 | 0 | -14 |
| [ios/Evenly/Images.xcassets/Contents.json](/ios/Evenly/Images.xcassets/Contents.json) | JSON | -6 | 0 | -1 | -7 |
| [ios/Evenly/Images.xcassets/SplashScreen.imageset/Contents.json](/ios/Evenly/Images.xcassets/SplashScreen.imageset/Contents.json) | JSON | -21 | 0 | 0 | -21 |
| [ios/Evenly/Images.xcassets/SplashScreenBackground.imageset/Contents.json](/ios/Evenly/Images.xcassets/SplashScreenBackground.imageset/Contents.json) | JSON | -21 | 0 | 0 | -21 |
| [ios/Evenly/SplashScreen.storyboard](/ios/Evenly/SplashScreen.storyboard) | XML | -51 | 0 | 0 | -51 |
| [ios/Evenly/main.m](/ios/Evenly/main.m) | Objective-C | -7 | 0 | -4 | -11 |
| [ios/Evenly/noop-file.swift](/ios/Evenly/noop-file.swift) | Swift | 0 | -4 | -1 | -5 |
| [navigation/index.tsx](/navigation/index.tsx) | TypeScript JSX | -33 | 0 | -10 | -43 |
| [package-lock.json](/package-lock.json) | JSON | -30,838 | 0 | -1 | -30,839 |
| [package.json](/package.json) | JSON | 1 | 0 | 0 | 1 |
| [provider/AuthProvider.tsx](/provider/AuthProvider.tsx) | TypeScript JSX | 3 | 0 | 0 | 3 |
| [provider/FollowProvider.tsx](/provider/FollowProvider.tsx) | TypeScript JSX | 5 | 0 | 1 | 6 |
| [provider/LocationProvider.tsx](/provider/LocationProvider.tsx) | TypeScript JSX | 11 | 0 | 1 | 12 |
| [screens/Conversation.tsx](/screens/Conversation.tsx) | TypeScript JSX | 26 | 0 | 4 | 30 |
| [screens/Event.tsx](/screens/Event.tsx) | TypeScript JSX | -67 | 1 | -5 | -71 |
| [screens/Filters.tsx](/screens/Filters.tsx) | TypeScript JSX | 10 | 0 | 0 | 10 |
| [screens/MyEventsWishlist.tsx](/screens/MyEventsWishlist.tsx) | TypeScript JSX | 6 | 0 | 1 | 7 |
| [screens/Place.tsx](/screens/Place.tsx) | TypeScript JSX | 59 | 0 | 3 | 62 |
| [screens/Search.tsx](/screens/Search.tsx) | TypeScript JSX | -1 | 0 | -1 | -2 |
| [screens/SignInOut.tsx](/screens/SignInOut.tsx) | TypeScript JSX | 16 | 0 | 1 | 17 |
| [screens/SignUp.tsx](/screens/SignUp.tsx) | TypeScript JSX | 57 | -17 | 5 | 45 |
| [screens/User.tsx](/screens/User.tsx) | TypeScript JSX | 169 | 0 | 8 | 177 |
| [services/auth/getGoogleUserWithToken.ts](/services/auth/getGoogleUserWithToken.ts) | TypeScript | 16 | 0 | 3 | 19 |
| [services/auth/getUserWithToken.ts](/services/auth/getUserWithToken.ts) | TypeScript | 28 | 0 | 6 | 34 |
| [services/auth/google.ts](/services/auth/google.ts) | TypeScript | 9 | 0 | 1 | 10 |
| [services/axios.ts](/services/axios.ts) | TypeScript | 0 | 1 | 0 | 1 |
| [services/events/getClubsEvents.ts](/services/events/getClubsEvents.ts) | TypeScript | -1 | 0 | -1 | -2 |
| [services/events/getConcerts.ts](/services/events/getConcerts.ts) | TypeScript | -1 | 0 | -1 | -2 |
| [services/place/getBars.ts](/services/place/getBars.ts) | TypeScript | 17 | 0 | 2 | 19 |
| [services/types.ts](/services/types.ts) | TypeScript | 3 | 0 | 1 | 4 |
| [services/user/getUserById.ts](/services/user/getUserById.ts) | TypeScript | 12 | 0 | 3 | 15 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details