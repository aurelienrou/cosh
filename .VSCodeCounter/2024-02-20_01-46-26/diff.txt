Date : 2024-02-20 01:46:26
Directory : /Users/aurelien/Documents/Developpement/cosh
Total : 103 files,  -30149 codes, -242 comments, -17 blanks, all -30408 lines

Languages
+--------------------+------------+------------+------------+------------+------------+
| language           | files      | code       | comment    | blank      | total      |
+--------------------+------------+------------+------------+------------+------------+
| TypeScript JSX     |         45 |      1,146 |        -58 |         78 |      1,166 |
| TypeScript         |         11 |         62 |         24 |         14 |        100 |
| JSON with Comments |          2 |          4 |          0 |          0 |          4 |
| Swift              |          2 |          0 |          0 |          0 |          0 |
| Objective-C++      |          2 |          0 |          0 |          0 |          0 |
| Objective-C        |          2 |          0 |          0 |          0 |          0 |
| C++                |          4 |          0 |          0 |          0 |          0 |
| Properties         |          2 |        -21 |        -33 |        -14 |        -68 |
| Batch              |          1 |        -41 |        -29 |        -22 |        -92 |
| XML                |         14 |        -93 |        -23 |         -5 |       -121 |
| Groovy             |          3 |       -147 |        -76 |        -35 |       -258 |
| Java               |          4 |       -167 |        -47 |        -32 |       -246 |
| JSON               |         11 |    -30,892 |          0 |         -1 |    -30,893 |
+--------------------+------------+------------+------------+------------+------------+

Directories
+-----------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                                                                  | files      | code       | comment    | blank      | total      |
+-----------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                                                                     |        103 |    -30,149 |       -242 |        -17 |    -30,408 |
| . (Files)                                                                                                             |          4 |    -30,833 |          0 |         -1 |    -30,834 |
| android                                                                                                               |         21 |       -529 |       -208 |       -109 |       -846 |
| android (Files)                                                                                                       |          4 |       -108 |        -68 |        -45 |       -221 |
| android/app                                                                                                           |         16 |       -415 |       -140 |        -63 |       -618 |
| android/app (Files)                                                                                                   |          2 |       -150 |        -70 |        -25 |       -245 |
| android/app/src                                                                                                       |         14 |       -265 |        -70 |        -38 |       -373 |
| android/app/src/debug                                                                                                 |          2 |        -63 |        -12 |         -9 |        -84 |
| android/app/src/debug (Files)                                                                                         |          1 |         -5 |          0 |         -3 |         -8 |
| android/app/src/debug/java                                                                                            |          1 |        -58 |        -12 |         -6 |        -76 |
| android/app/src/debug/java/com                                                                                        |          1 |        -58 |        -12 |         -6 |        -76 |
| android/app/src/debug/java/com/evenly                                                                                 |          1 |        -58 |        -12 |         -6 |        -76 |
| android/app/src/main                                                                                                  |         11 |       -195 |        -47 |        -26 |       -268 |
| android/app/src/main (Files)                                                                                          |          1 |        -40 |          0 |          0 |        -40 |
| android/app/src/main/java                                                                                             |          2 |       -102 |        -24 |        -23 |       -149 |
| android/app/src/main/java/com                                                                                         |          2 |       -102 |        -24 |        -23 |       -149 |
| android/app/src/main/java/com/evenly                                                                                  |          2 |       -102 |        -24 |        -23 |       -149 |
| android/app/src/main/res                                                                                              |          8 |        -53 |        -23 |         -3 |        -79 |
| android/app/src/main/res/drawable                                                                                     |          2 |        -14 |        -23 |         -3 |        -40 |
| android/app/src/main/res/mipmap-anydpi-v26                                                                            |          2 |        -10 |          0 |          0 |        -10 |
| android/app/src/main/res/values                                                                                       |          3 |        -28 |          0 |          0 |        -28 |
| android/app/src/main/res/values-night                                                                                 |          1 |         -1 |          0 |          0 |         -1 |
| android/app/src/release                                                                                               |          1 |         -7 |        -11 |         -3 |        -21 |
| android/app/src/release/java                                                                                          |          1 |         -7 |        -11 |         -3 |        -21 |
| android/app/src/release/java/com                                                                                      |          1 |         -7 |        -11 |         -3 |        -21 |
| android/app/src/release/java/com/evenly                                                                               |          1 |         -7 |        -11 |         -3 |        -21 |
| android/gradle                                                                                                        |          1 |         -6 |          0 |         -1 |         -7 |
| android/gradle/wrapper                                                                                                |          1 |         -6 |          0 |         -1 |         -7 |
| assets                                                                                                                |          2 |          5 |          0 |          1 |          6 |
| assets/icons                                                                                                          |          2 |          5 |          0 |          1 |          6 |
| components                                                                                                            |         32 |        885 |        -42 |         70 |        913 |
| components/BlockEvents                                                                                                |          2 |          0 |          0 |          0 |          0 |
| components/BlockToFollow                                                                                              |          1 |         -2 |         -1 |          0 |         -3 |
| components/Blocks                                                                                                     |          1 |          4 |          0 |          0 |          4 |
| components/Button                                                                                                     |          1 |          8 |          0 |          1 |          9 |
| components/Checkbox                                                                                                   |          1 |          1 |          0 |          0 |          1 |
| components/Description                                                                                                |          1 |         59 |          0 |          8 |         67 |
| components/DiscoverTabView                                                                                            |          7 |        109 |        -13 |          2 |         98 |
| components/DiscoverTabView (Files)                                                                                    |          1 |          3 |          0 |          0 |          3 |
| components/DiscoverTabView/screens                                                                                    |          6 |        106 |        -13 |          2 |         95 |
| components/Errors                                                                                                     |          1 |         32 |          0 |          4 |         36 |
| components/Filters                                                                                                    |          4 |        183 |          1 |         18 |        202 |
| components/Headers                                                                                                    |          2 |         27 |          0 |          4 |         31 |
| components/Lists                                                                                                      |          1 |          4 |          0 |          0 |          4 |
| components/Loader                                                                                                     |          1 |         86 |          0 |          7 |         93 |
| components/LocationMap                                                                                                |          1 |         61 |          0 |          7 |         68 |
| components/Profil                                                                                                     |          1 |        127 |          0 |          6 |        133 |
| components/SearchMap                                                                                                  |          2 |        127 |          2 |          7 |        136 |
| components/Section                                                                                                    |          1 |          0 |          0 |          1 |          1 |
| components/SignInOut                                                                                                  |          3 |         60 |        -31 |          4 |         33 |
| components/SwipeCards                                                                                                 |          1 |         -1 |          0 |          1 |          0 |
| hooks                                                                                                                 |          2 |        -21 |         23 |          0 |          2 |
| ios                                                                                                                   |         20 |          0 |          0 |          0 |          0 |
| ios/Closy                                                                                                             |         10 |        165 |         16 |         23 |        204 |
| ios/Closy (Files)                                                                                                     |          6 |        103 |         16 |         22 |        141 |
| ios/Closy/Images.xcassets                                                                                             |          4 |         62 |          0 |          1 |         63 |
| ios/Closy/Images.xcassets (Files)                                                                                     |          1 |          6 |          0 |          1 |          7 |
| ios/Closy/Images.xcassets/AppIcon.appiconset                                                                          |          1 |         14 |          0 |          0 |         14 |
| ios/Closy/Images.xcassets/SplashScreen.imageset                                                                       |          1 |         21 |          0 |          0 |         21 |
| ios/Closy/Images.xcassets/SplashScreenBackground.imageset                                                             |          1 |         21 |          0 |          0 |         21 |
| ios/Evenly                                                                                                            |         10 |       -165 |        -16 |        -23 |       -204 |
| ios/Evenly (Files)                                                                                                    |          6 |       -103 |        -16 |        -22 |       -141 |
| ios/Evenly/Images.xcassets                                                                                            |          4 |        -62 |          0 |         -1 |        -63 |
| ios/Evenly/Images.xcassets (Files)                                                                                    |          1 |         -6 |          0 |         -1 |         -7 |
| ios/Evenly/Images.xcassets/AppIcon.appiconset                                                                         |          1 |        -14 |          0 |          0 |        -14 |
| ios/Evenly/Images.xcassets/SplashScreen.imageset                                                                      |          1 |        -21 |          0 |          0 |        -21 |
| ios/Evenly/Images.xcassets/SplashScreenBackground.imageset                                                            |          1 |        -21 |          0 |          0 |        -21 |
| navigation                                                                                                            |          1 |        -33 |          0 |        -10 |        -43 |
| provider                                                                                                              |          3 |         19 |          0 |          2 |         21 |
| screens                                                                                                               |          9 |        275 |        -16 |         16 |        275 |
| services                                                                                                              |          9 |         83 |          1 |         14 |         98 |
| services (Files)                                                                                                      |          2 |          3 |          1 |          1 |          5 |
| services/auth                                                                                                         |          3 |         53 |          0 |         10 |         63 |
| services/events                                                                                                       |          2 |         -2 |          0 |         -2 |         -4 |
| services/place                                                                                                        |          1 |         17 |          0 |          2 |         19 |
| services/user                                                                                                         |          1 |         12 |          0 |          3 |         15 |
+-----------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+-----------------------------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+
| filename                                                                                                              | language           | code       | comment    | blank      | total      |
+-----------------------------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+
| /Users/aurelien/Documents/Developpement/cosh/android/app/build.gradle                                                 | Groovy             |        -95 |        -70 |        -25 |       -190 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/google-services.json                                         | JSON               |        -55 |          0 |          0 |        -55 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/debug/AndroidManifest.xml                                | XML                |         -5 |          0 |         -3 |         -8 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/debug/java/com/evenly/ReactNativeFlipper.java            | Java               |        -58 |        -12 |         -6 |        -76 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/AndroidManifest.xml                                 | XML                |        -40 |          0 |          0 |        -40 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/java/com/evenly/MainActivity.java                   | Java               |        -36 |        -21 |         -9 |        -66 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/java/com/evenly/MainApplication.java                | Java               |        -66 |         -3 |        -14 |        -83 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/res/drawable/rn_edit_text_material.xml              | XML                |        -11 |        -23 |         -3 |        -37 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/res/drawable/splashscreen.xml                       | XML                |         -3 |          0 |          0 |         -3 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/res/mipmap-anydpi-v26/ic_launcher.xml               | XML                |         -5 |          0 |          0 |         -5 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/res/mipmap-anydpi-v26/ic_launcher_round.xml         | XML                |         -5 |          0 |          0 |         -5 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/res/values-night/colors.xml                         | XML                |         -1 |          0 |          0 |         -1 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/res/values/colors.xml                               | XML                |         -6 |          0 |          0 |         -6 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/res/values/strings.xml                              | XML                |         -5 |          0 |          0 |         -5 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/main/res/values/styles.xml                               | XML                |        -17 |          0 |          0 |        -17 |
| /Users/aurelien/Documents/Developpement/cosh/android/app/src/release/java/com/evenly/ReactNativeFlipper.java          | Java               |         -7 |        -11 |         -3 |        -21 |
| /Users/aurelien/Documents/Developpement/cosh/android/build.gradle                                                     | Groovy             |        -45 |         -6 |         -6 |        -57 |
| /Users/aurelien/Documents/Developpement/cosh/android/gradle.properties                                                | Properties         |        -15 |        -33 |        -13 |        -61 |
| /Users/aurelien/Documents/Developpement/cosh/android/gradle/wrapper/gradle-wrapper.properties                         | Properties         |         -6 |          0 |         -1 |         -7 |
| /Users/aurelien/Documents/Developpement/cosh/android/gradlew.bat                                                      | Batch              |        -41 |        -29 |        -22 |        -92 |
| /Users/aurelien/Documents/Developpement/cosh/android/settings.gradle                                                  | Groovy             |         -7 |          0 |         -4 |        -11 |
| /Users/aurelien/Documents/Developpement/cosh/app.json                                                                 | JSON with Comments |         -5 |          0 |          0 |         -5 |
| /Users/aurelien/Documents/Developpement/cosh/assets/icons/nearme.svg                                                  | XML                |          1 |          0 |          0 |          1 |
| /Users/aurelien/Documents/Developpement/cosh/assets/icons/user-rounded.svg                                            | XML                |          4 |          0 |          1 |          5 |
| /Users/aurelien/Documents/Developpement/cosh/components/BlockEvents/BlockEvent.tsx                                    | TypeScript JSX     |          1 |          0 |          0 |          1 |
| /Users/aurelien/Documents/Developpement/cosh/components/BlockEvents/BlockEvents.tsx                                   | TypeScript JSX     |         -1 |          0 |          0 |         -1 |
| /Users/aurelien/Documents/Developpement/cosh/components/BlockToFollow/BlockToFollow.tsx                               | TypeScript JSX     |         -2 |         -1 |          0 |         -3 |
| /Users/aurelien/Documents/Developpement/cosh/components/Blocks/HeaderBackAndFollow.tsx                                | TypeScript JSX     |          4 |          0 |          0 |          4 |
| /Users/aurelien/Documents/Developpement/cosh/components/Button/FollowButton.tsx                                       | TypeScript JSX     |          8 |          0 |          1 |          9 |
| /Users/aurelien/Documents/Developpement/cosh/components/Checkbox/SelectorLine.tsx                                     | TypeScript JSX     |          1 |          0 |          0 |          1 |
| /Users/aurelien/Documents/Developpement/cosh/components/Description/Description.tsx                                   | TypeScript JSX     |         59 |          0 |          8 |         67 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/DiscoverTabView.tsx                           | TypeScript JSX     |          3 |          0 |          0 |          3 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/screens/All.tsx                               | TypeScript JSX     |         23 |        -13 |         -1 |          9 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/screens/Bars.tsx                              | TypeScript JSX     |         46 |          0 |          3 |         49 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/screens/Clubs.tsx                             | TypeScript JSX     |          7 |          0 |          0 |          7 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/screens/Concerts.tsx                          | TypeScript JSX     |          7 |          0 |          0 |          7 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/screens/ThisWeek.tsx                          | TypeScript JSX     |          7 |          0 |          0 |          7 |
| /Users/aurelien/Documents/Developpement/cosh/components/DiscoverTabView/screens/Today.tsx                             | TypeScript JSX     |         16 |          0 |          0 |         16 |
| /Users/aurelien/Documents/Developpement/cosh/components/Errors/MainMessage.tsx                                        | TypeScript JSX     |         32 |          0 |          4 |         36 |
| /Users/aurelien/Documents/Developpement/cosh/components/Filters/BarsFilters.tsx                                       | TypeScript JSX     |        164 |          0 |         20 |        184 |
| /Users/aurelien/Documents/Developpement/cosh/components/Filters/ClubsFilters.tsx                                      | TypeScript JSX     |         18 |          1 |         -1 |         18 |
| /Users/aurelien/Documents/Developpement/cosh/components/Filters/ConcertsFilters.tsx                                   | TypeScript JSX     |          2 |          0 |         -1 |          1 |
| /Users/aurelien/Documents/Developpement/cosh/components/Filters/FiltersButtons.tsx                                    | TypeScript JSX     |         -1 |          0 |          0 |         -1 |
| /Users/aurelien/Documents/Developpement/cosh/components/Headers/MainHeader.tsx                                        | TypeScript JSX     |         31 |          0 |          4 |         35 |
| /Users/aurelien/Documents/Developpement/cosh/components/Headers/PlaceOrArtistHeader.tsx                               | TypeScript JSX     |         -4 |          0 |          0 |         -4 |
| /Users/aurelien/Documents/Developpement/cosh/components/Lists/EventsByDateList.tsx                                    | TypeScript JSX     |          4 |          0 |          0 |          4 |
| /Users/aurelien/Documents/Developpement/cosh/components/Loader/MapLoader.tsx                                          | TypeScript JSX     |         86 |          0 |          7 |         93 |
| /Users/aurelien/Documents/Developpement/cosh/components/LocationMap/LocationMap.tsx                                   | TypeScript JSX     |         61 |          0 |          7 |         68 |
| /Users/aurelien/Documents/Developpement/cosh/components/Profil/Profil.tsx                                             | TypeScript JSX     |        127 |          0 |          6 |        133 |
| /Users/aurelien/Documents/Developpement/cosh/components/SearchMap/Map.tsx                                             | TypeScript JSX     |         86 |          2 |          3 |         91 |
| /Users/aurelien/Documents/Developpement/cosh/components/SearchMap/MyLocation.tsx                                      | TypeScript JSX     |         41 |          0 |          4 |         45 |
| /Users/aurelien/Documents/Developpement/cosh/components/Section/SectionHeader.tsx                                     | TypeScript JSX     |          0 |          0 |          1 |          1 |
| /Users/aurelien/Documents/Developpement/cosh/components/SignInOut/GoogleLoginButton.tsx                               | TypeScript JSX     |         19 |        -31 |         -1 |        -13 |
| /Users/aurelien/Documents/Developpement/cosh/components/SignInOut/LogIn.tsx                                           | TypeScript JSX     |         40 |          0 |          6 |         46 |
| /Users/aurelien/Documents/Developpement/cosh/components/SignInOut/Register.tsx                                        | TypeScript JSX     |          1 |          0 |         -1 |          0 |
| /Users/aurelien/Documents/Developpement/cosh/components/SwipeCards/SwipeCards.tsx                                     | TypeScript JSX     |         -1 |          0 |          1 |          0 |
| /Users/aurelien/Documents/Developpement/cosh/eas.json                                                                 | JSON with Comments |          9 |          0 |          0 |          9 |
| /Users/aurelien/Documents/Developpement/cosh/hooks/useAuthState.ts                                                    | TypeScript         |        -23 |         23 |          0 |          0 |
| /Users/aurelien/Documents/Developpement/cosh/hooks/useCachedResources.ts                                              | TypeScript         |          2 |          0 |          0 |          2 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Closy/AppDelegate.h                                                  | C++                |          5 |          0 |          3 |          8 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Closy/AppDelegate.mm                                                 | Objective-C++      |         40 |          9 |         13 |         62 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Closy/Closy-Bridging-Header.h                                        | C++                |          0 |          3 |          1 |          4 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Closy/Images.xcassets/AppIcon.appiconset/Contents.json               | JSON               |         14 |          0 |          0 |         14 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Closy/Images.xcassets/Contents.json                                  | JSON               |          6 |          0 |          1 |          7 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Closy/Images.xcassets/SplashScreen.imageset/Contents.json            | JSON               |         21 |          0 |          0 |         21 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Closy/Images.xcassets/SplashScreenBackground.imageset/Contents.json  | JSON               |         21 |          0 |          0 |         21 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Closy/SplashScreen.storyboard                                        | XML                |         51 |          0 |          0 |         51 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Closy/main.m                                                         | Objective-C        |          7 |          0 |          4 |         11 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Closy/noop-file.swift                                                | Swift              |          0 |          4 |          1 |          5 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/AppDelegate.h                                                 | C++                |         -5 |          0 |         -3 |         -8 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/AppDelegate.mm                                                | Objective-C++      |        -40 |         -9 |        -13 |        -62 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/Evenly-Bridging-Header.h                                      | C++                |          0 |         -3 |         -1 |         -4 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/Images.xcassets/AppIcon.appiconset/Contents.json              | JSON               |        -14 |          0 |          0 |        -14 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/Images.xcassets/Contents.json                                 | JSON               |         -6 |          0 |         -1 |         -7 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/Images.xcassets/SplashScreen.imageset/Contents.json           | JSON               |        -21 |          0 |          0 |        -21 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/Images.xcassets/SplashScreenBackground.imageset/Contents.json | JSON               |        -21 |          0 |          0 |        -21 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/SplashScreen.storyboard                                       | XML                |        -51 |          0 |          0 |        -51 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/main.m                                                        | Objective-C        |         -7 |          0 |         -4 |        -11 |
| /Users/aurelien/Documents/Developpement/cosh/ios/Evenly/noop-file.swift                                               | Swift              |          0 |         -4 |         -1 |         -5 |
| /Users/aurelien/Documents/Developpement/cosh/navigation/index.tsx                                                     | TypeScript JSX     |        -33 |          0 |        -10 |        -43 |
| /Users/aurelien/Documents/Developpement/cosh/package-lock.json                                                        | JSON               |    -30,838 |          0 |         -1 |    -30,839 |
| /Users/aurelien/Documents/Developpement/cosh/package.json                                                             | JSON               |          1 |          0 |          0 |          1 |
| /Users/aurelien/Documents/Developpement/cosh/provider/AuthProvider.tsx                                                | TypeScript JSX     |          3 |          0 |          0 |          3 |
| /Users/aurelien/Documents/Developpement/cosh/provider/FollowProvider.tsx                                              | TypeScript JSX     |          5 |          0 |          1 |          6 |
| /Users/aurelien/Documents/Developpement/cosh/provider/LocationProvider.tsx                                            | TypeScript JSX     |         11 |          0 |          1 |         12 |
| /Users/aurelien/Documents/Developpement/cosh/screens/Conversation.tsx                                                 | TypeScript JSX     |         26 |          0 |          4 |         30 |
| /Users/aurelien/Documents/Developpement/cosh/screens/Event.tsx                                                        | TypeScript JSX     |        -67 |          1 |         -5 |        -71 |
| /Users/aurelien/Documents/Developpement/cosh/screens/Filters.tsx                                                      | TypeScript JSX     |         10 |          0 |          0 |         10 |
| /Users/aurelien/Documents/Developpement/cosh/screens/MyEventsWishlist.tsx                                             | TypeScript JSX     |          6 |          0 |          1 |          7 |
| /Users/aurelien/Documents/Developpement/cosh/screens/Place.tsx                                                        | TypeScript JSX     |         59 |          0 |          3 |         62 |
| /Users/aurelien/Documents/Developpement/cosh/screens/Search.tsx                                                       | TypeScript JSX     |         -1 |          0 |         -1 |         -2 |
| /Users/aurelien/Documents/Developpement/cosh/screens/SignInOut.tsx                                                    | TypeScript JSX     |         16 |          0 |          1 |         17 |
| /Users/aurelien/Documents/Developpement/cosh/screens/SignUp.tsx                                                       | TypeScript JSX     |         57 |        -17 |          5 |         45 |
| /Users/aurelien/Documents/Developpement/cosh/screens/User.tsx                                                         | TypeScript JSX     |        169 |          0 |          8 |        177 |
| /Users/aurelien/Documents/Developpement/cosh/services/auth/getGoogleUserWithToken.ts                                  | TypeScript         |         16 |          0 |          3 |         19 |
| /Users/aurelien/Documents/Developpement/cosh/services/auth/getUserWithToken.ts                                        | TypeScript         |         28 |          0 |          6 |         34 |
| /Users/aurelien/Documents/Developpement/cosh/services/auth/google.ts                                                  | TypeScript         |          9 |          0 |          1 |         10 |
| /Users/aurelien/Documents/Developpement/cosh/services/axios.ts                                                        | TypeScript         |          0 |          1 |          0 |          1 |
| /Users/aurelien/Documents/Developpement/cosh/services/events/getClubsEvents.ts                                        | TypeScript         |         -1 |          0 |         -1 |         -2 |
| /Users/aurelien/Documents/Developpement/cosh/services/events/getConcerts.ts                                           | TypeScript         |         -1 |          0 |         -1 |         -2 |
| /Users/aurelien/Documents/Developpement/cosh/services/place/getBars.ts                                                | TypeScript         |         17 |          0 |          2 |         19 |
| /Users/aurelien/Documents/Developpement/cosh/services/types.ts                                                        | TypeScript         |          3 |          0 |          1 |          4 |
| /Users/aurelien/Documents/Developpement/cosh/services/user/getUserById.ts                                             | TypeScript         |         12 |          0 |          3 |         15 |
| Total                                                                                                                 |                    |    -30,149 |       -242 |        -17 |    -30,408 |
+-----------------------------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+