import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { IEventMinimize, IPlaceMinimize } from '../services/types';

interface MapState {
  data: GeoJSON.FeatureCollection;
  selected: number;
}

const initialState: MapState = {
  data: {
    type: 'FeatureCollection',
    features: [],
  },
  selected: 0,
};

export const mapSlice = createSlice({
  name: 'map',
  initialState,
  reducers: {
    setDataForMap: (
      state,
      action: PayloadAction<(IEventMinimize | IPlaceMinimize)[]>,
    ) => {
      state.data = {
        type: 'FeatureCollection',
        features: action.payload.map((data, index) => {
          return {
            type: 'Feature',
            properties: {
              index,
              ...data,
            },
            geometry: {
              type: 'Point',
              coordinates: [data.location[1], data.location[0]],
            },
          };
        }),
      };
    },
    setSelected: (state, action: PayloadAction<number>) => {
      state.selected = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setDataForMap, setSelected } = mapSlice.actions;

export default mapSlice.reducer;
