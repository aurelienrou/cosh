import React, {
  createContext,
  Dispatch,
  FC,
  SetStateAction,
  useContext,
  useEffect,
  useState,
} from 'react';
import { useAuthContext } from './AuthProvider';
import { useGetFollowings } from '../services/follow/follow';
import { IEventMinimize } from '../services/types';

export interface IFollow {
  id: string;
  type: string;
  follower: string;
  following: IEventMinimize;
}

interface FollowProvider {
  followMap: Record<string, IFollow>;
  setFollowMap: Dispatch<SetStateAction<Record<string, IFollow>>>;
}

export const FollowContext = createContext({} as FollowProvider);
export const FollowProvider: FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [followMap, setFollowMap] = useState({} as Record<string, IFollow>);
  const { isLoggin, user } = useAuthContext();
  const { data } = useGetFollowings(user?.id);

  useEffect(() => {
    if (!isLoggin) {
      setFollowMap({});
    }
  }, [isLoggin]);

  useEffect(() => {
    if (data.length) {
      const res = data.reduce((acc, cur) => {
        return {
          ...acc,
          [cur?.following?.id]: cur,
        };
      }, {});

      setFollowMap(res);
    }
  }, [data]);

  return (
    <FollowContext.Provider
      value={{
        followMap,
        setFollowMap,
      }}
    >
      {children}
    </FollowContext.Provider>
  );
};

export const useFollowContext = () => useContext(FollowContext);
