import React, { memo } from 'react';
import { darkTheme, lightTheme } from './../constants/theme';

// Our context provider will provide this object shape
interface ProvidedValue {
  theme: typeof darkTheme | typeof lightTheme;
  toggleTheme: () => void;
}

const Context = React.createContext<ProvidedValue>({
  theme: darkTheme,
  toggleTheme: () => {
    console.log('ThemeProvider is not rendered!');
  },
});

interface Props {
  initial: typeof darkTheme | typeof lightTheme;
  children?: React.ReactNode;
}

const ThemeProviderBeforeMemo = (props: Props) => {
  const [theme, setTheme] = React.useState<
    typeof darkTheme | typeof lightTheme
  >(props.initial);

  const ToggleThemeCallback = React.useCallback(() => {
    setTheme((currentTheme) => {
      if (currentTheme === darkTheme) {
        return lightTheme;
      }
      if (currentTheme === lightTheme) {
        return darkTheme;
      }
      return currentTheme;
    });
  }, []);

  const MemoizedValue = React.useMemo(() => {
    const value: ProvidedValue = {
      theme,
      toggleTheme: ToggleThemeCallback,
    };
    return value;
  }, [theme, ToggleThemeCallback]);

  return (
    <Context.Provider value={MemoizedValue}>{props.children}</Context.Provider>
  );
};

export const ThemeProvider = memo(ThemeProviderBeforeMemo);

export const useTheme = () => React.useContext(Context);
