import React, { createContext, useContext } from 'react';
import { IPlace } from '../services/types';
import { useLocationContext } from './LocationProvider';
import { getPlacesNearUser } from '../services/place/getPlaces';
import { useQuery } from '@tanstack/react-query';

interface PlacesProvider {
  bars?: IPlace[];
  clubs: IPlace[];
  clubsIsLoading: boolean;
}

export const PlacesContext = createContext({} as PlacesProvider);

export const PlacesProvider = ({ children }: { children: React.ReactNode }) => {
  const { location } = useLocationContext();
  const { data: clubs, isLoading: clubsIsLoading } = useQuery<IPlace[]>(
    ['placesNearUser'],
    () =>
      getPlacesNearUser(
        location.latitude,
        location.longitude,
        10000,
        JSON.stringify(['NIGHT_CLUB']),
      ),
    {
      initialData: [],
      enabled: Boolean(location.latitude || location.longitude),
    },
  );

  return (
    <PlacesContext.Provider
      value={{
        clubs,
        clubsIsLoading,
      }}
    >
      {children}
    </PlacesContext.Provider>
  );
};

export const usePlacesContext = () => useContext(PlacesContext);
