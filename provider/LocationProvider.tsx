import { useAsyncStorage } from '@react-native-async-storage/async-storage';
import React, {
  createContext,
  Dispatch,
  FC,
  useContext,
  useEffect,
  useState,
} from 'react';
import { AddressGoogle, LatitudeLongitude } from '../services/types';

interface LocationProvider {
  location: LatitudeLongitude;
  setLocation: Dispatch<LatitudeLongitude>;
  setIsOpenAddressEditor: Dispatch<boolean>;
  isOpenAddressEditor: boolean;
  altitude: number;
  setAltitude: Dispatch<number>;
  address?: AddressGoogle;
  setAddress: Dispatch<AddressGoogle>;
  setDistance: Dispatch<number>;
  distance: number;
  locationIsReady: boolean;
}

export const DEFAULT_COORD_FRANCE = {
  latitude: 46,
  longitude: 2,
};

export const LocationContext = createContext({} as LocationProvider);
export const LocationProvider: FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const { setItem: setStorageLocation, getItem: getStorageLocation } =
    useAsyncStorage('@location');
  const { setItem: setStorageAddress, getItem: getStorageAddress } =
    useAsyncStorage('@address');
  const [address, setAddress] = useState<AddressGoogle>({} as AddressGoogle);
  const [location, setLocation] = useState<LatitudeLongitude>(
    {} as LatitudeLongitude,
  );
  const [locationIsReady, setLocationIsReady] = useState(false);

  const [altitude, setAltitude] = useState<number>(3000000);
  const [distance, setDistance] = useState<number>(10000);

  useEffect(() => {
    if (Object.keys(location).length) {
      setStorageLocation(JSON.stringify(location));
    }
  }, [location]);

  useEffect(() => {
    if (Object.keys(address).length) {
      setStorageAddress(JSON.stringify(address));
    }
  }, [address]);

  useEffect(() => {
    (async () => {
      const storageLocation = await getStorageLocation();
      const storageAddress = await getStorageAddress();

      if (storageLocation) {
        setLocation(JSON.parse(storageLocation));
      }
      if (storageAddress) {
        setAddress(JSON.parse(storageAddress));
      }

      setLocationIsReady(true);
    })();
  }, []);

  const [isOpenAddressEditor, setIsOpenAddressEditor] =
    useState<boolean>(false);

  return (
    <LocationContext.Provider
      value={{
        isOpenAddressEditor,
        setIsOpenAddressEditor,
        location,
        setLocation,
        altitude,
        setAltitude,
        address,
        setAddress,
        setDistance,
        distance,
        locationIsReady,
      }}
    >
      {children}
    </LocationContext.Provider>
  );
};

export const useLocationContext = () => useContext(LocationContext);
