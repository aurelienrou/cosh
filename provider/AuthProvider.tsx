import React, { createContext, useContext, useState } from 'react';
import { IUser } from '../services/types';

interface AuthProvider {
  user: IUser;
  setUser: React.Dispatch<React.SetStateAction<IUser>>;
  isLoggin: boolean;
}

export const AuthContext = createContext({} as AuthProvider);
export const AuthProvider = ({ children }: { children: React.ReactNode }) => {
  const [user, setUser] = useState({} as IUser);
  const isLoggin = Boolean(Object.keys(user).length);

  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        isLoggin,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuthContext = () => useContext(AuthContext);
