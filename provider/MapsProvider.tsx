import React, {
  RefObject,
  createContext,
  useContext,
  useRef,
  useState,
} from 'react';
import BottomSheet from '@gorhom/bottom-sheet';
import { SharedValue, useSharedValue } from 'react-native-reanimated';

interface MapsProvider {
  bottomSheetRef: RefObject<BottomSheet>;
  animatedIndex: SharedValue<number>;
  setIsFullyOpen: React.Dispatch<React.SetStateAction<boolean>>;
  isFullyOpen: boolean;
  handleCollapse: () => void;
}

export const MapsContext = createContext({} as MapsProvider);

export const MapsProvider = ({ children }: { children: React.ReactNode }) => {
  const animatedIndex = useSharedValue(1);
  const [isFullyOpen, setIsFullyOpen] = useState(false);
  const bottomSheetRef = useRef<BottomSheet>(null);

  const handleCollapse = () => {
    if (bottomSheetRef.current) {
      bottomSheetRef.current.snapToIndex(0);
    }
  };

  return (
    <MapsContext.Provider
      value={{
        animatedIndex,
        setIsFullyOpen,
        isFullyOpen,
        bottomSheetRef,
        handleCollapse,
      }}
    >
      {children}
    </MapsContext.Provider>
  );
};

export const useMapsContext = () => useContext(MapsContext);
