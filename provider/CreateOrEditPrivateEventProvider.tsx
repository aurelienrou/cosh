import React, {
  createContext,
  Dispatch,
  useContext,
  useEffect,
  useState,
} from 'react';
import { IUser, PrivateEvent } from '../services/types';
import { useAuthContext } from './AuthProvider';

type EventWithoutId = Omit<PrivateEvent, 'id'>;
interface CreateOrEditPrivateEventProvider {
  setEvent: Dispatch<EventWithoutId>;
  event: EventWithoutId;
}

const createInitialEvent = (user: IUser): EventWithoutId => ({
  title: 'événement sans titre',
  description: null,
  cover: null,
  startDate: null,
  endDate: null,
  address: null,

  enableGuestReminders: true,
  showGuestList: true,
  showHostList: true,
  allowGuestPhotoUpload: true,
  showActivityTimestamps: true,
  displayInviteButton: true,
  guests: [],
  owners: [user],
  ownerIds: [user.id],
  rsvpsEnabled: true,
  allowGuestsToShare: true,
  allowGuestsToInviteMutuals: true,
  otherMutualsInvitedCount: 0,
  invitationMessage: null,
  invitesSentCount: 0,
  guestStatusCounts: {
    READY_TO_SEND: 0,
    SENDING: 0,
    SENT: 0,
    SEND_ERROR: 0,
    DELIVERY_ERROR: 0,
    MAYBE: 0,
    GOING: 0,
    DECLINED: 0,
    WAITLIST: 0,
    PENDING_APPROVAL: 0,
    APPROVED: 0,
    WITHDRAWN: 0,
    RESPONDED_TO_FIND_A_TIME: 0,
  },
  isCapped: false,
  guestCount: 0,
  respondedGuestCount: 0,
  invitedGuestCount: 0,
  attendedGuestCount: 0,
  goingGuestCount: 0,
  maybeGuestCount: 0,
  declinedGuestCount: 0,
  waitlistGuestCount: 0,
  withdrawnGuestCount: 0,
  pendingGuestCount: 0,
  approvedGuestCount: 0,
  respondedToFindATimeGuestCount: 0,
  hasGuests: true,
  atCapacity: false,
  status: 'DRAFT',
});

export const Context = createContext({} as CreateOrEditPrivateEventProvider);
export const CreateOrEditPrivateEventProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const { user } = useAuthContext();
  const [event, setEvent] = useState({} as EventWithoutId);

  useEffect(() => {
    if (user) {
      setEvent(createInitialEvent(user));
    }
  }, [user]);

  return (
    <Context.Provider
      value={{
        event,
        setEvent,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useCreateOrEditPrivateEventContext = () => useContext(Context);
