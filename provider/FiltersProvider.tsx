import { BottomSheetModal } from '@gorhom/bottom-sheet';
import React, {
  Dispatch,
  RefObject,
  SetStateAction,
  createContext,
  useContext,
  useRef,
  useState,
} from 'react';

export const EVENT_TYPES = [
  {
    name: 'techno',
    type: ['LIVE_MUSIC_TECHNO', 'DJ_TECHNO'],
  },
  {
    name: 'acid techno',
    type: ['DJ_ACIDTECHNO'],
  },
  {
    name: 'hardcore',
    type: ['DJ_HARDCORE'],
  },
  {
    name: 'hard techno',
    type: ['DJ_HARDTECHNO'],
  },
  {
    name: 'hard groove',
    type: ['DJ_HARDGROOVE'],
  },
  {
    name: 'house',
    type: ['LIVE_MUSIC_HOUSE', 'DJ_HOUSE'],
  },
  {
    name: 'german techno',
    type: ['DJ_GERMANTECHNO'],
  },
  {
    name: 'indus',
    type: ['DJ_INDUS'],
  },
  {
    name: 'ambient',
    type: ['DJ_AMBIENT'],
  },
  {
    name: 'ambient idm',
    type: ['DJ_AMBIENT'],
  },
  {
    name: 'salsa',
    type: ['LIVE_MUSIC_SALSA'],
  },
  {
    name: 'bachata',
    type: ['LIVE_MUSIC_BACHATA'],
  },
  {
    name: 'merengue',
    type: ['LIVE_MUSIC_MERENGUE'],
  },
  {
    name: 'reggaeton',
    type: ['LIVE_MUSIC_REGGAETON'],
  },
  {
    name: 'baile funk',
    type: ['LIVE_MUSIC_BAILEFUNK'],
  },
  {
    name: 'disco house',
    type: ['DJ_DISCOHOUSE'],
  },
  {
    name: 'electro house',
    type: ['DJ_ELECTROHOUSE'],
  },
  {
    name: 'funk',
    type: ['LIVE_MUSIC_FUNK', 'DJ_FUNK'],
  },
  {
    name: 'eurodance',
    type: ['DJ_EURODANCE'],
  },
  {
    name: 'trance',
    type: ['DJ_TRANCE'],
  },
  {
    name: 'pop',
    type: ['LIVE_MUSIC_POP', 'DJ_POP'],
  },
  {
    name: 'synthpop',
    type: ['DJ_SYNTHPOP'],
  },
  {
    name: 'acid house',
    type: ['DJ_ACIDHOUSE'],
  },
  {
    name: 'progressive trance',
    type: ['DJ_PROGRESSIVETRANCE'],
  },
  {
    name: 'psytrance',
    type: ['DJ_PSYTRANCE'],
  },
  {
    name: 'dancehall',
    type: ['DJ_DANCEHALL'],
  },
  {
    name: 'afrobeat',
    type: ['DJ_AFROBEAT'],
  },
  {
    name: 'afro house',
    type: ['DJ_AFROHOUSE'],
  },
  {
    name: 'balearic',
    type: ['DJ_BALEARIC'],
  },
  {
    name: 'electro',
    type: ['LIVE_MUSIC_ELECTRO', 'DJ_ELECTRO'],
  },
  {
    name: 'detroit techno',
    type: ['DJ_DETROITTECHNO'],
  },
  {
    name: 'deep techno',
    type: ['DJ_DEEPTECHNO'],
  },
  {
    name: 'downtempo',
    type: ['DJ_DOWNTEMPO'],
  },
  {
    name: 'soul',
    type: ['LIVE_MUSIC_SOUL', 'DJ_SOUL'],
  },
  {
    name: 'hip hop',
    type: ['LIVE_MUSIC_HIPHOP', 'DJ_HIPHOP'],
  },
  {
    name: 'jazz',
    type: ['LIVE_MUSIC_JAZZ'],
  },
  {
    name: 'rap',
    type: ['LIVE_MUSIC_RAP', 'DJ_RAP'],
  },
  {
    name: 'dub',
    type: ['DJ_DUB'],
  },
  {
    name: 'dub techno',
    type: ['DJ_DUBTECHNO'],
  },
  {
    name: 'drum & bass',
    type: ['DJ_DRUM& BASS'],
  },
  {
    name: 'trap',
    type: ['DJ_TRAP'],
  },
  {
    name: 'dark wave',
    type: ['LIVE_MUSIC_DARKWAVE'],
  },
  {
    name: 'minimal synth',
    type: ['DJ_MINIMALSYNTH'],
  },
  {
    name: 'metal',
    type: ['LIVE_MUSIC_METAL'],
  },
  {
    name: 'mpb',
    type: ['DJ_MPB'],
  },
  {
    name: 'experimental',
    type: ['LIVE_MUSIC_EXPERIMENTAL'],
  },
  {
    name: 'electronica',
    type: ['DJ_ELECTRONICA'],
  },
  {
    name: 'ghettotech',
    type: ['DJ_GHETTOTECH'],
  },
  {
    name: 'melodic house & techno',
    type: ['DJ_MELODICTECHNO'],
  },
  {
    name: 'industrial',
    type: ['DJ_INDUSTRIAL'],
  },
  {
    name: 'gabber',
    type: ['DJ_GABBER'],
  },
  {
    name: 'frenchcore',
    type: ['DJ_FRENCHCORE'],
  },
  {
    name: 'r&b',
    type: ['LIVE_MUSIC_RNB', 'DJ_RNB'],
  },
  {
    name: 'hard trance',
    type: ['DJ_HARDTRANCE'],
  },
  {
    name: 'hardtek',
    type: ['DJ_HARDTEK'],
  },
  {
    name: 'samba',
    type: ['DJ_SAMBA'],
  },
  {
    name: 'brazilian',
    type: ['DJ_BRAZILIAN'],
  },
  {
    name: 'disco',
    type: ['DJ_DISCO'],
  },
  {
    name: 'dark psytrance',
    type: ['DJ_DARKPSYTRANCE'],
  },
  {
    name: 'progressive psytrance',
    type: ['DJ_PROG RESSIVEPSYTRANCE'],
  },
  {
    name: 'deep house',
    type: ['DJ_DEEPHOUSE'],
  },
  {
    name: 'jersey club',
    type: ['DJ_JERSEYCLUB'],
  },
  {
    name: 'grime',
    type: ['DJ_GRIME'],
  },
  {
    name: 'dance',
    type: ['LIVE_MUSIC_DANCE', 'DJ_DANCE'],
  },
  {
    name: 'acidcore',
    type: ['DJ_ACIDCORE'],
  },
  {
    name: 'micro house',
    type: ['DJ_MICROHOUSE'],
  },
  {
    name: 'minimal house',
    type: ['DJ_MINIMALHOUSE'],
  },
  {
    name: 'deep tech',
    type: ['DJ_DEEPTECHNO'],
  },
  {
    name: 'indie dance',
    type: ['DJ_INDIEDANCE'],
  },
  {
    name: 'minimal techno',
    type: ['DJ_MINIMALTECHNO'],
  },
  {
    name: 'industrial techno',
    type: ['DJ_INDUSTRIALTECHNO'],
  },
  {
    name: 'tech house',
    type: ['DJ_TECHHOUSE'],
  },
  {
    name: 'italo disco',
    type: ['DJ_ITALODISCO'],
  },
  {
    name: 'progressive house',
    type: ['DJ_PROGRESSIVEHOUSE'],
  },
  {
    name: 'tribal house',
    type: ['DJ_TRIBALHOUSE'],
  },
  {
    name: 'new wave',
    type: ['DJ_NEWWAVE'],
  },
  {
    name: 'club',
    type: ['DJ_CLUB'],
  },
  {
    name: 'drill',
    type: ['DJ_DRILL'],
  },
  {
    name: 'uk garage',
    type: ['DJ_UKGARAGE'],
  },
  {
    name: 'new rave',
    type: ['DJ_NEWRAVE'],
  },
  {
    name: 'world music',
    type: ['DJ_WORLDMUSIC'],
  },
  {
    name: 'post-punk',
    type: ['LIVE_MUSIC_POSTPUNK'],
  },
  {
    name: 'punk',
    type: ['LIVE_MUSIC_PUNK'],
  },
  {
    name: 'bass',
    type: ['DJ_BASS'],
  },
  {
    name: 'pop rock',
    type: ['LIVE_MUSIC_POPROCK'],
  },
  {
    name: 'country',
    type: ['DJ_COUNTRY'],
  },
  {
    name: 'breakbeat',
    type: ['DJ_BREAKBEAT'],
  },
  {
    name: 'psychedelic rock',
    type: ['DJ_PSYCHEDELICROCK'],
  },
  {
    name: 'neorave',
    type: ['DJ_NEORAVE'],
  },
  {
    name: 'reggae',
    type: ['DJ_REGGAE'],
  },
  {
    name: 'dubstep',
    type: ['DJ_DUBSTEP'],
  },
  {
    name: 'ebm',
    type: ['DJ_EBM'],
  },
  {
    name: 'chicago house',
    type: ['DJ_CHICAGOHOUSE'],
  },
  {
    name: 'breakcore',
    type: ['DJ_BREAKCORE'],
  },
  {
    name: 'alternative dance',
    type: ['DJ_ALTERNATIVEDANCE'],
  },
  {
    name: 'rock',
    type: ['LIVE_MUSIC_ROCK'],
  },
  {
    name: 'hardstyle',
    type: ['DJ_HARDSTYLE'],
  },
  {
    name: 'blues',
    type: ['LIVE_MUSIC_BLUES'],
  },
  {
    name: 'jungle',
    type: ['DJ_JUNGLE'],
  },
  {
    name: 'garage',
    type: ['DJ_GARAGE'],
  },
  {
    name: 'edm',
    type: ['DJ_EDM'],
  },
  {
    name: 'axé',
    type: ['DJ_AXÉ'],
  },
  {
    name: 'synthwave',
    type: ['DJ_SYNTHWAVE'],
  },
  {
    name: 'goth',
    type: ['DJ_GOTH'],
  },
  {
    name: 'industrial metal',
    type: ['DJ_INDUSTRIALMETAL'],
  },
  {
    name: 'indie rock',
    type: ['LIVE_MUSIC_INDIEROCK'],
  },
  {
    name: 'k-pop',
    type: ['DJ_KPOP'],
  },
  {
    name: 'vaporwave',
    type: ['DJ_VAPORWAVE'],
  },
];

export const BAR_TYPES = [
  {
    name: 'biere',
    type: 'BEER_BAR',
  },
  {
    name: 'brasserie',
    type: 'BRASSERIE',
  },
  {
    name: 'cocktail',
    type: 'COCKTAIL_BAR',
  },
  {
    name: 'tapas',
    type: 'TAPAS_BAR',
  },
  {
    name: 'asiatique',
    type: 'ASIA_BAR',
  },
  {
    name: 'latino',
    type: 'LATINO_BAR',
  },
  {
    name: 'dansant',
    type: 'NIGHT_BAR',
  },
  {
    name: 'club',
    type: 'NIGHT_CLUB',
  },
  {
    name: 'spectacles',
    type: 'LIVE_BAR',
  },
  {
    name: 'pub',
    type: 'PUB',
  },
  {
    name: 'sportif',
    type: 'SPORTS_BAR',
  },
  {
    name: 'cafe concert',
    type: 'LIVE_MUSIC_CAFE',
  },
  {
    name: 'de quartier',
    type: 'NEIGHBORHOOD_BAR',
  },
  {
    name: 'etudiant',
    type: 'STUDENT_BAR',
  },
  {
    name: 'italien',
    type: 'ITALIAN_BAR',
  },
  {
    name: 'cafe',
    type: '',
  },
  {
    name: 'chic',
    type: 'LOUNGE',
  },
  {
    name: 'vin',
    type: 'WINE_BAR',
  },
  {
    name: 'alternatif',
    type: 'ALT_BAR',
  },
  {
    name: 'espagnol',
    type: 'SPANISH_BAR',
  },
  {
    name: 'salon de the',
    type: 'TEA_ROOM',
  },
  {
    name: 'spiritueux',
    type: 'ALCOOL_BAR',
  },
  {
    name: 'dhotel',
    type: 'HOTEL_BAR',
  },
  {
    name: 'rooftop',
    type: 'ROOFTOP_BAR',
  },
  {
    name: 'sympa pour travailler',
    type: 'WORKING_CAFE',
  },
  {
    name: 'cache',
    type: 'HIDDEN_BAR',
  },
  {
    name: 'hip hop',
    type: 'HIP_HOP_BAR',
  },
  {
    name: 'brasserie artisanale',
    type: 'ARTISANALE_BAR',
  },
  {
    name: 'karaoke',
    type: 'KARAOKE',
  },
  {
    name: 'gaming',
    type: 'GAME_BAR',
  },
  {
    name: 'jeux',
    type: 'GAME_BAR',
  },
  {
    name: 'irish pub',
    type: 'IRISH_PUB',
  },
  {
    name: 'insolite',
    type: 'INSOLITE_BAR',
  },
  {
    name: 'guinguette',
    type: 'GUINGUETTE',
  },
  {
    name: 'speakeasy',
    type: 'SPEAKEASY',
  },
  {
    name: 'musique',
    type: 'MUSIC_BAR',
  },
  {
    name: 'ecossais',
    type: 'SCOTT_BAR',
  },
  {
    name: 'coffee shop',
    type: 'COFFEE_SHOP',
  },
  {
    name: 'shooters',
    type: 'SHOT_BAR',
  },
  {
    name: 'jazz',
    type: 'JAZZ_CLUB',
  },
  {
    name: 'chicha',
    type: 'HOOKAH_LOUNGE',
  },
  {
    name: 'comedy club',
    type: 'COMEDY_CLUB',
  },
  {
    name: 'lgbtqi',
    type: 'GAY_BAR',
  },
  {
    name: 'peniche',
    type: 'BOAT_BAR',
  },
  {
    name: 'beer hall',
    type: 'BEER_BAR',
  },
  {
    name: 'billards',
    type: 'POOL_BAR',
  },
  {
    name: 'caveau',
    type: 'CAVE_BAR',
  },
  {
    name: 'rhum',
    type: 'RHUM_BAR',
  },
  {
    name: 'indie',
    type: 'INDIE_BAR',
  },
  {
    name: 'cidre',
    type: 'CIDRE_BAR',
  },
  {
    name: 'associatif',
    type: 'ASSO_BAR',
  },
  {
    name: 'pmu',
    type: 'PMU',
  },
  {
    name: 'soiree privee',
    type: 'NIGHT_CLUB',
  },
];

export const MIN_VALUE = 0;
export const MAX_VALUE = 50;

interface FiltersProvider {
  ref: RefObject<BottomSheetModal>;
  currentIndex: number;
  setCurrentIndex: Dispatch<SetStateAction<number>>;
  params: any;
  setParams: Dispatch<SetStateAction<any>>;
}
export const FiltersContext = createContext({} as FiltersProvider);
export const FiltersProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const ref = useRef<BottomSheetModal>(null);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [params, setParams] = useState({});

  return (
    <FiltersContext.Provider
      value={{
        ref,
        setCurrentIndex,
        currentIndex,
        params,
        setParams,
      }}
    >
      {children}
    </FiltersContext.Provider>
  );
};

export const useFiltersContext = () => useContext(FiltersContext);
