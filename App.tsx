import { StatusBar } from 'expo-status-bar';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { store } from './store';
import { Provider } from 'react-redux';

import { ThemeProvider } from 'styled-components';
import React from 'react';
import Navigation from './navigation';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { AuthProvider } from './provider/AuthProvider';
import { LocationProvider } from './provider/LocationProvider';
import { ThemeProvider as MainThemeProvider } from './provider/ThemeProvider';
import dayjs from 'dayjs';
import * as SplashScreen from 'expo-splash-screen';

import customParseFormat from 'dayjs/plugin/customParseFormat';
import { useDarkMode } from './hooks/useDarkMode';
import { darkTheme, lightTheme } from './constants/theme';
import { MapsProvider } from './provider/MapsProvider';
import { FollowProvider } from './provider/FollowProvider';
import { FiltersProvider } from './provider/FiltersProvider';
import { BottomSheetModalProvider } from '@gorhom/bottom-sheet';
import Mapbox from '@rnmapbox/maps';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import analytics from '@react-native-firebase/analytics';
import { CreateOrEditPrivateEventProvider } from './provider/CreateOrEditPrivateEventProvider';

GoogleSignin.configure({
  webClientId:
    '1076478989469-1por057jtvprmausqubjh8c4o6qf7o5g.apps.googleusercontent.com',
  iosClientId:
    '1076478989469-1por057jtvprmausqubjh8c4o6qf7o5g.apps.googleusercontent.com',
  offlineAccess: true,
  scopes: ['profile', 'email'],
});

dayjs.extend(customParseFormat);
SplashScreen.preventAutoHideAsync();

Mapbox.setAccessToken(
  'pk.eyJ1IjoiYXVyZWxpZW5yb3UiLCJhIjoiY2xvNXB3NjA5MGRkOTJxcWR3dDZmbWlwaSJ9.L_8vgJwdQmriVL_ZsKps8w',
);

export default function App() {
  const [theme] = useDarkMode();
  const themeMode = theme === 'light' ? lightTheme : darkTheme;

  const queryClient = new QueryClient();

  analytics().logAppOpen();

  return (
    <Provider store={store}>
      <SafeAreaProvider>
        <GestureHandlerRootView style={{ flex: 1 }}>
          <QueryClientProvider client={queryClient}>
            <ThemeProvider theme={themeMode}>
              <MainThemeProvider initial={lightTheme}>
                <MapsProvider>
                  <AuthProvider>
                    <FiltersProvider>
                      <FollowProvider>
                        <LocationProvider>
                          <CreateOrEditPrivateEventProvider>
                            <BottomSheetModalProvider>
                              <Navigation />
                              <StatusBar />
                            </BottomSheetModalProvider>
                          </CreateOrEditPrivateEventProvider>
                        </LocationProvider>
                      </FollowProvider>
                    </FiltersProvider>
                  </AuthProvider>
                </MapsProvider>
              </MainThemeProvider>
            </ThemeProvider>
          </QueryClientProvider>
        </GestureHandlerRootView>
      </SafeAreaProvider>
    </Provider>
  );
}
//÷÷÷¬
