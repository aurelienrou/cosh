import dayjs from 'dayjs';

const DATE_FORMAT = 'MMM, D à H:mm';
export const DATE_FORMAT_WITH_DAY = 'dddd D MMM à H:mm';

export const formateDate = (date: Date, shape = DATE_FORMAT) =>
  dayjs(date).format(shape).replace('.', '');

export const isBeforeXHour = (date: Date, hour: number) =>
  dayjs(date).isBefore(dayjs().hour(hour).minute(0).second(0));

export const daysTranslate = {
  mon: 'lundi',
  tue: 'mardi',
  wed: 'mercredi',
  thu: 'jeudi',
  fri: 'vendredi',
  sat: 'samedi',
  sun: 'dimanche',
} as const;
