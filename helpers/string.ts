export const toCamelCase = (str: string): string => {
  let newStr = '';
  if (str) {
    const wordArr = str.split(/[-_]/g);
    wordArr.forEach((word: string, index) => {
      if (index > 0) {
        newStr +=
          wordArr[index].charAt(0).toUpperCase() + wordArr[index].slice(1);
      } else {
        newStr += wordArr[index];
      }
    });
  } else {
    return newStr;
  }
  return newStr;
};

export const capitalizeFirstLetter = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1).replaceAll('-', ' ');
};

export const convertPrice = (price: number) =>
  price == 0 ? 'Free' : `${price} €`;
