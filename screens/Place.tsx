import React from 'react';
import { StyleSheet, View } from 'react-native';
import { PlaceOrArtistHeader } from '../components/Headers/PlaceOrArtistHeader';
import { Accordeon } from '../components/Blocks/Accordeon';
import { LinearGradient } from 'expo-linear-gradient';
import dayjs from 'dayjs';
import Animated, {
  Extrapolate,
  FadeIn,
  FadeInDown,
  interpolate,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import { BlockAdress } from '../components/Blocks/Adress';
import { Line } from '../components/Blocks/Line';
import { Tags } from '../components/Tags/Tags';
import { Section } from '../components/Section/Section';
import { BlockHeaderBackAndFollow } from '../components/Blocks/HeaderBackAndFollow';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { RootStackParamList } from '../navigation';
import { Text } from 'react-native';
import { usePlaceById } from '../services/place/getPlaceById';
import { Theme } from '../constants/theme';
import { useMainTheme } from '../hooks/useMainTheme';
import { useTheme } from '../provider/ThemeProvider';
import { BlockEvent } from '../components/BlockEvents/BlockEvent';
import { daysTranslate } from '../helpers/date';
import { capitalizeFirstLetter } from '../helpers/string';
import { LocationMap } from '../components/LocationMap/LocationMap';
import { Description } from '../components/Description/Description';
import { height, width } from '../constants/Layout';
import { Hours } from '../components/Hours/Hours';

type Props = NativeStackScreenProps<RootStackParamList, 'Place'>;

export const Place = ({ route }: Props) => {
  const { id } = route.params;
  const styles = useMainTheme(createStyles);
  const { theme } = useTheme();
  const { data: place, isLoading: isLoading } = usePlaceById(id);

  const translateY = useSharedValue(0);
  const handleScroll = useAnimatedScrollHandler({
    onScroll: (e) => {
      translateY.value = Math.abs(e.contentOffset.y);
    },
  });

  const style = useAnimatedStyle(() => {
    return {
      transform: [
        {
          scale: interpolate(
            translateY.value,
            [-height / 3, -10, 0, height / 4],
            [2, 1.3, 1, 1.3],
            Extrapolate.CLAMP,
          ),
        },
        {
          translateY: interpolate(
            translateY.value,
            [-height / 2, 0],
            [height / 5, 0],
            Extrapolate.CLAMP,
          ),
        },
      ],
    };
  });

  if (isLoading || !place) {
    return <Text>Loading</Text>;
  }

  return (
    <View style={styles.S_Main}>
      <Animated.View
        entering={FadeIn.delay(300).duration(200)}
        style={[style, styles.S_Cover]}
      >
        <PlaceOrArtistHeader
          name={place.name}
          followersCount={place.followersCount || 0}
          cover={place.cover}
        />
      </Animated.View>
      <BlockHeaderBackAndFollow id={place.id} type="None" />
      <Animated.ScrollView
        style={styles.S_Container}
        entering={FadeInDown.delay(300).duration(200)}
        onScroll={handleScroll}
        scrollEventThrottle={16}
        showsVerticalScrollIndicator={false}
      >
        <LinearGradient
          style={styles.S_Linear}
          colors={['rgba(21, 23, 24, 0)', theme.background.primary]}
        />
        <View style={styles.S_InnerView}>
          <Hours
            title="Aujourd'hui,"
            hours={place.hours}
            color={theme.fontColor.action}
          />
          <Hours title="Happy Hour" hours={place.happyHours} color="#FFFFFF" />
          <Line />
          <BlockAdress adress={place.address} />
          {place.subCategories ? (
            <>
              <Line />
              <Tags tags={place.subCategories} />
            </>
          ) : null}

          {place.desc ? (
            <Section notHorizontalPadding title="Description">
              <Description text={place.desc} />
            </Section>
          ) : null}

          {place.events.length ? (
            <Section notHorizontalPadding title="Upcoming events">
              <View style={styles.S_SectionEvent}>
                {place.events.map((event, index) => (
                  <BlockEvent
                    key={index}
                    style={{ marginBottom: 16 }}
                    event={event}
                    isBig
                  />
                ))}
              </View>
            </Section>
          ) : null}

          {place.location ? (
            <Section notHorizontalPadding title="Localisation">
              <LocationMap
                latitude={place.location[0]}
                longitude={place.location[1]}
              />
            </Section>
          ) : null}
        </View>
      </Animated.ScrollView>
    </View>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_Main: {
      flex: 1,
      backgroundColor: theme.background.primary,
      zIndex: 12,
    },
    S_Container: {
      position: 'absolute',
      top: 0,
      left: 0,
      flex: 1,
      height,
    },
    S_Cover: {
      width,
      height: 300,
    },
    S_AccordeonItem: {
      height: 30,
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    S_InnerView: {
      flex: 1,
      paddingHorizontal: 20,
      paddingBottom: 120,
      backgroundColor: theme.background.primary,
    },
    S_Linear: {
      width,
      height: 50,
      marginTop: 250,
    },
    S_Date: {
      fontSize: 14,
      fontFamily: 'neue',
      color: theme.fontColor.secondary,
    },
    S_SectionEvent: {
      paddingTop: 24,
    },
    S_ReadMore: {
      marginTop: 12,
      fontSize: 14,
      fontFamily: 'neue-bold',
      color: theme.fontColor.primary,
    },
    S_Description: {
      marginTop: 24,
      fontSize: 14,
      lineHeight: 20,
      fontFamily: 'neue',
      color: theme.fontColor.primary,
    },
  });
