import React, {
  FlatList,
  Pressable,
  Text,
  View,
  StyleSheet,
  Platform,
} from 'react-native';
import { useMainTheme } from '../hooks/useMainTheme';
import { Theme } from '../constants/theme';
import { useCallback } from 'react';
import { PrivateEvent } from '../services/types';
import { Image } from 'expo-image';
import { useNavigation } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

const TABS = ['A venir', 'Mes events', 'Passés'];
export const PrivateEvents = () => {
  const styles = useMainTheme(createStyles);
  const navigation = useNavigation();
  const { top } = useSafeAreaInsets();

  const handleNewEvent = () => {
    navigation.navigate('CreatePrivateEvent' as never);
  };

  const menuItem = useCallback(({ item }) => {
    return (
      <Pressable>
        <Text>{item}</Text>
      </Pressable>
    );
  }, []);

  const eventItem = useCallback(({ item }) => {
    return (
      <Pressable>
        <Image source={item.cover} />
        <Text>{item.title}</Text>
      </Pressable>
    );
  }, []);

  const events = [
    { id: '1', title: 'Youhou', cover: '', owners: [{ name: 'Aurelien' }] },
  ];

  return (
    <View style={[{ paddingTop: top }]}>
      <FlatList<string>
        style={styles.menu}
        keyExtractor={(item, index) => index.toString()}
        horizontal
        data={TABS}
        contentContainerStyle={{
          marginLeft: 20,
          paddingRight: 20,
        }}
        showsHorizontalScrollIndicator={false}
        decelerationRate={Platform.OS === 'ios' ? 5 : 0.98}
        renderItem={menuItem}
      />
      <Pressable onPress={handleNewEvent}>
        <Text>Ajouter</Text>
      </Pressable>

      <View style={styles.events}>
        <FlatList<Partial<PrivateEvent>>
          style={styles.menu}
          keyExtractor={(item, index) => index.toString()}
          horizontal
          data={events}
          contentContainerStyle={{
            marginLeft: 20,
            paddingRight: 20,
          }}
          showsHorizontalScrollIndicator={false}
          decelerationRate={Platform.OS === 'ios' ? 5 : 0.98}
          renderItem={eventItem}
        />
      </View>
    </View>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    menu: {},
    events: {},
  });
