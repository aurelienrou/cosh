import {
  vec,
  Canvas,
  useValue,
  Circle,
  Group,
  Blur,
  Paint,
  ColorMatrix,
  Text,
  useFont,
  SkFont,
  useComputedValue,
  useLoop,
  mix,
} from '@shopify/react-native-skia';
import React, { useMemo, useRef, useState } from 'react';
import { width, height } from '../constants/Layout';
import BottomSheet from '@gorhom/bottom-sheet';
import Animated, {
  Extrapolation,
  SharedValue,
  interpolate,
  useAnimatedStyle,
  useDerivedValue,
  useSharedValue,
} from 'react-native-reanimated';
import { Pressable, StyleSheet, View, Text as RNText } from 'react-native';
import { Register } from '../components/SignInOut/Register';
import { Theme } from '../constants/theme';
import { useMainTheme } from '../hooks/useMainTheme';
import { useTheme } from '../provider/ThemeProvider';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { LogIn } from '../components/SignInOut/LogIn';

function getRandomArbitrary(min: number, max: number) {
  'worklet';
  return Math.random() * (max - min) + min;
}

interface ParticuleProps {
  color: string;
}

export const Particule = ({ color }: ParticuleProps) => {
  const random = Math.random();

  const radius = getRandomArbitrary(100, width);
  const time = useLoop({ duration: random * 600000 });
  const loop = useLoop({ duration: random < 0.5 ? 0.5 : random * 50000 });
  const directionX = useValue(random > 0.5 ? 1 : -1);
  const directionY = useValue(random > 0.5 ? 1 : -1);
  const xPos = useValue(getRandomArbitrary(0, width));
  const yPos = useValue(getRandomArbitrary(0, height));
  const speedX = useValue(Math.random() * 2);
  const speedY = useValue(Math.random() * 2);

  const r = useComputedValue(() => mix(loop.current, radius, width), [loop]);

  const c = useComputedValue(() => {
    xPos.current += speedX.current * directionX.current;
    yPos.current += speedY.current * directionY.current;

    if (xPos.current >= width) {
      directionX.current = -1;
    }
    if (xPos.current <= 0) {
      directionX.current = 1;
    }

    if (yPos.current >= height) {
      directionY.current = -1;
    }

    if (yPos.current <= 0) {
      directionY.current = 1;
    }

    return vec(xPos.current, yPos.current);
  }, [time]);

  return <Circle c={c} r={r} color={color} />;
};

interface TProps {
  font: SkFont | null;
  position: SharedValue<number>;
  text: string;
  index: number;
}

const T = ({ position, font, text, index }: TProps) => {
  const blur = useDerivedValue(() => {
    return interpolate(position.value, [0, 1], [0, 7], Extrapolation.CLAMP);
  }, [position]);

  const letterWidth = font?.getTextWidth(text);
  const xPosition = (width - (letterWidth || 10)) / 2;
  return (
    <Text text={text} x={xPosition} y={44 * index} font={font} color={'white'}>
      <Blur blur={blur} />
    </Text>
  );
};

export const SignInOut = () => {
  const { bottom } = useSafeAreaInsets();
  const styles = useMainTheme(createStyles);
  const { theme } = useTheme();

  const [type, setType] = useState<'login' | 'register'>('login');
  const font = useFont(
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    require('./../assets/fonts/Gatwick-Bold.otf'),
    40,
  );
  const text = ['Vos favoris', 'sont à', 'un clic !'];

  const position = useSharedValue(0);
  const ref = useRef<BottomSheet>(null);
  const layer = useMemo(() => {
    return (
      <Paint>
        <Blur blur={100} />
        <ColorMatrix
          matrix={[1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, -1]}
        />
      </Paint>
    );
  }, []);

  const handleLogin = () => {
    setType('login');
    ref.current?.snapToIndex(1);
  };

  const handleRegister = () => {
    setType('register');
    ref.current?.expand();
  };

  const handleToggleLoginIn = () => {
    setType(type === 'login' ? 'register' : 'login');
  };

  const backdropStyle = useAnimatedStyle(() => {
    return {
      transform: [
        { translateY: interpolate(position.value, [0, 1], [height, 0]) },
      ],
    };
  });

  return (
    <>
      <Canvas style={{ width, height, backgroundColor: '#081c82' }}>
        <Group layer={layer} blendMode="difference">
          <Particule color="#980236" />
          <Particule color="#980236" />
          <Particule color="#980236" />
          <Particule color="#081c82" />
        </Group>
      </Canvas>
      <View style={styles.S_HomeTitleContainer}>
        <Canvas
          style={{
            width,
            height,
            justifyContent: 'center',
            alignItems: 'center',
            overflow: 'hidden',
          }}
        >
          {text.map((t, index) => (
            <T
              key={t}
              index={index + 1}
              text={t}
              font={font}
              position={position}
            />
          ))}
        </Canvas>
      </View>

      <View style={[styles.S_Buttons, { bottom: bottom + 90 }]}>
        <Pressable style={styles.S_Register} onPress={handleRegister}>
          <RNText style={[styles.S_RegisterText]}>{`S'INSCRIRE`}</RNText>
        </Pressable>
        <Pressable style={styles.S_SignIn} onPress={handleLogin}>
          <RNText style={styles.S_SignInText}>SE CONNECTER</RNText>
        </Pressable>
      </View>

      <BottomSheet
        style={styles.S_BottomSheet}
        ref={ref}
        animateOnMount={false}
        animatedIndex={position}
        snapPoints={[1, type === 'register' ? bottom + 380 : bottom + 330]}
        containerStyle={{ zIndex: 4 }}
        enablePanDownToClose
        backgroundStyle={{
          backgroundColor: theme.background.primary,
        }}
        backdropComponent={() => (
          <Animated.View style={[backdropStyle, styles.S_BackgroundPressable]}>
            <Pressable
              style={styles.S_Pressable}
              onPress={() => ref.current?.close()}
            />
          </Animated.View>
        )}
        handleIndicatorStyle={{
          backgroundColor: theme.fontColor.primary,
          width: 40,
        }}
      >
        {type === 'register' ? (
          <Register onToggle={handleToggleLoginIn} />
        ) : (
          <LogIn onToggle={handleToggleLoginIn} />
        )}
      </BottomSheet>
    </>
  );
};
const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_BottomSheet: {
      paddingBottom: 0,
      borderRadius: 32,
      overflow: 'hidden',
      zIndex: 20000,
    },
    title: {
      fontFamily: 'gatwick-bold',
      fontSize: 40,
      textAlign: 'center',
      color: '#fff',
    },
    S_BackgroundPressable: {
      height: '100%',
      position: 'absolute',
      top: 0,
      width: '100%',
    },
    S_Pressable: {
      height: '100%',
      width: '100%',
    },
    S_Buttons: {
      width,
      paddingHorizontal: 20,
      position: 'absolute',
      zIndex: 3,
    },
    S_HomeTitleContainer: {
      position: 'absolute',
      top: height / 2 - 120,
      justifyContent: 'center',
      alignItems: 'center',
      width: width,
    },
    S_RegisterText: {
      textAlign: 'center',
      fontFamily: 'neue-bold',
      fontSize: 14,
      color: theme.fontColor.primary,
      opacity: 0.5,
    },
    S_Register: {
      height: 48,
      justifyContent: 'center',
      alignItems: 'center',
    },
    S_SignIn: {
      height: 48,
      marginTop: 8,
      borderRadius: 24,
      backgroundColor: theme.fontColor.primary,
      justifyContent: 'center',
      alignItems: 'center',
    },
    S_SignInText: {
      fontFamily: 'neue-bold',
      fontSize: 14,
    },
  });
