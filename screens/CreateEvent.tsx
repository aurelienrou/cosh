import { StyleSheet, View } from 'react-native';
import React, { useState } from 'react';
import CreateEventForm from '../components/CreateEventForm/CreateEventForm';
import CreatePlaceForm from '../components/CreatePlaceForm/CreatePlaceForm';
import { useSharedValue, withSpring } from 'react-native-reanimated';
import { height, width } from '../constants/Layout';
import CreateEventHeader from '../components/CreateEventHeader/CreateEventHeader';
import { useNavigation } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

export type EventStackParamList = {
  CreatePlaceForm: undefined;
  CreateEventForm: undefined;
};
const CreateEventStack = createNativeStackNavigator<EventStackParamList>();

const CreateEvent = () => {
  const progress = useSharedValue(0);
  const navigation = useNavigation();

  const handleBackPlaceForm = () => {
    navigation.navigate('CreateEventForm' as never);
    progress.value = withSpring(0, { mass: 0.5 });
  };

  const handleOpenPlaceForm = () => {
    progress.value = withSpring(1, { mass: 0.5 });
    navigation.navigate('CreatePlaceForm' as never);
  };

  const handleClose = () => {
    navigation.navigate('MyEvents' as never);
  };

  return (
    <View style={styles.container}>
      <CreateEventHeader
        onClose={handleClose}
        onBackPlaceForm={handleBackPlaceForm}
        progress={progress}
      />
      <CreateEventStack.Navigator
        initialRouteName="CreateEventForm"
        screenOptions={{
          headerShown: false,
          gestureEnabled: false,
        }}
      >
        <CreateEventStack.Screen name="CreateEventForm">
          {() => (
            <CreateEventForm
              onSubmit={handleClose}
              onOpenPlaceForm={handleOpenPlaceForm}
            />
          )}
        </CreateEventStack.Screen>
        <CreateEventStack.Screen name="CreatePlaceForm">
          {() => <CreatePlaceForm />}
        </CreateEventStack.Screen>
      </CreateEventStack.Navigator>
    </View>
  );
};

export default CreateEvent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  innerContainer: {
    flex: 1,
    position: 'relative',
    width: width * 2,
    flexDirection: 'row',
  },
  formsContainer: {
    flex: 1,
    position: 'relative',
    flexDirection: 'row',
    width: 2 * width,
  },
  header: {
    height: 50,
    borderBottomColor: '#e2e2e2',
    borderBottomWidth: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingHorizontal: 20,
    justifyContent: 'space-between',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  iconContainer: {
    width: 30,
    height: 50,
    marginRight: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: { width: 15, height: 15 },
  headerTextContainer: { height: 100 },
  titleContainer: { height: 50, justifyContent: 'center' },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
