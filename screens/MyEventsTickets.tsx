import React, { useMemo } from 'react';
import { useNavigation } from '@react-navigation/native';
import dayjs from 'dayjs';
import { LinearGradient } from 'expo-linear-gradient';
import { BlockEvents } from '../components/BlockEvents/BlockEvents';
import { SectionHeader } from '../components/Section/SectionHeader';
import { capitalizeFirstLetter } from '../helpers/string';
import { useEventsContext } from '../provider/EventProvider';
import { IEvent } from '../services/types';
import { SectionList, StyleSheet, View } from 'react-native';
import { Theme } from '../constants/theme';
import { useMainTheme } from '../hooks/useMainTheme';
import { useTheme } from '../provider/ThemeProvider';

type Section = {
  title: string;
  data: IEvent[][];
};

export const MyEventsTickets = () => {
  const styles = useMainTheme(createStyles);
  const navigation = useNavigation();
  const { events } = useEventsContext();
  const { theme } = useTheme();
  const data = useMemo(() => {
    const eventsToSort: IEvent[] = events.map((e) => ({ ...e }));
    eventsToSort.sort(
      (a, b) => dayjs(a.beginAt).day() - dayjs(b.beginAt).day(),
    );

    const days: IEvent[][] = Array.from({ length: 7 }, () => []);

    for (const event of eventsToSort) {
      days[dayjs(event.beginAt).day()].push(event);
    }

    return days.map((events, i) => ({
      title: capitalizeFirstLetter(dayjs().day(i).format('dddd')),
      data: [events],
    }));
  }, [events]);

  return (
    <View style={styles.S_Container}>
      <SectionList<IEvent[], Section>
        sections={data}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 150 }}
        keyExtractor={(item, index) => item[0]?.id ?? 0 + index}
        renderItem={({ section }) =>
          section.data[0].length ? (
            <BlockEvents
              style={{ marginTop: 32 }}
              max={12}
              events={section.data[0]}
            />
          ) : null
        }
        renderSectionHeader={({ section: { data, title } }) =>
          data[0].length ? (
            <>
              <SectionHeader
                style={{ paddingTop: 32, paddingBottom: 10 }}
                title={title}
                onPress={
                  data[0].length > 12
                    ? () => navigation.navigate('' as never)
                    : undefined
                }
              />
              <LinearGradient
                style={styles.S_LinearGradient}
                colors={[theme.background.primary, '#15171800']}
              />
            </>
          ) : null
        }
      />
    </View>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_Container: {
      flex: 1,
      paddingHorizontal: 20,
      backgroundColor: theme.background.primary,
    },
    S_LinearGradient: {
      position: 'absolute',
      top: 59,
      height: 50,
      width: '100%',
      zIndex: 0,
    },
  });
