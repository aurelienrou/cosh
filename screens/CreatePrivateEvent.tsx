import React, {
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import { Theme } from '../constants/theme';
import { useMainTheme } from '../hooks/useMainTheme';
import { RefObject, useRef } from 'react';
import { Image } from 'expo-image';
import { useCreateOrEditPrivateEventContext } from '../provider/CreateOrEditPrivateEventProvider';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { BottomSheetModalMethods } from '@gorhom/bottom-sheet/lib/typescript/types';
import { PrivateEventDateModal } from '../components/PrivateEventParams/PrivateEventModal';
import { PrivateEventDate } from '../components/PrivateEventParams/PrivateEventDate';
import { DATE_FORMAT_WITH_DAY, formateDate } from '../helpers/date';
import { PrivateEventParams } from '../components/PrivateEventParams/PrivateEventParams';

export const CreatePrivateEvent = () => {
  const styles = useMainTheme(createStyles);
  const { event, setEvent } = useCreateOrEditPrivateEventContext();
  const { top } = useSafeAreaInsets();
  const dateRef = useRef(null) as RefObject<BottomSheetModalMethods>;
  const paramsRef = useRef(null) as RefObject<BottomSheetModalMethods>;

  const openImageSelector = () => {
    // OPEN IMAGE SELECTOR
  };

  const openDateSelector = () => {
    // OPEN DATE SELECTOR
    dateRef.current?.present();
  };

  const openParamsSelector = () => {
    // OPEN PARAMS SELECTOR
    paramsRef.current?.present();
  };

  const handleSave = () => {
    // SAVE
  };
  return (
    <>
      <View style={[styles.container, { paddingTop: top }]}>
        <TextInput
          style={styles.inputText}
          onChangeText={(title) => setEvent({ ...event, title })}
          value={event.title}
        />
        <Pressable onPress={openImageSelector}>
          <Image source={require('../assets/images/avatar.png')} />
        </Pressable>
        <Pressable onPress={openDateSelector}>
          <Text>
            {event.startDate
              ? formateDate(event.startDate, DATE_FORMAT_WITH_DAY)
              : 'Fixer une date'}
          </Text>
        </Pressable>
        <Text>Hebergé par {event.owners[0]?.name}</Text>
        <TextInput
          style={styles.inputText}
          onChangeText={(address) => setEvent({ ...event, address })}
          value={event.address || ''}
          placeholder="adresse de l'evenement"
        />
        <TextInput
          style={styles.inputText}
          onChangeText={(description) => setEvent({ ...event, description })}
          placeholder="Ajouter une description"
          value={event.description || ''}
          maxLength={5000}
          multiline
        />

        <Pressable onPress={openParamsSelector}>
          <Text>{"Parametres de l'évenement"}</Text>
        </Pressable>

        <Pressable onPress={handleSave}>
          <Text>Sauvegarder</Text>
        </Pressable>
      </View>

      <PrivateEventDateModal ref={dateRef} name="dateEvent">
        <PrivateEventDate onSubmit={() => dateRef.current?.dismiss()} />
      </PrivateEventDateModal>

      {/* <PrivateEventDateModal ref={paramsRef} name="paramsEvent"> */}
      <PrivateEventParams />
      {/* </PrivateEventDateModal> */}
    </>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#cf9e9e',
    },
    inputText: {},
  });
