import React, {
  memo,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  Pressable,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TextInput,
} from 'react-native';
import { height, width } from '../constants/Layout';
import { BlurView } from '@react-native-community/blur';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Image } from 'expo-image';
import {
  Blur,
  Canvas,
  ColorMatrix,
  Group,
  Paint,
} from '@shopify/react-native-skia';
import { Particule } from './SignInOut';
import { FlatList } from 'react-native';
import { Select } from '../components/Select/Select';
import { Controller, useForm } from 'react-hook-form';
import { useGoogleLogin } from '../services/auth/google';
import { useAuthContext } from '../provider/AuthProvider';
import { blurAmount } from '../constants/theme';

const rulesEmail = {
  email: {
    required: {
      value: true,
      message: 'Un titre est requis',
    },
    pattern: {
      value: /\S+@\S+\.\S+/,
      message: 'Entered value does not match email format',
    },
  },
  password: {
    required: {
      value: true,
      message: 'Une cover est requise',
    },
    minLength: {
      value: 5,
      message: 'min length is 5',
    },
  },
};

const rules = {
  name: {
    required: {
      value: true,
      message: 'Required',
    },
    maxLength: {
      value: 240,
      message: 'Too long',
    },
    minLength: {
      value: 1,
      message: 'Too short',
    },
  },
  age: {
    required: {
      value: true,
      message: 'Required',
    },
    min: 1,
    max: 100,
  },
  gender: {
    required: {
      value: true,
      message: 'Une date de debut est requise',
    },
  },
};

const initialeFormEmail = [
  {
    title: 'What’s your email?',
    name: 'email',
    props: {
      keyboardType: 'email-address',
      placeholder: 'Email adress',
    },
  },
  {
    title: 'Create a password',
    name: 'password',
    props: {
      placeholder: 'Password',
    },
  },
];

const initialForm = [
  {
    title: 'Create a username',
    name: 'name',
    props: {
      keyboardType: 'default',
      placeholder: 'Username',
    },
  },
  {
    title: 'How old are you?',
    name: 'age',
    props: {
      keyboardType: 'number-pad',
      placeholder: 'Age',
    },
  },
  {
    title: 'What’s your gender?',
    name: 'gender',
    type: 'select',
    options: ['Male', 'Female', 'Other'],
  },
];

const ParticuleMemo = memo(Particule);

type Params = {
  name: string;
  email: string;
  token: string;
  cover: string;
  isSocialLogin: boolean;
};

export const SignUp = () => {
  const route = useRoute();
  const params = route.params as Params;
  const navigation = useNavigation();
  const { setUser } = useAuthContext();
  const { mutate, data, isLoading } = useGoogleLogin();

  useEffect(() => {
    if (data?.user) {
      setUser(data.user);
      navigation.navigate('Discover' as never);
    }
  }, [data]);

  const { defaultValues, form } = useMemo(() => {
    const initialValue = {
      name: params?.name || '',
      age: '',
      gender: '',
    };

    return {
      defaultValues: params.isSocialLogin
        ? initialValue
        : Object.assign(initialValue, {
            email: params?.email || null,
            password: null,
          }),
      form: params.isSocialLogin
        ? initialForm
        : [...initialeFormEmail, initialForm],
    };
  }, [params]);

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
  } = useForm({ defaultValues });

  const { top, bottom } = useSafeAreaInsets();
  const [index, setIndex] = useState<number>(0);
  const flashListRef = useRef<FlatList<any>>(null);

  const layer = useMemo(() => {
    return (
      <Paint>
        <Blur blur={100} />
        <ColorMatrix
          matrix={[1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, -1]}
        />
      </Paint>
    );
  }, []);

  const handleNext = (nextIndex: number) => {
    handleSubmit((data) => {
      console.log(data);
    });
    if (nextIndex > form.length - 1) {
      const values = getValues();
      mutate({
        token: params.token,
        ...values,
      });
      return;
    }
    flashListRef.current?.scrollToOffset({
      offset: nextIndex * width,
    });
    setIndex(nextIndex);
  };

  const handlePrev = (prevIndex: number) => {
    if (prevIndex < 0) {
      navigation.goBack();

      return;
    }
    flashListRef.current?.scrollToOffset({
      offset: prevIndex * width,
    });
    setIndex(prevIndex);
  };

  console.log({ errors });

  const renderItem = useCallback(({ item }) => {
    return (
      <View style={{ width, paddingHorizontal: 20 }}>
        <Text style={styles.S_Question}>{item.title}</Text>
        <Controller
          control={control}
          // rules={rules[item.name]}
          render={({ field: { onChange, value } }) => {
            return item.type === 'select' ? (
              <Select items={item.options} onChange={onChange} />
            ) : (
              <TextInput
                style={styles.S_InputText}
                onChangeText={onChange}
                placeholderTextColor="#7f8383"
                value={value}
                {...item.props}
              />
            );
          }}
          name={item.name}
        />
        {/* {errors[item.name] ?? <Text>{errors?.[item.name]?.message}</Text>} */}
      </View>
    );
  }, []);

  return (
    <>
      <Canvas style={styles.S_Canvas}>
        <Group layer={layer}>
          <ParticuleMemo color="#0d302a" />
          <ParticuleMemo color="#0d302a" />
          <ParticuleMemo color="#0d302a" />
          <ParticuleMemo color="#040b2e" />
        </Group>
      </Canvas>
      <SafeAreaView style={styles.S_Container}>
        <View
          style={[styles.S_Header, { left: 20, top: top + 20, zIndex: 10 }]}
        >
          <Pressable
            style={styles.S_BackPressable}
            onPress={() => handlePrev(index - 1)}
          >
            <Image
              style={styles.S_BackIcon}
              source={require('../assets/icons/arrow.svg')}
            />
            <BlurView blurAmount={blurAmount} />
          </Pressable>
          <Text style={styles.S_HeaderText}>Sign Up</Text>
        </View>
        <FlatList
          data={form}
          ref={flashListRef}
          horizontal
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item) => item.title}
          contentContainerStyle={{ paddingTop: top + 80 }}
          renderItem={renderItem}
          snapToInterval={width}
          decelerationRate="fast"
          scrollEnabled={false}
        />
        <View style={[styles.S_BarsContainer, { bottom: bottom + 94 }]}>
          {Array.from({ length: form.length }).map((_, i) => (
            <View
              key={i}
              style={[
                styles.S_Bar,
                {
                  opacity: index >= i ? 1 : 0.12,
                  width: (width - 40 - form.length - 2) / form.length - 1,
                },
              ]}
            />
          ))}
        </View>
        <Pressable
          style={[styles.S_Button, { bottom: bottom + 20 }]}
          onPress={() => handleNext(index + 1)}
        >
          <Text style={styles.S_ButtonText}>
            {index === Object.keys(defaultValues).length - 1 ? 'DONE' : 'NEXT'}
          </Text>
        </Pressable>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  S_Container: {
    flex: 1,
    paddingHorizontal: 20,
    overflow: 'visible',
  },
  S_Canvas: { position: 'absolute', width, height, backgroundColor: '#000000' },
  S_Header: {
    width,
    position: 'relative',
    flexDirection: 'row',
    height: 48,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  S_BackPressable: {
    height: 48,
    width: 48,
    borderRadius: 24,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    position: 'absolute',
    zIndex: 10,
    overflow: 'hidden',
  },
  S_Blur: {
    height: 48,
    width: 48,
    borderRadius: 24,
    position: 'absolute',
  },
  S_BackIcon: {
    height: 14,
    width: 18,
    zIndex: 11,
  },
  S_HeaderText: {
    width: width - 40,
    textAlign: 'center',
    fontFamily: 'gatwick-bold',
    fontSize: 20,
    color: '#fff',
  },
  S_Button: {
    position: 'absolute',
    left: 20,
    width: width - 40,
    height: 48,
    borderRadius: 36,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  S_Question: {
    width,
    fontFamily: 'gatwick-bold',
    fontSize: 40,
    textAlign: 'left',
    color: '#fff',
    marginBottom: 24,
  },
  S_InputText: {
    fontFamily: 'neue',
    fontSize: 16,
    color: '#7f8383',
  },
  S_ButtonText: {
    fontFamily: 'neue-bold',
    fontSize: 14,
  },
  S_BarsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    position: 'absolute',
    left: 20,
    width: width - 40,
  },
  S_Bar: {
    height: 8,
    borderRadius: 4,
    backgroundColor: '#fff',
  },
});
