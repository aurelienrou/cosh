import React, { useEffect, useRef, useState } from 'react';
import { useLocationContext } from '../provider/LocationProvider';
import { View, StyleSheet, SafeAreaView } from 'react-native';
import { useWeeklyEvents } from '../services/events/getWeeklyEvents';
import { Theme } from '../constants/theme';
import { useMainTheme } from '../hooks/useMainTheme';
import { EventsLoader } from '../components/Loader/EventLoader';
import { useSearchByName } from '../services/search/search';
import { SearchHeader } from '../components/Headers/SearchHeader';
import { EventsByDateList } from '../components/Lists/EventsByDateList';
import {
  EventsAndPlacesList,
  dataType,
} from '../components/Lists/EventsAndPlacesList';
import {
  ModalDateTimePicker,
  SelectedDate,
} from '../components/ModalDateTimePicker/ModalDateTimePicker';
import BottomSheet from '@gorhom/bottom-sheet';
import { MainMessage } from '../components/Errors/MainMessage';
import { height } from '../constants/Layout';
import { useUserByName } from '../services/user/getUserByName';
import { usePlaceByName } from '../services/place/getPlacesByName';

export default function Search() {
  const { location } = useLocationContext();
  const styles = useMainTheme(createStyles);
  const [textInput, setTextInput] = useState('');
  const [date, setDate] = useState<SelectedDate>({
    START_DATE: null,
    END_DATE: null,
  });

  const { data: defaultEvents, isLoading: isLoadingEvent } = useWeeklyEvents(
    location.latitude,
    location.longitude,
    10000,
  );

  const { data: events } = useSearchByName(
    textInput,
    date.START_DATE,
    date.END_DATE,
  );

  const { data: places, isLoading: isLoadingPlaces } =
    usePlaceByName(textInput);

  const { data: users, isLoading: isLoadingUser } = useUserByName(textInput);

  const doNothing = !isLoadingEvent && !events.length && !textInput.length;

  const hasResults = events.length && textInput.length;

  const ref = useRef<BottomSheet>(null);

  const handleConfirmDate = (date: SelectedDate) => {
    setDate(date);
    ref.current?.close();
  };

  const data = [
    users.length && 'Utilisateurs',
    ...users,
    places.length && 'Lieux',
    ...places,
    events.length && 'Events',
    ...events,
  ].filter(Boolean) as dataType[];

  return (
    <SafeAreaView style={styles.S_Container}>
      <SearchHeader
        onChange={setTextInput}
        onClickSettings={() => {
          ref.current?.expand();
        }}
      />
      <ModalDateTimePicker
        refBottomSheet={ref}
        onConfirm={handleConfirmDate}
        onClose={() => {
          ref.current?.collapse();
        }}
      />
      {isLoadingEvent ? (
        <View style={styles.S_LoadingContainer}>
          <EventsLoader />
        </View>
      ) : null}

      <View style={{ height: height - 90, paddingHorizontal: 20 }}>
        {doNothing ? (
          <EventsByDateList events={defaultEvents} />
        ) : hasResults ? (
          <EventsAndPlacesList data={data} />
        ) : (
          <MainMessage title="Oups," subTitle="no results" />
        )}
      </View>
    </SafeAreaView>
  );
}

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_Container: {
      flex: 1,
      backgroundColor: theme.background.primary,
    },
    S_LoadingContainer: {
      paddingHorizontal: 20,
    },
    S_SearchBar: {
      height: 48,
      borderRadius: 36,
      paddingVertical: 12,
      paddingLeft: 44,
      paddingRight: 16,
      backgroundColor: theme.background.action,
      color: theme.fontColor.primary,
    },
    S_Header: {
      height: 48,
      flexDirection: 'row',
      paddingHorizontal: 20,
      marginTop: 16,
    },
    S_SearchBarContainer: {
      height: 48,
      justifyContent: 'center',
    },
    S_IconClear: {
      position: 'absolute',
      right: 0,
    },
    S_Button: {
      width: 48,
      height: 48,
      borderRadius: 24,
      backgroundColor: theme.background.action,
      justifyContent: 'center',
      alignItems: 'center',
    },
    S_Icon: {
      width: 22,
      height: 22,
    },
    S_IconSearch: {
      width: 22,
      height: 22,
      position: 'absolute',
      left: 16,
    },
    S_Padding: {
      padding: 0,
    },
  });
