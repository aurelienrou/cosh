import { memo } from 'react';
import React, { StyleSheet, View } from 'react-native';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
} from 'react-native-reanimated';
import { MainHeader } from '../components/Headers/MainHeader';
import { Map } from '../components/SearchMap/Map';
import { height } from '../constants/Layout';
import { useMapsContext } from '../provider/MapsProvider';
import { DiscoverTabView } from '../components/DiscoverTabView/DiscoverTabView';
import { Theme } from '../constants/theme';
import { useMainTheme } from '../hooks/useMainTheme';

const DiscoverTabViewMemo = memo(DiscoverTabView);
const MapMemo = memo(Map);

export const Discover = () => {
  const { animatedIndex } = useMapsContext();
  const styles = useMainTheme(createStyle);

  const styleMapContainer = useAnimatedStyle(() => {
    const scale = interpolate(
      animatedIndex.value,
      [0, 1],
      [1, 0.7],
      Extrapolate.CLAMP,
    );
    const borderRadius = interpolate(
      animatedIndex.value,
      [0, 1],
      [0, 64],
      Extrapolate.CLAMP,
    );

    return {
      transform: [{ scale }],
      borderRadius,
    };
  });

  return (
    <View style={styles.S_Container}>
      <MainHeader />
      <Animated.View style={[styles.S_MapContainer, styleMapContainer]}>
        <MapMemo />
      </Animated.View>
      <DiscoverTabViewMemo />
    </View>
  );
};

const createStyle = (theme: Theme) => {
  return StyleSheet.create({
    S_Container: {
      flex: 1,
      backgroundColor: theme.background.secondary,
    },
    S_MapContainer: {
      ...StyleSheet.absoluteFillObject,
      overflow: 'hidden',
      flex: 1,
      height,
    },
  });
};
