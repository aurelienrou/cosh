import { useNavigation } from '@react-navigation/native';
import React, { Platform, FlatList } from 'react-native';
import styled from 'styled-components/native';
import { width } from '../constants/Layout';
import { forFade } from '../components/DiscoverTabView/DiscoverTabView';
import { useRef, useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { MyEventsWishlist } from './MyEventsWishlist';
import { TextMenu } from '../components/Headers/MainHeader';

const SCREENS = {
  Wishlist: {
    title: 'Wishlist',
    component: MyEventsWishlist,
  },
};

const SCREEN_CFG = Object.entries(SCREENS);
const SCREEN_NAMES = Object.keys(SCREENS) as (keyof typeof SCREENS)[];
const Stack = createStackNavigator();

export default function MyEvents() {
  const [currentIndex, setCurrentIndex] = useState(0);
  const flatlistRef = useRef<FlatList>(null);
  const navigation = useNavigation();

  const handleOnPress = (index: number, name: string) => {
    navigation.navigate(name as never);
    setCurrentIndex(index);
    flatlistRef.current?.scrollToIndex({ animated: true, index });
  };

  return (
    <S_Container>
      <S_Header>
        <S_HeaderTextContainer>
          <S_HeaderTitle>My tickets</S_HeaderTitle>
          <S_HeaderSubTitle>I bought</S_HeaderSubTitle>
        </S_HeaderTextContainer>
      </S_Header>
      <S_HorizontalMenu
        ref={flatlistRef}
        horizontal
        showsHorizontalScrollIndicator={false}
        data={SCREEN_CFG}
        decelerationRate={Platform.OS === 'ios' ? 5 : 0.98}
        renderItem={({ item: [name, cfg], index }: any) => {
          return (
            <S_TextMenuContainer
              key={index}
              onPress={() => handleOnPress(index, name)}
              current={index === currentIndex}
            >
              <S_TextMenu current={index === currentIndex}>
                {cfg.title}
              </S_TextMenu>
            </S_TextMenuContainer>
          );
        }}
      ></S_HorizontalMenu>
      <Stack.Navigator
        initialRouteName="Wishlist"
        screenOptions={{ headerShown: false }}
      >
        {SCREEN_NAMES.map((name) => (
          <Stack.Screen
            key={name}
            name={name}
            getComponent={() => SCREENS[name].component}
            options={{ cardStyleInterpolator: forFade }}
          />
        ))}
      </Stack.Navigator>
    </S_Container>
  );
}

const S_Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${(props) => props.theme.background.primary};
`;

const S_Header = styled.View`
  justify-content: center;
  padding: 0 20px;
  height: 100px;
`;

const S_HeaderTitle = styled.Text`
  color: ${(props) => props.theme.fontColor.primary};
  font-size: 18px;
  font-weight: bold;
  font-family: 'gatwick-bold';
`;

const S_HeaderSubTitle = styled.Text`
  color: ${(props) => props.theme.fontColor.action};
  font-size: 14px;
  font-family: 'neue';
`;

const S_HeaderTextContainer = styled.View`
  height: 48px;
  justify-content: space-between;
  width: ${width - 40 + 'px'};
`;

const S_HorizontalMenu = styled.FlatList`
  max-height: 32px;
  padding: 0px 0px 16px 20px;
  border-bottom-color: rgba(255, 255, 255, 0.14);
  border-bottom-width: 1px;
`;

const S_TextMenuContainer = styled.Pressable<TextMenu>`
  border-bottom-color: ${(props) =>
    props.current ? 'rgba(255, 255, 255, 1)' : 'transparent'};
  border-bottom-width: 1px;
  min-height: 32px;
  margin-right: 24px;
`;

const S_TextMenu = styled.Text<TextMenu>`
  font-family: ${(props) => (props.current ? 'neue-bold' : 'neue')};
  font-size: 16px;
  color: ${(props) =>
    props.current
      ? props.theme.fontColor.primary
      : props.theme.fontColor.secondary};
`;
