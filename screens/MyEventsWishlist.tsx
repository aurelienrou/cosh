import React, { useMemo } from 'react';
import { useFollowContext } from '../provider/FollowProvider';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { ScrollView, StyleSheet, View, Text } from 'react-native';
import { BlockEvent } from '../components/BlockEvents/BlockEvent';
import Animated, { FadeIn, FadeOut, Layout } from 'react-native-reanimated';
import { Theme } from '../constants/theme';
import { useMainTheme } from '../hooks/useMainTheme';
import { IEventMinimize } from '../services/types';
import { useAuthContext } from '../provider/AuthProvider';
import { SignInOut } from './SignInOut';

export const MyEventsWishlist = () => {
  const { followMap } = useFollowContext();
  const { isLoggin } = useAuthContext();
  const { top } = useSafeAreaInsets();
  const styles = useMainTheme(createStyles);

  const events: IEventMinimize[] = useMemo(
    () =>
      Object.values(followMap)
        .map(({ following }) => following)
        .filter(Boolean),
    [followMap],
  );

  if (!isLoggin) {
    return <SignInOut />;
  }

  return (
    <View style={[styles.S_Container, { paddingTop: top }]}>
      <View style={styles.S_Header}>
        <Text style={styles.S_HeaderTitle}>
          My wishlist <Text style={styles.S_EventCount}>({events.length})</Text>
        </Text>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 150 }}
      >
        {events.map((event) => (
          <Animated.View
            key={event.id}
            entering={FadeIn}
            exiting={FadeOut}
            layout={Layout}
          >
            <BlockEvent style={{ marginTop: 16 }} event={event} isBig />
          </Animated.View>
        ))}
      </ScrollView>
    </View>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_Container: {
      flex: 1,
      paddingHorizontal: 20,
      backgroundColor: theme.background.primary,
    },
    S_Header: {
      height: 100,
      justifyContent: 'center',
      backgroundColor: theme.background.primary,
    },
    S_HeaderTitle: {
      fontFamily: 'gatwick-bold',
      fontSize: 20,
      color: theme.fontColor.primary,
    },
    S_EventCount: {
      fontFamily: 'gatwick',
      fontSize: 20,
      color: theme.fontColor.primary,
    },
  });
