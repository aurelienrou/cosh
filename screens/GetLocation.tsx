import React, { useMemo, useState } from 'react';
import {
  StyleSheet,
  KeyboardAvoidingView,
  Pressable,
  Text,
  View,
  Platform,
} from 'react-native';
import * as ExpoLocation from 'expo-location';
import { useLocationContext } from '../provider/LocationProvider';
import { height, width } from '../constants/Layout';
import Animated, {
  interpolate,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import { getAddressFromGeoCode } from '../services/googleGeocode';
import { AddressGoogle } from '../services/types';
import { BlurView } from '@react-native-community/blur';
import {
  Blur,
  Canvas,
  ColorMatrix,
  Group,
  Paint,
} from '@shopify/react-native-skia';
import { Particule } from './SignInOut';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import InputAddressFinder from '../components/InputAddressFinder/InputAddressFinder';
import { blurAmount } from '../constants/theme';
import { useNavigation, useRoute } from '@react-navigation/native';

type Coord = {
  latitude: number;
  longitude: number;
};
const GetLocation = () => {
  const { setAddress, setLocation, setAltitude } = useLocationContext();
  const [selectedLocation, setSelectedLocation] = useState<Coord>({} as Coord);
  const [selectedAdress, setSelectedAdress] = useState('');
  const { bottom, top } = useSafeAreaInsets();
  const route = useRoute<any>();

  const navigation = useNavigation();
  const isModal = route.params?.isModal;

  const handleCurrentLocation = async () => {
    try {
      const foreground = await ExpoLocation.requestForegroundPermissionsAsync();

      if (foreground.granted) {
        const requestedLocation = await ExpoLocation.getCurrentPositionAsync({
          accuracy: ExpoLocation.LocationAccuracy.Highest,
        });

        const { latitude, longitude } = requestedLocation.coords;

        const address: AddressGoogle = await getAddressFromGeoCode({
          latitude,
          longitude,
        });

        setSelectedLocation({ latitude, longitude });
        setAddress(address);
        setSelectedAdress(address.formatted_address);
        setAltitude(6000);
      }
    } catch (err) {
      console.log({ err });
    }
  };

  const handleValidate = async () => {
    console.log(selectedLocation, isModal);
    setLocation(selectedLocation);

    if (isModal) {
      navigation.goBack();
    }
  };

  const handleSelectAddress = (address: AddressGoogle): void => {
    setAddress(address);

    setSelectedLocation({
      latitude: address.geocode.lat,
      longitude: address.geocode.lng,
    });
  };

  const layer = useMemo(() => {
    return (
      <Paint>
        <Blur blur={100} />
        <ColorMatrix
          matrix={[1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, -1]}
        />
      </Paint>
    );
  }, []);

  const position = useSharedValue(0);
  const [y, setY] = useState(0);
  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        { translateY: interpolate(position.value, [0, 1], [0, -y - 76]) },
      ],
    };
  });

  const animatedBottom = useAnimatedStyle(() => {
    return {
      bottom: interpolate(
        position.value,
        [0, 1],
        [bottom + 40, bottom + isModal ? 90 : 20],
      ),
    };
  });

  const animatedBlur = useAnimatedStyle(() => {
    return {
      opacity: interpolate(position.value, [0, 1], [0, 1]),
    };
  });

  const animatedtext = useAnimatedStyle(() => {
    return {
      transform: [{ translateY: interpolate(position.value, [0, 1], [0, 48]) }],
      opacity: interpolate(position.value, [0, 1], [1, 0]),
    };
  });

  const handleBlur = () => {
    position.value = withTiming(0);
  };

  const handleFocus = () => {
    position.value = withTiming(1);
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{ flex: 1 }}
      contentContainerStyle={{ flex: 1 }}
      enabled
    >
      <Canvas style={styles.canvas}>
        <Group layer={layer} blendMode="difference">
          <Particule color="#0d302a" />
          <Particule color="#0d302a" />
          <Particule color="#0d302a" />
          <Particule color="#040b2e" />
        </Group>
      </Canvas>
      <View style={[styles.container, { paddingTop: top }]}>
        <Animated.View style={[styles.innerContainer, animatedStyle]}>
          <Text
            onLayout={({ nativeEvent }) => setY(nativeEvent.layout.height)}
            style={styles.title}
          >
            Quelle est votre adresse ?
          </Text>
          <InputAddressFinder
            onSelect={handleSelectAddress}
            selectedAdress={selectedAdress}
            setSelectedAdress={setSelectedAdress}
            placeholder="Ecrivez votre adresse"
            onBlur={handleBlur}
            onFocus={handleFocus}
          />
        </Animated.View>
      </View>
      <Animated.View
        style={[styles.blurContainer, { height: top }, animatedBlur]}
      >
        <BlurView blurAmount={blurAmount} style={{ flex: 1 }} />
      </Animated.View>
      <View>
        <Animated.View style={[animatedBottom]}>
          <Animated.View style={[animatedtext]}>
            <Pressable
              style={[styles.button, styles.current]}
              onPress={handleCurrentLocation}
            >
              <Text style={[styles.buttonText, styles.currentText]}>
                Utiliser ma position actuelle
              </Text>
            </Pressable>
          </Animated.View>
          <Pressable style={[styles.button]} onPress={handleValidate}>
            <Text style={styles.buttonText}>Valider</Text>
          </Pressable>
        </Animated.View>
      </View>
    </KeyboardAvoidingView>
  );
};

export default GetLocation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
  },
  innerContainer: {
    height,
    paddingTop: 60,
  },
  canvas: { width, height, backgroundColor: '#040b2e', position: 'absolute' },
  button: {
    width: width - 40,
    height: 48,
    left: 20,
    borderRadius: 36,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 8,
  },
  current: {
    backgroundColor: 'transparent',
  },
  currentText: {
    color: '#7f8383',
  },
  blurContainer: { ...StyleSheet.absoluteFillObject, width },
  blur: { width },
  gradient: {
    position: 'absolute',
    width: width - 40,
    left: 20,
    bottom: 24,
    height: 255,
  },
  title: {
    fontFamily: 'gatwick-bold',
    fontSize: 40,
    textAlign: 'left',
    color: '#fff',
    marginBottom: 24,
  },
  inputText: {
    fontFamily: 'neue',
    fontSize: 16,
    color: '#7f8383',
  },
  buttonText: {
    fontFamily: 'neue-bold',
    fontSize: 14,
  },
});
