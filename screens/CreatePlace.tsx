import { StyleSheet, View } from 'react-native';
import React from 'react';
import CreatePlaceForm from '../components/CreatePlaceForm/CreatePlaceForm';
import { width } from '../constants/Layout';
import { useNavigation } from '@react-navigation/native';

const CreatePlace = () => {
  const navigation = useNavigation();

  const handleSubmit = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <CreatePlaceForm onSubmit={handleSubmit} />
    </View>
  );
};

export default CreatePlace;

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#FFF' },
  innerContainer: {
    flex: 1,
    position: 'relative',
    width: width * 2,
    flexDirection: 'row',
  },
});
