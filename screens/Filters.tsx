import React, { useMemo } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useFiltersContext } from '../provider/FiltersProvider';
import { useRoute } from '@react-navigation/native';
import { Theme } from '../constants/theme';
import { useMainTheme } from '../hooks/useMainTheme';
import { ConcertsFilters } from '../components/Filters/ConcertsFilters';
import { BottomSheetBackdrop, BottomSheetModal } from '@gorhom/bottom-sheet';
import { height, width } from '../constants/Layout';
import { useTheme } from '../provider/ThemeProvider';
import { SCREENS_NAME } from '../components/DiscoverTabView/helper';
import { ClubsFilters } from '../components/Filters/ClubsFilters';
import { BarsFilters } from '../components/Filters/BarsFilters';

export const Filters = () => {
  const route = useRoute<any>();
  const styles = useMainTheme(createStyles);
  const { ref, currentIndex, setParams } = useFiltersContext();

  const handleValidate = async (params: any) => {
    setParams({
      ...params,
      [SCREENS_NAME[currentIndex]]: params,
    });

    ref.current?.close();
  };

  const ScreenFilter = useMemo(() => {
    if (SCREENS_NAME[currentIndex] === 'Concerts') {
      return <ConcertsFilters onValidate={handleValidate} />;
    }
    if (SCREENS_NAME[currentIndex] === 'Clubs') {
      return <ClubsFilters onValidate={handleValidate} />;
    }
    if (SCREENS_NAME[currentIndex] === 'Bars') {
      return <BarsFilters onValidate={handleValidate} />;
    }
    return <></>;
  }, [route.params?.screen]);

  const { theme } = useTheme();

  return (
    <BottomSheetModal
      ref={ref}
      key="filters"
      name="filters"
      style={styles.S_BottomSheet}
      index={0}
      snapPoints={['90%']}
      enablePanDownToClose
      backgroundStyle={{
        backgroundColor: theme.background.primary,
      }}
      handleStyle={{
        backgroundColor: theme.background.primary,
      }}
      handleIndicatorStyle={{
        backgroundColor: theme.fontColor.primary,
        width: 40,
      }}
      backdropComponent={(props) => (
        <BottomSheetBackdrop
          {...props}
          opacity={0.7}
          enableTouchThrough={false}
          appearsOnIndex={0}
          disappearsOnIndex={-1}
          style={[
            { backgroundColor: 'rgba(0, 0, 0, 1)' },
            StyleSheet.absoluteFillObject,
          ]}
        />
      )}
    >
      <View style={[styles.S_Container]}>
        <View style={styles.S_Header}>
          <Text style={styles.S_HeaderTitle}>Filtres</Text>
        </View>
        {ScreenFilter}
      </View>
    </BottomSheetModal>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_Container: {
      flex: 1,
      backgroundColor: theme.background.primary,
      zIndex: 10,
    },
    S_BottomSheet: {
      borderTopLeftRadius: 32,
      borderTopRightRadius: 32,
      overflow: 'hidden',
    },
    S_Blur: {
      position: 'absolute',
      zIndex: 10,
      width,
      height,
      backgroundColor: 'rgba(0,0,0,0.8)',
    },
    S_Header: {
      alignItems: 'center',
      marginVertical: 24,
    },
    S_HeaderTitle: {
      fontFamily: 'gatwick-bold',
      fontSize: 20,
      textAlign: 'center',
      color: theme.fontColor.primary,
    },
    S_Separator: {
      width: '100%',
      height: 1,
      marginTop: 24,
      backgroundColor: theme.background.action,
    },
    S_BlockHeader: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginBottom: 28,
      marginTop: 32,
    },
    S_Title: {
      fontFamily: 'gatwick-bold',
      fontSize: 16,
      color: theme.fontColor.primary,
    },
    S_SubTitle: {
      fontFamily: 'gatwick',
      fontSize: 14,
      color: theme.fontColor.secondary,
    },
  });
