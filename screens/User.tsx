import React from 'react';
import { StyleSheet, View } from 'react-native';
import { PlaceOrArtistHeader } from '../components/Headers/PlaceOrArtistHeader';
import { LinearGradient } from 'expo-linear-gradient';
import dayjs from 'dayjs';
import Animated, {
  Extrapolate,
  FadeIn,
  FadeInDown,
  interpolate,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import { Section } from '../components/Section/Section';
import { BlockHeaderBackAndFollow } from '../components/Blocks/HeaderBackAndFollow';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { RootStackParamList } from '../navigation';
import { Text } from 'react-native';
import { Theme } from '../constants/theme';
import { useMainTheme } from '../hooks/useMainTheme';
import { useTheme } from '../provider/ThemeProvider';
import { BlockEvent } from '../components/BlockEvents/BlockEvent';
import { Description } from '../components/Description/Description';
import { useUserById } from '../services/user/getUserById';
import { height, width } from '../constants/Layout';

type Props = NativeStackScreenProps<RootStackParamList, 'User'>;

export const User = ({ route }: Props) => {
  const { id } = route.params;
  const { theme } = useTheme();
  const styles = useMainTheme(createStyles);
  const { data: user, isLoading: isLoading } = useUserById(id);

  const translateY = useSharedValue(0);
  const handleScroll = useAnimatedScrollHandler({
    onScroll: (e) => {
      translateY.value = Math.abs(e.contentOffset.y);
    },
  });

  const style = useAnimatedStyle(() => {
    return {
      transform: [
        {
          scale: interpolate(
            translateY.value,
            [-height / 3, -10, 0, height / 4],
            [2, 1.3, 1, 1.3],
            Extrapolate.CLAMP,
          ),
        },
        {
          translateY: interpolate(
            translateY.value,
            [-height / 2, 0],
            [height / 5, 0],
            Extrapolate.CLAMP,
          ),
        },
      ],
    };
  });

  if (isLoading || !user) {
    return <Text>Loading</Text>;
  }

  return (
    <View style={styles.S_Main}>
      <Animated.View
        entering={FadeIn.delay(300).duration(200)}
        style={[style, styles.S_Cover]}
      >
        <PlaceOrArtistHeader
          name={user.name}
          followersCount={user.followersCount || 0}
          cover={user.cover}
        />
      </Animated.View>
      <BlockHeaderBackAndFollow id={user.id} type="None" />
      <Animated.ScrollView
        style={styles.S_Container}
        entering={FadeInDown.delay(300).duration(200)}
        onScroll={handleScroll}
        scrollEventThrottle={16}
        showsVerticalScrollIndicator={false}
      >
        <LinearGradient
          style={styles.S_Linear}
          colors={['rgba(21, 23, 24, 0)', theme.background.primary]}
        />
        <View style={styles.S_InnerView}>
          {user.desc ? (
            <Section notHorizontalPadding title="Description">
              <Description text={user.desc} />
            </Section>
          ) : null}
          {user.events.length ? (
            <Section notHorizontalPadding title="Upcoming events">
              <View style={styles.S_SectionEvent}>
                {user.events.map((event, index) => (
                  <BlockEvent
                    key={index}
                    style={{ marginBottom: 16 }}
                    event={event}
                    isBig
                  />
                ))}
              </View>
            </Section>
          ) : null}
        </View>
      </Animated.ScrollView>
    </View>
  );
};

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_Main: {
      flex: 1,
      backgroundColor: theme.background.primary,
      zIndex: 12,
    },
    S_Container: {
      position: 'absolute',
      top: 0,
      left: 0,
      flex: 1,
      height,
    },
    S_Cover: {
      width,
      height: 300,
    },
    S_AccordeonItem: {
      height: 30,
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    S_InnerView: {
      flex: 1,
      paddingHorizontal: 20,
      paddingBottom: 120,
      backgroundColor: theme.background.primary,
    },
    S_Linear: {
      width,
      height: 50,
      marginTop: 250,
    },
    S_Date: {
      fontSize: 14,
      fontFamily: 'neue',
      color: theme.fontColor.secondary,
    },
    S_SectionEvent: {
      paddingTop: 24,
    },
    S_ReadMore: {
      marginTop: 12,
      fontSize: 14,
      fontFamily: 'neue-bold',
      color: theme.fontColor.primary,
    },
    S_Description: {
      marginTop: 24,
      fontSize: 14,
      lineHeight: 20,
      fontFamily: 'neue',
      color: theme.fontColor.primary,
    },
  });
