import React, { FC, memo, useCallback, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Image } from 'expo-image';
import { RouteProp, useRoute, useNavigation } from '@react-navigation/native';
import { height, width } from '../constants/Layout';
import dayjs from 'dayjs';
import Animated, {
  Extrapolate,
  FadeIn,
  FadeInDown,
  FadeInUp,
  interpolate,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import * as Linking from 'expo-linking';
import { LinearGradient } from 'expo-linear-gradient';
import { Section } from '../components/Section/Section';
import { Tags } from '../components/Tags/Tags';
import { BlockAdress } from '../components/Blocks/Adress';
import { Line } from '../components/Blocks/Line';
import { BlockHeaderBackAndFollow } from '../components/Blocks/HeaderBackAndFollow';
import { useEventById } from '../services/events/getEventById';
import { useTheme } from '../provider/ThemeProvider';
import { Theme, blurAmount } from '../constants/theme';
import { useMainTheme } from '../hooks/useMainTheme';
import styled from 'styled-components/native';
import { capitalizeFirstLetter } from '../helpers/string';
import { LocationMap } from '../components/LocationMap/LocationMap';
import { Description } from '../components/Description/Description';
import { BlockToFollow } from '../components/BlockToFollow/BlockToFollow';
import { BlurView } from '@react-native-community/blur';
import analytics from '@react-native-firebase/analytics';
import { useOpenMap } from '../hooks/useOpenMap';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

const EventScreen: FC = () => {
  const styles = useMainTheme(createStyles);
  const { theme } = useTheme();
  const { top } = useSafeAreaInsets();
  const navigation = useNavigation<any>();
  const route: RouteProp<{ params: { id: string } }, 'params'> = useRoute();
  const eventId = route.params.id;
  const onClickOpenMap = useOpenMap();

  const { data: event, isLoading, isFetching } = useEventById(eventId);

  useEffect(() => {
    if (event.name) {
      analytics().logEvent('EventView', { name: event.name, id: eventId });
    }
  }, [event]);

  const translateY = useSharedValue(0);
  const handleScroll = useAnimatedScrollHandler({
    onScroll: (e) => {
      translateY.value = Math.abs(e.contentOffset.y);
    },
  });

  const style = useAnimatedStyle(() => {
    return {
      transform: [
        {
          scale: interpolate(
            translateY.value,
            [-height / 3, -10, 0, height / 4],
            [2, 1.3, 1, 1.3],
            Extrapolate.CLAMP,
          ),
        },
        {
          translateY: interpolate(
            translateY.value,
            [-height / 2, 0],
            [height / 5, 0],
            Extrapolate.CLAMP,
          ),
        },
      ],
    };
  });

  const styleBlur = useAnimatedStyle(() => {
    return {
      opacity: interpolate(
        translateY.value,
        [0, height / 3, height / 3 + 40],
        [0, 0, 1],
        Extrapolate.CLAMP,
      ),
    };
  });

  const handlePressCreatedBy = (id: string, type: 'Place' | 'User') => {
    navigation.navigate(type, { id, type });
  };

  return (
    <View style={styles.S_Main}>
      <Animated.View
        style={[
          styleBlur,
          {
            ...StyleSheet.absoluteFillObject,
            zIndex: 100,
            height: top,
            width,
          },
        ]}
      >
        <BlurView
          blurAmount={blurAmount}
          style={{
            height: top,
            width,
          }}
        />
      </Animated.View>
      <Animated.View
        entering={FadeIn.delay(300).duration(200)}
        style={[style, styles.S_Cover]}
      >
        <Image style={styles.S_Cover} source={event.cover} contentFit="cover" />
      </Animated.View>
      <View style={styles.S_Container}>
        <BlockHeaderBackAndFollow id={event.id} type="Event" />
        <Animated.ScrollView
          style={styles.S_AnimatedScrollView}
          entering={FadeInDown.delay(300).duration(200)}
          onScroll={handleScroll}
          scrollEventThrottle={16}
          contentContainerStyle={{
            paddingBottom: 32,
          }}
          showsVerticalScrollIndicator={false}
        >
          <LinearGradient
            style={styles.S_Linear}
            colors={['rgba(21, 23, 24, 0)', theme.background.primary]}
          />
          <View style={styles.S_InnerView}>
            <Text style={styles.S_Title} numberOfLines={3}>
              {event.name}
            </Text>
            <Text style={styles.S_Date}>
              {capitalizeFirstLetter(
                dayjs(event.beginAt).format('ddd, D MMMM').replace('.', ''),
              )}
            </Text>
            <Text style={styles.S_Hour}>{`${dayjs(event.beginAt).format(
              'HH:mm',
            )} ${
              event.endAt ? '- '.concat(dayjs(event.endAt).format('HH:mm')) : ''
            }`}</Text>

            <Line />
            {event.place?.address ? (
              <>
                <BlockAdress
                  name={event.place?.name}
                  adress={event.place?.address}
                />
                <Line />
              </>
            ) : null}

            {event.subCategories?.length ? (
              <>
                <Tags tags={event.subCategories} />
                <Line />
              </>
            ) : null}

            {event.createdByModel ? (
              <BlockToFollow
                name={event.createdBy.name}
                cover={event.createdBy.cover}
                count={event.createdBy.followersCount}
                onPress={() =>
                  handlePressCreatedBy(event.createdBy.id, event.createdByModel)
                }
              />
            ) : null}

            {event.desc ? (
              <Section notHorizontalPadding title="Description">
                <Description text={event.desc} />
              </Section>
            ) : null}

            {event.location ? (
              <Section notHorizontalPadding title="Localisation">
                <LocationMap
                  latitude={event.location.coordinates[0]}
                  longitude={event.location.coordinates[1]}
                />
              </Section>
            ) : null}
          </View>
        </Animated.ScrollView>
        <Animated.View
          style={styles.S_BuyTicketContainer}
          entering={FadeInDown.delay(500).duration(200)}
          exiting={FadeInUp}
        >
          <BlurView
            blurAmount={blurAmount}
            style={styles.S_BlurTicketContainer}
          >
            {!event.minPrice ? (
              <S_Price>Free</S_Price>
            ) : (
              <View>
                <S_Price>{event.minPrice + ' €'}</S_Price>
                <S_PriceTitle>Price</S_PriceTitle>
              </View>
            )}
            <S_Pressable onPress={() => Linking.openURL(event.url)}>
              <S_TextBuy>
                {!event.minPrice ? 'GET TICKET' : 'BUY NOW'}
              </S_TextBuy>
            </S_Pressable>
          </BlurView>
        </Animated.View>
      </View>
    </View>
  );
};

export default memo(EventScreen);

const createStyles = (theme: Theme) =>
  StyleSheet.create({
    S_Cover: {
      width,
      height: height / 2,
    },
    S_Linear: {
      width,
      height: width / 2,
      marginTop: width / 2,
    },
    S_Main: {
      flex: 1,
      backgroundColor: theme.background.primary,
    },
    S_Container: {
      position: 'absolute',
      top: 0,
      left: 0,
      flex: 1,
      height,
    },
    S_AnimatedScrollView: {
      flex: 1,
      height,
    },
    S_InnerView: {
      flex: 1,
      paddingHorizontal: 20,
      paddingBottom: 120,
      backgroundColor: theme.background.primary,
    },
    S_MarkerImage: {
      width: 10,
      height: 10,
    },
    S_Title: {
      marginVertical: 32,
      fontSize: 20,
      fontFamily: 'gatwick-bold',
      color: theme.fontColor.primary,
      lineHeight: 28,
    },
    S_TitleSection: {
      fontFamily: 'gatwick-bold',
      fontSize: 16,
      color: theme.fontColor.primary,
      // marginBottom: 32,
    },
    S_Date: {
      fontSize: 16,
      fontFamily: 'neue-bold',
      color: theme.fontColor.primary,
      marginBottom: 16,
    },
    S_Hour: {
      fontSize: 16,
      fontFamily: 'neue',
      color: theme.fontColor.primary,
    },
    S_BlurTicketContainer: {
      width: width,
      height: '100%',
      flexDirection: 'row',
      justifyContent: 'space-between',
      padding: 24,
    },
    S_BuyTicketContainer: {
      position: 'absolute',
      bottom: 0,
      height: 110,
      backgroundColor: '#15171850',
    },
  });

const S_Pressable = styled.Pressable`
  height: 48px;
  border-radius: 36px;
  background-color: ${(props) => props.theme.fontColor.action};
  justify-content: center;
  align-items: center;
  width: 110px;
`;

const S_TextBuy = styled.Text`
  font-size: 14px;
  font-family: 'neue';
  color: 'rgb(0,0,0)';
`;

const S_Price = styled.Text`
  font-size: 20px;
  font-family: 'gatwick-bold';
  color: ${(props) => props.theme.fontColor.primary};
`;

const S_PriceTitle = styled.Text`
  margin-top: 12px;
  font-size: 16px;
  font-family: 'neue';
  color: ${(props) => props.theme.fontColor.action};
`;
